package com.embdes.youcloud_resto;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import static android.content.Context.MODE_PRIVATE;
import static com.embdes.youcloud_resto.R.attr.height;


/**
 * A simple {@link Fragment} subclass.
 * Here we will add products to listview in cartstaticfragment
 */
public class ProductFragment extends Fragment {
    GridView productList;
    MyDb myDb;
    ArrayList<Imagepojo> productarray;
    Random r;
    Bitmap mBitmapproduct;
    boolean value;
    ProductFragment productFragment;
    int pritype;
    int FOODID;
    int i = 1;
    Group group;
    int count1 = 1;
    MyAdapter adapter;
    SQLiteDatabase db;
    String type;
    String PRODUCTNAME;
    float PRODUCTPRICE;
    static ProductFragment PRODUCTID;
    String searchFlag;
    // String qty = "1";
    String itemId;
    GroupFragment groupFragment;
    TextView product;
    String descrpition;
    String item;
    boolean[] thumb;
    File file;
    File[] allFiles;
    View v = null;
    /**
     * Different modes of operation
     */
    String groupSearch = "0";
    String notSearchMode = "1";

    public boolean checkInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || networkInfo.isConnected() == false) {
            return false;
        }
        return true;
    }

    static class ViewHolder {
        TextView itemName;
        ImageView itemImage;
        int id;
    }

    public class MyAdapter extends BaseAdapter {
        MyDb my;
        private Activity activity;

        @SuppressWarnings("unused")
        private LayoutInflater inflater = null;

        public MyAdapter(Activity a) {
            my = new MyDb(getActivity());
            activity = a;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            return productarray.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            /**
             * getting the thumbnail for image and putting in image view
             *
             */
            View vi = convertView;
            ViewHolder holder = null;
            if (vi == null) {
                // if it's not recycled, initialize some attributes
                final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                vi = inflater.inflate(R.layout.productxml, parent, false);
                holder = new ViewHolder();
                holder.itemName = (TextView) vi.findViewById(R.id.text);
                holder.itemImage = (ImageView) vi.findViewById(R.id.image);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
                //vi = (View) convertView;
            }
            holder.itemName.setText(productarray.get(position).getPaymnet());
            holder.itemImage.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View v) {
                    my = new MyDb(getActivity());
                    my.open();
                    if (!db.isOpen())
                        db = getActivity().openOrCreateDatabase("rebelPOS.db", Context.MODE_PRIVATE, null);
                    if (CartStaticSragment.CARTStaticSragment.tableno.getText().toString().trim().equals("")) {
                        Toast.makeText(getActivity(), "Select table", Toast.LENGTH_SHORT).show();
                    } else if (Integer.parseInt(CartStaticSragment.CARTStaticSragment.tableno.getText().toString()) < 1500) {
                        if (!CartStaticSragment.CARTStaticSragment.noofguests.getText().toString().matches("0")) {
                            Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
                            PRODUCTNAME = productarray.get(position).getPaymnet();

                            Cursor locprice = db.rawQuery("SELECT price from locationprice WHERE locationid ='" +
                                    user_Login.user_LoginID.LOCATIONNO + "';", null);
                            int count1 = locprice.getCount();
                            if (count1 > 0) {
                                locprice.moveToFirst();
                                pritype = locprice.getInt(0);

                            } else {
                                Toast.makeText(getActivity(), "NO location price", Toast.LENGTH_SHORT).show();
                            }
                            Cursor typec = db.rawQuery("SELECT price1, categoryid, groupid,foodid,price2,price3 from product WHERE productdescriptionterminal ='" +
                                    PRODUCTNAME + "';", null);
                            int count11 = typec.getCount();
                            typec.moveToFirst();
                            if (pritype == 1) {
                                PRODUCTPRICE = typec.getFloat(0);
                            } else if (pritype == 2) {
                                PRODUCTPRICE = typec.getFloat(4);
                            } else if (pritype == 3) {
                                PRODUCTPRICE = typec.getFloat(5);
                            } else {
                                PRODUCTPRICE = typec.getFloat(0);
                            }
                            FOODID = typec.getInt(3);
                            Cursor catId = db.rawQuery("SELECT categoryname from category WHERE categoryid =" + typec.getInt(1) + ";", null);
                            catId.moveToFirst();
                            Group.getInstance().categoryName = catId.getString(0);
                            Cursor groupId = db.rawQuery("SELECT groupname from grouptable WHERE groupid =" + typec.getInt(2) + ";", null);
                            groupId.moveToFirst();
                            Group.getInstance().groupName = groupId.getString(0);
                            String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                            try {

                                if (Integer.parseInt(Group.GROUPID.table) > 0) {
                                    db.beginTransaction();
                                    Calendar now = Calendar.getInstance();
                                    SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd");
                                    String nowDate = formatter.format(now.getTime());
                                    Cursor ordercheck = db.rawQuery("SELECT orderid FROM tables WHERE tableno =" + Group.GROUPID.table + " AND locationid = " + user_Login.user_LoginID.LOCATIONNO + ";", null);
                                    int count = ordercheck.getCount();
                                    String checkorder = "0";
                                    if (count > 0) {
                                        while (ordercheck.moveToNext()) {
                                            checkorder = ordercheck.getString(ordercheck.getColumnIndex("orderid"));
                                        }
                                    }
                                    Cursor c1 = db.rawQuery("SELECT orderno FROM ordernos WHERE orderno LIKE '" + nowDate.toString() + "%';", null);
                                    int orderNOCount = c1.getCount();

                                    String oldOrderNO = "0";
                                    if (orderNOCount == 0) {
                                        oldOrderNO = nowDate.toString() + "1";
                                        db.execSQL("INSERT INTO ordernos values(1," + user_Login.user_LoginID.LOCATIONNO + "," +
                                                Group.GROUPID.table + ",'" + oldOrderNO + "', 0, 0);");
                                        my.Insert(String.valueOf(MainActivity.MAINID.outletid), CartStaticSragment.CARTStaticSragment.tableno.getText().toString(), CartStaticSragment.CARTStaticSragment.noofguests.getText().toString(), user_Login.user_LoginID.nameid, oldOrderNO);
                                        if (checkInternet()) {
                                            Intent intent = new Intent(getActivity(), ServiceOccupiedChairs.class);
                                            getActivity().startService(intent);
                                        }

                                    } else {
                                        c1.moveToLast();
                                        String orderNumber = c1.getString(0);
                                        Cursor orderNoCheck = db.rawQuery("SELECT * FROM ordernos WHERE tableno =" + Group.GROUPID.table +
                                                " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + " AND billflag = 0 ;", null);
                                        if (orderNoCheck.getCount() > 0) {
                                            orderNoCheck.moveToFirst();
                                            oldOrderNO = orderNoCheck.getString(orderNoCheck.getColumnIndex("orderno"));

                                        } else {

                                            Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" + Group.GROUPID.table + " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
                                            int occupiedCount = chairs.getCount();
                                            chairs.moveToFirst();
                                            if (occupiedCount > 0 && chairs.getInt(chairs.getColumnIndex("occupiedchairs")) < 1) {
                                                int billNoIncrement = Integer.parseInt(orderNumber.substring(6, orderNumber.length())) + 1;
                                                oldOrderNO = orderNumber.substring(0, 6) + billNoIncrement;
                                                db.execSQL("INSERT INTO ordernos values(1," + user_Login.user_LoginID.LOCATIONNO + "," + Group.GROUPID.table + ",'"
                                                        + oldOrderNO + "', 0, 0);");
                                                my.Insert(String.valueOf(MainActivity.MAINID.outletid), CartStaticSragment.CARTStaticSragment.tableno.getText().toString(), CartStaticSragment.CARTStaticSragment.noofguests.getText().toString(), user_Login.user_LoginID.nameid, oldOrderNO);
                                                if (checkInternet()) {
                                                    Intent intent = new Intent(getActivity(), ServiceOccupiedChairs.class);
                                                    getActivity().startService(intent);
                                                }
                                            }
                                        }
                                    }

                                    Cursor tax1 = db.rawQuery("SELECT taxper from product WHERE productdescriptionterminal='" + productarray.get(position).getPaymnet() + "';", null);
                                    tax1.moveToFirst();
                                    double totax = Double.parseDouble(tax1.getString(tax1.getColumnIndex("taxper")));
                                    double total = PRODUCTPRICE;

                                    //is item exist
                                    Cursor cartData = db.rawQuery("SELECT productqty,totalprice from cart WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO
                                            + " AND tableno=" + Group.GROUPID.table + " AND productname ='" + PRODUCTNAME
                                            + "' AND pgroup='" + Group.getInstance().groupName
                                            + "' AND pcategory='" + Group.getInstance().categoryName
                                            + "' AND foodid=" + ProductFragment.PRODUCTID.FOODID + ";", null);
                                    int cartDatacount = cartData.getCount();
                                    if (cartDatacount > 0) {
                                        cartData.moveToFirst();
                                        int newQty = cartData.getInt(cartData.getColumnIndex("productqty"));
                                        Double preTotal = cartData.getDouble(cartData.getColumnIndex("totalprice"));
                                        preTotal += total;
                                        cartData.moveToFirst();

                                        if (newQty > 0) {
                                            newQty += 1;
                                            db.execSQL("UPDATE cart SET productqty =" + newQty + ", totalprice=" + preTotal + " WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO
                                                    + " AND tableno=" + Group.GROUPID.table + " AND productname ='" + PRODUCTNAME
                                                    + "' AND pgroup='" + Group.getInstance().groupName
                                                    + "' AND pcategory='" + Group.getInstance().categoryName
                                                    + "' AND foodid=" + ProductFragment.PRODUCTID.FOODID + ";");
                                            //db.close();
                                        }

                                    } else {

                                        db.execSQL("INSERT INTO cart( productname,typename,productprice ,typeprice ," +
                                                "productqty ,typeqty ,totalprice ,orderqty ,ordertypeqty ," +
                                                "orderflag ,billqty ,billtypeqty ,billedqty ,billedtypeqty ," +
                                                "billflag ,locationid ,tableno ,covers ," +
                                                "checkflag ,orderno,tdiscount ," +
                                                "discount ,totaltax ,datetime," +
                                                "tbillflag ,pgroup,pcategory ,username,foodid,consumerid) values('" + PRODUCTNAME + "','" +
                                                "NO TYPE" + "'," + PRODUCTPRICE + "," + 0.0 + "," +
                                                1 + "," + 1 + "," + total + ",0,0," +
                                                "0," + 1 + "," + 1 + ", 0,0," +
                                                "0," + user_Login.user_LoginID.LOCATIONNO + "," + Group.GROUPID.table + "," +
                                                TablesFragment.COVERS + ",0,'" + oldOrderNO + "', 0, 0," + totax + ",'" + timeStamp + "'," +
                                                "0,'" + Group.getInstance().groupName + "','" + Group.getInstance().categoryName + "','" +
                                                MainActivity.getInstance().USERNAME + "'," + ProductFragment.PRODUCTID.FOODID + "," + Group.GROUPID.CONSUMERID +");");

                                        CartStaticSragment.CARTStaticSragment.plus.setEnabled(false);
                                        CartStaticSragment.CARTStaticSragment.minus.setEnabled(false);
                                        if (Group.GROUPID.productflag) {
                                            if (!Group.GROUPID.groupfragment.isResumed()) {
                                                Group.GROUPID.search.setVisibility(View.INVISIBLE);
                                                Group.GROUPID.delete.setVisibility(View.INVISIBLE);
                                                Group.GROUPID.lay_search.setVisibility(View.GONE);
                                                Group.GROUPID.lay_info.setVisibility(View.VISIBLE);
                                                View view = Group.GROUPID.getCurrentFocus();
                                                InputMethodManager inputMethodManager = (InputMethodManager) Group.GROUPID.getSystemService(Context.INPUT_METHOD_SERVICE);
                                                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                                Group.GROUPID.fragmentTransaction = Group.GROUPID.fragmentManager.beginTransaction();
                                                Group.GROUPID.fragmentTransaction.replace(R.id.container2, Group.GROUPID.groupfragment);
                                                Group.GROUPID.fragmentTransaction.commit();
                                            }
                                        }

                                    }


                                    Cursor tableData = db.rawQuery("SELECT occupiedchairs from tables WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO + " AND " +
                                            "tableno=" + Group.GROUPID.table + ";", null);
                                    tableData.moveToFirst();
                                    if (tableData.getInt(tableData.getColumnIndex("occupiedchairs")) == 0) {
                                        db.execSQL("UPDATE tables SET occupiedchairs = " + TablesFragment.COVERS +
                                                " WHERE locationid = " + user_Login.user_LoginID.LOCATIONNO + " AND tableno =" + Group.GROUPID.table + ";");
                                    }
                                    db.setTransactionSuccessful();
                                    db.endTransaction();
                                    db.close();
                                    my.close();
                                }

                                group.alert();

                            } catch (NullPointerException e) {

                            }
                        } else {
                            AlertDialog.Builder adb = new AlertDialog.Builder(
                                    activity, android.R.style.Theme_Dialog);
                            adb.setTitle("");
                            adb.setMessage("Select no of guests before adding data");
                            adb.setPositiveButton("Ok", null);
                            adb.show();
                        }
                    } else {  //table numbers greater than 1500
                        TablesFragment.COVERS = "1";
                        Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
                        if (!db.isOpen())
                            db = getActivity().openOrCreateDatabase("rebelPOS.db", Context.MODE_PRIVATE, null);
                        PRODUCTNAME = productarray.get(position).getPaymnet();
                        Cursor locprice = db.rawQuery("SELECT price from locationprice WHERE locationid ='" +
                                user_Login.user_LoginID.LOCATIONNO + "';", null);
                        int count1 = locprice.getCount();
                        if (count1 > 0) {
                            locprice.moveToFirst();
                            pritype = locprice.getInt(0);

                        } else {
                            Toast.makeText(getActivity(), "NO location price", Toast.LENGTH_SHORT).show();
                        }
                        Cursor typec = db.rawQuery("SELECT price1, categoryid, groupid,foodid,price2,price3 from product WHERE productdescriptionterminal ='" +
                                PRODUCTNAME + "';", null);
                        int count11 = typec.getCount();
                        typec.moveToFirst();
                        if (pritype == 1) {
                            PRODUCTPRICE = typec.getFloat(0);

                        } else if (pritype == 2) {
                            PRODUCTPRICE = typec.getFloat(4);
                        } else if (pritype == 3) {

                            PRODUCTPRICE = typec.getFloat(5);
                        } else {
                            PRODUCTPRICE = typec.getFloat(0);
                        }
                        FOODID = typec.getInt(3);
                        Cursor catId = db.rawQuery("SELECT categoryname from category WHERE categoryid =" + typec.getInt(1) + ";", null);
                        catId.moveToFirst();
                        Group.getInstance().categoryName = catId.getString(0);

                        Cursor groupId = db.rawQuery("SELECT groupname from grouptable WHERE groupid =" + typec.getInt(2) + ";", null);
                        groupId.moveToFirst();
                        Group.getInstance().groupName = groupId.getString(0);
                        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                        try {

                            if (Integer.parseInt(Group.GROUPID.table) > 0) {
                                db.beginTransaction();
                                Calendar now = Calendar.getInstance();
                                SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd");
                                String nowDate = formatter.format(now.getTime());
                                Cursor c1 = db.rawQuery("SELECT orderno FROM ordernos WHERE orderno LIKE '" + nowDate.toString() + "%';", null);
                                ;
                                int orderNOCount = c1.getCount();
                                String oldOrderNO = "0";
                                if (orderNOCount == 0) {
                                    oldOrderNO = nowDate.toString() + "1";
                                    db.execSQL("INSERT INTO ordernos values(1," + user_Login.user_LoginID.LOCATIONNO + "," +
                                            Group.GROUPID.table + ",'" + oldOrderNO + "', 0, 0);");
                                    my.Insert(String.valueOf(MainActivity.MAINID.outletid), CartStaticSragment.CARTStaticSragment.tableno.getText().toString(), TablesFragment.COVERS, user_Login.user_LoginID.nameid, oldOrderNO);
                                    if (checkInternet()) {
                                        Intent intent = new Intent(getActivity(), ServiceOccupiedChairs.class);
                                        getActivity().startService(intent);
                                    }

                                } else {
                                    c1.moveToLast();
                                    String orderNumber = c1.getString(0);
                                    Cursor orderNoCheck = db.rawQuery("SELECT * FROM ordernos WHERE tableno =" + Group.GROUPID.table +
                                            " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + " AND billflag = 0 ;", null);
                                    if (orderNoCheck.getCount() > 0) {
                                        orderNoCheck.moveToFirst();
                                        oldOrderNO = orderNoCheck.getString(orderNoCheck.getColumnIndex("orderno"));

                                    } else {
                                        Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" + Group.GROUPID.table + " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
                                        int occupiedCount = chairs.getCount();
                                        chairs.moveToFirst();
                                        if (occupiedCount > 0 && chairs.getInt(chairs.getColumnIndex("occupiedchairs")) < 1) {
                                            int billNoIncrement = Integer.parseInt(orderNumber.substring(6, orderNumber.length())) + 1;
                                            oldOrderNO = orderNumber.substring(0, 6) + billNoIncrement;
                                            db.execSQL("INSERT INTO ordernos values(1," + user_Login.user_LoginID.LOCATIONNO + "," + Group.GROUPID.table + ",'"
                                                    + oldOrderNO + "', 0, 0);");
                                            my.Insert(String.valueOf(MainActivity.MAINID.outletid), CartStaticSragment.CARTStaticSragment.tableno.getText().toString(), TablesFragment.COVERS, user_Login.user_LoginID.nameid, oldOrderNO);
                                            if (checkInternet()) {
                                                Intent intent = new Intent(getActivity(), ServiceOccupiedChairs.class);
                                                getActivity().startService(intent);
                                            }
                                        }
                                    }
                                }
                                Cursor tax1 = db.rawQuery("SELECT taxper from product WHERE productdescriptionterminal='" + productarray.get(position).getPaymnet() + "';", null);
                                tax1.moveToFirst();
                                double totax = Double.parseDouble(tax1.getString(tax1.getColumnIndex("taxper")));
                                double total = PRODUCTPRICE;

                                //is item exist
                                Cursor cartData = db.rawQuery("SELECT productqty,totalprice from cart WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO
                                        + " AND tableno=" + Group.GROUPID.table + " AND productname ='" + PRODUCTNAME
                                        + "' AND pgroup='" + Group.getInstance().groupName
                                        + "' AND pcategory='" + Group.getInstance().categoryName
                                        + "' AND consumerid='" + Group.GROUPID.CONSUMERID
                                        + "' AND foodid=" + ProductFragment.PRODUCTID.FOODID + ";", null);
                                int cartDatacount = cartData.getCount();
                                if (cartDatacount > 0) {
                                    cartData.moveToFirst();
                                    int newQty = cartData.getInt(cartData.getColumnIndex("productqty"));
                                    Double preTotal = cartData.getDouble(cartData.getColumnIndex("totalprice"));
                                    preTotal += total;
                                    cartData.moveToFirst();

                                    if (newQty > 0) {
                                        newQty += 1;
                                        db.execSQL("UPDATE cart SET productqty =" + newQty + ", totalprice=" + preTotal + " WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO
                                                + " AND tableno=" + Group.GROUPID.table + " AND productname ='" + PRODUCTNAME
                                                + "' AND pgroup='" + Group.getInstance().groupName
                                                + "' AND pcategory='" + Group.getInstance().categoryName
                                                + "' AND foodid=" + ProductFragment.PRODUCTID.FOODID + ";");
                                        //db.close();
                                    }

                                } else {

                                    db.execSQL("INSERT INTO cart( productname,typename,productprice ,typeprice ," +
                                            "productqty ,typeqty ,totalprice ,orderqty ,ordertypeqty ," +
                                            "orderflag ,billqty ,billtypeqty ,billedqty ,billedtypeqty ," +
                                            "billflag ,locationid ,tableno ,covers ," +
                                            "checkflag ,orderno,tdiscount ," +
                                            "discount ,totaltax ,datetime," +
                                            "tbillflag ,pgroup,pcategory ,username,foodid,consumerid) values('" + PRODUCTNAME + "','" +
                                            "NO TYPE" + "'," + PRODUCTPRICE + "," + 0.0 + "," +
                                            1 + "," + 1 + "," + total + ",0,0," +
                                            "0," + 1 + "," + 1 + ", 0,0," +
                                            "0," + user_Login.user_LoginID.LOCATIONNO + "," + Group.GROUPID.table + "," +
                                            TablesFragment.COVERS + ",0,'" + oldOrderNO + "', 0, 0," + totax + ",'" + timeStamp + "'," +
                                            "0,'" + Group.getInstance().groupName + "','" + Group.getInstance().categoryName + "','" +
                                            MainActivity.getInstance().USERNAME + "'," + ProductFragment.PRODUCTID.FOODID +"," + Group.GROUPID.CONSUMERID + ");");

                                    CartStaticSragment.CARTStaticSragment.plus.setEnabled(false);
                                    CartStaticSragment.CARTStaticSragment.minus.setEnabled(false);
                                    if (Group.GROUPID.productflag) {
                                        if (!Group.GROUPID.groupfragment.isResumed()) {
                                            Group.GROUPID.search.setVisibility(View.INVISIBLE);
                                            Group.GROUPID.delete.setVisibility(View.INVISIBLE);
                                            Group.GROUPID.lay_search.setVisibility(View.GONE);
                                            Group.GROUPID.lay_info.setVisibility(View.VISIBLE);
                                            View view = Group.GROUPID.getCurrentFocus();
                                            InputMethodManager inputMethodManager = (InputMethodManager) Group.GROUPID.getSystemService(Context.INPUT_METHOD_SERVICE);
                                            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                            Group.GROUPID.fragmentTransaction = Group.GROUPID.fragmentManager.beginTransaction();
                                            Group.GROUPID.fragmentTransaction.replace(R.id.container2, Group.GROUPID.groupfragment);
                                            Group.GROUPID.fragmentTransaction.commit();
                                        }
                                    }

                                }
                                db.setTransactionSuccessful();
                                db.endTransaction();
                                Cursor tableData = db.rawQuery("SELECT occupiedchairs from tables WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO + " AND " +
                                        "tableno=" + Group.GROUPID.table + ";", null);
                                tableData.moveToFirst();
                                if (tableData.getInt(tableData.getColumnIndex("occupiedchairs")) == 0) {
                                    db.execSQL("UPDATE tables SET occupiedchairs = " + TablesFragment.COVERS +
                                            " WHERE locationid = " + user_Login.user_LoginID.LOCATIONNO + " AND tableno =" + Group.GROUPID.table + ";");
                                }
                                //Here we will add products to listview in cartstaticfragment
                                // we are making typerray clear because its adding the view in typedialog

                            }
                            db.close();
                            my.close();
                            group.alert();

                        } catch (NullPointerException e) {

                        }
                    }
                    if (search != null) {
                        search.removeTextChangedListener(searchWatcher);
                        search.setText("");
                    }
                }
            });
            File imgFile = new File(Environment.getExternalStorageDirectory().getPath() + "/pos/img/" + productarray.get(position).getImageur());
            //Here we are passing path of the image to display on the pop up window.
            if (imgFile.exists()) {
                mBitmapproduct = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                holder.itemImage.setImageBitmap(mBitmapproduct);

            } else {
                holder.itemImage.setImageResource(R.drawable.you_cloud);
            }
            Intent intent = new Intent(getActivity(), ServiceOccupiedChairs.class);
            getActivity().stopService(intent);


            return vi;
        }
    }

    public ProductFragment() {
        // Required empty public constructor
    }

    public void onCreate() {
        myDb = new MyDb(getActivity());
        myDb.open();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_product, container, false);
        PRODUCTID = this;
        this.thumb = new boolean[this.count1];
        product = (TextView) view.findViewById(R.id.product);
        group = (Group) getActivity();
        myDb = new MyDb(getActivity());
        groupFragment = new GroupFragment();
        productFragment = new ProductFragment();
        productarray = new ArrayList<Imagepojo>();
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Toast.makeText(getActivity(), "Error! No SDCARD Found!", Toast.LENGTH_LONG)
                    .show();
        } else {
            file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "/pos/img");
            allFiles = file.listFiles();
            if (allFiles.length == 0) {
                Toast.makeText(getActivity(), "FOLDER IS EMPTY", Toast.LENGTH_LONG).show();
            }
        }

        productList = (GridView) view.findViewById(R.id.gridview3);

       /* //set item height
        ViewGroup.LayoutParams layoutParams = productList.getLayoutParams();
        layoutParams.height = convertDpToPixels(98, getActivity()); //this is in pixels
        productList.setLayoutParams(layoutParams);
*/

        Bundle bundle = getArguments();
        itemId = bundle.getString("SearchText");
        searchFlag = bundle.getString("searchFlag");
        db = getActivity().openOrCreateDatabase("rebelPOS.db", Context.MODE_PRIVATE, null);
        if (searchFlag.equals(groupSearch)) {
            getActivity().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            Cursor productData = db.rawQuery("SELECT productdescriptionterminal,productdescription from product where groupid =" + itemId + " ;", null);
            int count = productData.getCount();
            productData.moveToFirst();
            for (Integer rowCount = 0; rowCount < count; rowCount++) {
                Imagepojo imagepojo = new Imagepojo();
                String product = "";
                String image = "";
                product = productData.getString(productData.getColumnIndex("productdescriptionterminal"));
                image = productData.getString(productData.getColumnIndex("productdescription"));// it should be "foodimages" but server database is having some more (3 column for french language but application doesn't so there is some mismatch because of that it happening )
                String[] path = image.split("\\/");
                String val = path[path.length - 1];
                imagepojo.setImageur(val);
                imagepojo.setPaymnet(product);
                productarray.add(imagepojo);
                productData.moveToNext();


            }
            search();
        } else if (searchFlag.equals(notSearchMode)) {
            getActivity().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            if (!db.isOpen())
                db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor c = db.rawQuery("SELECT productdescriptionterminal,foodimages,productdescription from product where categoryid =" + itemId + " ;", null);
            int count = c.getCount();
            c.moveToFirst();
            for (Integer rowCount = 0; rowCount < count; rowCount++) {
                Imagepojo imagepojo = new Imagepojo();
                String product = "";
                String image = "";
                product = c.getString(c.getColumnIndex("productdescriptionterminal"));
                image = c.getString(c.getColumnIndex("productdescription"));
                descrpition = c.getString(c.getColumnIndex("foodimages"));
                String[] path = image.split("\\/");
                String val = path[path.length - 1];
                imagepojo.setPaymnet(product);
                imagepojo.setImageur(val);
                productarray.add(imagepojo);
                c.moveToNext();
            }
            db.close();
        }
        adapter = new MyAdapter(getActivity());
        productList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        return view;
    }

    public void test() {
        adapter = new MyAdapter(getActivity());
        productList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public static int convertDpToPixels(float dp, Context context) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                resources.getDisplayMetrics()
        );
    }

    TextWatcher searchWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if (s.length() == 0) {
                search.setVisibility(View.INVISIBLE);
                Group.GROUPID.delete.setVisibility(View.INVISIBLE);
                Group.GROUPID.lay_search.setVisibility(View.GONE);
                Group.GROUPID.lay_info.setVisibility(View.VISIBLE);
                searchView.setVisibility(View.VISIBLE);

                if (!Group.GROUPID.groupfragment.isResumed()) {
                    Group.GROUPID.fragmentTransaction = Group.GROUPID.fragmentManager.beginTransaction();
                    Group.GROUPID.fragmentTransaction.replace(R.id.container2, Group.GROUPID.groupfragment);
                    Group.GROUPID.fragmentTransaction.commit();
//                        View view = Group.GROUPID.getCurrentFocus();
//                        InputMethodManager inputMethodManager = (InputMethodManager) Group.GROUPID.getSystemService(Context.INPUT_METHOD_SERVICE);
//                        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
            if (searchFlag.equals(groupSearch)) {
                if (db != null && !db.isOpen())
                    db = getActivity().openOrCreateDatabase("rebelPOS.db", Context.MODE_PRIVATE, null);

                Cursor c = null;
                c = db.rawQuery("SELECT productdescriptionterminal,productdescription FROM product WHERE productdescriptionterminal LIKE '" +
                        s.toString() + "%';", null);
                int count = c.getCount();
                c.moveToFirst();
                productarray.clear();
                for (Integer j = 0; j < count; j++) {
                    Imagepojo imagepojo = new Imagepojo();
                    String product = "";
                    String image = "";
                    product = c.getString(c.getColumnIndex("productdescriptionterminal"));
                    image = c.getString(c.getColumnIndex("productdescription"));
                    String[] path = image.split("\\/");
                    String val = path[path.length - 1];
                    imagepojo.setPaymnet(product);
                    imagepojo.setImageur(val);
                    productarray.add(imagepojo);
                    c.moveToNext();
                }
                c.close();
                db.close();
                test();
            }

        }
    };
    EditText search;
    ImageView searchView;

    public void search() {
//        SQLiteDatabase   db;
        search = (EditText) getActivity().findViewById(R.id.titleditext);
        search.requestFocus();
        searchView = (ImageView) getActivity().findViewById(R.id.titlebarSearchview);
        search.addTextChangedListener(searchWatcher);

    }
};




