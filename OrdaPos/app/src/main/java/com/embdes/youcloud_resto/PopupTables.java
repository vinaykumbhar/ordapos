package com.embdes.youcloud_resto;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static com.embdes.youcloud_resto.TablesFragment.COVERS;

/**
 * Created by mom on 26-06-2017.
 */

public class PopupTables extends DialogFragment {
    public static PopupTables popupTables;
    SQLiteDatabase db;
    FragmentManager fragmentManager;
    GroupFragment groupFragment;
    MyDb myDb;
    Intent intent;
    TablesFragment tablesFragment;
    ArrayList<tablespojo> tableno;
    CartStaticSragment cartstaticfragment;
    ArrayList<Integer> arraylist;
    GridView gridView;
    int noofchairs;
    boolean flag =false;
    MYAdapter myAdapter;
    Dialog dialog = null;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setIcon(R.drawable.logo);
        popupTables =this;
        if (flag){

        }else {
              Group.GROUPID.table  = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
            }
        builder.setTitle(" Change Table : "+Group.GROUPID.table);
        View view  =  getActivity().getLayoutInflater().inflate(R.layout.fragment_tables, null);
        gridView = (GridView) view.findViewById(R.id.grid);
        view.findViewById(R.id.txt_table).setVisibility(View.GONE);
        fragmentManager = getFragmentManager();
        myDb  =new MyDb(getActivity());
        groupFragment = new GroupFragment();
        tablesFragment =  new TablesFragment();
        tableno = new ArrayList<tablespojo>();
        arraylist = new ArrayList<Integer>();
        cartstaticfragment = new CartStaticSragment();
        method();
        myAdapter = new MYAdapter(getActivity(),tableno);
        gridView.setAdapter(myAdapter);
        myAdapter.notifyDataSetChanged();
        builder.setView(view);
        dialog = builder.create();
        return dialog;
    }
    public void method(){
        db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor chairs = db.rawQuery("SELECT tableno,noofchairs,occupiedchairs FROM tables WHERE locationid ="
                +user_Login.user_LoginID.LOCATIONNO+";", null);


        int c = chairs.getCount();
        if (c>0) {

            while (chairs.moveToNext()) {
                   tablespojo mtablespojo = new tablespojo();
                   String table = chairs.getString(chairs.getColumnIndex("tableno"));
                   String chair = chairs.getString(chairs.getColumnIndex("noofchairs"));
                   String occ = chairs.getString(chairs.getColumnIndex("occupiedchairs"));
                        if (table.equalsIgnoreCase("1500")){
                            break;
                            }else {
                                    mtablespojo.setNoofchaors(chair);
                                    mtablespojo.setTableno(table);
                                    mtablespojo.setOccupied(occ);
                                    tableno.add(mtablespojo);
                }
            }
        }
        db.close();
    }
    public class MYAdapter extends BaseAdapter {
        private Activity activity;
        private LayoutInflater inflater = null;
        ArrayList<tablespojo> tablenos ;
        MyDb myDb1 = new MyDb(getActivity());
        public MYAdapter(Activity a,ArrayList<tablespojo> table){

            this.activity = a;
            this.tablenos =table;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public int getViewTypeCount() {
            return getCount();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getCount() {
            return tablenos.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public View getView(final int position, final View convertView, ViewGroup parent) {
            View vi = convertView;
            TablesFragment.ViewHolder holder = null;
//            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            myDb1.open();
            if (vi == null){
                // if it's not recycled, initialize some attributes
                final LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE );
                vi = inflater.inflate(R.layout.tableadapter, parent, false);
                holder = new TablesFragment.ViewHolder();
                holder.tableEdit = (ImageView) vi.findViewById(R.id.myImageView);
                holder.coversEdit = (TextView) vi.findViewById(R.id.myImageViewText);
                holder.coverdetail = (TextView) vi.findViewById(R.id.myImageViewText1);
                holder.r2 = (RelativeLayout) vi.findViewById(R.id.r2);
                vi.setTag(holder);
            }else {
                holder = (TablesFragment.ViewHolder) vi.getTag();
            }
            //holder.tableEdit.setLayoutParams(lp);
//            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.tableEdit.getLayoutParams();
//            params.width = getpixelsfromDps(PopupTables.this,80);
            holder.coversEdit.setText(String.valueOf(tablenos.get(position).getTableno()));
            noofchairs = Integer.parseInt(tablenos.get(position).getNoofchaors());
            final TablesFragment.ViewHolder finalHolder1 = holder;
            //here we are setting the colors for the tables
            if(holder.coversEdit.getText().length()!=0 &&
                    Integer.parseInt(holder.coversEdit.getText().toString()) > 0 ) {
                if (CartStaticSragment.CARTStaticSragment.noofguests.getText().length() != 0) {
                    COVERS = CartStaticSragment.CARTStaticSragment.noofguests.getText().toString();
                }
                int occupiedchairs = Integer.parseInt(tableno.get(position).getOccupied());

                if (occupiedchairs < 1) {
                    holder.tableEdit.setImageResource(R.drawable.btn_tableavailable);
                    holder.coversEdit.setTextColor(Color.GREEN);
                    holder.coverdetail.setTextColor(Color.GREEN);
                } else {
                    holder.tableEdit.setImageResource(R.drawable.btn_tableoccupied);
                    holder.coverdetail.setText("OCCUPIED");
                    holder.coverdetail.setTextColor(Color.RED);
                    holder.coversEdit.setTextColor(Color.RED);
                    final TablesFragment.ViewHolder finalHolder = holder;
                }



                holder.tableEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // we are replacing the tables fragment with group fragment
                        db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                        myDb1.open();
                        myDb1.Insert(String.valueOf(MainActivity.MAINID.outletid),Group.GROUPID.table,"0","NULL","NULL");
                        Intent intent = new Intent(getActivity(),ServiceOccupiedChairs.class);
                        getActivity().startService(intent);
                        if(user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.Tchange ){
                        int occupiedchairs = 0;
                            Cursor order = db.rawQuery("SELECT * FROM ordernos WHERE tableno =" + Group.GROUPID.table +
                                    " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + " AND billflag = 0 ;", null);
                            int cou = order.getCount();
                            String ordercheck ="0";
                            if (cou>0){
                                while (order.moveToNext()){
                                    ordercheck = order.getString(order.getColumnIndex("orderno"));}}
                    if (Group.GROUPID.table.length()!=0){
                        Cursor c = db.rawQuery("SELECT occupiedchairs from tables WHERE tableno = "+
                            Group.GROUPID.table+ " AND locationid = "+
                            user_Login.user_LoginID.LOCATIONNO+";", null);
                        c.moveToFirst();
                        occupiedchairs =c.getInt(c.getColumnIndex("occupiedchairs"));

                        db.close();}
                    else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                getActivity(), android.R.style.Theme_Dialog);
                        msg.setTitle("ERROR");
                        msg.setMessage("User Didn't select any Table");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }
                            int flag =0;

                            if(finalHolder1.coversEdit.getText().length() > 0)
                            {
                                db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                                Cursor c = db.rawQuery("SELECT occupiedchairs from tables WHERE tableno = "+
                                       finalHolder1.coversEdit.getText().toString()+ " AND locationid = "+
                                        user_Login.user_LoginID.LOCATIONNO+";", null);
                                c.moveToFirst();
                                flag =c.getInt(c.getColumnIndex("occupiedchairs"));
                                if(flag > 0)
                                {
                                   finalHolder1.coversEdit.getText();
                                    AlertDialog.Builder msg = new AlertDialog.Builder(
                                            getActivity(), android.R.style.Theme_Dialog);
                                    msg.setTitle("");
                                    msg.setMessage("Table is Not Free To Change");
                                    msg.setPositiveButton("Ok", null);
                                    msg.show();
                                }
                                else{
                                    db.execSQL("UPDATE ordernos SET tableno = "+ finalHolder1.coversEdit.getText().toString()+" WHERE tableno = "+
                                            Group.GROUPID.table+ " AND locationid = "+user_Login.user_LoginID.LOCATIONNO+" AND billflag = 0;");

                                    db.execSQL("UPDATE tables SET occupiedchairs = 0 WHERE tableno = "+Group.GROUPID.table+
                                            " AND locationid = "+user_Login.user_LoginID.LOCATIONNO+";");
                                    db.execSQL("UPDATE tables SET occupiedchairs = "+occupiedchairs+" WHERE tableno = "+
                                           finalHolder1.coversEdit.getText().toString()+ " AND locationid = "+user_Login.user_LoginID.LOCATIONNO+";");

                                    db.execSQL("UPDATE cart SET tableno = "+ finalHolder1.coversEdit.getText().toString()+" WHERE tableno = "+
                                            Group.GROUPID.table+ " AND locationid = "+user_Login.user_LoginID.LOCATIONNO+ "; ");//AND billflag = 0 AND orderflag = 1 ;");

                                    dialog.dismiss();
                                    Group.GROUPID.table =finalHolder1.coversEdit.getText().toString();
                                    Intent intent1=  new Intent(getActivity(),Group.class);
                                    startActivity(intent1);
                                    CartStaticSragment.CARTStaticSragment.tableno.setText("");
                                }

                                db.close();
                                myDb1.Insert(String.valueOf(MainActivity.MAINID.outletid),finalHolder1.coversEdit.getText().toString(),String.valueOf(occupiedchairs),user_Login.user_LoginID.nameid,ordercheck);
                                Intent  intent1 = new Intent(getActivity(),ServiceOccupiedChairs.class);
                                getActivity().startService(intent1);
                            }else{

                            }


                }else{
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            getActivity(), android.R.style.Theme_Dialog);
                    msg.setTitle("Opration Failed");
                    msg.setMessage("User Doesn't have the permission for this opration");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                }}

                });

            }
            myDb1.close();
            return vi;
        }}

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private int getpixelsfromDps(PopupTables tableView, int dps) {
        Resources r = tableView.getResources();
        int  px = (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
        return px;
    }
    public static PopupTables getInstance(){
        return  popupTables ;
    }
}

