package com.embdes.youcloud_resto;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class CartStaticSragment extends Fragment {
    TextView tableno, tabletext;
    TextView username;
    int pritype;
    int FOODID;
    float PRODUCTPRICE;
    MyDb myDb;
    ArrayList<Imagepojo> productarray;
    TextView nofgueststext;
    ImageView plus, minus;
    Button changetables, tables, billout;
    TextView noofguests;
    GroupFragment groupFragment;
    Group group;
    int noofchairs;
    String grand;
    Button send;
    String TYPENAME;
    String qty1 = "1";
    PopupTables popupTables;
    com.embdes.youcloud_resto.BillFragment billfragment;
    int occupied;
    ArrayList<Cartpojo> cart;
    static CartStaticSragment CARTStaticSragment;
    SQLiteDatabase db;
    TextView item, qty, each, total;
    double totalvalue;
    TextView totalamount, totaltax, totaltaxvalue, totalamountvalue;
    TextView items, itemno;
    ListView listView;
    String sno;
    Button takeorder;
    FragmentManager fragmentManager;
    android.app.FragmentTransaction fragmentTransaction;
    CartAdapter cartAdapter;
    String editLocation;
    TablesFragment tablesFragment;
    TablesFragment tablesfragment1;
    Dialog tableChnage_popup;
    int occupiedchairs;
    String editTable;
    double TYPEPRICE;
    Boolean billoutFlag = false;

    public boolean checkInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || networkInfo.isConnected() == false) {
            return false;
        }
        return true;
    }

    public CartStaticSragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_cart_static_sragment, container, false);
        CARTStaticSragment = this;
        billoutFlag = false;
        tableno = (TextView) view.findViewById(R.id.tableno1);
        username = (TextView) view.findViewById(R.id.userna);
        plus = (ImageView) view.findViewById(R.id.plus);
        minus = (ImageView) view.findViewById(R.id.minus);
        item = (TextView) view.findViewById(R.id.item);
        qty = (TextView) view.findViewById(R.id.qty);
        each = (TextView) view.findViewById(R.id.each);
        total = (TextView) view.findViewById(R.id.total);
        tabletext = (TextView) view.findViewById(R.id.table1);
        nofgueststext = (TextView) view.findViewById(R.id.noofguests);
        totalamount = (TextView) view.findViewById(R.id.totalamount);
        totaltax = (TextView) view.findViewById(R.id.totaltax);
        totaltaxvalue = (TextView) view.findViewById(R.id.taxvalue);
        totalamountvalue = (TextView) view.findViewById(R.id.totalamountvalue);
        items = (TextView) view.findViewById(R.id.items);
        itemno = (TextView) view.findViewById(R.id.itemno);
        listView = (ListView) view.findViewById(R.id.list1);
        listView.setDivider(new ColorDrawable(Color.parseColor("#1681c4")));
        listView.setDividerHeight(1);
        changetables = (Button) view.findViewById(R.id.changetables);
        tables = (Button) view.findViewById(R.id.tables);
        billout = (Button) view.findViewById(R.id.bilout);
        groupFragment = new GroupFragment();
        takeorder = (Button) view.findViewById(R.id.tables);
        send = (Button) view.findViewById(R.id.send);
        tablesFragment = new TablesFragment();
        tablesfragment1 = new TablesFragment();
        popupTables = new PopupTables();
        tableChnage_popup = new Dialog(getActivity());
        fragmentManager = getFragmentManager();
        billfragment = new BillFragment();
        plus.setEnabled(false);
        minus.setEnabled(false);
        noofguests = (TextView) view.findViewById(R.id.quantityamount);
        group = (Group) getActivity();
        cart = new ArrayList<Cartpojo>();
        productarray = new ArrayList<Imagepojo>();
        myDb = new MyDb(getActivity());
        myDb.open();
        db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CartStaticSragment.CARTStaticSragment.tableno.getText().toString().equals("")) {
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            getActivity(), android.R.style.Theme_Dialog);
                    msg.setTitle("");
                    msg.setMessage("Select table");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                } else send();
            }
        });
        takeorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!groupFragment.isResumed()) {
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container2, groupFragment);
                    fragmentTransaction.commit();
                }
            }
        });
        changetables.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CartStaticSragment.CARTStaticSragment.tableno.getText().length() == 0) {
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            getActivity(), android.R.style.Theme_Dialog);
                    msg.setTitle("");
                    msg.setMessage("Select Table");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                } else {
                    popupTables.show(getFragmentManager(), null);
                    listView.setAdapter(null);
                    totalamountvalue.setText("");
                    totaltax.setText("");
                    itemno.setText("");
                }
            }
        });
        billout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ImageView searchView = (ImageView) getActivity().findViewById(R.id.titlebarSearchview);
                if (CARTStaticSragment.tableno.getText().toString().trim().equals("")) {
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            getActivity(), android.R.style.Theme_Dialog);
                    msg.setTitle("");
                    msg.setMessage("Select table");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                } else if (!billfragment.isResumed()) {
                    billoutFlag = true;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container2, billfragment);
                    searchView.setVisibility(View.INVISIBLE);
                    fragmentTransaction.commit();
                }
            }
        });
        tables.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listView != null) {
                    listView.setAdapter(null);
                    totalamountvalue.setText("");
                    totaltax.setText("");
                    itemno.setText("");
                    tableno.setText("");
                    if (!tablesFragment.isResumed()) {
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container2, tablesFragment);
                        fragmentTransaction.commit();
                    }
                } else {
                    tableno.setText("");
                    listView.setAdapter(null);
                    totalamountvalue.setText("");
                    totaltax.setText("");
                    itemno.setText("");
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container2, tablesFragment);
                    fragmentTransaction.commit();
                    group.alert();
                }

            }
        });
        username.setText(user_Login.user_LoginID.nameid);
        db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor chairs = db.rawQuery("SELECT tableno,noofchairs,occupiedchairs FROM tables WHERE locationid ="
                + user_Login.user_LoginID.LOCATIONNO + ";", null);
        int count = chairs.getCount();
        chairs.moveToFirst();
        for (Integer rowCount = 0; rowCount < count; rowCount++) {
            String chair = chairs.getString(chairs.getColumnIndex("noofchairs"));
            String occupiedchairs = chairs.getString(chairs.getColumnIndex("occupiedchairs"));
            noofchairs = Integer.parseInt(chair);
            occupied = Integer.parseInt(occupiedchairs);
            noofguests.setText(String.valueOf(occupied));
            chairs.moveToNext();
        }
        db.close();
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val = Integer.parseInt((noofguests.getText().toString()));
                noofchairs = val;
                if (val <= noofchairs) {
                    noofchairs = noofchairs + 1;
                    noofguests.setText("" + noofchairs);
                    TablesFragment.COVERS = String.valueOf(noofchairs);
                }
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int value = Integer.parseInt(noofguests.getText().toString());
                noofchairs = value;
                if (noofchairs != 0) {
                    noofchairs = noofchairs - 1;
                    noofguests.setText("" + noofchairs);
                    TablesFragment.COVERS = String.valueOf(noofchairs);
                }
            }
        });
        return view;
    }

    public void alertadapter(Activity activity) {
        Double grandTotal = Double.valueOf(0);
        if (cart.size() != 0) {
            cart.clear();
        }
        myDb = new MyDb(getActivity());
        myDb.open();
        editLocation = user_Login.user_LoginID.LOCATIONNO;
        editTable = Group.GROUPID.table;
        db = activity.openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        CartStaticSragment.CARTStaticSragment.billoutFlag = false;
        Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid=" + editLocation + " AND tableno=" + editTable +
                " AND billflag = 0;", null);
        int count = cartData.getCount();
        if (count > 0) {
            cartData.moveToLast();
            do {
                Cartpojo cartpojo = new Cartpojo();
                sno = cartData.getString(cartData.getColumnIndex("sno")).toString();
                String productname = cartData.getString(cartData.getColumnIndex("productname"));
                String typename = cartData.getString(cartData.getColumnIndex("typename"));
                String productprice = cartData.getString(cartData.getColumnIndex("productprice"));
                String typeprice = cartData.getString(cartData.getColumnIndex("typeprice"));
                String productqty = cartData.getString(cartData.getColumnIndex("productqty"));
                String typeqty = cartData.getString(cartData.getColumnIndex("typeqty"));
                Double totax = Double.parseDouble(cartData.getString(cartData.getColumnIndex("totaltax")));
                cartpojo.setSno(sno);
                cartpojo.setName(productname);
                cartpojo.setType(typename);
                cartpojo.setPprice(productprice);
                cartpojo.setTprice(typeprice);
                cartpojo.setPqty(productqty);
                cartpojo.setTqty(typeqty);
                cartpojo.setTax(String.valueOf(totax));
                totalvalue = Double.valueOf((Float.parseFloat(productprice) *
                        Integer.parseInt(productqty))) + Double.valueOf(typeprice) * Double.valueOf(typeqty);
                grandTotal = grandTotal + totalvalue;
                cartpojo.setTotal(String.valueOf(totalvalue));
                cart.add(cartpojo);
            } while (cartData.moveToPrevious());
        }
        db.close();
        grand = String.format("%.2f", grandTotal);
        itemno.setText(String.valueOf(cart.size()));
        db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor taxDetails = db.rawQuery("SELECT servicetaxper,taxper1,taxper2,taxper3 FROM tax;", null);
        int taxDetailsCount = taxDetails.getCount();
        double tax1 = 0;
        if (taxDetailsCount > 0) {
            taxDetails.moveToFirst();
            tax1 = taxDetails.getFloat(taxDetails.getColumnIndex("servicetaxper"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper1"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper2"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper3"));

        } else {
        }
        db.close();
        double taxAmount = (tax1 * grandTotal) / 100;
        String grand_tax = String.format("%.2f", taxAmount);
        totaltax.setText(String.valueOf(grand_tax));
        grandTotal += (tax1 * grandTotal) / 100;
        String grand_Amount = String.format("%.2f", grandTotal);
        totalamountvalue.setText(String.valueOf(grand_Amount));
        Group.GROUPID.finaltotal = grand_Amount;
        if (CartStaticSragment.CARTStaticSragment.cart.size() != 0) {
            cartAdapter = new CartAdapter(activity);
            listView.setAdapter(cartAdapter);
            cartAdapter.notifyDataSetChanged();
        } else {
            AlertDialog.Builder msg = new AlertDialog.Builder(
                    getActivity(), android.R.style.Theme_Dialog);
            msg.setTitle("");
            msg.setMessage("Table Data has been cleared ");
            msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // AlertDialog.Builder msg = new AlertDialog.Builder(
                    // getActivity(), android.R.style.Theme_Dialog);
                    //msg.setTitle("");
                    //  msg.setMessage("Do you want to clear Cart ?");
//                    msg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            myDb = new MyDb(getActivity());
//                            myDb.open();
//                            myDb.Insert(String.valueOf(MainActivity.MAINID.outletid),CartStaticSragment.CARTStaticSragment.tableno.getText().toString(),"0","NULL","0");
//                            Intent intent = new Intent(getActivity(),ServiceOccupiedChairs.class);
//                            getActivity().startService(intent);
//                            if (!tablesFragment.isResumed()){
//                                fragmentTransaction = fragmentManager.beginTransaction();
//                                fragmentTransaction.replace(R.id.container2,tablesFragment);
//                                fragmentTransaction.commit();}else {
//                                fragmentTransaction = fragmentManager.beginTransaction();
//                                fragmentTransaction.replace(R.id.container2,tablesfragment1);
//                                fragmentTransaction.commit();
//                            }
//                            tableno.setText("");
//                            noofguests.setText("0");
//                        }
//                    });
//                    msg.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                        }
//                    });
                    // msg.show();
                }
            });
            msg.show();
        }
    }

    static class ViewHolder {
        TextView productname, producttype;
        TextView qtyvalue, eachvalue, totalvalue;
        ImageView delete, increase, decerese, custom;
    }

    public class CartAdapter extends BaseAdapter {
        private Activity activity;

        @SuppressWarnings("unused")
        private LayoutInflater inflater = null;

        public CartAdapter(Activity a) {
            activity = a;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getViewTypeCount() {
            return getCount();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getCount() {
            return cart.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            ViewHolder holder = null;
            if (vi == null) {
                final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                vi = inflater.inflate(R.layout.cartadapter, parent, false);
                holder = new ViewHolder();
                holder.productname = (TextView) vi.findViewById(R.id.productname);
                holder.producttype = (TextView) vi.findViewById(R.id.type);
                holder.increase = (ImageView) vi.findViewById(R.id.decrease);
                holder.decerese = (ImageView) vi.findViewById(R.id.increase);
                holder.qtyvalue = (TextView) vi.findViewById(R.id.value);
                holder.eachvalue = (TextView) vi.findViewById(R.id.eachvalue);
                holder.totalvalue = (TextView) vi.findViewById(R.id.Totalvalue1);
                holder.delete = (ImageView) vi.findViewById(R.id.delete);
                holder.custom = (ImageView) vi.findViewById(R.id.custom);
                holder.delete.setTag(position);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }
            final ViewHolder finalHolder1 = holder;
            holder.custom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (billoutFlag) {

                    } else {
                        db = getActivity().openOrCreateDatabase("rebelPOS.db", Context.MODE_PRIVATE, null);
                        Cursor c = db.rawQuery("SELECT type from product WHERE  productdescriptionterminal = '" +
                                cart.get(position).getName() + "';", null);
                        c.moveToFirst();
                        String typeId = c.getString(c.getColumnIndex("type"));
                        TypeDialog typeDialog = new TypeDialog();
                        Bundle b = new Bundle();
                        b.putString("sno", cart.get(position).getSno());
                        typeDialog.setArguments(b);
                        typeDialog.typeid(typeId);
                        typeDialog.show(getFragmentManager(), null);
                        Cursor locprice = db.rawQuery("SELECT price from locationprice WHERE locationid ='" +
                                user_Login.user_LoginID.LOCATIONNO + "';", null);
                        int count1 = locprice.getCount();
                        if (count1 > 0) {
                            locprice.moveToFirst();
                            pritype = locprice.getInt(0);

                        } else {
                            Toast.makeText(getActivity(), "NO location price", Toast.LENGTH_SHORT).show();
                        }
                        Cursor typec = db.rawQuery("SELECT price1, categoryid, groupid,foodid,price2,price3 from product WHERE productdescriptionterminal ='" +
                                cart.get(position).getName() + "';", null);
                        int count11 = typec.getCount();
                        typec.moveToFirst();
                        if (pritype == 1) {
                            PRODUCTPRICE = typec.getFloat(0);
                        } else if (pritype == 2) {
                            PRODUCTPRICE = typec.getFloat(4);
                        } else if (pritype == 3) {
                            PRODUCTPRICE = typec.getFloat(5);
                        } else {
                            PRODUCTPRICE = typec.getFloat(0);
                        }
                        FOODID = typec.getInt(3);
                        Cursor catId = db.rawQuery("SELECT categoryname from category WHERE categoryid =" + typec.getInt(1) + ";", null);
                        catId.moveToFirst();
                        Group.getInstance().categoryName = catId.getString(0);
                        Cursor groupId = db.rawQuery("SELECT groupname from grouptable WHERE groupid =" + typec.getInt(2) + ";", null);
                        groupId.moveToFirst();
                        Group.getInstance().groupName = groupId.getString(0);

                    }
                }
            });
            holder.delete.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (billoutFlag) {

                    } else {
                        //we are clearing the listview and refilling with and making occupied chairs 0
                        Integer index = (Integer) v.getTag();
                        db = activity.openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                        //here we are commeting the delete database when home button is clicked
                        db.execSQL("DELETE FROM cart WHERE sno = " + cart.get(position).getSno() + ";");
                        Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
                        Cursor tableData = db.rawQuery("SELECT occupiedchairs from tables WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO + " AND " +
                                "tableno=" + Group.GROUPID.table + ";", null);
                        cart.clear();
                        Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO + " AND tableno=" + Group.GROUPID.table +
                                " AND billflag = 0;", null);
                        tableData.moveToFirst();
                        if ((cartData.getCount() < 1)) {
                            if (tableData.getInt(tableData.getColumnIndex("occupiedchairs")) > 0) {
                                TablesFragment.TABLESID.COVERS = "0";
                                db.execSQL("UPDATE tables SET occupiedchairs = " + TablesFragment.TABLESID.COVERS +
                                        " WHERE locationid = " + user_Login.user_LoginID.LOCATIONNO + " AND tableno =" + Group.GROUPID.table + ";");
                                db.execSQL("DELETE FROM ordernos WHERE tableno = " + Group.GROUPID.table +
                                        " AND locationid = " +/*user_Login.user_LoginID.LOCATIONNO*/user_Login.user_LoginID.LOCATIONNO + " AND billflag = 0;");
                                db.execSQL("DELETE FROM cart WHERE tableno = " + Group.GROUPID.table +
                                        " AND locationid = " +/*user_Login.user_LoginID.LOCATIONNO*/user_Login.user_LoginID.LOCATIONNO + " AND billflag = 0;");
                                if (cart.size() == 0) {
                                    myDb.Insert(String.valueOf(MainActivity.MAINID.outletid), CartStaticSragment.CARTStaticSragment.tableno.getText().toString(), "0", "NULL", "NULL");
                                    Intent intent = new Intent(getActivity(), ServiceOccupiedChairs.class);
                                    getActivity().startService(intent);
                                }
                                tableno.setText("");
                                listView.setAdapter(null);
                                totalamountvalue.setText("");
                                totaltax.setText("");
                                itemno.setText("");
                                noofguests.setText("0");
                                if (!tablesFragment.isResumed()) {
                                    fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.container2, tablesFragment);
                                    fragmentTransaction.commit();
                                } else {
                                    fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.container2, tablesfragment1);
                                    fragmentTransaction.commit();
                                }
                            }
                        } else {
                            alertadapter(activity);
                        }

                        db.close();
                        notifyDataSetChanged();
                    }
                }
            });


            final ViewHolder finalHolder2 = holder;
            holder.increase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (billoutFlag) {

                    } else {
                        double changedTotal = 0;
                        String newQty;
                        if (cart.get(position).getTqty().length() > 0) {
                            if (cart.get(position).getTqty().length() > 0) {
                                changedTotal = Float.parseFloat(cart.get(position).getTprice()) * Integer.parseInt(cart.get(position).getTqty());
                            } else {
                                changedTotal = 0;
                            }
                            finalHolder1.totalvalue.setText("" + changedTotal);
                            newQty = "0";
                        }
                        Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
                        int i = Integer.parseInt(finalHolder1.qtyvalue.getText().toString());
                        if (i >= 0) {
                            i = i + 1;
                            finalHolder1.qtyvalue.setText("" + i);
                            db = activity.openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                            Cursor cartData = db.rawQuery("SELECT orderqty from cart WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO + " AND tableno=" + Group.GROUPID.table +
                                    " AND billflag = 0 AND sno=" + cart.get(position).getSno() + ";", null);
                            int count = cartData.getCount();
                            if (cart.get(position).getPqty().length() > 0) {
                                changedTotal = Double.valueOf(cart.get(position).getPprice()) * i + Double.valueOf(cart.get(position).getTprice()) * Double.valueOf(cart.get(position).getTqty());
                            } else {
                                changedTotal = Float.parseFloat(cart.get(position).getPprice()) * i;
                            }
                            newQty = String.valueOf(i);
                            if (count > 0) {
                                cartData.moveToFirst();
                                if (Integer.parseInt(newQty) > cartData.getInt(cartData.getColumnIndex("orderqty"))) {
                                    finalHolder1.totalvalue.setText("" + changedTotal);
                                    db.execSQL("UPDATE cart SET productqty = " + newQty + " , totalprice=" + changedTotal + " WHERE sno=" + cart.get(position).getSno() + ";");
                                    db.close();
                                    cart();
                                } else {
                                    finalHolder1.totalvalue.setText("" + changedTotal);
                                    db.close();
                                }

                            } else {
                                finalHolder1.totalvalue.setText("" + changedTotal);
                                db.execSQL("UPDATE cart SET productqty = " + newQty + " , totalprice=" + changedTotal + " WHERE sno=" + cart.get(position).getSno() + ";");
                                db.close();
                                cart();
                            }
                        }
                    }
                }
            });
            holder.decerese.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (billoutFlag) {

                    } else {
                        float changedTotal = 0;
                        String newQty;
                        Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
                        int i = Integer.parseInt(finalHolder1.qtyvalue.getText().toString());
                        if (i > 0) {
                            i = i - 1;
                            finalHolder2.qtyvalue.setText("" + i);
                            db = activity.openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                            Cursor cartData = db.rawQuery("SELECT orderqty from cart WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO + " AND tableno=" + Group.GROUPID.table +
                                    " AND billflag = 0 AND sno=" + cart.get(position).getSno() + ";", null);
                            int count = cartData.getCount();
                            if (cart.get(position).getPqty().length() > 0) {
                                changedTotal = Float.parseFloat(cart.get(position).getPprice()) * i;
                            } else {
                                changedTotal = Float.parseFloat(cart.get(position).getPprice()) * i;
                            }
                            newQty = String.valueOf(i);
                            if (count > 0) {
                                cartData.moveToFirst();
                                if (Integer.parseInt(newQty) > cartData.getInt(cartData.getColumnIndex("orderqty"))) {
                                    finalHolder1.totalvalue.setText("" + changedTotal);
                                    db.execSQL("UPDATE cart SET productqty = " + newQty + " , totalprice=" + changedTotal + " WHERE sno=" + cart.get(position).getSno() + ";");
                                    db.close();
                                    cart();
                                } else {
                                    finalHolder1.totalvalue.setText("" + changedTotal);
                                    db.close();
                                }
                            } else {
                                db.execSQL("UPDATE cart SET productqty = " + newQty + " , totalprice=" + changedTotal + " WHERE sno=" + cart.get(position).getSno() + ";");
                                db.close();
                                cart();
                            }
                        }
                    }
                }
            });
            holder.qtyvalue.setText(cart.get(position).getPqty());
            holder.productname.setText(cart.get(position).getName());
            if (cart.get(position).getType().equalsIgnoreCase("no type")) {
                holder.producttype.setVisibility(View.GONE);
            } else {
                holder.producttype.setVisibility(View.VISIBLE);
                holder.producttype.setText(cart.get(position).getType());
            }
            holder.eachvalue.setText(cart.get(position).getPprice());
            holder.totalvalue.setText(cart.get(position).getTotal());
            return vi;
        }
    }

    public void cart() {
        double grandTotal = 0;
        db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid=" + editLocation + " AND tableno=" + editTable +
                " AND billflag = 0;", null);
        int count = cartData.getCount();
        if (count > 0) {
            cartData.moveToLast();
            do {
                String productqty = cartData.getString(cartData.getColumnIndex("productqty"));
                String productprice = cartData.getString(cartData.getColumnIndex("productprice"));
                String typeprice = cartData.getString(cartData.getColumnIndex("typeprice"));
                String typeqty = cartData.getString(cartData.getColumnIndex("typeqty"));
                Double totalvalue = Double.valueOf((Float.parseFloat(productprice) *
                        Integer.parseInt(productqty))) + Double.valueOf(typeprice) * Double.valueOf(typeqty);
                grandTotal = grandTotal + totalvalue;
            } while (cartData.moveToPrevious());
        }
        db.close();
        db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor taxDetails = db.rawQuery("SELECT servicetaxper,taxper1,taxper2,taxper3 FROM tax;", null);
        int taxDetailsCount = taxDetails.getCount();
        double tax1 = 0;
        if (taxDetailsCount > 0) {
            taxDetails.moveToFirst();
            tax1 = taxDetails.getFloat(taxDetails.getColumnIndex("servicetaxper"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper1"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper2"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper3"));

        } else {
        }
        db.close();
        grand = String.format("%.2f", grandTotal);
        double taxAmount = (tax1 * grandTotal) / 100;
        String grand_tax = String.format("%.2f", taxAmount);
        totaltax.setText(String.valueOf(grand_tax));
        grandTotal += (tax1 * grandTotal) / 100;
        String grand_Amount = String.format("%.2f", grandTotal);
        totalamountvalue.setText(String.valueOf(grand_Amount));
        Group.GROUPID.finaltotal = grand_Amount;
    }

    public void send() {
        db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor productDetails = db.rawQuery("SELECT * FROM cart WHERE tableno= " + CartStaticSragment.CARTStaticSragment.tableno.getText().toString() +
                " AND locationid= " + user_Login.user_LoginID.LOCATIONNO + " AND (billflag = 0 AND " +
                "productqty-orderqty > 0 OR typeqty-ordertypeqty > 0) ;", null);
        int count = productDetails.getCount();
        db.close();
        if (count > 0) {
            Toast.makeText(getActivity(), "Order Saved Successfully", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getActivity(), Printer.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("TABLENO", Integer.parseInt(CartStaticSragment.CARTStaticSragment.tableno.getText().toString()));
            intent.putExtra("LOCATIONNO", Integer.parseInt(user_Login.user_LoginID.LOCATIONNO));
            intent.putExtra("FLAG", 3);
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "No New Order Data To Print",
                    Toast.LENGTH_LONG).show();
        }
    }

    public static CartStaticSragment getInstance() {
        return CARTStaticSragment;
    }
}
