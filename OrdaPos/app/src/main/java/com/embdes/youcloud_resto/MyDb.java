package com.embdes.youcloud_resto;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.support.annotation.RequiresApi;

/**
 * Created by EmbDes on 19-07-2017.
 */

public class MyDb {
    MyHelper myHelper;
    SQLiteDatabase database;
    public MyDb(Context context){
        myHelper = new MyHelper(context,"db",null,1);
    }
    public void open(){
        database = myHelper.getWritableDatabase();
    }
    public long Insert(String mid, String tno, String ochairs, String username,String order){
        ContentValues contentValues = new ContentValues();
        contentValues.put("mid", mid);
      //  contentValues.put("uid", uid);
        contentValues.put("tno",tno);
        contentValues.put("ochairs", ochairs);
        contentValues.put("username", username);
        contentValues.put("orderid",order);
        long dat =  database.insert("ServerValues",null,contentValues);
        return dat;
    }
    public Cursor query(){
        Cursor cursor = null;
        cursor = database.query("ServerValues",null,null,null,null,null,null);
        return cursor;
    }
    public void delete(long q) {
        database.delete("ServerValues","_id="+q,null);
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void close(){
        database.close();
    }
    public static class MyHelper extends SQLiteOpenHelper{
        public MyHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table ServerValues(_id integer primary key,mid text,tno text,ochairs text,username text,orderid text)");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
