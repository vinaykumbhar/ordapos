package com.embdes.youcloud_resto;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class TablesFragment extends Fragment {
    ArrayList<tablespojo> tableno;
    static String COVERS;
    static TablesFragment TABLESID;
    CartStaticSragment cartstaticfragment;
    SQLiteDatabase db;
    ArrayList<Integer> arraylist;
    ArrayList<receivedochairs> received123;
    int noofchairs;
    Group group;
    MyTask myTask;
    boolean variable = false;
    TablesFragment tablesFragment;
    int i = 1;
    Timer timer;
    android.os.Handler handler;
    FragmentManager fragmentManager;
    android.app.FragmentTransaction fragmentTransaction;
    MYAdapter myAdapter;
    GridView gridView;

    public boolean checkInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || networkInfo.isConnected() == false) {
            return false;
        }
        return true;
    }

    public TablesFragment() {
        // Required empty public constructor
    }

    public class MyTask extends AsyncTask<String, Void, String> {
        URL myurl;
        HttpURLConnection connection;
        InputStream inputstream;
        InputStreamReader inputstreamreader;
        BufferedReader bufferreader;
        String line;
        StringBuilder result;
        String quicktable;
        String deliverytable;

        @Override
        protected String doInBackground(String... params) {
            try {
                myurl = new URL(params[0]);
                connection = (HttpURLConnection) myurl.openConnection();
                connection.setConnectTimeout(10000);
                inputstream = connection.getInputStream();
                inputstreamreader = new InputStreamReader(inputstream);
                bufferreader = new BufferedReader(inputstreamreader);
                line = bufferreader.readLine();
                result = new StringBuilder();
                while (line != null) {
                    result.append(line);
                    line = bufferreader.readLine();
                }
                return result.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (SocketTimeoutException e) {
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "Something went wrong";
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);

                JSONObject j = new JSONObject(s);
                JSONArray k = j.getJSONArray("table");
                for (int i = 0; i < k.length(); i++) {
                    JSONObject m = k.getJSONObject(i);
                    String tableno = m.getString("tableno");

                    String location = m.getString("locationid");
                    String ochairs = m.getString("occupiedchairs");
                    String orderid = m.has("orderid") ? m.getString("orderid") : "";
                    String mid = m.has("user") ? m.getString("user") : "";

                    receivedochairs receivedochairs = new receivedochairs();
                    //receivedochairs.setMid(mid);
                    receivedochairs.setLocation(location);
                    receivedochairs.setTableno(tableno);
                    receivedochairs.setOrderid(orderid);
//                receivedochairs.setOchairs(ochairs);
                    receivedochairs.setMid(mid);
                    received123.add(receivedochairs);
                    if (ochairs != null) {//0

                        Cursor order = db.rawQuery("SELECT * FROM ordernos WHERE tableno =" + tableno +
                                " AND locationid =" + location + " AND billflag = 0 ;", null);
                        int count = order.getCount();
                        String ordercheck = null;
                        boolean check = false;
                        if (count > 0) {
                            while (order.moveToNext()) {
                                ordercheck = order.getString(order.getColumnIndex("orderno"));
                                if (ordercheck.matches(orderid)) {
                                    check = true;
                                    break;
                                }
                            }
                        }

                        if (check == false) {
                            if (!orderid.matches("NULL") && !ochairs.matches("0")) {
                                db.execSQL("INSERT INTO ordernos values(1," + location + "," +
                                        tableno + ",'" + orderid + "', 0, 0);");
                            }
                        }

                        if (orderid.matches("")) {
                            orderid = "NULL";
                            String tab_num_query = "UPDATE tables SET occupiedchairs = " + ochairs + ",orderid = " + orderid +
                                    " WHERE locationid = " + location + " AND tableno =" + tableno + ";";
                            db.execSQL(tab_num_query);
                        } else {
                            db.execSQL("UPDATE tables SET occupiedchairs = " + ochairs + ",orderid = " + orderid +
                                    " WHERE locationid = " + location + " AND tableno =" + tableno + ";");
                        }
                        if (!ochairs.matches("0")) {

                            if (Integer.parseInt(tableno) >= 1500) {
                                quicktable = tableno;
                            }

                            if (ochairs.matches("0") && Integer.parseInt(tableno) >= 2000) {
                                deliverytable = tableno;
                            }
                        }
                    }

                    if (quicktable != null) {
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                        prefs.edit().putInt("tableno", Integer.parseInt(quicktable)).apply();
                    }
                    if (deliverytable != null) {
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                        prefs.edit().putInt("deliverytable", Integer.parseInt(deliverytable)).apply();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                db.close();
            }
            if (variable) {
                database();
            }
            super.onPostExecute(s);

//            db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
//            Cursor tableno = db.rawQuery("SELECT tableno FROM tables", null);
//            while (tableno.moveToNext()) {
//                int value = Integer.parseInt(tableno.getString(0));
//                Log.e("value","="+value);
//            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tables, container, false);
        gridView = (GridView) view.findViewById(R.id.grid);
        TABLESID = this;
        received123 = new ArrayList<receivedochairs>();
        fragmentManager = getFragmentManager();
        tablesFragment = new TablesFragment();
        group = (Group) getActivity();
        tableno = new ArrayList<tablespojo>();
        arraylist = new ArrayList<Integer>();
        cartstaticfragment = new CartStaticSragment();
        handler = new android.os.Handler();
        Group.GROUPID.searchView.setVisibility(View.INVISIBLE);
        Group.GROUPID.txt_info.setVisibility(View.VISIBLE);
        Group.GROUPID.lay_info.setVisibility(View.VISIBLE);
        Group.GROUPID.lay_search.setVisibility(View.GONE);
        Group.GROUPID.titlebar.setText("ORDERS");
        Group.GROUPID.txt_info.setText("Select available tables and select guest’s order");
        callAsynchronousTask();
        return view;
    }

    static class ViewHolder {
        ImageView tableEdit;
        TextView coversEdit, coverdetail;
        RelativeLayout r2;
    }

    public class MYAdapter extends BaseAdapter {
        private Activity activity;
        private LayoutInflater inflater = null;
        ArrayList<tablespojo> received;

        public MYAdapter(Activity a, ArrayList<tablespojo> table) {
            this.activity = a;
            this.received = table;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getViewTypeCount() {
            return getCount();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getCount() {
            return received.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, final View convertView, ViewGroup parent) {
            View vi = convertView;
            ViewHolder holder = null;
//            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            if (vi == null) {
                // if it's not recycled, initialize some attributes
                final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                vi = inflater.inflate(R.layout.tableadapter, parent, false);
                holder = new ViewHolder();
                holder.tableEdit = (ImageView) vi.findViewById(R.id.myImageView);
                holder.coversEdit = (TextView) vi.findViewById(R.id.myImageViewText);
                holder.coverdetail = (TextView) vi.findViewById(R.id.myImageViewText1);
                holder.r2 = (RelativeLayout) vi.findViewById(R.id.r2);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }

//            holder.tableEdit.setLayoutParams(lp);
//            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.tableEdit.getLayoutParams();
//            params.width = getpixelsfromDps(TablesFragment.this,150);
            holder.coversEdit.setText(String.valueOf(received.get(position).getTableno()));
            noofchairs = Integer.parseInt(received.get(position).getNoofchaors());
            final ViewHolder finalHolder1 = holder;
            if (holder.coversEdit.getText().length() != 0 &&
                    Integer.parseInt(holder.coversEdit.getText().toString()) > 0) {
                Group.GROUPID.table = holder.coversEdit.getText().toString();
                if (CartStaticSragment.CARTStaticSragment.noofguests.getText().length() != 0) {
                    COVERS = CartStaticSragment.CARTStaticSragment.noofguests.getText().toString();
                }
                final int occupiedchairs = Integer.parseInt(received.get(position).getOccupied());

                if (occupiedchairs < 1) {
                    holder.tableEdit.setImageResource(R.drawable.btn_tableavailable);
                    holder.coversEdit.setTextColor(Color.GREEN);
                    holder.coverdetail.setTextColor(Color.GREEN);
                } else {
                    holder.tableEdit.setImageResource(R.drawable.btn_tableoccupied);
                    holder.coverdetail.setText("OCCUPIED");
                    holder.coverdetail.setTextColor(Color.RED);
                    holder.coversEdit.setTextColor(Color.RED);
                    final ViewHolder finalHolder = holder;
                    holder.tableEdit.setTag(received.get(position).getTableno());
                    holder.tableEdit.setFocusable(true);
                    holder.tableEdit.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                if (tableno.get(position).getMid().matches(user_Login.user_LoginID.nameid)) {
                                    if (arraylist.contains(new Integer(position))) {
                                        arraylist.remove(new Integer(position));
                                        Group.GROUPID.table = finalHolder1.coversEdit.getText().toString();
                                        CartStaticSragment.CARTStaticSragment.tableno.setText(Group.GROUPID.table);
                                        db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                                        Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" +
                                                Group.GROUPID.table + " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
                                        int cou = chairs.getCount();
                                        chairs.moveToFirst();
                                        if (cou > 0) {
                                            String guests = chairs.getString(chairs.getColumnIndex("occupiedchairs"));
                                            CartStaticSragment.CARTStaticSragment.noofguests.setText(guests);
                                        }
                                        db.close();
                                        finalHolder.tableEdit.setImageResource(R.drawable.btn_tableselected);
                                        finalHolder.coverdetail.setText("SELECTED");
                                        finalHolder.coverdetail.setTextColor(Color.WHITE);
                                        finalHolder.coversEdit.setTextColor(Color.WHITE);
                                        CartStaticSragment.CARTStaticSragment.tables.setText("Take Order");
                                        CartStaticSragment.CARTStaticSragment.tables.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (!Group.GROUPID.groupfragment.isResumed()) {
                                                    Group.GROUPID.fragmentTransaction = Group.GROUPID.fragmentManager.beginTransaction();
                                                    Group.GROUPID.fragmentTransaction.replace(R.id.container2, Group.GROUPID.groupfragment);
                                                    Group.GROUPID.fragmentTransaction.commit();
                                                    Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
                                                    CartStaticSragment.CARTStaticSragment.tables.setText("Tables");
                                                }
                                                CartStaticSragment.CARTStaticSragment.tables.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        if (CartStaticSragment.CARTStaticSragment.listView != null) {
                                                            if (!tablesFragment.isResumed()) {
                                                                CartStaticSragment.CARTStaticSragment.tableno.setText("");
                                                                CartStaticSragment.CARTStaticSragment.noofguests.setText("0");
                                                                CartStaticSragment.CARTStaticSragment.totalamountvalue.setText("");
                                                                CartStaticSragment.CARTStaticSragment.totaltax.setText("");
                                                                CartStaticSragment.CARTStaticSragment.itemno.setText("");
                                                                CartStaticSragment.CARTStaticSragment.listView.setAdapter(null);
                                                                fragmentTransaction = fragmentManager.beginTransaction();
                                                                fragmentTransaction.replace(R.id.container2, tablesFragment);
                                                                fragmentTransaction.commit();
                                                            }
                                                        } else {
                                                            fragmentTransaction = fragmentManager.beginTransaction();
                                                            fragmentTransaction.replace(R.id.container2, tablesFragment);
                                                            fragmentTransaction.commit();
                                                            group.alert();

                                                        }
                                                    }
                                                });
                                            }
                                        });
                                        group.alert();
                                    } else {
                                        arraylist.add(new Integer(position));
                                        notifyDataSetChanged();
                                        finalHolder.tableEdit.setImageResource(R.drawable.btn_tableoccupied);
                                        finalHolder.coverdetail.setText("OCCUPIED");
                                        finalHolder.coverdetail.setTextColor(Color.RED);
                                        finalHolder.coversEdit.setTextColor(Color.RED);
                                    }
                                } else {
                                    AlertDialog.Builder msg = new AlertDialog.Builder(
                                            getActivity(), android.R.style.Theme_Dialog);
                                    msg.setTitle("");
                                    msg.setMessage("Table is Occupied by " + tableno.get(position).getMid());
                                    msg.setPositiveButton("Ok", null);
                                    msg.show();
                                }
                            }
                            return true;
                        }
                    });
                }
                holder.tableEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // we are replacing the tables fragment with group fragment
                        if (Integer.parseInt(finalHolder1.coversEdit.getText().toString()) < 1500) {
                            CartStaticSragment.CARTStaticSragment.listView.setAdapter(null);
                            CartStaticSragment.CARTStaticSragment.totalamountvalue.setText("");
                            CartStaticSragment.CARTStaticSragment.totaltax.setText("");
                            CartStaticSragment.CARTStaticSragment.itemno.setText("");
                            db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                            if (finalHolder1.coversEdit.getText().length() != 0) {
                                Group.GROUPID.table = finalHolder1.coversEdit.getText().toString();
                                CartStaticSragment.CARTStaticSragment.tableno.setText(Group.GROUPID.table);
                                COVERS = CartStaticSragment.CARTStaticSragment.noofguests.getText().toString();
                                Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" +
                                        Group.GROUPID.table + " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);

                                int chairsCount = chairs.getCount();
                                chairs.moveToFirst();
                                if (chairsCount > 0) {
                                    if (chairs.getInt(chairs.getColumnIndex("occupiedchairs")) < 1) {
                                        String value = String.valueOf(chairs.getInt(chairs.getColumnIndex("occupiedchairs")));
                                        CartStaticSragment.CARTStaticSragment.noofguests.setText(value);
                                        Cursor c = db.rawQuery("SELECT noofchairs FROM tables WHERE tableno =" + Group.GROUPID.table +
                                                " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
                                        int count = c.getCount();
                                        c.moveToFirst();
                                        if (count > 0) {
                                            if (noofchairs >= Integer.parseInt(COVERS)) {
                                                finalHolder1.coversEdit.getText();
                                                CartStaticSragment.CARTStaticSragment.noofguests.getText();
                                                group.groupfragment();
                                            } else {
                                                CartStaticSragment.CARTStaticSragment.noofguests.getText();
                                                AlertDialog.Builder msg = new AlertDialog.Builder(
                                                        getActivity(), android.R.style.Theme_Dialog);
                                                msg.setTitle("");
                                                msg.setMessage("Covers Should Not Exceed " + noofchairs);
                                                msg.setPositiveButton("Ok", null);
                                                msg.show();
                                            }

                                        } else {
                                            finalHolder1.coversEdit.getText();
                                            CartStaticSragment.CARTStaticSragment.noofguests.getText();
                                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                                    getActivity(), android.R.style.Theme_Dialog);
                                            msg.setTitle("");
                                            msg.setMessage("Table is Not Available");
                                            msg.setPositiveButton("Ok", null);
                                            msg.show();
                                        }
                                    } else {
                                        finalHolder1.coversEdit.getText();
                                        CartStaticSragment.CARTStaticSragment.noofguests.getText();
                                        Intent intent = new Intent(getActivity(), Group.class);
                                        startActivity(intent);
                                    }
                                } else {
                                    finalHolder1.coversEdit.getText();
                                    CartStaticSragment.CARTStaticSragment.noofguests.getText();
                                    AlertDialog.Builder msg = new AlertDialog.Builder(
                                            getActivity(), android.R.style.Theme_Dialog);
                                    msg.setTitle("");
                                    msg.setMessage("Table Is Not Available");
                                    msg.setPositiveButton("Ok", null);
                                    msg.show();

                                }
                            } else {

                                finalHolder1.coversEdit.getText();
                                CartStaticSragment.CARTStaticSragment.noofguests.getText();
                                AlertDialog.Builder msg = new AlertDialog.Builder(
                                        getActivity(), android.R.style.Theme_Dialog);
                                msg.setTitle("");
                                msg.setMessage("Table Number Is Not Valid");
                                msg.setPositiveButton("Ok", null);
                                msg.show();
                            }
                            db.close();
                        } else {
                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                    getActivity(), android.R.style.Theme_Dialog);
                            msg.setTitle("");
                            msg.setMessage("Entry Restricted");
                            msg.setPositiveButton("Ok", null);
                            msg.show();
//                            CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away:-");
//                            CartStaticSragment.CARTStaticSragment.tableno.setText("#####");
//                            CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
//                            CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
//                            CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
//                            CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
//                            CartStaticSragment.CARTStaticSragment.listView.setAdapter(null);
//                            CartStaticSragment.CARTStaticSragment.totalamountvalue.setText("");
//                            CartStaticSragment.CARTStaticSragment.totaltax.setText("");
//                            CartStaticSragment.CARTStaticSragment.itemno.setText("");
//                            db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
//                            if (finalHolder1.coversEdit.getText().length() != 0 ) {
//                                Group.GROUPID.table = finalHolder1.coversEdit.getText().toString();
//                                CartStaticSragment.CARTStaticSragment.tableno.setText(Group.GROUPID.table);
//                                COVERS = CartStaticSragment.CARTStaticSragment.noofguests.getText().toString();
//                                Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" +
//                                        Group.GROUPID.table + " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
//
//                                int chairsCount = chairs.getCount();
//                                chairs.moveToFirst();
//                                if (chairsCount > 0) {
//                                    if (chairs.getInt(chairs.getColumnIndex("occupiedchairs"))< 1) {
//                                        String value = String.valueOf(chairs.getInt(chairs.getColumnIndex("occupiedchairs")));
//                                        CartStaticSragment.CARTStaticSragment.noofguests.setText(value);
//                                        Cursor c = db.rawQuery("SELECT noofchairs FROM tables WHERE tableno =" + Group.GROUPID.table +
//                                                " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
//                                        int count = c.getCount();
//                                        c.moveToFirst();
//
//                                        if (count > 0) {
//                                            if (noofchairs >= Integer.parseInt(COVERS)) {
//                                                finalHolder1.coversEdit.getText();
//                                                CartStaticSragment.CARTStaticSragment.noofguests.getText();
//                                                group.groupfragment();}}}}
//                        }
                        }
                    }

                    ;

                });
            }
            return vi;
        }

        private int getpixelsfromDps(TablesFragment tableView, int dps) {
            Resources r = tableView.getResources();
            int px = (int) (TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
            return px;
        }
    }

    public void database() {
        db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor chairs = db.rawQuery("SELECT tableno,noofchairs,occupiedchairs FROM tables WHERE locationid ="
                + user_Login.user_LoginID.LOCATIONNO + ";", null);
        int c = chairs.getCount();
        if (c > 0) {

            while (chairs.moveToNext()) {
                tablespojo mtablespojo = new tablespojo();
                String table = chairs.getString(chairs.getColumnIndex("tableno"));
                String chair = chairs.getString(chairs.getColumnIndex("noofchairs"));
                String occ = chairs.getString(chairs.getColumnIndex("occupiedchairs"));
                mtablespojo.setNoofchaors(chair);
                mtablespojo.setTableno(table);
                mtablespojo.setOccupied(occ);
                String notmatchtable = "x";

                if (Integer.parseInt(table) < 1500) {
                    for (int i = 0; i < received123.size(); i++) {
                        notmatchtable = received123.get(i).getTableno();
                        if (table.matches(notmatchtable)) {
                            mtablespojo.setMid(received123.get(i).getMid());
                            break;
                        }
                    }
                    if (!(notmatchtable.matches(table))) {
                        mtablespojo.setMid(user_Login.nameid);
                    }
                    tableno.add(mtablespojo);
                }
            }
        }
        db.close();
        //if tableno array is null run a if condition
        if (tableno.size() != 0) {
            myAdapter = new MYAdapter(getActivity(), tableno);
            gridView.setAdapter(myAdapter);
            myAdapter.notifyDataSetChanged();
        }
    }

    public void callAsynchronousTask() {
        timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            myTask = new MyTask();
                            if (checkInternet()) {
                                myTask.execute("http://52.202.56.230:8080/OrdaposServer/servlet/JSONServlet/?terminalid=" + MainActivity.MAINID.outletid);
                                System.out.println("http://52.202.56.230:8080/OrdaposServer/servlet/JSONServlet/?terminalid=" + MainActivity.MAINID.outletid);
                                variable = true;
                                if (tableno.size() != 0) {
                                    tableno.clear();
                                }
                                if (received123.size() != 0) {
                                    received123.clear();
                                }
                            } else {
                                if (tableno.size() != 0) {
                                    tableno.clear();
                                }
                                if (received123.size() != 0) {
                                    received123.clear();
                                }
                                database();
                                Toast.makeText(getActivity(), "NO INTERNET", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 50000);

    }

    public void stop() {
        variable = false;
        timer.cancel();
        myTask.cancel(true);
        if (tableno.size() != 0) {
            tableno.clear();
        }
        if (received123.size() != 0) {
            received123.clear();
        }
    }

    public static TablesFragment getInstance() {
        return TABLESID;
    }
}
