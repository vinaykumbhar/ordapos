package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.embdes.utills.CustomMarkerView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


/**
 * Created by Vinay on 12/09/2017.
 */

public class SalesSummaryFragment extends Fragment implements OnChartValueSelectedListener {
//    JFreeChart piechartPDF;
//    JFreeChart barchartPDF;

    static SalesSummaryFragment COLUMNCHART;
    //column chart
    private HorizontalBarChart Columnchart;
    private boolean hasAxes = true;
    private boolean hasAxesNames = true;
    private boolean hasLabels = true;
    private boolean hasLines = false;
    private boolean hasLabelForSelected = false;
    Spinner groupSpinner, spinner_amt;
    //pie chart
    private PieChart piechart;
    private boolean hasLabelsOutside = false;
    private boolean hasCenterCircle = false;
    private boolean isExploded = false;
    SQLiteDatabase db;
    ArrayList<String> groups;

    ImageView image_groups;
    boolean date_flag = false;
    Button btn_go;
    LinearLayout btn_send, btn_print;
    String toDateString;
    String fromDateString;
    public static String toReportsDate;
    public static String fromReportsDate;
    DatePickerDialog toDate;//
    TextView fromDate, toDateTime;
    ImageView image_amtorqty;
    public int yearSelected, monthSelected, daySelected, hourSelected, minuteSelected;

    // declare  the variables to show the date and time whenTime and Date Picker Dialog first appears
    private int mYear, mMonth, mDay, mHour, mMinute/*mMinute1*/;
    boolean isShow = false;

    static final int DATE_DIALOG_ID = 0;
    static final int FROM_DATE_DIALOG_ID = 2;
    ArrayList<String> sales_by = new ArrayList<String>();

    public SalesSummaryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_column_chart, container, false);
        COLUMNCHART = this;


        fromDate = (TextView) rootView.findViewById(R.id.fromDateView);
        toDateTime = (TextView) rootView.findViewById(R.id.toDateView);
        image_groups = (ImageView) rootView.findViewById(R.id.image_groups);
        String timeStamp = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(Calendar.getInstance().getTime());
        toReportsDate = timeStamp;
        fromReportsDate = timeStamp;
        toDateTime.setText(" " + timeStamp);
        fromDate.setText("" + timeStamp);

        Columnchart = (HorizontalBarChart) rootView.findViewById(R.id.column_chart);
        groupSpinner = (Spinner) rootView.findViewById(R.id.spinner_groups);
        spinner_amt = (Spinner) rootView.findViewById(R.id.spinner_amt);
        image_amtorqty = (ImageView) rootView.findViewById(R.id.image_amtorqty);
        ImageButton toSetButton = (ImageButton) rootView.findViewById(R.id.toButton);
        ImageButton fromButton = (ImageButton) rootView.findViewById(R.id.fromButton);
        btn_send = (LinearLayout) rootView.findViewById(R.id.send);
        btn_go = (Button) rootView.findViewById(R.id.btn_go);
        btn_go.setOnClickListener(btngoclickListener);
        fromButton.setOnClickListener(fromsethandler);
        toSetButton.setOnClickListener(tosethandler);
        btn_send.setOnClickListener(btnsendListener);
        image_groups.setOnClickListener(grpSpinnerbtnListener);
        image_amtorqty.setOnClickListener(amtSpinnerbtnListener);


        piechart = (PieChart) rootView.findViewById(R.id.pie_chart);
        piechart.setUsePercentValues(false);
//        piechart.getDescription().setEnabled(false);
        piechart.setExtraOffsets(5, 10, 5, 5);
        piechart.setDragDecelerationFrictionCoef(0.95f);
        piechart.setRotationEnabled(false);
        piechart.setDrawHoleEnabled(false);
//        piechart.setTouchscreenBlocksFocus(false);
        piechart.setTouchEnabled(false);
        piechart.setDescription("");

        Columnchart.setDoubleTapToZoomEnabled(false);
//        Columnchart.getDescription().setEnabled(false);
        Columnchart.getAxisRight().setEnabled(false);
        Columnchart.setDescription("");

        YAxis leftAxis = Columnchart.getAxisLeft();
        Columnchart.getAxisLeft().setEnabled(false);
        leftAxis.setEnabled(true);
        leftAxis.setDrawLabels(true);
        leftAxis.setDrawGridLines(false);

        XAxis x = Columnchart.getXAxis();
        x.setPosition(XAxis.XAxisPosition.BOTTOM);
        x.setDrawGridLines(false);
        x.setLabelsToSkip(0);
        x.setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);//set label inside bar
        //x.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));//label color of x axis
        Columnchart.getAxisRight().setEnabled(false);
        Columnchart.getLegend().setEnabled(false);

        YAxis yAxis = Columnchart.getAxis(YAxis.AxisDependency.LEFT);
        yAxis.setStartAtZero(true);
        Columnchart.setOnChartValueSelectedListener(this);
        Columnchart.setDrawValueAboveBar(true);
        Columnchart.setDrawBorders(true);
        Columnchart.setBorderColor(ContextCompat.getColor(getActivity(), R.color.app_bg_color));
        sales_by.clear();
        sales_by.add("Sales Amount");
        sales_by.add("Sales Quantity");
        ArrayAdapter<String> oppor_adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.productpinner, sales_by);
        oppor_adapter.setDropDownViewResource(R.layout.productpinner);

        spinner_amt.setAdapter(oppor_adapter);

        groups = new ArrayList<>();
        categoryReportsFromDate(fromReportsDate, toReportsDate, date_flag, 0);//barchart
        groupReportsByDate(fromReportsDate, toReportsDate, date_flag);//piechart onCreate
        groupSpinner.setOnItemSelectedListener(listener);
        spinner_amt.setOnItemSelectedListener(salesbyListenter);
        return rootView;
    }


    View.OnClickListener tosethandler = new View.OnClickListener() {
        @SuppressWarnings("deprecation")
        @SuppressLint("NewApi")
        public void onClick(View v) {
            final Calendar c = Calendar.getInstance();
            isShow = true;
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);
            MainReportActivity.mainreportView.showDialog(DATE_DIALOG_ID);
        }
    };

    View.OnClickListener amtSpinnerbtnListener = new View.OnClickListener() {
        @SuppressWarnings("deprecation")
        @SuppressLint("NewApi")
        public void onClick(View v) {
            spinner_amt.performClick();
        }
    };
    View.OnClickListener grpSpinnerbtnListener = new View.OnClickListener() {
        @SuppressWarnings("deprecation")
        @SuppressLint("NewApi")
        public void onClick(View v) {
            groupSpinner.performClick();
        }
    };
    View.OnClickListener btnsendListener = new View.OnClickListener() {
        @SuppressWarnings("deprecation")
        @SuppressLint("NewApi")
        public void onClick(View v) {
            writeChartToPDF writeChartToPDF = new writeChartToPDF();
            writeChartToPDF.execute();
        }
    };

    View.OnClickListener btngoclickListener = new View.OnClickListener() {
        @SuppressWarnings("deprecation")
        @SuppressLint("NewApi")
        public void onClick(View v) {
            date_flag = true;
            categoryReportsFromDate(fromReportsDate, toReportsDate, date_flag, spinner_amt.getSelectedItemPosition());
            groupReportsByDate(fromReportsDate, toReportsDate, date_flag);

        }
    };

    View.OnClickListener fromsethandler = new View.OnClickListener() {
        @SuppressWarnings("deprecation")
        @SuppressLint("NewApi")
        public void onClick(View v) {
            final Calendar c = Calendar.getInstance();
            isShow = true;
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);
            MainReportActivity.getInstance().showDialog(FROM_DATE_DIALOG_ID);

        }
    };

    /**
     * Dialog will come when user press date button in reports screen.
     */
    protected Dialog onCreateDialog(int id) {
        switch (id) {

            case FROM_DATE_DIALOG_ID:
                final DatePickerDialog fromDate = new DatePickerDialog(getActivity(),
                        fromDateSetListener,
                        mYear, mMonth, mDay);
                fromDate.getDatePicker().setMaxDate(System.currentTimeMillis());
                fromDate.setCancelable(true);
                fromDate.setCanceledOnTouchOutside(true);
                fromDate.setButton(DialogInterface.BUTTON_NEGATIVE,
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                isShow = false; //Cancel flag, used in mTimeSetListener
                            }
                        });
                return fromDate;

            case DATE_DIALOG_ID:
                toDate = new DatePickerDialog(getActivity(),
                        toDateSetListener,
                        mYear, mMonth, mDay);
                toDate.getDatePicker().setMaxDate(System.currentTimeMillis());
                toDate.setButton(DialogInterface.BUTTON_NEGATIVE,
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                isShow = false; //Cancel flag, used in mTimeSetListener
                            }
                        });
                return toDate;
        }
        return null;
    }

    // Register  fromDatePickerDialog listener
    private DatePickerDialog.OnDateSetListener fromDateSetListener =
            new DatePickerDialog.OnDateSetListener() {                 // the callback received when the user "sets" the Date in the DatePickerDialog
                public void onDateSet(DatePicker view, int yearSelected,
                                      int monthOfYear, int dayOfMonth) {
                    if (isShow == true) {
                        isShow = false;
                        monthSelected = ++monthOfYear;
                        daySelected = dayOfMonth;
                        //  TextView fromDate = (TextView) findViewById(R.id.fromDateView);
                        String newYear = "" + yearSelected;
                        fromDateString = String.format("" + String.format("%02d", daySelected) + "-" +
                                String.format("%02d", monthSelected) + "-" + newYear);
                        fromDate.setText("" + fromDateString + " " + String.format("%02d", hourSelected) + ":" +
                                String.format("%02d", minuteSelected));
                        fromTime();
                    }
                }
            };

    // Register  toDatePickerDialog listener
    private DatePickerDialog.OnDateSetListener toDateSetListener =
            new DatePickerDialog.OnDateSetListener() {                 // the callback received when the user "sets" the Date in the DatePickerDialog
                public void onDateSet(DatePicker view, int yearSelected,
                                      int monthOfYear, int dayOfMonth) {
                    if (isShow == true) {
                        isShow = false;
                        System.out.println("*** To Date Set on click Listner ***");
                        monthSelected = ++monthOfYear;
                        daySelected = dayOfMonth;
                        String newYear = "" + yearSelected;
                        // TextView toDate = (TextView) findViewById(R.id.toDateView);
                        toDateString = String.format("" + String.format("%02d", daySelected) + "-" +
                                String.format("%02d", monthSelected) + "-" + newYear);
                        toDateTime.setText("" + toDateString + " " + String.format("%02d", hourSelected) + ":" +
                                String.format("%02d", minuteSelected));
                        toTime();
                    }
                }
            };

    public void fromTime() {
        TimePickerDialog fromTime = new TimePickerDialog(getActivity(),
                fromTimeSetListener, mHour, mMinute, false);
        fromTime.show();
    }

    public void toTime() {
        TimePickerDialog toTime = new TimePickerDialog(getActivity(),
                toTimeSetListener, mHour, mMinute, false);
        toTime.show();
    }

    // Register  TimePickerDialog listener
    private TimePickerDialog.OnTimeSetListener fromTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                // the callback received when the user "sets" the TimePickerDialog in the dialog
                public void onTimeSet(TimePicker view, int hourOfDay, int min) {
                    hourSelected = hourOfDay;
                    minuteSelected = min;

                    fromReportsDate = "" + fromDateString + " " + String.format("%02d", hourSelected) + ":" +
                            String.format("%02d", minuteSelected);
                    fromDate.setText("" + fromDateString + " " + String.format("%02d", hourSelected) + ":" +
                            String.format("%02d", minuteSelected));
                }
            };

    // Register  TimePickerDialog listener
    private TimePickerDialog.OnTimeSetListener toTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                // the callback received when the user "sets" the TimePickerDialog in the dialog
                public void onTimeSet(TimePicker view, int hourOfDay, int min) {
                    hourSelected = hourOfDay;
                    minuteSelected = min;

                    toReportsDate = "" + toDateString + " " + String.format("%02d", hourSelected) + ":" +
                            String.format("%02d", minuteSelected);
                    toDateTime.setText("" + toDateString + " " + String.format("%02d", hourSelected) + ":" +
                            String.format("%02d", minuteSelected));

                }
            };

    AdapterView.OnItemSelectedListener salesbyListenter = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            groupSpinner.setSelection(0);
            categoryReportsFromDate(fromReportsDate, toReportsDate, date_flag, position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
    AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0) {
                categoryReportsFromDate(fromReportsDate, toReportsDate, date_flag, spinner_amt.getSelectedItemPosition());//barchart
            } else if (position > 0) {
                if (groups.size() > 1) {
                    categoryReportsFromDateGroup(fromReportsDate, toReportsDate, date_flag, groups.get(position), spinner_amt.getSelectedItemPosition());//barchart
                } else {
                    date_flag=false;
                    Toast.makeText(getActivity(), "No data found between selected date range!", Toast.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    public String reverseDateFormat(String date) {
        //return date;
        return date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2) + date.substring(10, 16);
    }

    ArrayList<String> barEntriesLabel;

    public void categoryReportsFromDate(String astartDate, String aendDate, boolean dateflag, int salesBy) {
        ArrayList<BarData> barDataList = new ArrayList<>();
        String startDate = reverseDateFormat(astartDate);
        String endDate = reverseDateFormat(aendDate);

        barEntriesLabel = new ArrayList<>();
        Float total = 0f;

        String newHeader = "EmbDes";

        SQLiteDatabase db;
        db = getActivity().openOrCreateDatabase("rebelPOS.db", Context.MODE_PRIVATE, null);
        Cursor shopDetails = db.rawQuery("SELECT terminalname FROM masterterminal;", null);
        int shopCount = shopDetails.getCount();
        if (shopCount > 0) {
            shopDetails.moveToFirst();
        } else {
            db.close();
            Toast.makeText(getActivity(), "No Data in master_terminal", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getActivity(), Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            return;
        }
        barEntriesLabel.clear();
        Cursor categoryDetails = db.rawQuery("SELECT categoryname,categoryid from category;", null);
        int categoryCount = categoryDetails.getCount();
        if (categoryCount > 0) {
            categoryDetails.moveToPrevious();
            int axis_pos = 0;
            float start = 1f;
            int count = 1;
            ArrayList<BarEntry> entries = new ArrayList<>();

            while (categoryDetails.moveToNext()) {
                ArrayList<IBarDataSet> dataSets = new ArrayList<>();
                String catName = categoryDetails.getString(0);//copying all group data into struct//item name
                Cursor cartDetails;
                if (!dateflag) {
                    cartDetails = db.rawQuery("SELECT pcategory, productqty, productprice FROM cart WHERE pcategory='" + catName + "';", null);// GROUP BY pcategory
                } else
                    cartDetails = db.rawQuery("SELECT pcategory, productqty, productprice FROM cart WHERE pcategory='" + catName + "' AND datetime >='" + startDate + "' AND datetime <= '" + endDate + "';", null);//GROUP BY pcategory
                int cartDetailscount = cartDetails.getCount();

                if (cartDetailscount > 0) {
                    cartDetails.moveToPrevious();
                    float totalValue = 0;
                    int totalqty = 0;
                    while (cartDetails.moveToNext()) {
                        Float itemValue = cartDetails.getInt(1) * cartDetails.getFloat(2);//total qty * price
                        totalValue += itemValue;
                        totalqty += cartDetails.getInt(1);
                    }
                    total = total + totalValue;
                    if (totalValue > 0 && salesBy == 0) {
                        entries.add(new BarEntry(totalValue, axis_pos));
                        barEntriesLabel.add(catName + " - " + totalValue);
                        axis_pos++;
                    } else if (totalqty > 0 && salesBy == 1) {
                        entries.add(new BarEntry(totalqty, axis_pos));
                        barEntriesLabel.add(catName + " - " + totalqty);
                        axis_pos++;
                    }


                    BarDataSet b = new BarDataSet(entries, "Categories");
                    b.setColor(ContextCompat.getColor(getActivity(), R.color.yellow));
                    b.setDrawValues(false);
                    dataSets.add(b);

                    BarData barData = new BarData(barEntriesLabel, dataSets);
                    barData.setValueTextSize(9f);
                    barData.setHighlightEnabled(true);
                    barDataList.add(barData);
                }
            }

            for (BarData barData : barDataList) {
                Columnchart.setData(barData);
                //barData.setHighlightEnabled(false);
                // Columnchart.setVisibleXRangeMaximum(5);

                Columnchart.animateXY(1000, 1000);
                Columnchart.invalidate();

            }
            Columnchart.setMarkerView(new CustomMarkerView(getActivity(), R.layout.custom_marker_view, barEntriesLabel));

        } else {
            db.close();
            Toast.makeText(getActivity(), "No Group Data In Cart", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getActivity(), Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            return;
        }
        db.close();
        return;
    }

    public void categoryReportsFromDateGroup(String astartDate, String aendDate, boolean dateflag, String grpname, int salesBy) {
        String startDate = reverseDateFormat(astartDate);
        String endDate = reverseDateFormat(aendDate);
        ArrayList<BarData> barDataList = new ArrayList<>();
        ArrayList<String> barEntriesLabel = new ArrayList<>();
//        float totalValue = 0;
        String newHeader = "EmbDes";
        Float total = 0f;
        String newName;
        Float newValue = 0f;
        SQLiteDatabase db;
        db = getActivity().openOrCreateDatabase("rebelPOS.db", Context.MODE_PRIVATE, null);
        Cursor shopDetails = db.rawQuery("SELECT terminalname FROM masterterminal;", null);
        int shopCount = shopDetails.getCount();
        if (shopCount > 0) {
            shopDetails.moveToFirst();
        } else {
            db.close();
            Toast.makeText(getActivity(), "No Data in master_terminal", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getActivity(), Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            return;
        }
        Cursor groupDetails = db.rawQuery("SELECT groupid FROM grouptable WHERE groupname='" + grpname + "';", null);
        if (groupDetails.getCount() > 0) {
            groupDetails.moveToFirst();
            String group_id = groupDetails.getString(0);
            Cursor categoryDetails = db.rawQuery("SELECT categoryname,categoryid from category WHERE groupid='" + group_id + "';", null);
            int categoryCount = categoryDetails.getCount();
            if (categoryCount > 0) {
                categoryDetails.moveToPrevious();
                int axis_pos = 0;
                ArrayList<BarEntry> entries = new ArrayList<>();
                while (categoryDetails.moveToNext()) {
                    ArrayList<IBarDataSet> dataSets = new ArrayList<>();
                    newName = categoryDetails.getString(0);//copying all group data into struct//item name
                    String catName = categoryDetails.getString(0);//copying all group data into struct//item name
                    Cursor cartDetails;
                    if (!dateflag) {
                        cartDetails = db.rawQuery("SELECT pcategory, productqty, productprice FROM cart WHERE pcategory='" + newName + "' AND pgroup='" + grpname + "';", null);
                    } else
                        cartDetails = db.rawQuery("SELECT pcategory, productqty, productprice FROM cart WHERE pcategory='" + newName + "' AND datetime >='" + startDate + "' AND datetime <= '" + endDate + "' AND pgroup='" + grpname + "';", null);
                    int cartDetailscount = cartDetails.getCount();
                    if (cartDetailscount > 0) {
                        cartDetails.moveToPrevious();
                        float totalValue = 0;
                        int totalqty = 0;
                        while (cartDetails.moveToNext()) {
                            newValue = cartDetails.getInt(1) * cartDetails.getFloat(2);//total qty * price
                            totalValue += newValue;
                            totalqty += cartDetails.getInt(1);
                        }
                        total = total + totalValue;
//                        Log.e("cartDetails", "=" + totalValue + " " + totalqty);
                        if (totalValue > 0 && salesBy==0) {
                            entries.add(new BarEntry(totalValue, axis_pos));
                            barEntriesLabel.add(catName + " - " + totalValue);
                            axis_pos++;
                        } else if (totalqty > 0 && salesBy==1) {
                            entries.add(new BarEntry(totalqty, axis_pos));
                            barEntriesLabel.add(catName + " - " + totalqty);
                            axis_pos++;
                        }

                    }
                    /*else {//if item not in cart
                        newValue = 0f;//total qty * price
                        totalValue += 0;
                        entries.add(new BarEntry(newValue, axis_pos));
                        barEntriesLabel.add(newName);
                        axis_pos++;
                    }*/
                    BarDataSet b = new BarDataSet(entries, grpname);
                    b.setColor(ContextCompat.getColor(getActivity(), R.color.yellow));
                    b.setDrawValues(false);
                    dataSets.add(b);

                    if (barEntriesLabel.size() > 0 && dataSets.size() > 0) {
                        BarData barData = new BarData(barEntriesLabel, dataSets);
                        barData.setValueTextSize(9f);
                        //Columnchart.setData(barData);
                        //barData.setHighlightEnabled(false);
                        barDataList.add(barData);
                    }
                }

                for (BarData barData : barDataList) {
                    Columnchart.setData(barData);
                    // barData.setHighlightEnabled(false);
//                    Columnchart.setVisibleXRangeMaximum(5);
                    Columnchart.animateXY(1000, 1000);
                    Columnchart.invalidate();

                }
                Columnchart.setMarkerView(new CustomMarkerView(getActivity(), R.layout.custom_marker_view, barEntriesLabel));

            } else {
                db.close();
                Toast.makeText(getActivity(), "No Group Data In Cart", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), Home.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return;
            }
        }
        db.close();
        return;
    }

    public void groupReportsByDate(String astartDate, String aendDate, boolean dateFlag) {
        String startDate = reverseDateFormat(astartDate);
        String endDate = reverseDateFormat(aendDate);

        float totalValue = 0;
        String newHeader = "EmbDes";
        String newName;
        boolean flag = false;

        SQLiteDatabase db;
        db = getActivity().openOrCreateDatabase("rebelPOS.db", Context.MODE_PRIVATE, null);
        Cursor shopDetails = db.rawQuery("SELECT terminalname FROM masterterminal;", null);
        int shopCount = shopDetails.getCount();
        if (shopCount > 0) {
            shopDetails.moveToFirst();
        } else {
            db.close();
            Toast.makeText(getActivity(), "No Data in master_terminal", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getActivity(), Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            return;
        }
        Cursor groupDetails = db.rawQuery("SELECT groupname,groupid from grouptable;", null);
        int productCount = groupDetails.getCount();
        if (productCount > 0) {
            int i = 0;
            groupDetails.moveToPrevious();
            groups = new ArrayList<>();
            groups.add("All");
            PieData pieData = new PieData();
            ArrayList<Entry> entries = new ArrayList<Entry>();
            ArrayList<String> entryLabel = new ArrayList<String>();
            while (groupDetails.moveToNext()) {
                newName = groupDetails.getString(0);//group name
                Cursor cartDetails;
                String groupQuery = groupQuery = "SELECT pgroup, productqty, productprice FROM cart WHERE pgroup='" + newName + "';";
                if (!dateFlag)
                    groupQuery = "SELECT pgroup, productqty, productprice FROM cart WHERE pgroup='" + newName + "';";
                else
                    groupQuery = "SELECT pgroup, productqty, productprice FROM cart WHERE pgroup='" + newName + "' AND datetime >='" + startDate + "' AND datetime <= '" + endDate + "';";

//                Log.e("query", "=" + groupQuery);
                cartDetails = db.rawQuery(groupQuery, null);

                int cartDetailscount = cartDetails.getCount();
                if (cartDetailscount > 0) {
                    flag = true;
                    cartDetails.moveToPrevious();
                    Float totalVal = 0f;

                    while (cartDetails.moveToNext()) {
                        Float itemValue = cartDetails.getInt(1) * cartDetails.getFloat(2);//total qty * price
                        totalVal += itemValue;
                    }
                    Entry pieEntry = new Entry(totalVal, i);
                    entryLabel.add(newName);
                    entries.add(pieEntry);
                    groups.add(newName);
                    pieData.addEntry(pieEntry, i);
                    i++;


                }

            }
            // undo all highlights
//            Log.e("entryLabel", "" + entryLabel.size());
//            Log.e("entries", "" + entries.size());
            if (flag) {
                PieDataSet dataSet = new PieDataSet(entries, "");

                pieData = new PieData(entryLabel, dataSet);
                pieData.setValueTextSize(12f);
                pieData.setValueTextColor(Color.WHITE);
                dataSet.setColors(COLORFUL_COLORS);
                piechart.setData(pieData);
                piechart.highlightValues(null);
                piechart.invalidate();
                ArrayAdapter<String> adapter =
                        new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.groupspinneritem, groups);
                groupSpinner.setAdapter(adapter);
            } else {
                date_flag=false;
                Toast.makeText(getActivity(), "No data found in selected date range!", Toast.LENGTH_LONG).show();
            }
        } else {
            db.close();
            Toast.makeText(getActivity(), "No Group Data In Cart", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getActivity(), Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        db.close();
    }

    public void write() {
        writeChartToPDF writeChartToPDF = new writeChartToPDF();
        writeChartToPDF.execute();
    }

    protected RectF mOnValueSelectedRectF = new RectF();

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        if (e == null)
            return;
        RectF bounds = mOnValueSelectedRectF;
        Columnchart.getBarBounds((BarEntry) e);
        PointF position = Columnchart.getPosition(e, YAxis.AxisDependency.LEFT);

        // Log.e("Bar_value", "=" + Columnchart.getEntriesAtIndex(e.getXIndex()).get(0).getVal());
        //Log.i("position", position.toString());
        //MPPointF.recycleInstance(position);
    }

    @Override
    public void onNothingSelected() {

    }

    class writeChartToPDF extends AsyncTask<String, Void, Boolean> {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        Document document = new Document(PageSize.A4);
        String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "pos" + File.separator + "reports/";
        String fileName = new SimpleDateFormat("dd-MM-yyyy_HH:mm", Locale.US).format(Calendar.getInstance().getTime());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {

                        File folder = new File(baseDir);
                        if (!folder.exists()) {
                            folder.mkdir();//If there is no folder it will be created.
//                            Log.e("folder====", "created");
                        }

                        File f = new File(baseDir + fileName + ".pdf");
                        PdfWriter.getInstance(document, new FileOutputStream(f));
                        document.open();

                        Paragraph p1 = new Paragraph("TOTAL SALES per CATEGORY in AED");
                        p1.setAlignment(Paragraph.ALIGN_CENTER);

                        Paragraph prDates = new Paragraph();
                        prDates.add("\n\nFrom Date :" + fromReportsDate);
                        prDates.add("\nTo Date :" + toReportsDate + "\n\n");
                        int tempW = Columnchart.getWidth();
                        Columnchart.getLayoutParams().width = piechart.getWidth();
                        Columnchart.setLayoutParams(Columnchart.getLayoutParams());
                        //Columnchart.setLayoutParams(new ViewGroup.LayoutParams(piechart.getWidth(),Columnchart.getHeight()));

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        Bitmap columnchartBM = Columnchart.getChartBitmap();
                        columnchartBM.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        Image myImg1 = Image.getInstance(stream.toByteArray());
                        myImg1.setAlignment(Image.MIDDLE);
                        document.add(p1);
                        if (date_flag)
                            document.add(prDates);
                        document.add(myImg1);
                        document.newPage();
                        //Columnchart.setLayoutParams(new ViewGroup.LayoutParams(tempW,tempH));
                        Columnchart.getLayoutParams().width = tempW;
                        Columnchart.setLayoutParams(Columnchart.getLayoutParams());
                        Paragraph p2 = new Paragraph("TOTAL SALES per GROUP in AED");
                        p2.setAlignment(Paragraph.ALIGN_CENTER);

                        ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                        Bitmap piechartBM = piechart.getChartBitmap();
                        piechartBM.compress(Bitmap.CompressFormat.JPEG, 100, stream1);

                        Image myImg2 = Image.getInstance(stream1.toByteArray());
                        myImg2.setAlignment(Image.MIDDLE);
                        document.add(p2);
                        if (date_flag)
                            document.add(prDates);
                        document.add(myImg2);
                        document.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            progressDialog.dismiss();
                        } catch (Exception ignore) {
                        }
                        return;
                    }
                }
            });
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            try {
                progressDialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, "");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Main Reports");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "Attachment For Report");
                File file = new File(baseDir, fileName + ".pdf");

                if (!file.exists() || !file.canRead()) {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "No Attachment Found..", Toast.LENGTH_SHORT).show();
                    return;
                }
                Uri uri = Uri.fromFile(file);
                intent.putExtra(Intent.EXTRA_STREAM, uri);
                startActivity(Intent.createChooser(intent, "Send email.."));

            } catch (Exception ignore) {
            }
            if (aBoolean) {
                Toast.makeText(getActivity(), "Report generated Successfully", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public int[] COLORFUL_COLORS = {
            Color.rgb(255, 211, 0), Color.rgb(22, 129, 197), Color.rgb(245, 199, 0),
            Color.rgb(106, 150, 31), Color.rgb(179, 100, 53)
    };

    public static SalesSummaryFragment getInstance() {
        return COLUMNCHART;
    }
}
