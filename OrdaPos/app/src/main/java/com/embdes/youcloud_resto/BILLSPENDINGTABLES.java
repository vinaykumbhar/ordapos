/**
 *
 */
package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.instabug.library.InstabugTrackingDelegate;
import com.itextpdf.text.pdf.parser.Line;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author embdes
 */
@SuppressLint("ShowToast")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class BILLSPENDINGTABLES extends Activity {
    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    public static String[] prgmNameList = {"Continue Order", "Cart Check", "Change Table", "Change Location"};
    SQLiteDatabase db;
    ListView pendingList;
    Group group;
    MyDb myDb;
    TextView date, titlebar;
    ListView pending_menuorder_list;
    Dialog pending_Bill_option;
    Dialog tableChnage_popup, locationChnage_popup;
    PopupTables popupTables;
    String grand;
    public static final int Continue_order = 0;
    public static final int Change_table = 2;

    static BILLSPENDINGTABLES bILLSPENDINGTABLES;
    ArrayList<String> locid = new ArrayList<String>();
    ArrayList<String> tableid = new ArrayList<String>();
    ArrayList<String> orderno = new ArrayList<String>();
    ArrayList<String> amount = new ArrayList<String>();
    private GridViewAdapter mLeDeviceListAdapter;
    //search/info
    TextView txt_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.pending_bill);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        date = (TextView) findViewById(R.id.date);
        titlebar = (TextView) findViewById(R.id.titelabar);
        Button menu = (Button) findViewById(R.id.Imageview);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BILLSPENDINGTABLES.this, Home.class);
                startActivity(intent);
            }
        });
        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        titlebar.setText("PENDING ORDERS");
        txt_info = (TextView) findViewById(R.id.txt_info);
        txt_info.setText("View Pending Orders or manage delivery");
        findViewById(R.id.lay_info).setVisibility(View.VISIBLE);
        findViewById(R.id.lay_search).setVisibility(View.GONE);
        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);

        group = new Group();
        myDb = new MyDb(getApplicationContext());
        myDb.open();
        popupTables = new PopupTables();
        pendingList = (ListView) findViewById(R.id.pendinglist);
        pendingList.setDivider(new ColorDrawable(Color.parseColor("#1681c4")));
        pendingList.setDividerHeight(1);
        pending_Bill_option = new Dialog(BILLSPENDINGTABLES.this);
        pending_Bill_option.setContentView(R.layout.pending_bill_option_menu_popup);
        pending_Bill_option.setCancelable(true);
        pending_menuorder_list = (ListView) pending_Bill_option.findViewById(R.id.pending_bill_option_list);
        tableChnage_popup = new Dialog(BILLSPENDINGTABLES.this);
        locationChnage_popup = new Dialog(BILLSPENDINGTABLES.this);
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
//		Cursor cart = db.rawQuery("SELECT * FROM cart ;",null);
//		int count = cart.getCount();
//		Log.d("Check", "onCreate: "+count);
//		if (count>0){
//			while (cart.moveToNext()) {
//				String table = cart.getString(cart.getColumnIndex("tableno"));
//				String location = cart.getString(cart.getColumnIndex("locationid"));
        Cursor pendindDetails = db.rawQuery("SELECT  locationid, tableno," +
                " orderno FROM ordernos WHERE billflag = 0 ;", null);
//				int co = pendindDetails.getCount();
//				Log.d("Check", "onCreate1: "+co);
//				int i = 0;
        while (pendindDetails.moveToNext()) {
            System.out.println("LOc ;" + pendindDetails.getString(0) +
                    pendindDetails.getString(1) +
                    pendindDetails.getString(2));

            locid.add(pendindDetails.getString(0));
            tableid.add(pendindDetails.getString(1));
            orderno.add(pendindDetails.getString(2));
        }
        db.close();
        mLeDeviceListAdapter = new GridViewAdapter(this);
        pendingList.setAdapter(mLeDeviceListAdapter);
        mLeDeviceListAdapter.notifyDataSetChanged();
    }

    protected void onResume() {
        super.onResume();
        if (MainActivity.getInstance() == null || user_Login.user_LoginID == null) {
            Intent userLoginIntent = new Intent();
            userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
            userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(userLoginIntent);
            this.finish();
        }

    }

    ;

    public class GridViewAdapter extends BaseAdapter {

        // Declare variables
        private Activity activity;

        @SuppressWarnings("unused")
        private LayoutInflater inflater = null;

        public GridViewAdapter(Activity a) {
            activity = a;

            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            return locid.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            /**
             * getting the thumbnail for image and putting in image view
             *
             */
            View vi = convertView;
            ViewHolder viewh = null;
            System.out.println("outside if" + position);
            if (convertView == null) {
                // if it's not recycled, initialize some attributes
                LayoutInflater inflater = (LayoutInflater) BILLSPENDINGTABLES.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                vi = inflater.inflate(R.layout.pendinglistforbill, parent, false);
                viewh = new ViewHolder();
//                System.out.println("View is null" + position);
//			        viewh.Loc_id = (TextView) vi.findViewById(R.id.loc_id);
                viewh.Table_no = (TextView) vi.findViewById(R.id.table_no);
                viewh.Order_no = (TextView) vi.findViewById(R.id.order_no);

                viewh.printer = (LinearLayout) vi.findViewById(R.id.printer);
                viewh.payment = (LinearLayout) vi.findViewById(R.id.paybill);

                viewh.print = (ImageView) vi.findViewById(R.id.print);
                viewh.pay = (ImageView) vi.findViewById(R.id.pay);

                // viewh.amount = (TextView) vi.findViewById(R.id.amount);
                viewh.printer.setOnClickListener(printerConnect);
                viewh.print.setOnClickListener(printerConnect);
                viewh.payment.setOnClickListener(paymentConnect);
                viewh.pay.setOnClickListener(paymentConnect);
                //viewh.Order_no .setOnClickListener(popupWindow);
                viewh.Order_no.setOnLongClickListener(longPop);
                vi.setTag(viewh);
            } else {
                System.out.println("else" + position);
                viewh = (ViewHolder) vi.getTag();
            }
            //viewh.amount.setText();
//			viewh.Loc_id.setText(locid.get(position));
            viewh.Table_no.setText(tableid.get(position));
            viewh.Order_no.setText(orderno.get(position));
            return vi;

        }
    }

    private android.view.View.OnClickListener popupWindow = new android.view.View.OnClickListener() {

        @Override
        public void onClick(View v) {
            final int position = pendingList.getPositionForView(v);
            if (user_Login.user_LoginID.LOCATIONNO.equalsIgnoreCase(locid.get(position))) {
                if (tableid.get(position).length() > 0) {
                    System.out.println("New Table :-" + tableid.get(position).toString());
                    pending_Bill_option.setTitle("Location:" + locid.get(position) + ", Table NO:" + tableid.get(position));
                    pending_Bill_option.show();
                    pending_menuorder_list.setOnItemClickListener(new OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1,
                                                int arg2, long arg3) {
                            TablesFragment.TABLESID = TablesFragment.getInstance();
                            TablesFragment.COVERS = "1";
                            if (arg2 == Continue_order) {
                                if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.TOrder) {
                                    if (tableid.get(position).length() != 0 &&
                                            Integer.parseInt(tableid.get(position).toString()) > 0) {
                                        Group.GROUPID.table = tableid.get(position).toString();
                                        CartStaticSragment.CARTStaticSragment.tableno.setText(Group.GROUPID.table);
                                        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                                        Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" +
                                                Group.GROUPID.table + " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
                                        while (chairs.moveToNext()) {
                                            if (chairs.getString(0).length() != 0) {
                                                TablesFragment.COVERS = chairs.getString(0).toString();
                                            } else {
                                                TablesFragment.COVERS = "111";
                                            }
                                        }
                                        int chairsCount = chairs.getCount();
                                        chairs.moveToFirst();
                                        if (chairsCount > 0) {
                                            if (chairs.getInt(chairs.getColumnIndex("occupiedchairs")) < 1) {
                                                if (chairs.getString(0).length() != 0 &&
                                                        Integer.parseInt(chairs.getString(0).toString()) > 0) {
                                                    Cursor c = db.rawQuery("SELECT noofchairs FROM tables WHERE tableno =" + Group.GROUPID.table +
                                                            " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
                                                    int count = c.getCount();
                                                    c.moveToFirst();
                                                    if (count > 0) {
                                                        int covers = c.getInt(c.getColumnIndex("noofchairs"));
                                                        if (covers >= Integer.parseInt(TablesFragment.COVERS)) {
                                                            Intent intent = new Intent(BILLSPENDINGTABLES.this, Group.class);
                                                            startActivity(intent);
                                                            group.groupfragment();
                                                            group.alert();
                                                        } else {
                                                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                                                    BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                                                            msg.setTitle("");
                                                            msg.setMessage("Covers Should Not Exceed " + covers);
                                                            msg.setPositiveButton("Ok", null);
                                                            msg.show();
                                                        }

                                                    } else {
                                                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                                                BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                                                        msg.setTitle("");
                                                        msg.setMessage("Table is Not Available");
                                                        msg.setPositiveButton("Ok", null);
                                                        msg.show();
                                                    }
                                                }
                                            } else {
                                                Intent intent = new Intent(BILLSPENDINGTABLES.this, Group.class);
                                                startActivity(intent);
                                            }
                                        } else {
                                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                                    BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                                            msg.setTitle("");
                                            msg.setMessage("Table Is Not Available");
                                            msg.setPositiveButton("Ok", null);
                                            msg.show();

                                        }
                                        db.close();
                                    } else {
                                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                                BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                                        msg.setTitle("");
                                        msg.setMessage("Table Number Is Not Valid");
                                        msg.setPositiveButton("Ok", null);
                                        msg.show();
                                    }

                                } else {
                                    AlertDialog.Builder msg = new AlertDialog.Builder(
                                            BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                                    msg.setTitle("Opration Failed");
                                    msg.setMessage("User Doesn't have the permission for this opration");
                                    msg.setPositiveButton("Ok", null);
                                    msg.show();
                                }
                            } else if (arg2 == Change_table) {
                                PopupTables.popupTables.flag = true;
                                Group.GROUPID.table = tableid.get(position).toString();
                                popupTables.show(getFragmentManager(), null);
                                CartStaticSragment.CARTStaticSragment.listView.setAdapter(null);
                                CartStaticSragment.CARTStaticSragment.totalamountvalue.setText("");
                                CartStaticSragment.CARTStaticSragment.totaltax.setText("");
                                CartStaticSragment.CARTStaticSragment.itemno.setText("");
                            }
                            if (pending_Bill_option.isShowing()) {
                                pending_Bill_option.dismiss();
                            }

                        }
                    });
                }
            } else {
                AlertDialog.Builder msg = new AlertDialog.Builder(
                        BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                msg.setTitle("");
                msg.setMessage("Location Does not match to Print " + "\n Current Loc:" + user_Login.user_LoginID.LOCATIONNO + "\n Table Loc:" + locid.get(position));
                msg.setPositiveButton("Ok", null);
                msg.show();
            }

        }
    };
    private android.view.View.OnLongClickListener longPop = new android.view.View.OnLongClickListener() {

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public boolean onLongClick(View v) {
            final int position = pendingList.getPositionForView(v);
            if (user_Login.user_LoginID.LOCATIONNO.equalsIgnoreCase(locid.get(position))) {
                if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.MTUpdate) {

                    Group.GROUPID.table = tableid.get(position).toString();
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                    msg.setTitle("Do you want to close the table ?");
                    msg.setMessage("It will delete all data belongs to table" + "\n Table :" + tableid.get(position) + "\n Location :" + locid.get(position));

                    msg.setPositiveButton("Yes", new
                            DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    myDb = new MyDb(getApplicationContext());
                                    myDb.open();
                                    myDb.Insert(String.valueOf(MainActivity.MAINID.outletid), Group.GROUPID.table, "0", "NULL", "NULL");
                                    Intent intent = new Intent(BILLSPENDINGTABLES.this, ServiceOccupiedChairs.class);
                                    startService(intent);
                                    db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                                    db.execSQL("UPDATE tables SET occupiedchairs = 0 WHERE tableno = " + Group.GROUPID.table +
                                            " AND locationid = " + user_Login.user_LoginID.LOCATIONNO + ";");

                                    db.execSQL("DELETE FROM ordernos WHERE tableno = " + Group.GROUPID.table +
                                            " AND locationid = " +/*user_Login.user_LoginID.LOCATIONNO*/user_Login.user_LoginID.LOCATIONNO + " AND billflag = 0;");
                                    db.execSQL("DELETE FROM cart WHERE tableno = " + Group.GROUPID.table +
                                            " AND locationid = " +/*user_Login.user_LoginID.LOCATIONNO*/user_Login.user_LoginID.LOCATIONNO + " AND billflag = 0;");

                                    db.close();
                                    BILLSPENDINGTABLES.this.finish();
                                    Intent refresh = new Intent(BILLSPENDINGTABLES.this, BILLSPENDINGTABLES.class);
                                    startActivity(refresh);
                                }
                            });
                    msg.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    myDb.close();
                    msg.show();
                } else {
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                    msg.setTitle("Opration Failed");
                    msg.setMessage("User Doesn't have the permission for this opration");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                }
            } else {
                AlertDialog.Builder msg = new AlertDialog.Builder(
                        BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                msg.setTitle("");
                msg.setMessage("Location Does not match for opration " + "\n Current Loc:" + user_Login.user_LoginID.LOCATIONNO + "\n Table Loc:" + locid.get(position));
                msg.setPositiveButton("Ok", null);
                msg.show();
            }
            return false;
        }


    };

    private String editTable;
    float grandTotal = 0;
    private android.view.View.OnClickListener paymentConnect = new android.view.View.OnClickListener() {

        @SuppressLint("ShowToast")
        @Override
        public void onClick(View v) {
            final int position = pendingList.getPositionForView(v);
            if (user_Login.user_LoginID.LOCATIONNO.equalsIgnoreCase(locid.get(position))) {
                if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.PBill) {
                    if (tableid.get(position).length() > 0) {

                        addRowsToDisplay pay = new addRowsToDisplay();
                        pay.execute(tableid.get(position).toString());




                        /*
                        Group.GROUPID.table = tableid.get(position).toString();
                        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                        Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid = " +
                                user_Login.user_LoginID.LOCATIONNO + " AND tableno = " + tableid.get(position) +
                                " AND billflag = 0 AND orderflag = 1;", null);
                        int count = cartData.getCount();
                        db.close();
                        if (count > 0) {
                            Intent intent = new Intent(BILLSPENDINGTABLES.this, Printer.class);
                            intent.putExtra("TABLENO", Integer.parseInt(tableid.get(position).toString()));
                            intent.putExtra("LOCATIONNO", Integer.parseInt(user_Login.user_LoginID.LOCATIONNO));
                            intent.putExtra("FLAG", 0);
                            startActivity(intent);
                        } else {
                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                    BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                            msg.setTitle("");
                            msg.setMessage("Table Is Empty To Print ");
                            msg.setPositiveButton("Ok", null);
                            msg.show();
                        }*/

//                        Group.GROUPID.table = tableid.get(position).toString();
//                        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
//                        Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid = " +
//                                user_Login.user_LoginID.LOCATIONNO + " AND tableno = " + tableid.get(position) +
//                                " AND billflag = 0 AND orderflag = 1;", null);
//                        int count = cartData.getCount();
//                        db.close();
//                        if (count > 0) {
//                            Intent intent = new Intent(BILLSPENDINGTABLES.this, Payment.class);
//                            intent.putExtra("TABLE", tableid.get(position).toString());
//                            startActivity(intent);
//                        } else {
//                            AlertDialog.Builder msg = new AlertDialog.Builder(
//                                    BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
//                            msg.setTitle("");
//                            msg.setMessage("Table Is Empty To Pay ");
//                            msg.setPositiveButton("Ok", null);
//                            msg.show();
//                        }
                    }
                } else {
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                    msg.setTitle("Opration Failed");
                    msg.setMessage("User Doesn't have the permission for this opration");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                }
            } else {
                AlertDialog.Builder msg = new AlertDialog.Builder(
                        BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                msg.setTitle("");
                msg.setMessage("Location Does not match to Print " + "\n Current Loc:" + user_Login.user_LoginID.LOCATIONNO + "\n Table Loc:" + locid.get(position));
                msg.setPositiveButton("Ok", null);
                msg.show();
            }
        }
    };

    private android.view.View.OnClickListener printerConnect = new android.view.View.OnClickListener() {

        @Override
        public void onClick(View v) {
            final int position = pendingList.getPositionForView(v);
            if (user_Login.user_LoginID.LOCATIONNO.equalsIgnoreCase(locid.get(position))) {
                if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.BPrint) {
                    if (tableid.get(position).length() > 0) {
                        Group.GROUPID.table = tableid.get(position).toString();
                        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                        String query = "SELECT * from cart WHERE locationid = " +
                                user_Login.user_LoginID.LOCATIONNO + " AND tableno = " + tableid.get(position) +";";
//                                " AND billflag = 0 AND orderflag = 0;";
                        Log.e("query", "=" + query);
                        Cursor cartData = db.rawQuery(query, null);
                        int count = cartData.getCount();
                        db.close();
                        if (count > 0) {
                            Intent intent = new Intent(BILLSPENDINGTABLES.this, Printer.class);
                            intent.putExtra("TABLENO", Integer.parseInt(tableid.get(position).toString()));
                            intent.putExtra("LOCATIONNO", Integer.parseInt(user_Login.user_LoginID.LOCATIONNO));
                            intent.putExtra("FLAG", 0);
                            startActivity(intent);
                        } else {
                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                    BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                            msg.setTitle("");
                            msg.setMessage("Table Is Empty To Print ");
                            msg.setPositiveButton("Ok", null);
                            msg.show();
                        }
                    }
                } else {
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                    msg.setTitle("Opration Failed ");
                    msg.setMessage("User Doesn't have the permission for this opration");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                }
            } else {
                AlertDialog.Builder msg = new AlertDialog.Builder(
                        BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                msg.setTitle("");
                msg.setMessage("Location Does not match to Print " + "\n Current Loc:" + user_Login.user_LoginID.LOCATIONNO + "\n Table Loc:" + locid.get(position));
                msg.setPositiveButton("Ok", null);
                msg.show();
            }

        }
    };

    private class addRowsToDisplay extends AsyncTask<String, Void, Boolean> {
        String editLocation = user_Login.user_LoginID.LOCATIONNO;


        @Override
        protected Boolean doInBackground(String... params) {
            editTable = params[0];
            Log.e("editTable", "=" + editTable);
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("UPDATE cart SET  billqty = orderqty-billedqty, billtypeqty = ordertypeqty-billedtypeqty WHERE tableno = " +
                    editTable + " AND locationid = " +
                    user_Login.user_LoginID.LOCATIONNO + ";");

            Cursor c = db.rawQuery("SELECT * from cart WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO + " AND tableno=" + editTable + " AND checkflag = 0 " +
                    " AND billflag = 0;", null);
            int count = c.getCount();
            c.moveToFirst();
            grandTotal = 0;
            for (Integer j = 0; j < count; j++) {
                int billtypeqty = c.getInt(c.getColumnIndex("ordertypeqty")) - c.getInt(c.getColumnIndex("billedtypeqty"));
                float total = (Float.parseFloat(c.getString(c.getColumnIndex("productprice"))) * Integer.parseInt(c.getString(c.getColumnIndex("billqty"))) +
                        Float.parseFloat(c.getString(c.getColumnIndex("typeprice"))) * billtypeqty);
                grandTotal = grandTotal + total;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                String cQuery = "SELECT productname,billflag,orderno,billtypeqty,checkflag,tbillflag,billtypeqty,billqty FROM cart WHERE tableno = " + editTable + " AND locationid= " + editLocation + ";";
                Cursor cart = db.rawQuery(cQuery, null);
                while (cart.moveToNext()) {
                    String data = cart.getString(0) + " , "//productname
                            + cart.getString(1)+" , "//billflag
                            + cart.getString(2)+" , "//orderno
                            + cart.getString(3)+" , "//billtypeqty
                            + cart.getString(4)+" , "//checkflag
                            + cart.getString(5)+" , "//tbillflag
                            + cart.getString(6)+" , "//billtypeqty
                            + cart.getString(7);//billqty
                    Log.e("data","="+data);
                }

                String query = "SELECT productname,productprice,productqty,billqty,discount,billedqty,typename,typeqty,typeprice," +
                        "orderno,billtypeqty FROM cart WHERE tableno = " + editTable + " AND locationid= " + editLocation + " AND billflag= 0 AND checkflag=0 AND " +
                        "tbillflag=0 AND (billtypeqty != 0 OR billqty != 0) ;";
                Log.e("query", "=" + query);
                Cursor productDetails = db.rawQuery(query, null);
                int count = productDetails.getCount();
                db.close();
                if (count > 0) {
                    Group.getInstance().totalAmount = grandTotal;
                    Group.getInstance().subTotalAmount = grandTotal;
                    cashpay();
                } else {
//                    AlertDialog.Builder adb = new AlertDialog.Builder(
//                            BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
//                    adb.setTitle("");
//                    adb.setMessage("Products Not Found To Pay");
//                    adb.setCancelable(false);
//                    adb.setPositiveButton("Ok", new
//                            DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
                    db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                    db.execSQL("UPDATE cart SET checkflag = 0 WHERE locationid=" + editLocation + " AND tableno=" +
                            editTable + " AND checkflag = 1 AND billflag = 0;");
                    db.close();
                    Intent intent = new Intent(BILLSPENDINGTABLES.this, Home.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
//                                }
//                            });
//                    adb.show();
                }
            }
        }
    }

    static class ViewHolder {
        // TextView Loc_id;
        TextView Table_no;
        TextView Order_no;
        LinearLayout printer;
        LinearLayout payment;
        ImageView print;
        ImageView pay;
        //TextView amount;
    }

    public void cashpay() {
        float cash = 0;
        {
            if (Group.GROUPID.finaltotal == null) {
                pay();
                cash = Float.parseFloat(Group.GROUPID.finaltotal);
            } else {
                pay();
                cash = Float.parseFloat(Group.GROUPID.finaltotal);
            }
            if (cash > 0) {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                String datetime = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());
                Cursor recieptno = db.rawQuery("SELECT * FROM receiptnos WHERE receipthead = '" + datetime + "' ;", null);
                if (recieptno.getCount() == 0) {
                    if (cash > 0) {
                        db.execSQL("INSERT INTO receiptnos VALUES('" + datetime + "',1);");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('R" + datetime + "1','CASH', '0'," + cash + " ,'" + timeStamp + "','" + MainActivity.getInstance().USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");
                    }

                    if (!Group.getInstance().voucherNumber.equals("0")) {
                        db.execSQL("UPDATE voucher SET usedflag = 1 WHERE voucherid = " + Group.getInstance().voucherNumber + ";");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('" + Group.getInstance().voucherNumber + "','VOUCHER', '" + Group.getInstance().voucherNumber + "'," +
                                Group.getInstance().voucherAmount + " ,'" + timeStamp + "','" + MainActivity.getInstance().USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");
                    }
                } else {
                    if (cash > 0) {
                        recieptno.moveToLast();
                        String newReceiptNo = "R" + recieptno.getString(0) + (recieptno.getInt(1) + (int) 1);
                        db.execSQL("UPDATE receiptnos SET receiptnumber = receiptnumber+1 WHERE receipthead= '" +
                                recieptno.getString(0) + "';");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('" + newReceiptNo + "','CASH', '0'," + cash + " ,'" + timeStamp + "','" + MainActivity.getInstance().USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");

                    }
                    if (!Group.getInstance().voucherNumber.equals("0")) {
                        db.execSQL("UPDATE voucher SET usedflag = 1 WHERE voucherid = " + Group.getInstance().voucherNumber + ";");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('" + Group.getInstance().voucherNumber + "','VOUCHER', '" + Group.getInstance().voucherNumber + "'," +
                                Group.getInstance().voucherAmount + " ,'" + timeStamp + "','" + MainActivity.getInstance().USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");
                    }
                }

                db.execSQL("UPDATE cart SET discount = " + Group.getInstance().discountAmount +
                        " WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO + " AND tableno=" + editTable + " AND billflag = 0 AND orderflag = 1;");
                db.close();
                Intent intent = new Intent(BILLSPENDINGTABLES.this, Printer.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("TABLENO", Integer.parseInt(editTable));
                intent.putExtra("LOCATIONNO", Integer.parseInt(user_Login.user_LoginID.LOCATIONNO));
                intent.putExtra("FLAG", 1);
                startActivity(intent);
            }
        }
    }

    public void pay() {
        double grandTotal = 0;
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO + " AND tableno=" + Group.GROUPID.table +
                " AND billflag = 0;", null);
        int count = cartData.getCount();
        if (count > 0) {
            cartData.moveToLast();
            do {
                String productqty = cartData.getString(cartData.getColumnIndex("productqty"));
                String productprice = cartData.getString(cartData.getColumnIndex("productprice"));
                String typeprice = cartData.getString(cartData.getColumnIndex("typeprice"));
                String typeqty = cartData.getString(cartData.getColumnIndex("typeqty"));
                Double totalvalue = Double.valueOf((Float.parseFloat(productprice) *
                        Integer.parseInt(productqty))) + Double.valueOf(typeprice) * Double.valueOf(typeqty);
                grandTotal = grandTotal + totalvalue;
            } while (cartData.moveToPrevious());
        }
        db.close();
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor taxDetails = db.rawQuery("SELECT servicetaxper,taxper1,taxper2,taxper3 FROM tax;", null);
        int taxDetailsCount = taxDetails.getCount();
        double tax1 = 0;
        if (taxDetailsCount > 0) {
            taxDetails.moveToFirst();            //Here we are inserting the tax information into the structure
            tax1 = taxDetails.getFloat(taxDetails.getColumnIndex("servicetaxper"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper1"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper2"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper3"));

        } else {
        }
        db.close();
        grand = String.format("%.2f", grandTotal);
        double taxAmount = (tax1 * grandTotal) / 100;
        String grand_tax = String.format("%.2f", taxAmount);
        grandTotal += (tax1 * grandTotal) / 100;
        String grand_Amount = String.format("%.2f", grandTotal);
        Group.GROUPID.finaltotal = grand_Amount;
    }

    public static BILLSPENDINGTABLES getInstance() {
        return bILLSPENDINGTABLES;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
