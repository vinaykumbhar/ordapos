/**
 *
 */
package com.embdes.youcloud_resto;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.instabug.library.InstabugTrackingDelegate;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Vinay
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ManagerReports extends Activity {
    SQLiteDatabase db;
    public float discountAmount;
    public float voucherAmount = 0;
    int occupiedchairs;
    GridView listView;
    TextView date, titlebar;
    int MAINREPORTS = 0;
    int FLASHSALE = 1;
    int FLASHPRODUCT = 2;
    int Update = 1;
    int Reset_Terminal = 2;
    GridViewAdapter adapter;
    String[] Menu = {"MAIN REPORTS",
            /*"FLASH SALE REPORTS",
            "FLASH PRODUCT REPORTS",*/
            "UPDATE",
            "RESET TERMINAL"};
    //search/info
    TextView txt_info;

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.manager_reports);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        date = (TextView) findViewById(R.id.date);
        titlebar = (TextView) findViewById(R.id.titelabar);
        Button menu = (Button) findViewById(R.id.Imageview);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManagerReports.this, Home.class);
                startActivity(intent);
            }
        });

        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        titlebar.setText("Manager Reports");
        txt_info = (TextView) findViewById(R.id.txt_info);
        txt_info.setText("Select desired options");
        findViewById(R.id.lay_info).setVisibility(View.VISIBLE);
        findViewById(R.id.lay_search).setVisibility(View.GONE);
        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);


        listView = (GridView) findViewById(R.id.gridviewreports);
      //  listView.setDividerHeight(0);
        adapter = new GridViewAdapter(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int arg2, long l) {
                if (arg2 == MAINREPORTS) {
                    if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.BPrint) {
                        Intent mainreport = new Intent(ManagerReports.this, MainReportActivity.class);//MainReportActivity
                        startActivity(mainreport);
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                ManagerReports.this, android.R.style.Theme_Dialog);
                        msg.setTitle("Opration Failed");
                        msg.setMessage("User Doesn't have the permission for this opration");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }
                }
               /* else if (arg2 == FLASHSALE) {
                    Intent mainreport = new Intent(ManagerReports.this, MainreportViewFSR.class);//Flash sale report
                    startActivity(mainreport);
                } else if (arg2 == FLASHPRODUCT) {
                    Intent intent = new Intent(ManagerReports.this, PRODUCTREPORTS.class);//Flash Product report
                    startActivity(intent);
                } */
                else if (arg2 == Update) {
                    if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.MTUpdate) {
                        AlertDialog.Builder adb = new AlertDialog.Builder(
                                ManagerReports.this, android.R.style.Theme_Dialog);
                        adb.setTitle("");
                        adb.setMessage("Do You Want To Update");
                        adb.setPositiveButton("Yes", new
                                DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Group.GROUPID.logout();
                                        finish();
                                        MainActivity.getInstance().updateflag = 1;
                                        Intent mIntent = new Intent();
                                        mIntent.setClass(getApplicationContext(), UpdateData.class);
                                        startActivity(mIntent);
                                    }
                                });
                        adb.setNegativeButton("No", new
                                DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                        adb.show();
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                ManagerReports.this, android.R.style.Theme_Dialog);
                        msg.setTitle("Opration Failed");
                        msg.setMessage("User Doesn't have the permission for this opration");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }

                } else if (arg2 == Reset_Terminal) {
                    if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.MTUpdate) {
                        final AlertDialog.Builder mesg = new AlertDialog.Builder(ManagerReports.this, android.R.style.Theme_Dialog);
                        mesg.setMessage("	It Will Delete All Old Data \n	 Do you want to Continue ?");
                        mesg.setNegativeButton("No", null);
                        mesg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                TablesFragment.TABLESID.stop();
                                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                                db.execSQL("DROP TABLE IF EXISTS product ;");
                                db.execSQL("DROP TABLE IF EXISTS userterminal;");
                                db.execSQL("DROP TABLE IF EXISTS loginrecord;");
                                db.execSQL("Drop Table IF EXISTS location;");
                                db.execSQL("DROP TABLE IF EXISTS masterterminal;");
                                db.execSQL("Drop Table IF EXISTS grouptable;");
                                db.execSQL("Drop Table IF EXISTS category;");
                                db.execSQL("DROP TABLE IF EXISTS type;");
                                db.execSQL("DROP TABLE IF EXISTS tables;");
                                db.execSQL("DROP TABLE IF EXISTS receipt;");
                                db.execSQL("DROP TABLE IF EXISTS ordernos;");
                                db.execSQL("DROP TABLE IF EXISTS cart;");
                                db.execSQL("DROP TABLE IF EXISTS locationprinter;");
                                db.execSQL("DROP TABLE IF EXISTS locationprice;");
                                db.execSQL("DROP TABLE IF EXISTS queuetable;");
                                db.close();
                                finish();
                                Intent registration = new Intent();
                                registration.setClass(ManagerReports.this, MainActivity.class);
                                registration.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(registration);
                            }
                        });
                        mesg.show();
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                ManagerReports.this, android.R.style.Theme_Dialog);
                        msg.setTitle("Opration Failed");
                        msg.setMessage("User Doesn't have the permission for this opration");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }
                }
            }
        });
    }

    public class GridViewAdapter extends BaseAdapter {

        // Declare variables
        private Activity activity;

        private LayoutInflater inflater = null;

        public GridViewAdapter(Activity a) {
            activity = a;

            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            System.out.println("getCount " + Menu.length);
            return Menu.length;
        }

        public Object getItem(int position) {
            System.out.println("Object getItem " + position);
            return position;
        }

        public long getItemId(int position) {
            System.out.println("getItemId " + position);
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            System.out.println("View " + position);
            View vi = convertView;
            if (convertView == null)
                System.out.println("IF " + position);
            vi = inflater.inflate(R.layout.gridview_item, null);
            TextView text = (TextView) vi.findViewById(R.id.text);
            text.setText(Menu[position]);

            return vi;
        }
    }

    protected void onResume() {
        super.onResume();
        if (MainActivity.getInstance() == null || user_Login.user_LoginID == null) {
            Intent userLoginIntent = new Intent(); //Created new Intent to
            userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
            userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(userLoginIntent);
            this.finish();
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
