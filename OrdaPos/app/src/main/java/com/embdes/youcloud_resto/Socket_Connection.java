package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.instabug.library.InstabugTrackingDelegate;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Socket_Connection extends Activity {
    private static Socket_Connection Socket_ConnectionId = null;
    String fileRequestId;
    public static SQLiteDatabase db;
    ProgressDialog dialog;
    String[] splitedReceiveData;
    static String imagepathdata;
    int i;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Socket_ConnectionId = this;
        setContentView(R.layout.progress);
        if (MainActivity.getInstance().queueMessage.equals("1")) {
            new queue().execute("OK");
        } else {
            connectionToServer();
        }
    }

    protected void onResume() {
        super.onResume();
        if (MainActivity.getInstance() == null || user_Login.user_LoginID == null) {
            Intent userLoginIntent = new Intent(); //Created new Intent to
            userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
            userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(userLoginIntent);
            this.finish();
        }
    }


    @SuppressLint("NewApi")
    public int connectionToServer() {
        PrintStream os = null;
        BufferedReader in;

        try {
            Log.e("Message", "=" + MainActivity.getInstance().message);
            if (MainActivity.getInstance().queueMessage.split("\\#").length > 5) {
                MainActivity.getInstance().finnalMesgToSend = MainActivity.getInstance().queueMessage;
            } else {
                MainActivity.getInstance().finnalMesgToSend = "" + MainActivity.getInstance().outletid + "#" + MainActivity.getInstance().messageid + "#" +
                        MainActivity.getInstance().checkid + "#" + MainActivity.getInstance().messagecontentarray + "#" +
                        MainActivity.getInstance().noofpackets + "#" + MainActivity.getInstance().packetno + "#" +
                        MainActivity.getInstance().currentmessagelength + "#" +
                        MainActivity.getInstance().totalmessagelength + "#" + MainActivity.getInstance().message + "#%";
            }

            Socket clientSocket = new Socket();
            clientSocket.connect(new InetSocketAddress(MainActivity.getInstance().ipAddress, MainActivity.getInstance().portNumber), 1000);
            os = new PrintStream(clientSocket.getOutputStream());
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String responseLine = null;
            Log.e("ipAddress", "=" + MainActivity.getInstance().ipAddress);
            Log.e("portNumber", "=" + MainActivity.getInstance().portNumber);
            if (clientSocket != null && os != null && in != null) {
                os.println("" + MainActivity.getInstance().finnalMesgToSend);

                Log.e("finnalMesgToSend", "=" + MainActivity.getInstance().finnalMesgToSend);
                boolean isDataPending = true;
                String tempreceivedMessage = null;
                while (isDataPending) {
                    responseLine = in.readLine();
                    if (responseLine != null) {
                        //call the method messageReceived from ServerBoard class
                        String[] splitedReceiveData = responseLine.split("\\#");
                        Log.e("responseLine", "=" + responseLine);// 16#M02#2#1111111111111111111111111#6#1#50#275#16|Egg Dhaba|Egg Dhaba|1234|52.202.56.230|4443|353#%
                        if (tempreceivedMessage != null) {
                            tempreceivedMessage = tempreceivedMessage + splitedReceiveData[8];
                        } else {
                            tempreceivedMessage = splitedReceiveData[8];
                        }
                        MainActivity.getInstance().receivedMessage = tempreceivedMessage;
                        Log.e("tempreceivedMessage", "=" + tempreceivedMessage);
                        if (splitedReceiveData[4].equals(splitedReceiveData[5])) {
                            isDataPending = false;
                        }
                    }

                }

            }
            clientSocket.close();

            if (MainActivity.getInstance().queueMessage.split("\\#").length < 5) {
                splitedReceiveData = responseLine.split("\\#");
                if (splitedReceiveData[1].equals("M02")) {
                    if (splitedReceiveData[8].equals("No Data")) {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                Socket_Connection.this, android.R.style.Theme_Dialog);
                        msg.setMessage("	Terminal Id Not Found");
                        msg.setCancelable(false);
                        msg.setPositiveButton("OK", new OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                Intent registration = new Intent();
                                registration.setClass(Socket_Connection.this, MainActivity.class);
                                registration.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(registration);

                            }
                        });
                        msg.show();
                        return 0;

                    } else {
                        MainActivity.getInstance().insertMasterTerminalData();
                    }
                }

                if (splitedReceiveData[1].equals("M08") || splitedReceiveData[1].equals("M10")
                        || splitedReceiveData[1].equals("M12") || splitedReceiveData[1].equals("M14")
                        || splitedReceiveData[1].equals("M16") || splitedReceiveData[1].equals("M18")
                        || splitedReceiveData[1].equals("M20") || splitedReceiveData[1].equals("M22")
                        || splitedReceiveData[1].equals("M24") || splitedReceiveData[1].equals("M26")
                        || splitedReceiveData[1].equals("M50") || splitedReceiveData[1].equals("M54")
                        || splitedReceiveData[1].equals("M58")|| splitedReceiveData[1].equals("M64")) {

                    if (splitedReceiveData[8].equals("No Data")) {
                        if (splitedReceiveData[1].equals("M08")) {
                            MainActivity.getInstance().messageid = "M29";
                        } else if (splitedReceiveData[1].equals("M10")) {
                            MainActivity.getInstance().messageid = "M31";
                        } else if (splitedReceiveData[1].equals("M12")) {
                            MainActivity.getInstance().messageid = "M33";
                        } else if (splitedReceiveData[1].equals("M14")) {
                            MainActivity.getInstance().messageid = "M35";
                        } else if (splitedReceiveData[1].equals("M16")) {
                            MainActivity.getInstance().messageid = "M37";
                        } else if (splitedReceiveData[1].equals("M18")) {
                            MainActivity.getInstance().messageid = "M39";
                        }
                        if (splitedReceiveData[1].equals("M20")) {
                            MainActivity.getInstance().messageid = "M41";
                        } else if (splitedReceiveData[1].equals("M22")) {
                            MainActivity.getInstance().messageid = "M43";
                        } else if (splitedReceiveData[1].equals("M24")) {
                            MainActivity.getInstance().messageid = "M45";
                        } else if (splitedReceiveData[1].equals("M26")) {
                            MainActivity.getInstance().messageid = "M47";
                        } else if (splitedReceiveData[1].equals("M50")) {
                            MainActivity.getInstance().messageid = "M51";
                        } else if (splitedReceiveData[1].equals("M58")) {
                            MainActivity.getInstance().messageid = "M59";
                        }else if (splitedReceiveData[1].equals("M64")) {
                            MainActivity.getInstance().messageid = "M65";
                        }
                        Toast.makeText(this, "Error Occured in File Download", Toast.LENGTH_SHORT)
                                .show();

                        MainActivity.getInstance().checkid = 1;
                        MainActivity.getInstance().messagecontentarray = "1";
                        MainActivity.getInstance().noofpackets = 1;
                        MainActivity.getInstance().packetno = 1;
                        MainActivity.getInstance().message = "ERROR";
                        MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                        MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                        connectionToServer();
                        return 0;
                    } else {
                        fileRequestId = "" + splitedReceiveData[1];
                        getCsvFilePath();
                    }
                } else if (splitedReceiveData.length == 11) {
                    replayForRequest(splitedReceiveData[9]);
                } else if (!splitedReceiveData[1].equals("M04")
                        && !splitedReceiveData[1].equals("M06") && !splitedReceiveData[1].equals("M28")
                        && !splitedReceiveData[1].equals("M62")) {
                    MainActivity.getInstance().userloginflag = 1;
                } else if (splitedReceiveData[1].equals("M28")) {
                    MainActivity.getInstance().homeloginflag = 1;
                }
            } else {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                db.execSQL("DELETE FROM queuetable WHERE message = '" + MainActivity.getInstance().queueMessage + "';");
                db.close();
                MainActivity.getInstance().queueMessage = "0";

            }

        } catch (UnknownHostException e) {
            MainActivity.getInstance().unknownHost = true;
            MainActivity.getInstance().errormessage = e.getMessage();
            return 1;

        } catch (RuntimeException e) {
            MainActivity.getInstance().runtime = true;
            MainActivity.getInstance().errormessage = e.getMessage();
            return 1;
        } catch (IOException e) {
            MainActivity.getInstance().inputOutput = true;
            MainActivity.getInstance().errormessage = e.getMessage();
            return 1;
        }
        return 0;
    }

    public void replayForRequest(String requestMessage) {
        if (MainActivity.getInstance().USERNAME.length() == 0) {
            MainActivity.getInstance().outletid = MainActivity.getInstance().outletid;
            MainActivity.getInstance().messageid = requestMessage;
            MainActivity.getInstance().checkid = 1;
            MainActivity.getInstance().messagecontentarray = "11";
            MainActivity.getInstance().noofpackets = 1;
            MainActivity.getInstance().packetno = 1;
            MainActivity.getInstance().message = "" + requestMessage + "|" + "dummy";
            MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
            MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
            MainActivity.getInstance().ipAddress = MainActivity.getInstance().ipAddress;
            MainActivity.getInstance().portNumber = MainActivity.getInstance().portNumber;
            connectionToServer();
        } else {
            MainActivity.getInstance().outletid = MainActivity.getInstance().outletid;
            MainActivity.getInstance().messageid = requestMessage;
            MainActivity.getInstance().checkid = 1;
            MainActivity.getInstance().messagecontentarray = "11";
            MainActivity.getInstance().noofpackets = 1;
            MainActivity.getInstance().packetno = 1;
            MainActivity.getInstance().message = "" + requestMessage + "|" + MainActivity.getInstance().USERNAME;
            MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
            MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
            MainActivity.getInstance().ipAddress = MainActivity.getInstance().ipAddress;
            MainActivity.getInstance().portNumber = MainActivity.getInstance().portNumber;
            connectionToServer();
        }
    }

    public void getCsvFilePath() {

        String[] splitedMessage = MainActivity.getInstance().receivedMessage.split("\\|");
        imagepathdata = splitedMessage[1];
        ftpDownloadFile(splitedMessage[1]);
    }

    public void ftpDownloadFile(String ftpInformation) {         //  0         1         2            3
        Log.e("ftpInformation", "=" + ftpInformation);//sathish:sathish:52.202.56.230:21:/var/www/ordapos/Exported/Merchantid_16/category.csv
        String[] filePath = ftpInformation.split("\\:");

        String[] filePathloc = filePath[4].split("\\/");//     /var/www/ordapos/Exported/Merchantid_16/location.csv
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(InetAddress.getByName("" + filePath[2]));
            ftpClient.login("" + filePath[0], "" + filePath[1]);
            ftpClient.changeWorkingDirectory("/");
            ftpClient.setFileType(FTP.ASCII_FILE_TYPE);
            ftpClient.enterLocalPassiveMode();
            File directory = new File(Environment.getExternalStorageDirectory().getPath() + "/pos");
            directory.mkdirs();
            FileOutputStream desFileStream = new FileOutputStream(directory + "/" + filePathloc[filePathloc.length - 1]);
            String srcFilePath = filePath[4];
            Log.e("srcFilePath", "=" + srcFilePath);
            boolean status = ftpClient.retrieveFile(srcFilePath, desFileStream);
            desFileStream.close();
            ftpClient.logout();
            ftpClient.disconnect();
            if (status == true) {
                checkreturnmid();
            }
            return;
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void checkreturnmid() {
        String returnmid = "ERROR";
        if (fileRequestId.equals("M08")) {
            MainActivity.getInstance().insertGroupData();
            returnmid = "M29";
        } else if (fileRequestId.equals("M10")) {
            MainActivity.getInstance().insertCategoryData();
            returnmid = "M31";
        } else if (fileRequestId.equals("M12")) {
            MainActivity.getInstance().insertProductData();
            returnmid = "M33";
        } else if (fileRequestId.equals("M14")) {
            MainActivity.getInstance().insertTypeData();
            returnmid = "M35";
        } else if (fileRequestId.equals("M16")) {

            MainActivity.getInstance().insertUserterminalData();
            returnmid = "M37";
        } else if (fileRequestId.equals("M18")) {
            MainActivity.getInstance().insertTaxData();
            returnmid = "M39";
        } else if (fileRequestId.equals("M20")) {
            MainActivity.getInstance().insertLocationData();
            returnmid = "M41";

        } else if (fileRequestId.equals("M22")) {
            MainActivity.getInstance().insertLocationPrinter();
            returnmid = "M43";
        } else if (fileRequestId.equals("M24")) {
            MainActivity.getInstance().insertTablesData();
            returnmid = "M45";
        } else if (fileRequestId.equals("M26")) {
            MainActivity.getInstance().insertLocationPrice();
            returnmid = "M47";
        } else if (fileRequestId.equals("M50")) {
            MainActivity.getInstance().insertColorData();
            returnmid = "M51";
        } else if (fileRequestId.equals("M54")) {
            MainActivity.getInstance().insertDiscountData();
            returnmid = "M55";
        } else if (fileRequestId.equals("M58")) {
            MainActivity.getInstance().insertVoucherData();
            returnmid = "M59";
        }
        else if (fileRequestId.equals("M64")) {
            MainActivity.getInstance().insertDeliveryData();
            returnmid = "M65";
        }
        else {
            return;
        }
        MainActivity.getInstance().outletid = MainActivity.getInstance().outletid;
        MainActivity.getInstance().messageid = returnmid;
        MainActivity.getInstance().checkid = 1;
        MainActivity.getInstance().messagecontentarray = "1";
        MainActivity.getInstance().noofpackets = 1;
        MainActivity.getInstance().packetno = 1;
        MainActivity.getInstance().message = "OK";
        MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
        MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
        connectionToServer();
    }

    private class queue extends AsyncTask<String, Void, Boolean> {
		/* progress dialog to show user that the backup is processing. */
        /**
         * application context.
         */
        boolean isConnectionSucc = true;

        public void onPreExecute() {
        }

        @SuppressLint("NewApi")
        @Override
        public void onPostExecute(final Boolean success) {
            if (isConnectionSucc == false) {
                MainActivity.networkcheck = true;
                finish();
            } else {
                Intent home = new Intent();
                home.setClass(Socket_Connection.this, Home.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(home);

            }
        }

        public Boolean doInBackground(final String... args) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor queueData = db.rawQuery("SELECT * FROM queuetable;", null);
            int recCount = queueData.getCount();
            if (recCount > 0) {
                queueData.moveToPrevious();
                while (queueData.moveToNext()) {
                    MainActivity.getInstance().queueMessage = queueData.getString(1);
                    int status = connectionToServer();
                    if (status != 0) {
                        isConnectionSucc = false;
                        db.close();
                        return false;
                    } else {
                        isConnectionSucc = true;
                    }
                }
            }
            db.close();
            return false;
        }
    }

    /**
     * Here we are creating instance for class
     *
     * @return
     */
    public static Socket_Connection getInstance() {
        return Socket_ConnectionId;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }
}
