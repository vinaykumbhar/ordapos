package com.embdes.youcloud_resto;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * This contains Product Fragment,GroupStatic Fragment
 * here we are enabling buttons
 * Table no is Assigned
 */
public class GroupFragment extends Fragment {
    TextView group, catogery;
    GroupStaticFragment groupStaticFragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    public GroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group, container, false);
        group = (TextView) view.findViewById(R.id.group);
        catogery = (TextView) view.findViewById(R.id.Category);
        groupStaticFragment = new GroupStaticFragment();
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.staticfragmet, groupStaticFragment);
        fragmentTransaction.commit();
        CartStaticSragment.CARTStaticSragment.plus.setEnabled(true);
        CartStaticSragment.CARTStaticSragment.minus.setEnabled(true);
        Group.GROUPID.searchView.setVisibility(view.VISIBLE);
        Group.GROUPID.txt_info.setText("Select or search food item");

        InputMethodManager inputMethodManager = (InputMethodManager) Group.GROUPID.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        return view;
    }

}
