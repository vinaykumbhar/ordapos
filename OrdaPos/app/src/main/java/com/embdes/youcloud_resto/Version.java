/**
 * 
 */
package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.instabug.library.InstabugTrackingDelegate;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author embdes
 *
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Version extends Activity{
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	LinearLayout  homemenu;
	TextView date,titlebar;
	int versionflag=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.custom_type);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.titlemenu);
		date = (TextView) findViewById(R.id.date);
		titlebar = (TextView) findViewById(R.id.titelabar);
		Button menu = (Button) findViewById(R.id.Imageview);
		menu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent  =new Intent(Version.this,Home.class);
				startActivity(intent);
			}
		});
		final Calendar now = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM");
		String nowDate = formatter.format(now.getTime());
		date.setText(nowDate);
		titlebar.setText("Version");
		findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);

	    findViewById(R.id.coversView);
		LinearLayout tableLayout = (LinearLayout) findViewById(R.id.taglelinearLayout);
//		EditText TableNo =(EditText)findViewById(R.id.covers);
//		TableNo.setInputType(InputType.TYPE_CLASS_NUMBER);
//		TableNo.requestFocus();
//		InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//		imm1.showSoftInput(TableNo, InputMethodManager.SHOW_IMPLICIT);
//        TableNo.setVisibility(View.GONE);
		findViewById(R.id.typeok);
		tableLayout.setVisibility(View.INVISIBLE);
		EditText tableEdit =(EditText)findViewById(R.id.table);
		tableEdit.setVisibility(View.GONE);
		LinearLayout  coversLayout =(LinearLayout)findViewById(R.id.coversLayout);
		LinearLayout  buttonsLayout =(LinearLayout)findViewById(R.id.customButtonsLayout);
	    homemenu =(LinearLayout)findViewById(R.id.homemenu);
		tableLayout.setVisibility(View.VISIBLE);
		homemenu.setVisibility(View.VISIBLE);
//		buttonsLayout.setVisibility(View.INVISIBLE);
//		coversLayout.setVisibility(View.INVISIBLE);

		TextView checkLocationName = (TextView) findViewById(R.id.tablesView);
//		checkLocationName.setWidth(150);
		checkLocationName.setText("VERSION NO.\n1.70.02");
		checkLocationName.setTextSize(15);
		versionflag=1;
		final ImageView home = (ImageView)findViewById(R.id.imageView11);
		final ImageView openTabs = (ImageView)findViewById(R.id.imageView3);
		home.setVisibility(View.VISIBLE);
		openTabs.setVisibility(View.VISIBLE);
		openTabs.setOnClickListener(new View.OnClickListener() {
			@SuppressLint("NewApi")
			public void onClick(View v) {
				versionflag=0;
				Intent intent=new Intent(Version.this, BILLSPENDINGTABLES.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				   
			}
		});

		home.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				versionflag=0;
				finish();
				homemenu.setVisibility(View.INVISIBLE);
				Intent intent=new Intent(Version.this, Home.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				
			}
		});

	}
	protected void onResume() {
		super.onResume();
		if(MainActivity.getInstance() ==null || user_Login.user_LoginID==null){
			Intent userLoginIntent = new Intent(); //Created new Intent to 
			userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
			userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(userLoginIntent);
			this.finish();
		}
		
	};
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
		return super.dispatchTouchEvent(ev);
	}
	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}
}
