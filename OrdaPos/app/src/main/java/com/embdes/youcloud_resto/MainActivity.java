package com.embdes.youcloud_resto;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.embdes.youcloud_resto.R.layout;
import com.instabug.library.InstabugTrackingDelegate;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author EMBDES.
 * @version 1.0.
 *          This Activity contains screen user can login by entering password and update the terminal by selecting update button.
 */

public class MainActivity extends Activity {

    EditText terminalId, eddtportNumber, ipAddress1, ipAddress2, ipAddress3, ipAddress4;
    static MainActivity MAINID = null;
    StringBuilder macadress;
    SQLiteDatabase db;
    Spinner username;
    TelephonyManager telephonyManager;
    public static boolean networkcheck = false;
    public String USERNAME;
    String messageid = "";
    String message;
    String IMEI;
    String ipAddress;
    String messagecontentarray;
    String receivedMessage;
    int statuscheck = 5;
    int outletid;
    int checkid;
    int noofpackets;
    int packetno;
    int userloginflag = 0;
    int homeloginflag = 0;
    int updateflag = 0;
    int currentmessagelength;
    int totalmessagelength;
    int portNumber;
    Socket_Connection socket;
    boolean isWifiPrinterEnable;
    public boolean kotPrinterEnable;
    String queueMessage;
    String finnalMesgToSend;
    int registrationCount;
    boolean unknownHost = false;
    boolean inputOutput = false;
    boolean runtime = false;
    String errormessage;
    ProgressDialog pDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.registration);
        MAINID = this;
        USERNAME = "dummy";
        ipAddress = "0";
        portNumber = 0;
        isWifiPrinterEnable = false;
        kotPrinterEnable = false;
        MainActivity.getInstance().queueMessage = "0";
        MainActivity.getInstance().finnalMesgToSend = "0";
        int permissioncheck = ContextCompat.checkSelfPermission(MAINID, Manifest.permission.READ_PHONE_STATE);
        int permissin = ContextCompat.checkSelfPermission(MAINID, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissin != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10);
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.fromParts("package", getPackageName(), null));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (permissioncheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 10);
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.fromParts("package", getPackageName(), null));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
        }
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        try {
            db.execSQL(String.format("CREATE TABLE if not exists queuetable(sno Integer PRIMARY KEY AUTOINCREMENT, message VARCHAR[5000]);"));
            db.execSQL(String.format("CREATE TABLE if not exists ordernos(sno INTEGER NOT NULL ,locationid INTEGER NOT NULL, tableno INTEGER NOT NULL, orderno VARCHAR[15], billflag INTEGER, orderflag INTEGER);"));
            db.execSQL(String.format("CREATE TABLE if not exists receiptnos(receipthead VARCHAR[20] NOT NULL, receiptnumber INTEGER NOT NULL);"));
            db.execSQL("CREATE TABLE if not exists loginrecord(userid INTEGER NOT NULL,datetime " +
                    "VARCHAR[35] NOT NULL, openingcash DECIMAL(15.2) NOT NULL," +
                    "locationid INTEGER NOT NULL,updateflag INTEGER NOT NULL);");
            db.execSQL("CREATE TABLE if not exists receipt(receiptno VARCHAR[20] NOT NULL," +
                    "docname VARCHAR[35] NOT NULL,docid VARCHAR[35] NOT NULL," +
                    "amount DECIMAL(9.2) NOT NULL,datetime VARCHAR[35] NOT NULL," +
                    "username VARCHAR[35] NOT NULL,orderid VARCHAR[35] NOT NULL," +
                    "checkflag INTEGER NOT NULL);");
            db.execSQL("CREATE TABLE if not exists tax(tinno VARCHAR[35] NOT NULL," +
                    "ctsno VARCHAR[35] NOT NULL,servicetaxno VARCHAR[35] NOT NULL" +
                    ",servicetaxper FLOAT NOT NULL,taxper1 FLOAT NOT NULL," +
                    "taxper2 FLOAT NOT NULL,taxper3 FLOAT NOT NULL);");
            db.execSQL("CREATE TABLE if not exists voucher(voucherid INTEGER NOT NULL," +
                    "vouchervalues DECIMAL(9.2) NOT NULL,expdate VARCHAR[35] NOT NULL," +
                    "vouchertype VARCHAR[35] NOT NULL,usedflag INTEGER NOT NULL);");
            db.execSQL("CREATE TABLE if not exists tables(locationid INTEGER NOT NULL," +
                    "tableno INTEGER NOT NULL,noofchairs INTEGER NOT NULL," +
                    "occupiedchairs INTEGER NOT NULL,freechairs INTEGER NOT NULL," +
                    "fullybooked INTEGER NOT NULL,colorid INTEGER NOT NULL,orderid VARCHAR[15]);");
            db.execSQL("CREATE TABLE if not exists type(typeid INTEGER NOT NULL," +
                    "typename VARCHAR[35] NOT NULL,price DECIMAL(9.2) NOT NULL);");
            db.execSQL("CREATE TABLE if not exists product(productid INTEGER NOT NULL," +
                    " categoryid INTEGER NOT NULL,groupid INTEGER NOT NULL," +
                    "productdescriptionterminal VARCHAR[35] NOT NULL," +
                    "productdescriptionkitchen VARCHAR[35] NOT NULL," +
                    "productdescriptionbill VARCHAR[35] NOT NULL,active INTEGER NOT NULL," +
                    "type VARCHAR[35],price1 DECIMAL(9.2) NOT NULL,price2 DECIMAL(9.2) NOT NULL," +
                    "price3 DECIMAL(9.2) NOT NULL,taxper DECIMAL(9.2) NOT NULL," +
                    "available INTEGER NOT NULL,foodid INTEGER NOT NULL," +
                    "discountper DECIMAL(9.2) NOT NULL,productdescription VARCHAR[1000] NOT NULL," +
                    "foodimages VARCHAR[10] NOT NULL);");
            db.execSQL("CREATE TABLE if not exists userterminal(userid INTEGER NOT NULL," +
                    "username VARCHAR[35] NOT NULL,password VARCHAR[35] NOT NULL," +
                    "permissionsmenu INTEGER NOT NULL,permissionterminal INTEGER NOT NULL," +
                    "permissionslocation INTEGER NOT NULL);");
            db.execSQL("CREATE TABLE if not exists loginrecord(userid INTEGER NOT NULL," +
                    "datetime VARCHAR[35] NOT NULL,openingcash DECIMAL(9.2) NOT NULL," +
                    "locationid INTEGER NOT NULL,updateflag INTEGER NOT NULL);");
            db.execSQL("CREATE TABLE if not exists location(locationid INTEGER NOT NULL," +
                    "locationname VARCHAR[35] NOT NULL,locationprinterid VARCHAR[35] NOT NULL," +
                    "permissionslocation INTEGER NOT NULL);");
            db.execSQL("CREATE TABLE if not exists grouptable(groupname VARCHAR[35] NOT NULL," +
                    "groupid INTEGER NOT NULL,imageurl VARCHAR[25] NOT NULL," +
                    "discountper DECIMAL(9.2));");
            db.execSQL("CREATE TABLE if not exists category(categoryname VARCHAR[35] NOT NULL," +
                    "categoryid INTEGER NOT NULL,groupid INTEGER NOT NULL,imageurl VARCHAR[25] NOT NULL);");
            db.execSQL("CREATE TABLE if not exists cart(sno Integer PRIMARY KEY AUTOINCREMENT,productname VARCHAR[35] NOT NULL," +
                    "typename VARCHAR[35] NOT NULL,productprice DECIMAL(9.2) NOT NULL,typeprice DECIMAL(9.2) NOT NULL," +
                    "productqty INTEGER NOT NULL,typeqty INTEGER NOT NULL,totalprice DECIMAL(15.2) NOT NULL,orderqty INTEGER NOT NULL,ordertypeqty INTEGER NOT NULL," +
                    "orderflag INTEGER NOT NULL,billqty INTEGER NOT NULL,billtypeqty INTEGER NOT NULL,billedqty INTEGER NOT NULL,billedtypeqty INTEGER NOT NULL," +
                    "billflag INTEGER NOT NULL,locationid INTEGER NOT NULL,tableno INTEGER NOT NULL,covers INTEGER NOT NULL," +
                    "checkflag INTEGER NOT NULL,orderno VARCHAR[15] NOT NULL,tdiscount DECIMAL(9.2) NOT NULL," +
                    "discount DECIMAL(9.2) NOT NULL,totaltax DECIMAL(9.2) NOT NULL,datetime VARCHAR[25] NOT NULL," +
                    "tbillflag INTEGER NOT NULL,pgroup VARCHAR[35],pcategory VARCHAR[35],username VARCHAR[35],foodid INTEGER NOT NULL,consumerid INTEGER NOT NULL);");
            db.execSQL("CREATE TABLE if not exists masterterminal(terminalid INTEGER PRIMARY KEY NOT NULL," +
                    "terminalname VARCHAR[35] NOT NULL,shortname VARCHAR[10] NOT NULL,checkid INTEGER NOT NULL," +
                    "ipaddress VARCHAR[25] NOT NULL,portno INTEGER NOT NULL,imeino VARCHAR[20] NOT NULL,openingcash DECIMAL(15.2)," +
                    "currentcash DECIMAL(15.2),lastloginusername VARCHAR[20],logindate VARCHAR[20],cashdate VARCHAR[10] NOT NULL," +
                    "terminalpermisiions INTEGER NOT NULL,webadd VARCHAR[35] NOT NULL,facebookid  VARCHAR[35] NOT NULL," +
                    "twitterid  VARCHAR[35] NOT NULL,email  VARCHAR[35] NOT NULL,add1  VARCHAR[35] NOT NULL, " +
                    "add2  VARCHAR[35] NOT NULL, add3  VARCHAR[35] NOT NULL, city  VARCHAR[20] NOT NULL, pinno  VARCHAR[10] NOT NULL," +
                    " mobileno VARCHAR[10] NOT NULL,message1  VARCHAR[20] NOT NULL, message2 VARCHAR[20] NOT NULL);");
            db.execSQL("CREATE TABLE if not exists locationprinter(foodid INTEGER NOT NULL,locationorderid INTEGER NOT NULL," +
                    "ipaddress VARCHAR[35] NOT NULL,portno INTEGER NOT NULL, btprintername VARCHAR[150]);");
            db.execSQL("CREATE TABLE if not exists locationprice(locationid INTEGER NOT NULL,price INTEGER NOT NULL);");
            db.execSQL("CREATE TABLE if not exists consumerinfo(cid Integer PRIMARY KEY AUTOINCREMENT,consumerid VARCHAR[10],cname VARCHAR[20] NOT NULL,cmobileno VARCHAR[10] NOT NULL,caddress VARCHAR[35] NOT NULL);");
            db.execSQL("CREATE TABLE if not exists deliveryboy(deliveryboyid INTEGER PRIMARY KEY," +
                    "dname VARCHAR[20] NOT NULL,terminalid INTEGER NOT NULL,mobileno VARCHAR[10] NOT NULL);");

            db.execSQL("CREATE TABLE if not exists color(colorid INTEGER NOT NULL,colorname VARCHAR[20] NOT NULL,status VARCHAR[20] NOT NULL);");
            db.execSQL("CREATE TABLE if not exists discount(discountid INTEGER NOT NULL,maxamount  FLOAT NOT NULL,"
                    + "minamount FLOAT NOT NULL,discountper FLOAT NOT NULL,discountvalue FLOAT NOT NULL,"
                    + "val_per FLOAT NOT NULL,working FLOAT NOT NULL);");

            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            macid();
            Log.e("macadress", "=" + macadress);

            terminalId = (EditText) findViewById(R.id.editTerminalId);
            eddtportNumber = (EditText) findViewById(R.id.editPortNumber);
            ipAddress1 = (EditText) findViewById(R.id.editIpAddress1);
            ipAddress2 = (EditText) findViewById(R.id.editIpAddress2);
            ipAddress3 = (EditText) findViewById(R.id.editIpAddress3);
            ipAddress4 = (EditText) findViewById(R.id.editIpAddress4);

            terminalId.setTransformationMethod(new NumericKeyBoardTransformationMethod());
            eddtportNumber.setTransformationMethod(new NumericKeyBoardTransformationMethod());
            ipAddress1.setTransformationMethod(new NumericKeyBoardTransformationMethod());
            ipAddress2.setTransformationMethod(new NumericKeyBoardTransformationMethod());
            ipAddress3.setTransformationMethod(new NumericKeyBoardTransformationMethod());
            ipAddress4.setTransformationMethod(new NumericKeyBoardTransformationMethod());

            Cursor login = db.rawQuery("SELECT * from masterterminal;", null);
            registrationCount = login.getCount();
            if (registrationCount > 0) {
                login.moveToNext();
                outletid = login.getInt(login.getColumnIndex("terminalid"));
                ipAddress = login.getString(login.getColumnIndex("ipaddress"));
                portNumber = login.getInt(login.getColumnIndex("portno"));
                db.close();
                new UploadTask().execute();
            } else if ((MainActivity.getInstance().updateflag == 1) && ((MainActivity.getInstance().outletid != 0)
                    && (MainActivity.getInstance().ipAddress != null)
                    && (MainActivity.getInstance().portNumber != 0))) {
                MainActivity.getInstance().updateflag = 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        Button registrationOk = (Button) findViewById(R.id.registration_ok);
        registrationOk.setOnClickListener(registrationokhandler);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    return;
        }
    }


    protected void onResume() {
        super.onResume();
        if (MainActivity.getInstance() == null) {
            Intent userLoginIntent = new Intent(); //Created new Intent to
            userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
            userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(userLoginIntent);
            this.finish();
        }

    }

    private class NumericKeyBoardTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return source;
        }
    }


    View.OnClickListener registrationokhandler = new View.OnClickListener() {
        public void onClick(View v) {
            if (terminalId.getText().length() != 0 && ipAddress1.getText().length() != 0
                    && ipAddress2.getText().length() != 0 && ipAddress3.getText().length() != 0
                    && ipAddress4.getText().length() != 0 && eddtportNumber.getText().length() != 0) {
                final String ipAddress = "" + Integer.parseInt(ipAddress1.getText().toString()) + "." +
                        Integer.parseInt(ipAddress2.getText().toString()) + "." +
                        Integer.parseInt(ipAddress3.getText().toString()) + "." +
                        Integer.parseInt(ipAddress4.getText().toString());

                terminalRegistration(terminalId.getText().toString(),
                        ipAddress, eddtportNumber.getText().toString());
            } else {
                AlertDialog.Builder msg = new AlertDialog.Builder(
                        MainActivity.this);
                msg.setTitle("");
                msg.setMessage("Fields Are Not Valid");
                msg.setPositiveButton("Ok", null);
                msg.show();
            }
        }
    };

    @SuppressLint({"InlinedApi", "NewApi"})
    public void terminalRegistration(String terminalId,
                                     String ip, String port) {
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        IMEI = telephonyManager.getDeviceId();
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor login = db.rawQuery("SELECT * from masterterminal;", null);
        registrationCount = login.getCount();
        db.close();
        if (registrationCount <= 0) {
            outletid = Integer.parseInt(terminalId);
            messageid = "M01";
            checkid = 1;
            messagecontentarray = "1111";
            noofpackets = 1;
            packetno = 1;
            message = outletid + "|" +
                    ip + "|" + port + "|" + macadress;
            currentmessagelength = message.length();

            totalmessagelength = message.length();
            ipAddress = ip;
            portNumber = Integer.parseInt(port);
        }
        new SocketThread().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class SocketThread extends AsyncTask<Void, Integer, Void> {
        int status = 2;

        protected void onPreExecute() {
            if (!pDialog.isShowing()) {
                pDialog.show();
            }
            if (socket == null) {
                socket = new Socket_Connection();
            }
            if (MainActivity.getInstance().queueMessage.equals("1")) {
                new queue().execute("OK");
            }
        }

        protected Void doInBackground(Void... params) {

            if (!MainActivity.getInstance().queueMessage.equals("1")) {
                status = socket.connectionToServer();
            }
            return null;
        }

        @SuppressLint("NewApi")
        protected void onPostExecute(Void result) {
            if (status == 0) {
                statuscheck = status;
                new UploadTask().execute();
            } else {
                statuscheck = status;
                if (pDialog.isShowing()) {
                    pDialog.cancel();
                }
                if (MainActivity.getInstance().unknownHost == true) {
                    MainActivity.getInstance().unknownHost = false;
                    if (!MainActivity.getInstance().messageid.equals("M03")
                            && !MainActivity.getInstance().messageid.equals("M05")
                            && !MainActivity.getInstance().messageid.equals("M27")
                            && !MainActivity.getInstance().messageid.equals("M61")) {
                        AlertDialog.Builder msg = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Dialog);
                        msg.setMessage("Invalid hostname for server or IP address of a host could not be determined. \n"/*+MainActivity.getInstance().errormessage*/);
                        msg.setCancelable(false);
                        msg.setPositiveButton("OK", new android.content.DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        msg.show();
                    } else if (MainActivity.getInstance().messageid.equals("M27")) {
                        if (MainActivity.getInstance().queueMessage.split("\\#").length < 7) {
                            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                            db.execSQL("INSERT INTO queuetable(message) VALUES('" + MainActivity.getInstance().finnalMesgToSend + "');");
                            MainActivity.getInstance().queueMessage = "0";
                            db.close();
                            Intent registration = new Intent();
                            registration.setClass(MainActivity.this, Home.class);
                            registration.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(registration);
                        }

                    }
                } else if (MainActivity.getInstance().inputOutput == true) {
                    MainActivity.getInstance().inputOutput = false;
                    if (!MainActivity.getInstance().messageid.equals("M03")
                            && !MainActivity.getInstance().messageid.equals("M05")
                            && !MainActivity.getInstance().messageid.equals("M27")
                            && !MainActivity.getInstance().messageid.equals("M61")) {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                MainActivity.this, android.R.style.Theme_Dialog);
                        msg.setMessage("Unable to connect to server Please check the Internet Conectivity  or \n Ip Address or port no. is not correct"/*+MainActivity.getInstance().errormessage*/);
                        msg.setCancelable(false);
                        msg.setPositiveButton("OK", new android.content.DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        msg.show();
                    } else if (MainActivity.getInstance().messageid.equals("M27")) {
                        if (MainActivity.getInstance().queueMessage.split("\\#").length < 7) {
                            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                            db.execSQL("INSERT INTO queuetable(message) VALUES('" + MainActivity.getInstance().finnalMesgToSend + "');");
                            MainActivity.getInstance().queueMessage = "0";
                            db.close();
                            Intent registration = new Intent();
                            registration.setClass(MainActivity.this, Home.class);
                            registration.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(registration);
                        }
                    }
                } else if (MainActivity.getInstance().runtime == true) {
                    MainActivity.getInstance().runtime = false;
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            MainActivity.this, android.R.style.Theme_Dialog);
                    msg.setMessage("Please make sure you have given correct terminal id"/*+MainActivity.getInstance().errormessage*/);
                    msg.setCancelable(false);
                    msg.setPositiveButton("OK", new android.content.DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    msg.show();
                }
            }
        }
    }

    class UploadTask extends AsyncTask<Void, Integer, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!pDialog.isShowing()) {
                pDialog.show();
            }

            if (socket == null) {
                socket = new Socket_Connection();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            if ((statuscheck == 5) || (statuscheck == 0)) {
                checkTables();
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing()) {
                pDialog.cancel();
            }
            if (((statuscheck == 5) || (statuscheck == 0)) && (MainActivity.getInstance().unknownHost == false)
                    && (MainActivity.getInstance().runtime == false)
                    && (MainActivity.getInstance().inputOutput == false)) {
                MainActivity.getInstance().userloginflag = 0;
                moveToUersLogin();
            } else if (((statuscheck == 5) || (statuscheck == 0)) && (MainActivity.getInstance().unknownHost == false)
                    && (MainActivity.getInstance().runtime == false)
                    && (MainActivity.getInstance().inputOutput == false)) {
                MainActivity.getInstance().homeloginflag = 0;
                Intent intent = new Intent(MainActivity.this, Home.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            } else {
                if ((MainActivity.getInstance().unknownHost == true)
                        || (MainActivity.getInstance().runtime == true)
                        || (MainActivity.getInstance().inputOutput == true)) {

                    if (MainActivity.getInstance().unknownHost == true) {
                        MainActivity.getInstance().unknownHost = false;
                        if (!MainActivity.getInstance().messageid.equals("M03")
                                && !MainActivity.getInstance().messageid.equals("M05")
                                && !MainActivity.getInstance().messageid.equals("M27")
                                && !MainActivity.getInstance().messageid.equals("M61")) {
                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                    MainActivity.this, android.R.style.Theme_Dialog);
                            msg.setMessage("Detected missing files, \n Invalid hostname for server or IP address of a host could not be determined."/*+MainActivity.getInstance().errormessage*/);
                            msg.setCancelable(false);
                            msg.setPositiveButton("OK", new android.content.DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            msg.show();
                        } else if (MainActivity.getInstance().messageid.equals("M27")) {
                            if (MainActivity.getInstance().queueMessage.split("\\#").length < 7) {
                                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                                db.execSQL("INSERT INTO queuetable(message) VALUES('" + MainActivity.getInstance().finnalMesgToSend + "');");
                                MainActivity.getInstance().queueMessage = "0";
                                db.close();
                                Intent registration = new Intent();
                                registration.setClass(MainActivity.this, Home.class);
                                registration.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(registration);
                            }
                        }
                    } else if (MainActivity.getInstance().inputOutput == true) {
                        MainActivity.getInstance().inputOutput = false;
                        if (!MainActivity.getInstance().messageid.equals("M03")
                                && !MainActivity.getInstance().messageid.equals("M05")
                                && !MainActivity.getInstance().messageid.equals("M27")
                                && !MainActivity.getInstance().messageid.equals("M61")) {
                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                    MainActivity.this, android.R.style.Theme_Dialog);
                            msg.setMessage("Detected missing files, \n Unable to connect to server Please check the Internet Conectivity  or \n Ip Address or port no. is not correct"/*+MainActivity.getInstance().errormessage*/);
                            msg.setCancelable(false);
                            msg.setPositiveButton("OK", new android.content.DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            msg.show();
                        } else if (MainActivity.getInstance().messageid.equals("M27")) {
                            if (MainActivity.getInstance().queueMessage.split("\\#").length < 7) {
                                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                                db.execSQL("INSERT INTO queuetable(message) VALUES('" + MainActivity.getInstance().finnalMesgToSend + "');");
                                MainActivity.getInstance().queueMessage = "0";
                                db.close();
                                Intent registration = new Intent();
                                registration.setClass(MainActivity.this, Home.class);
                                registration.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(registration);
                            }
                        }
                    } else if (MainActivity.getInstance().runtime == true) {
                        MainActivity.getInstance().runtime = false;
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                MainActivity.this, android.R.style.Theme_Dialog);
                        msg.setMessage("Detected missing files, \n Please make sure you have given correct terminal id "/*+MainActivity.getInstance().errormessage*/);
                        msg.setCancelable(false);
                        msg.setPositiveButton("OK", new android.content.DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        msg.show();

                    }

                }
            }
        }
    }

    private void moveToUersLogin() {
        Intent userLoginIntent = new Intent(); //Created new Intent to
        userLoginIntent.setClass(getApplicationContext(), user_Login.class);
        userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(userLoginIntent);
        this.finish();

    }

    public class queue extends AsyncTask<String, Void, Boolean> {

        boolean isConnectionSucc = true;

        public void onPreExecute() {

        }

        @SuppressLint("NewApi")
        @Override
        public void onPostExecute(final Boolean success) {

        }

        public Boolean doInBackground(final String... args) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor queueData = db.rawQuery("SELECT * FROM queuetable;", null);
            int recCount = queueData.getCount();
            if (recCount > 0) {
                queueData.moveToPrevious();
                while (queueData.moveToNext()) {
                    MainActivity.getInstance().queueMessage = queueData.getString(1);
                    int status = socket.connectionToServer();
                    if (status != 0) {
                        isConnectionSucc = false;
                        db.close();
                        return false;
                    } else {
                        isConnectionSucc = true;
                    }
                }
            }
            db.close();
            return false;
        }
    }

    public void insertMasterTerminalData() {
        String[] receiveMessage = receivedMessage.split("\\|");
        if (receiveMessage.length == 25) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("INSERT INTO masterterminal values(" + receiveMessage[0] + ", '" + receiveMessage[1] + "' , '" +
                    receiveMessage[2] + "' , " + receiveMessage[3] + " , '" + receiveMessage[4] + "' ," + receiveMessage[5] + ", '" +
                    receiveMessage[6] + "'," + receiveMessage[7] + "," + receiveMessage[8] + " ,'" + receiveMessage[9] +
                    "','" + receiveMessage[10] + "','" + receiveMessage[11] + "'," + receiveMessage[12] + ",'" +
                    receiveMessage[13] + "','" + receiveMessage[14] + "','" + receiveMessage[15] + "'" +
                    ",'" + receiveMessage[16] + "','" + receiveMessage[17] + "','" + receiveMessage[18] + "','" +
                    receiveMessage[19] + "','" + receiveMessage[20] + "','" + receiveMessage[21] + "','" +
                    receiveMessage[22] + "','" + receiveMessage[23] + "','" + receiveMessage[24] + "');");
            db.close();
        } else {
        }
    }

    public void insertLocationPrice() {
        BufferedReader user_bf;
        try {
            user_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/locationprice.csv"));
            String user_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM locationprice");
            db.close();
            while ((user_count = user_bf.readLine()) != null) {
                String[] value = user_count.split(",");    // seperator
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                db.execSQL("INSERT INTO locationprice VALUES(" + value[0] + "," + value[1] + ");");
                db.close();
            }
            user_bf.close();
            MainActivity.getInstance().message = "OK";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            MainActivity.getInstance().message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            MainActivity.getInstance().message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            MainActivity.getInstance().message = "ERROR";
        }
    }

    public void insertTaxData() {
        BufferedReader tax_bf;
        try {
            tax_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/tax.csv"));
            String tax_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM tax");
            while ((tax_count = tax_bf.readLine()) != null) {
                String[] value = tax_count.split(",");    // seperator
                db.execSQL("INSERT INTO tax VALUES('" + value[0] + "','" + value[1] + "','" + value[2] + "'," +
                        value[3] + "," + value[4]
                        + "," + value[5] + "," + value[6] + ");");
            }
            tax_bf.close();
            db.close();

            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertDiscountData() {
        BufferedReader discount_bf;
        try {
            discount_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/discount.csv"));
            String discount_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM discount");

            while ((discount_count = discount_bf.readLine()) != null) {
                String[] value = discount_count.split(",");    // seperator
                db.execSQL("INSERT INTO discount VALUES(" + value[0] + "," + value[1] + "," + value[2] + "," +
                        value[3] + "," + value[4]
                        + "," + value[5] + "," + value[6] + ");");
            }
            discount_bf.close();
            db.close();

            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }

    }

    public void insertProductData() {
        BufferedReader product_bf = null;
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        try {
            int count = 0;
            product_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/product.csv"));
            String productline;
            db.execSQL("DELETE FROM product");
            while ((productline = product_bf.readLine()) != null) {
                String[] value = productline.split(",");
                db.execSQL("INSERT INTO product VALUES(" + value[0] + "," + value[1] + "," + value[2] + ",'" + value[3] + "','"
                        + value[4] + "','" + value[5] + "'," + value[6] + ",'" + value[7] + "'," + value[8] + "," + value[9]
                        + "," + value[10] + "," + value[11] + "," + value[12] + "," + value[13] + "," + value[14] + ",'" + value[15] + "','" + value[16] + "');");
                count++;
                ftpDownloadImage(Socket_Connection.imagepathdata, value[15]);
            }
            product_bf.close();
            db.close();
            message = "OK";
            return;
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertTablesData() {
        BufferedReader tables_bf;
        try {
            tables_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/tables.csv"));
            String line;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM tables");
            db.close();
            while ((line = tables_bf.readLine()) != null) {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                String[] value = line.split(",");    // seperator

                db.execSQL("INSERT INTO tables VALUES(" + value[0] + "," + value[1] + "," + value[2] + "," + value[3] + ","
                        + value[4] + "," + value[5] + "," + value[6] + "," + value[7] + ");");

                db.close();
            }
            tables_bf.close();
            db.close();
            message = "OK";

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertUserterminalData() {
        BufferedReader user_bf;
        try {
            user_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/userterminal.csv"));

            String user_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM userterminal");
            db.close();
            while ((user_count = user_bf.readLine()) != null) {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                String[] value = user_count.split(",");
                db.execSQL("INSERT INTO userterminal VALUES(" + value[0] + ",'" + value[1] + "','" + value[2] + "'," + value[3] + ","
                        + value[4] + "," + value[5] + ");");
                db.close();

            }
            user_bf.close();
            db.close();
            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertLocationData() {
        BufferedReader location_bf;
        try {
            location_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/location.csv"));
            String location_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM location");
            db.close();
            while ((location_count = location_bf.readLine()) != null) {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                String[] value = location_count.split(",");    // seperator
                db.execSQL("INSERT INTO location VALUES(" + value[0] + ",'" + value[1] + "','" + value[2] + "'," + value[3] + ");");
                db.close();
            }
            location_bf.close();
            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertColorData() {
        BufferedReader color_bf;
        try {
            color_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/color.csv"));
            String color_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM color");
            while ((color_count = color_bf.readLine()) != null) {
                String[] value = color_count.split(",");    // seperator
                db.execSQL("INSERT INTO color VALUES(" + value[0] + ",'" + value[1] + "','" + value[2] + "');");
            }
            color_bf.close();
            db.close();
            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertGroupData() {
        BufferedReader group_bf;
        try {
            group_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/group.csv"));
            String group_count;
            int count = 0;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM grouptable");
            while ((group_count = group_bf.readLine()) != null) {
                String[] value = group_count.split(",");    // seperator
                db.execSQL("INSERT INTO grouptable VALUES('" + value[0] + "'," + value[1] + ",'" + value[2] + "'," + value[3] + ");");
                count++;
                ftpDownloadImage(Socket_Connection.imagepathdata, value[2]);
            }
            group_bf.close();
            db.close();
            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertCategoryData() {
        BufferedReader category_bf;
        try {
            int count = 0;
            category_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/category.csv"));
            String category_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM category");
            while ((category_count = category_bf.readLine()) != null) {

                String[] value = category_count.split(",");    // seperator
                db.execSQL("INSERT INTO category VALUES('" + value[0] + "'," + value[1] + "," + value[2] + ",'" + value[3] + "');");
                count++;
                ftpDownloadImage(Socket_Connection.imagepathdata, value[3]);

            }
            category_bf.close();
            db.close();
            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertTypeData() {
        BufferedReader type_bf;
        try {
            type_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/type.csv"));
            String type_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM type");
            while ((type_count = type_bf.readLine()) != null) {
                String[] value = type_count.split(",");
                db.execSQL("INSERT INTO type VALUES(" + value[0] + ",'" + value[1] + "'," + value[2] + ");");
            }
            type_bf.close();
            db.close();
            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertLocationPrinter() {
        BufferedReader user_bf;
        try {
            user_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/locationprinter.csv"));
            String user_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM locationprinter");
            db.close();
            while ((user_count = user_bf.readLine()) != null) {
                String[] value = user_count.split(",");    // seperator
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                db.execSQL("INSERT INTO locationprinter VALUES(" + value[0] + "," + value[1] + ",'" +
                        value[2] + "'," + value[3] + ", 'Empty\n00:00:00:00:00:00');");
                db.close();
            }
            user_bf.close();
            MainActivity.getInstance().message = "OK";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            MainActivity.getInstance().message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            MainActivity.getInstance().message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            MainActivity.getInstance().message = "ERROR";
        }
    }

    public void insertVoucherData() {
        BufferedReader voucher_bf;
        try {
            voucher_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/voucher.csv"));
            String voucher_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM voucher");
            while ((voucher_count = voucher_bf.readLine()) != null) {

                String[] value = voucher_count.split(",");
                db.execSQL("INSERT INTO voucher VALUES(" + value[0] + "," + value[1] + ",'" + value[2] + "','" + value[3] +
                        "'," + value[4] + ");");
            }
            voucher_bf.close();
            db.close();
            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertDeliveryData() {
        BufferedReader delivery_bf;
        try {
            delivery_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/delivery_boy.csv"));
            String delivery_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM deliveryboy");
            while ((delivery_count = delivery_bf.readLine()) != null) {

                String[] value = delivery_count.split(",");
                db.execSQL("INSERT INTO deliveryboy VALUES(" + value[0] + ",'" + value[2] + "'," + value[1] + ",'" + value[3] + "');");
            }
            delivery_bf.close();
            db.close();
            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }


    public void checkTables() {

        for (boolean isTableEmpty = true; isTableEmpty == true; ) {
            //checking for location
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT * FROM location;", null);
            int recCount = tableData.getCount();
            if (recCount == 0) {
                db.close();
                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M19";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M19|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                }
            } else {
                isTableEmpty = false;
                tableData.close();
                db.close();
            }
        }


        for (boolean isTableEmpty = true; isTableEmpty == true; ) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT * FROM type;", null);
            int recCount = tableData.getCount();
            if (recCount == 0) {
                db.close();
                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M13";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M13|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                }
            } else {
                isTableEmpty = false;
                db.close();
            }
        }

        for (boolean isTableEmpty = true; isTableEmpty == true; ) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT groupname FROM grouptable;", null);
            int recCount = tableData.getCount();
            if (recCount == 0) {
                db.close();
                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M07";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M07|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                } else {
                    isTableEmpty = false;
                    db.close();
                }
            } else {
                isTableEmpty = false;
                db.close();
            }
        }


        for (boolean isTableEmpty = true; isTableEmpty == true; ) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT categoryname FROM category;", null);
            int recCount = tableData.getCount();
            if (recCount == 0) {
                db.close();
                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M09";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M09|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                } else {
                    isTableEmpty = false;
                    db.close();
                }
            } else {
                isTableEmpty = false;
                db.close();
            }
        }

        for (boolean isTableEmpty = true; isTableEmpty == true; ) {

            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT productid FROM product;", null);
            int recCount = tableData.getCount();
            if (recCount == 0) {
                db.close();
                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M11";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M11|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                }

            } else {
                isTableEmpty = false;
                db.close();
            }

        }


        for (boolean isTableEmpty = true; isTableEmpty == true; ) {

            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT userid FROM userterminal;", null);
            int recCount = tableData.getCount();

            if (recCount == 0) {
                db.close();
                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M15";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M15|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                }
            } else {
                isTableEmpty = false;
                db.close();
            }
        }


        for (boolean isTableEmpty = true; isTableEmpty == true; ) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT locationid FROM tables;", null);

            int recCount = tableData.getCount();
            if (recCount == 0) {
                db.close();

                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M23";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M23|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                }
            } else {
                isTableEmpty = false;
                db.close();
            }
        }


        for (boolean isTableEmpty = true; isTableEmpty == true; ) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT locationid FROM locationprice;", null);
            int recCount = tableData.getCount();
            if (recCount == 0) {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                db.close();
                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M25";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M25|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                }
            } else {
                isTableEmpty = false;
                db.close();
            }
        }


        for (boolean isTableEmpty = true; isTableEmpty == true; ) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT colorid FROM color;", null);
            int recCount = tableData.getCount();
            if (recCount == 0) {
                db.close();
                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M49";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M49|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                }
            } else {
                isTableEmpty = false;
                db.close();
            }
        }


        for (boolean isTableEmpty = true; isTableEmpty == true; ) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT * FROM tax;", null);
            int recCount = tableData.getCount();
            if (recCount == 0) {
                db.close();
                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M17";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M17|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                }
            } else {
                isTableEmpty = false;
                db.close();
            }
        }


        for (boolean isTableEmpty = true; isTableEmpty == true; ) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT discountid FROM discount;", null);
            int recCount = tableData.getCount();
            if (recCount == 0) {
                db.close();
                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M53";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M53|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                }
            } else {
                isTableEmpty = false;
                db.close();
            }
        }

        for (boolean isTableEmpty = true; isTableEmpty == true; ) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT voucherid FROM voucher;", null);
            int recCount = tableData.getCount();
            if (recCount == 0) {
                db.close();
                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M57";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M57|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                }
            } else {
                isTableEmpty = false;
                db.close();
            }
        }
        for (boolean isTableEmpty = true; isTableEmpty == true; ) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT * FROM locationprinter;", null);
            int recCount = tableData.getCount();
            int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
            if (recCount == 0) {
                db.close();
                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M21";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M21|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                }
            } else {
                isTableEmpty = false;
                db.close();
            }
        }

        for (boolean isTableEmpty = true; isTableEmpty == true; ) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT * FROM deliveryboy;", null);
            int recCount = tableData.getCount();
            if (recCount == 0) {
                db.close();
                MainActivity.getInstance().outletid = outletid;
                MainActivity.getInstance().messageid = "M63";
                MainActivity.getInstance().checkid = 1;
                MainActivity.getInstance().messagecontentarray = "11";
                MainActivity.getInstance().noofpackets = 1;
                MainActivity.getInstance().packetno = 1;
                MainActivity.getInstance().message = "M63|DUMMY";
                MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                int status = socket.connectionToServer();
                if (status != 0) {
                    break;
                }
            } else {
                isTableEmpty = false;
                tableData.close();
                db.close();

            }

        }

    }

    public void ftpDownloadImage(String ftpInformation, String fileName) {
        String[] filePath = ftpInformation.split("\\:");
        String[] filePathloc = filePath[4].split("\\/");
        String[] newname = fileName.split("\\/");
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(InetAddress.getByName("" + filePath[2]));
            ftpClient.login("" + filePath[0], "" + filePath[1]);
            ftpClient.changeWorkingDirectory("/");
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            ftpClient.enterLocalPassiveMode();
            File directory = new File(Environment.getExternalStorageDirectory().getPath() + "/pos/img");
            directory.mkdirs();
            FileOutputStream desFileStream = new FileOutputStream(directory + "/" + newname[(newname.length - 1)]);
            String srcFilePath = null;
            StringBuffer sBuffer = new StringBuffer(filePathloc.length - 1);
            for (int i = 0; i < filePathloc.length - 1; i++) {
                sBuffer.append(filePathloc[i]).append("/");
            }
            for (int f = 0; f < (newname[newname.length - 1]).length(); f++) {
                String values = newname[newname.length - 1];
            }
            sBuffer.append(newname[newname.length - 2]).append("/").append(newname[newname.length - 1]);
            srcFilePath = String.valueOf(sBuffer);
            boolean status = ftpClient.retrieveFile(srcFilePath, desFileStream);
            desFileStream.close();
            ftpClient.logout();
            ftpClient.disconnect();

            return;
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static MainActivity getInstance() {
        return MAINID;
    }

    public String macid() {
        try {
            // get all the interfaces
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            //find network interface wlan0
            for (NetworkInterface networkInterface : all) {
                if (!networkInterface.getName().equalsIgnoreCase("wlan0")) continue;
                //get the hardware address (MAC) of the interface
                byte[] macBytes = networkInterface.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }


                macadress = new StringBuilder();
            /*	for (byte b : macBytes) {
                    //gets the last byte of b
					macadress.append(Integer.toHexString(b & 0xFF) + ":");
				}

				if (macadress.length() > 0) {
					macadress.deleteCharAt(macadress.length() - 1);
				}*/
//				return macadress.toString();
                return macadress.append("54:27:58:a5:f4:0d").toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}

