package com.embdes.youcloud_resto;


/**
 * Created by mom on 29-05-2017.
 */

public class tablespojo {
    String mid;

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    String tableno;
    String hometableno;
    String locationid ;
    String occupied;
    private String noofchaors;

    public String getOccupied() {
        return occupied;
    }

    public void setOccupied(String occupied) {
        this.occupied = occupied;
    }

    public String getNoofchaors() {
        return noofchaors;
    }

    public void setNoofchaors(String noofchaors) {
        this.noofchaors = noofchaors;
    }

    public String getTableno() {
        return tableno;
    }

    public void setTableno(String tableno) {
        this.tableno = tableno;
    }

    public String getHometableno() {
        return hometableno;
    }

    public void setHometableno(String hometableno) {
        this.hometableno = hometableno;
    }

    public String getLocationid() {
        return locationid;
    }

    public void setLocationid(String locationid) {
        this.locationid = locationid;
    }
}
