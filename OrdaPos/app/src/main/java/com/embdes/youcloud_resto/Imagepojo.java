package com.embdes.youcloud_resto;

/**
 * Created by EmbDes on 10-07-2017.
 */

public class Imagepojo {
    private String paymnet, imageur;

    public String getPaymnet() {
        return paymnet;
    }

    public void setPaymnet(String paymnet) {
        this.paymnet = paymnet;
    }

    public String getImageur() {
        return imageur;
    }

    public void setImageur(String imageur) {
        this.imageur = imageur;
    }


}

