package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.instabug.library.InstabugTrackingDelegate;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author EMBDES.
 * @version 1.0.
 * This Activity contains voucher, discount and cash operations.
 * 
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SubMenu extends Activity {

	private static SubMenu SubMenuId ;
	String tableNo;
	public float grandTotal;
	SQLiteDatabase db;
	int cash;
	int idNo;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.payment);
		SubMenuId = this;
		grandTotal = 0;
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		tableNo = bundle.getString("TABLE");
		TextView amountToPay = (TextView) findViewById(R.id.AmountToPay);
		amountToPay.setText("0.0");

		TextView discountAmount = (TextView) findViewById(R.id.discountAmount);
		discountAmount.setText("0.0");

		TextView grandTotalAmount = (TextView) findViewById(R.id.grandTotalAmount);
		grandTotalAmount.setText("0.0");

		TextView voucherAmount = (TextView) findViewById(R.id.voucherAmount);
		voucherAmount.setText("-"+ Group.getInstance().voucherAmount);

		TextView userName = (TextView) findViewById(R.id.userName);
		userName.setText("User: "+ MainActivity.getInstance().USERNAME);

		TextView dateTime = (TextView) findViewById(R.id.dateTimeView);
		String timeStamp = new SimpleDateFormat("dd/MM/yy HH:mm").format(Calendar.getInstance().getTime());
		dateTime.setText(""+timeStamp);

		grandTotal = Group.getInstance().subTotalAmount + Group.getInstance().taxAmount-
				Group.getInstance().voucherAmount- Group.getInstance().discountAmount;
		int taxFlag = bundle.getInt("TAXFLAG");
		float discountMoney = Group.getInstance().discountAmount;

		discountAmount.setText("-" + discountMoney);

		if(taxFlag == 1)
		{
			db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
			Cursor taxDetails = db.rawQuery("SELECT servicetaxper,taxper1,taxper2,taxper3 FROM tax;", null);

			int taxDetailsCount = taxDetails.getCount();
			float tax = 0;
			if(taxDetailsCount > 0)
			{	
				taxDetails.moveToFirst();
				tax = taxDetails.getFloat(taxDetails.getColumnIndex("servicetaxper"));
				tax += taxDetails.getFloat(taxDetails.getColumnIndex("taxper1"));
				tax += taxDetails.getFloat(taxDetails.getColumnIndex("taxper2"));
				tax += taxDetails.getFloat(taxDetails.getColumnIndex("taxper3"));

			}
			else
			{
			}
			db.close();

			Group.getInstance().taxAmount = (tax * grandTotal)/100;
			grandTotal  += (tax * grandTotal)/100;
		}

		TextView taxAmount = (TextView) findViewById(R.id.taxAmount);
		taxAmount.setText("+"+ Group.getInstance().taxAmount);

		amountToPay.setText(""+grandTotal);
		float constantTotal = Group.getInstance().totalAmount;

		grandTotalAmount.setText(""+constantTotal);
		ImageButton cardPay = (ImageButton)findViewById(R.id.imageButton1);
		ImageButton cashPay = (ImageButton)findViewById(R.id.imageButton2);
		ImageButton voucherPay = (ImageButton)findViewById(R.id.imageButton3);
		ImageButton discount = (ImageButton)findViewById(R.id.imageButton4);

		cardPay.setOnClickListener(new View.OnClickListener() {
			@SuppressLint("NewApi")
			public void onClick(View v) {
			}
		});

		cashPay.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				int key = 2;
				Intent intent=new Intent(SubMenu.this, Cash_Discount.class);
				intent.putExtra("KEY", key);
				intent.putExtra("TABLE", tableNo );
				startActivity(intent);
			}
		});

		voucherPay.setOnClickListener(new View.OnClickListener() {
			@SuppressLint("NewApi")
			public void onClick(View v) {
				Intent mIntent = new Intent();
				mIntent.setClass(getApplicationContext(), Cash_Discount.class);
				mIntent.putExtra("KEY", 3);
				mIntent.putExtra("TABLE", tableNo );
				startActivity(mIntent);

			}
		});

		discount.setOnClickListener(new View.OnClickListener() {
			@SuppressLint("NewApi")
			public void onClick(View v) {
				Intent mIntent = new Intent();
				mIntent.setClass(getApplicationContext(), Cash_Discount.class);
				mIntent.putExtra("TABLE", tableNo );
				mIntent.putExtra("KEY", 4);
				startActivity(mIntent);
			}
		});
	}
	protected void onResume() {
		super.onResume();
		if(MainActivity.getInstance() ==null || user_Login.user_LoginID==null){
			Intent userLoginIntent = new Intent();
			userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
			userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(userLoginIntent);
			this.finish();
		}
	};
	public void onBackPressed()
	{
		this.finish();
		Intent intent = new Intent(this, Payment.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra("TABLE", tableNo);
		startActivity(intent);
	}

	public static SubMenu getInstance(){
		return SubMenuId;
	}
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
		return super.dispatchTouchEvent(ev);
	}
}
