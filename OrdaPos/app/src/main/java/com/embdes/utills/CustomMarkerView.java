package com.embdes.utills;

import android.content.Context;
import android.widget.TextView;

import com.embdes.youcloud_resto.R;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;

import java.util.ArrayList;

/**
 * Created by Develpomenteight on 28/09/2017.
 */

public class CustomMarkerView extends MarkerView {
    ArrayList<String> mXLabels;
    private TextView tvContent;

    public CustomMarkerView(Context context, int layoutResource, ArrayList<String> xLabels) {
        super(context, layoutResource);
        mXLabels = xLabels;
        tvContent = (TextView) findViewById(R.id.tvContent);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        //get x value
//        String xVal = mXLabels.get(e.getXIndex())+ " , " + e.getVal();
        String xVal = mXLabels.get(e.getXIndex());
        tvContent.setText(xVal );
    }

    @Override
    public int getXOffset(float xpos) {
        // this will center the marker-view horizontally
        return -(getWidth() / 2);

    }

    @Override
    public int getYOffset(float ypos) {
        // this will cause the marker-view to be above the selected value
        return -getHeight();
    }
}
