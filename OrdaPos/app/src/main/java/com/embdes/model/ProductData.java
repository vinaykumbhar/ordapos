package com.embdes.model;

import java.util.ArrayList;

/**
 * Created by Develpomenteight on 19/09/2017.
 */

public class ProductData {
    String name="";
    String amount="";
    String quantity="";
    String datetime="";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public ArrayList<ProductData> getList() {
        return columnHeadersList;
    }

    public void setList(ArrayList<ProductData> columnHeadersList) {
        this.columnHeadersList = columnHeadersList;
    }

    ArrayList<ProductData> columnHeadersList =new ArrayList<>();
}
