package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.instabug.library.InstabugTrackingDelegate;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author EMBDES.
 * @version 1.0.
 *          This Activity display all menus available in home screen.
 */

@SuppressLint("NewApi")
public class Home extends Activity {

    SQLiteDatabase db;
    GridView listView;
    static Home HOMEID;
    TextView date, titlebar;
    public static boolean variable;
    Socket_Connection socket;
    int TAKEORDER = 0;
    int QuickService = 1;
    int DELIVERY = 2;
    int BILLSPENDINGTABLES = 3;
    int MANAGER = 4;
    int QUEUESEND = 5;
    int SETTINGS = 6;
    int VERSION = 7;
    int LOGOUT = 8;
    NetworkChangeReceiver mConnReceiver;

    //search/info
    TextView txt_info;

    static int flag = 0;
    GridViewAdapter adapter;
    int quickTable;
    String[] Menu = {"TAKE ORDER",
            "QUICK SERVICE",
            "DELIVERY", "PENDING ORDERS",
            "MANAGER",
            "QUEUE SEND",
            "SETTINGS", "VERSION", "LOGOUT"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HOMEID = this;
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.home_screen);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        titlebar = (TextView) findViewById(R.id.titelabar);
        date = (TextView) findViewById(R.id.date);


        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        titlebar.setText("MENU");
        listView = (GridView) findViewById(R.id.gridview);
      //  listView.setDividerHeight(0);
        adapter = new GridViewAdapter(this);
        listView.setAdapter(adapter);
        txt_info = (TextView) findViewById(R.id.txt_info);
        txt_info.setText("Select desired options");
        findViewById(R.id.lay_info).setVisibility(View.VISIBLE);
        findViewById(R.id.lay_search).setVisibility(View.GONE);
        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                if (arg2 == TAKEORDER) {
                    if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.TOrder) {
                        Intent intent = new Intent(Home.this, Group.class);
                        startActivity(intent);
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                Home.this, android.R.style.Theme_Dialog);
                        msg.setTitle("Opration Failed");
                        msg.setMessage("User Doesn't have the permission for this opration");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }
                } else if (arg2 == QUEUESEND) {

                    db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                    Cursor queueData = db.rawQuery("SELECT * FROM queuetable;", null);
                    int recCount = queueData.getCount();
                    db.close();
                    if (recCount > 0) {
                        MainActivity.getInstance().queueMessage = "1";
                        Intent userLoginIntent = new Intent();
                        userLoginIntent.setClass(getApplicationContext(), Socket_Connection.class);
                        userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(userLoginIntent);
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                Home.this);
                        msg.setTitle("");
                        msg.setMessage("Queue Is Empty To Send ...");
                        msg.setCancelable(true);
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }
                } else if (arg2 == QuickService) {

                    final SharedPreferences mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    int restoredText = mSharedPreference.getInt("tableno", 0);
                    if (restoredText != 0) {
                        quickTable = mSharedPreference.getInt("tableno", 0);
                        System.out.println("quick table : " + quickTable);
                        if (quickTable < 1500) {
                            quickTable = 1500;
                        } else {
                        }
                        if (quickTable > 1599) {
                            quickTable = 1500;
                        } else {

                        }
                    }
                    if (!db.isOpen())
                    db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                    Cursor pendindDetails = db.rawQuery("SELECT  locationid, tableno," +
                            " orderno FROM ordernos WHERE billflag = 0;", null);
                    while (pendindDetails.moveToNext()) {
                        int value = Integer.parseInt(pendindDetails.getString(1));
                        if (pendindDetails.getString(0).
                                equalsIgnoreCase(user_Login.user_LoginID.LOCATIONNO)) {
                            if (value > 1500 && value < 1599) {
                                quickTable = value;
                            }
                        }
                    }
                    db.close();
                    if (quickTable != 0) {
                        if (quickTable < 1500) {
                            quickTable = 1500;

                        } else {
                        }
                        if (quickTable >= 1599) {
                            quickTable = 1500;
                        } else {

                        }

                        Group.GROUPID.table = String.valueOf(quickTable + 1);
                        quickTable = Integer.parseInt(Group.GROUPID.table);
                        TablesFragment.COVERS = "1";
                        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                        Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" +
                                Group.GROUPID.table + " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);

                        int chairsCount = chairs.getCount();
                        chairs.moveToFirst();
                        if (chairsCount > 0) {
                            if (chairs.getInt(chairs.getColumnIndex("occupiedchairs")) < 1) {
                                Cursor c = db.rawQuery("SELECT noofchairs FROM tables WHERE tableno =" + Group.GROUPID.table +
                                        " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
                                int count = c.getCount();
                                c.moveToFirst();
                                if (count > 0) {
                                    int covers = c.getInt(c.getColumnIndex("noofchairs"));
                                    if (covers >= Integer.parseInt(TablesFragment.COVERS)) {
                                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                        SharedPreferences.Editor editor = prefs.edit();
                                        if (chairs.getInt(chairs.getColumnIndex("occupiedchairs")) > 1) {
                                            editor.putInt("tableno", quickTable);

                                            editor.commit();
                                        }
                                        variable = true;
                                        CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away");
                                        CartStaticSragment.CARTStaticSragment.tableno.setText("#####");
                                        CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
                                        CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
                                        CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
                                        CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
                                        Intent intent = new Intent(Home.this, Group.class);
                                        startActivity(intent);
                                    } else {
                                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                                Home.this, android.R.style.Theme_Dialog);
                                        msg.setTitle("");
                                        msg.setMessage("Covers Should Not Exceed " + covers);
                                        msg.setPositiveButton("Ok", null);
                                        msg.show();
                                    }
                                } else {
                                    AlertDialog.Builder msg = new AlertDialog.Builder(
                                            Home.this, android.R.style.Theme_Dialog);
                                    msg.setTitle("");
                                    msg.setMessage("Table is Not Available");
                                    msg.setPositiveButton("Ok", null);
                                    msg.show();
                                }

                            } else {
                                variable = true;
                                CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away");
                                CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
                                CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
                                CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
                                CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
                                Intent intent = new Intent(Home.this, Group.class);
                                startActivity(intent);

                            }
                        } else {
                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                    Home.this, android.R.style.Theme_Dialog);
                            msg.setTitle("");
                            msg.setMessage("Table Is Not Available");
                            msg.setPositiveButton("Ok", null);
                            msg.show();

                        }
                        db.close();
                    } else {
                        Group.GROUPID.table = "1501";
                        quickTable = Integer.parseInt(Group.GROUPID.table);
                        TablesFragment.COVERS = "1";
                        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                        Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" +
                                Group.GROUPID.table + " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);

                        int chairsCount = chairs.getCount();
                        chairs.moveToFirst();
                        if (chairsCount > 0) {
                            if (chairs.getInt(chairs.getColumnIndex("occupiedchairs")) < 1) {
                                Cursor c = db.rawQuery("SELECT noofchairs FROM tables WHERE tableno =" + Group.GROUPID.table +
                                        " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
                                int count = c.getCount();
                                c.moveToFirst();
                                if (count > 0) {
                                    int covers = c.getInt(c.getColumnIndex("noofchairs"));
                                    if (covers >= Integer.parseInt(TablesFragment.COVERS)) {
                                        variable = true;
                                        Intent intent = new Intent(Home.this, Group.class);
                                        startActivity(intent);

                                    } else {
                                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                                Home.this, android.R.style.Theme_Dialog);
                                        msg.setTitle("");
                                        msg.setMessage("Covers Should Not Exceed " + covers);
                                        msg.setPositiveButton("Ok", null);
                                        msg.show();
                                    }

                                } else {

                                    AlertDialog.Builder msg = new AlertDialog.Builder(
                                            Home.this, android.R.style.Theme_Dialog);
                                    msg.setTitle("");
                                    msg.setMessage("Table is Not Available");
                                    msg.setPositiveButton("Ok", null);
                                    msg.show();
                                }

                            } else {
                                variable = true;
                                CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away");
                                CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
                                CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
                                CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
                                CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
                                Intent intent = new Intent(Home.this, Group.class);
                                startActivity(intent);
                            }
                        } else {
                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                    Home.this, android.R.style.Theme_Dialog);
                            msg.setTitle("");
                            msg.setMessage("Table Is Not Available");
                            msg.setPositiveButton("Ok", null);
                            msg.show();
                        }
                        db.close();
                    }
                } else if (arg2 == DELIVERY) {
                    Intent i = new Intent(Home.this, CheckUserRecord.class);
                    startActivity(i);
//                    try {
//                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
//                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
//                        final ComponentName cn = new ComponentName("com.rsl.restaurantapp", "com.rsl.restaurantapp.LandingPageActivity");
//                        intent.setComponent(cn);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                    } catch (ActivityNotFoundException e) {
//                        Toast.makeText(getApplicationContext(),
//                                "OrdaMia application not installed", Toast.LENGTH_LONG).show();
//                    }

                } else if (arg2 == MANAGER) {
                    Intent i = new Intent(Home.this, ManagerReports.class);
                    startActivity(i);

                   /* if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.BPrint) {
                        Intent mainreport = new Intent(Home.this, MainreportViewFSR.class);
                        startActivity(mainreport);
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                Home.this, android.R.style.Theme_Dialog);
                        msg.setTitle("Opration Failed");
                        msg.setMessage("User Doesn't have the permission for this opration");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }*/
                }/* else if (arg2 == PRODUCTREPORTS) {
                    if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.BPrint) {
                        Intent productreport = new Intent(Home.this, PRODUCTREPORTS.class);
                        startActivity(productreport);
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                Home.this, android.R.style.Theme_Dialog);
                        msg.setTitle("Opration Failed");
                        msg.setMessage("User Doesn't have the permission for this opration");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }
                }*/ else if ((arg2 == BILLSPENDINGTABLES)) {
                    Intent pendingbill = new Intent(Home.this, BILLSPENDINGTABLES.class);
                    startActivity(pendingbill);
                }/* else if (arg2 == Update) {
                    if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.MTUpdate) {
                        AlertDialog.Builder adb = new AlertDialog.Builder(
                                Home.this, android.R.style.Theme_Dialog);
                        adb.setTitle("");
                        adb.setMessage("Do You Want To Update");
                        adb.setPositiveButton("Yes", new
                                DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Group.GROUPID.logout();
                                        finish();
                                        MainActivity.getInstance().updateflag = 1;
                                        Intent mIntent = new Intent();
                                        mIntent.setClass(getApplicationContext(), UpdateData.class);
                                        startActivity(mIntent);
                                    }
                                });
                        adb.setNegativeButton("No", new
                                DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                        adb.show();
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                Home.this, android.R.style.Theme_Dialog);
                        msg.setTitle("Opration Failed");
                        msg.setMessage("User Doesn't have the permission for this opration");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }
                }*/
//                else if (arg2 == Reset_Terminal) {
//                    if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.MTUpdate) {
//                        final AlertDialog.Builder mesg = new AlertDialog.Builder(Home.this, android.R.style.Theme_Dialog);
//                        mesg.setMessage("	It Will Delete All Old Data \n	 Do you want to Continue ?");
//                        mesg.setNegativeButton("No", null);
//                        mesg.setPositiveButton("Yes", new OnClickListener() {
//
//                            public void onClick(DialogInterface dialog, int which) {
//                                TablesFragment.TABLESID.stop();
//                                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
//                                db.execSQL("DROP TABLE IF EXISTS product ;");
//                                db.execSQL("DROP TABLE IF EXISTS userterminal;");
//                                db.execSQL("DROP TABLE IF EXISTS loginrecord;");
//                                db.execSQL("Drop Table IF EXISTS location;");
//                                db.execSQL("DROP TABLE IF EXISTS masterterminal;");
//                                db.execSQL("Drop Table IF EXISTS grouptable;");
//                                db.execSQL("Drop Table IF EXISTS category;");
//                                db.execSQL("DROP TABLE IF EXISTS type;");
//                                db.execSQL("DROP TABLE IF EXISTS tables;");
//                                db.execSQL("DROP TABLE IF EXISTS receipt;");
//                                db.execSQL("DROP TABLE IF EXISTS ordernos;");
//                                db.execSQL("DROP TABLE IF EXISTS cart;");
//                                db.execSQL("DROP TABLE IF EXISTS locationprinter;");
//                                db.execSQL("DROP TABLE IF EXISTS locationprice;");
//                                db.execSQL("DROP TABLE IF EXISTS queuetable;");
//                                db.close();
//                                finish();
//                                Intent registration = new Intent();
//                                registration.setClass(Home.this, MainActivity.class);
//                                registration.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                startActivity(registration);
//                            }
//                        });
//                        mesg.show();
//                    } else {
//                        AlertDialog.Builder msg = new AlertDialog.Builder(
//                                Home.this, android.R.style.Theme_Dialog);
//                        msg.setTitle("Opration Failed");
//                        msg.setMessage("User Doesn't have the permission for this opration");
//                        msg.setPositiveButton("Ok", null);
//                        msg.show();
//                    }
//                }
                else if (arg2 == SETTINGS) {
                    Intent i = new Intent(Home.this, Settingoption.class);
                    startActivity(i);
                } else if (arg2 == VERSION) {
                    Intent ver = new Intent(Home.this, Version.class);
                    startActivity(ver);

                } else if (arg2 == LOGOUT) {
                    AlertDialog.Builder mesg = new AlertDialog.Builder(Home.this,
                            android.R.style.Theme_Dialog);
                    mesg.setMessage("Do You Want To Exit Application ?");
                    mesg.setNegativeButton("No", null);
                    mesg.setPositiveButton("Yes", new OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            Group.GROUPID.logout();
                        }
                    });
                    mesg.show();
                }
            }
        });
    }

    protected void onResume() {
        super.onResume();
        if (MainActivity.getInstance() == null || user_Login.user_LoginID == null) {
            Intent userLoginIntent = new Intent(); //Created new Intent to
            userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
            userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(userLoginIntent);
            this.finish();
        } else {
            if (MainActivity.networkcheck == false) {
                mConnReceiver = new NetworkChangeReceiver();
                registerReceiver(mConnReceiver,
                        new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            }
        }

    }

    ;

    /* (non-Javadoc)
     * @see android.app.Activity#onDestroy()
     */
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        MainActivity.networkcheck = false;
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onPause()
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (!MainActivity.networkcheck && mConnReceiver != null) {
            unregisterReceiver(mConnReceiver);
        }
    }

    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {

            if (checkInternet(context)) {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                Cursor queueData = db.rawQuery("SELECT * FROM queuetable;", null);
                int recCount = queueData.getCount();
                db.close();
                if (recCount > 0) {
                    System.out.println("In Quesend ");
                    MainActivity.getInstance().queueMessage = "1";
                    Intent userLoginIntent = new Intent(); //Created new Intent to
                    userLoginIntent.setClass(getApplicationContext(), Socket_Connection.class);
                    userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(userLoginIntent);
                } else {
                }
            } else {
            }

        }


        boolean checkInternet(Context context) {
            ServiceManager serviceManager = new ServiceManager(context);
            if (serviceManager.isNetworkAvailable()) {
                return true;
            } else {
                return false;
            }
        }

    }


    @SuppressLint("NewApi")
    public void onBackPressed() {
        AlertDialog.Builder mesg = new AlertDialog.Builder(Home.this, android.R.style.Theme_Dialog);
        mesg.setMessage("Do You Want To Logout ?\n");
        mesg.setNegativeButton("No", null);
        mesg.setPositiveButton("Yes", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Group.GROUPID.logout();
            }
        });
        mesg.show();
    }

    public class GridViewAdapter extends BaseAdapter {

        // Declare variables
        private Activity activity;

        private LayoutInflater inflater = null;

        public GridViewAdapter(Activity a) {
            activity = a;

            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            System.out.println("getCount " + Menu.length);
            return Menu.length;
        }

        public Object getItem(int position) {
            System.out.println("Object getItem " + position);
            return position;
        }

        public long getItemId(int position) {
            System.out.println("getItemId " + position);
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            System.out.println("View " + position);
            View vi = convertView;
            if (convertView == null)
                System.out.println("IF " + position);
            vi = inflater.inflate(R.layout.gridview_item, null);
            TextView text = (TextView) vi.findViewById(R.id.text);
            text.setText(Menu[position]);

            return vi;
        }
    }

    public void stop() {
        TablesFragment.TABLESID.handler.removeMessages(0);
    }

    /**
     * Here we are creating instance for class
     *
     * @param
     * @return Instance
     */
    public static Home getInstance() {
        return HOMEID;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
