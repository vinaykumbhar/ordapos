package com.embdes.youcloud_resto;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.renderscript.ScriptGroup;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class GetUserInfo extends Activity {
    TextView date, titlebar;
    //search/info on title bar
    TextView txt_info;
    EditText edtusername, edtmobilenumber, edtuseraddress;
    String consumer_id = "";
    Button btn_add_consumer;
    SQLiteDatabase db;
    int deliveryTable;
    static GetUserInfo User;
    public static boolean deliveryvariable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_get_user_info);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        User = this;
        initview();
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Bundle b = getIntent().getExtras();
        assert b != null;
        consumer_id = b.getString("cid");
        edtusername.setText(b.getString("cname"));
        edtmobilenumber.setText(b.getString("cmobileno"));
        edtuseraddress.setText(b.getString("caddress"));
        btn_add_consumer.setOnClickListener(proceedbtnclickListenter);

    }

    View.OnClickListener proceedbtnclickListenter = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String strName = edtusername.getText().toString();
            String strAddress = edtuseraddress.getText().toString();
            String strMobile = edtmobilenumber.getText().toString();
            if (strName.equals("")) {
                edtusername.setError(getResources().getString(R.string.enter_cname));
                edtusername.requestFocus();
            } else if (strName.length() < 3) {
                edtusername.setError(getResources().getString(R.string.enter_minlimitname));
                edtusername.requestFocus();
            } else if (strAddress.equals("")) {
                edtuseraddress.setError(getResources().getString(R.string.enter_caddress));
                edtuseraddress.requestFocus();
            } else if (strAddress.length() < 3) {
                edtuseraddress.setError(getResources().getString(R.string.enter_minlimitaddress));
                edtuseraddress.requestFocus();
            } else if(consumer_id.equals("0")){
                ContentValues values = new ContentValues();
                values.put("consumerid", "00");
                values.put("cname", strName);
                values.put("cmobileno", "" + strMobile);
                values.put("caddress", "" + strAddress);
                long id=  db.insert("consumerinfo",null,values);
                Group.GROUPID.CONSUMERID=String.valueOf(id);
                db.close();
                takeOrder();
            }else{
                db.execSQL("UPDATE consumerinfo SET consumerid = '00', cname = '" + strName +"', caddress='"+strAddress+"' WHERE cmobileno = '" + strMobile + "' AND cid =" + consumer_id + ";");
                db.close();
                Group.GROUPID.CONSUMERID=consumer_id;
                takeOrder();
            }
        }
    };

    private void initview() {
        date = (TextView) findViewById(R.id.date);
        titlebar = (TextView) findViewById(R.id.titelabar);
        Button menu = (Button) findViewById(R.id.Imageview);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GetUserInfo.this, Home.class);
                startActivity(intent);
            }
        });
        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        titlebar.setText("DELIVERY");
        txt_info = (TextView) findViewById(R.id.txt_info);
        txt_info.setText("Consumer Details");
        findViewById(R.id.lay_info).setVisibility(View.VISIBLE);
        findViewById(R.id.lay_search).setVisibility(View.GONE);
        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);
        edtusername = (EditText) findViewById(R.id.edtusername);
        btn_add_consumer = (Button) findViewById(R.id.btn_add_consumer);
        edtmobilenumber = (EditText) findViewById(R.id.edtmobilenumber);
        edtuseraddress = (EditText) findViewById(R.id.edtuseraddress);
        edtmobilenumber.setInputType(InputType.TYPE_NULL);
        edtmobilenumber.setFocusable(false);
    }


    private void takeOrder() {
        final SharedPreferences mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int restoredText = mSharedPreference.getInt("deliverytable", 0);
        int tableno = mSharedPreference.getInt("tableno", 0);

        if (restoredText != 0) {
            deliveryTable = mSharedPreference.getInt("deliverytable", 0);
            System.out.println("deliveryTable : " + deliveryTable);
            if (deliveryTable < 2000) {
                deliveryTable = 2000;
            } else {
            }
            if (deliveryTable >= 2100) {
                deliveryTable = 2000;
            } else {

            }
        }
        if (!db.isOpen())
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor pendindDetails = db.rawQuery("SELECT  locationid, tableno, orderno FROM ordernos WHERE billflag = 0;", null);
        while (pendindDetails.moveToNext()) {
            int value = Integer.parseInt(pendindDetails.getString(1));
            if (pendindDetails.getString(0).
                    equalsIgnoreCase(user_Login.user_LoginID.LOCATIONNO)) {
                if (value >= 2000 && value <= 2100) {
                    deliveryTable = value;
                }
            }
        }
        db.close();
        if (deliveryTable != 0) {
            if (deliveryTable < 2000) {
                deliveryTable = 2000;

            } else {
            }
            if (deliveryTable >= 2100) {
                deliveryTable = 2000;
            } else {

            }

            Group.GROUPID.table = String.valueOf(deliveryTable + 1);
            deliveryTable = Integer.parseInt(Group.GROUPID.table);
            TablesFragment.COVERS = "1";
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" +Group.GROUPID.table + " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);

            int chairsCount = chairs.getCount();
            chairs.moveToFirst();
            if (chairsCount > 0) {
                if (chairs.getInt(chairs.getColumnIndex("occupiedchairs")) < 1) {
                    Cursor c = db.rawQuery("SELECT noofchairs FROM tables WHERE tableno =" + Group.GROUPID.table +
                            " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
                    int count = c.getCount();
                    c.moveToFirst();
                    if (count > 0) {
                        int covers = c.getInt(c.getColumnIndex("noofchairs"));
                        if (covers >= Integer.parseInt(TablesFragment.COVERS)) {
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = prefs.edit();
                            if (chairs.getInt(chairs.getColumnIndex("occupiedchairs")) > 1) {
                                editor.putInt("deliverytable", deliveryTable);
                                editor.commit();
                            }
                            deliveryvariable = true;
                            CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away");
                            CartStaticSragment.CARTStaticSragment.tableno.setText("#####");
                            CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
                            CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
                            CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
                            CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
                            Intent intent = new Intent(GetUserInfo.this, Group.class);
                            startActivity(intent);
                        } else {
                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                    GetUserInfo.this, android.R.style.Theme_Dialog);
                            msg.setTitle("");
                            msg.setMessage("Covers Should Not Exceed " + covers);
                            msg.setPositiveButton("Ok", null);
                            msg.show();
                        }
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                GetUserInfo.this, android.R.style.Theme_Dialog);
                        msg.setTitle("");
                        msg.setMessage("Table is Not Available");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }

                } else {
                    deliveryvariable = true;
                    CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away");
                    CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
                    CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
                    CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
                    CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(GetUserInfo.this, Group.class);
                    startActivity(intent);

                }
            } else {
                AlertDialog.Builder msg = new AlertDialog.Builder(
                        GetUserInfo.this, android.R.style.Theme_Dialog);
                msg.setTitle("");
                msg.setMessage("Table Is Not Available");
                msg.setPositiveButton("Ok", null);
                msg.show();

            }
            db.close();
        } else {
            Group.GROUPID.table = "2000";
            deliveryTable = Integer.parseInt(Group.GROUPID.table);
            TablesFragment.COVERS = "1";
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" +
                    Group.GROUPID.table + " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
            int chairsCount = chairs.getCount();
            chairs.moveToFirst();
            if (chairsCount > 0) {
                if (chairs.getInt(chairs.getColumnIndex("occupiedchairs")) < 1) {
                    Cursor c = db.rawQuery("SELECT noofchairs FROM tables WHERE tableno =" + Group.GROUPID.table +
                            " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
                    int count = c.getCount();
                    c.moveToFirst();
                    if (count > 0) {
                        int covers = c.getInt(c.getColumnIndex("noofchairs"));
                        if (covers >= Integer.parseInt(TablesFragment.COVERS)) {
                            deliveryvariable = true;
                            Intent intent = new Intent(GetUserInfo.this, Group.class);
                            startActivity(intent);

                        } else {
                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                    GetUserInfo.this, android.R.style.Theme_Dialog);
                            msg.setTitle("");
                            msg.setMessage("Covers Should Not Exceed " + covers);
                            msg.setPositiveButton("Ok", null);
                            msg.show();
                        }
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                GetUserInfo.this, android.R.style.Theme_Dialog);
                        msg.setTitle("");
                        msg.setMessage("Table is Not Available");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }

                } else {
                    deliveryvariable = true;
                    CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away");
                    CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
                    CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
                    CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
                    CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(GetUserInfo.this, Group.class);
                    startActivity(intent);
                }
            } else {
                AlertDialog.Builder msg = new AlertDialog.Builder(
                        GetUserInfo.this, android.R.style.Theme_Dialog);
                msg.setTitle("");
                msg.setMessage("Table Is Not Available");
                msg.setPositiveButton("Ok", null);
                msg.show();
            }
            db.close();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
