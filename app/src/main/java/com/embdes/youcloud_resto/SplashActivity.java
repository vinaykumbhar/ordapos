package com.embdes.youcloud_resto;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(4 * 1000);
                    // After 5 seconds redirect to another intent
                    Intent i;
                    i = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(i);
                    // Remove activity
                    finish();
                } catch (Exception e) {

                }
            }
        };

        // start thread
        background.start();
    }
}
