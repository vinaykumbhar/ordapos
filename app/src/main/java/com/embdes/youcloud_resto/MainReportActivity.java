/**
 *
 */
package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.instabug.library.InstabugTrackingDelegate;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author Vinay
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MainReportActivity extends Activity {
    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    android.app.FragmentTransaction fragmentTransaction;
    android.app.FragmentManager fragmentManager;
    SalesSummaryFragment salesSummaryFragment;
    SalesReportFragment salesReportFragment;

    Button btn_summary, btn_reports;
    SQLiteDatabase db;
    TextView titlebar, date;
    static MainReportActivity mainreportView;
    //search/info
    TextView txt_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.main_reports_activity);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        date = (TextView) findViewById(R.id.date);
        titlebar = (TextView) findViewById(R.id.titelabar);
        Button menu = (Button) findViewById(R.id.Imageview);
        mainreportView = this;
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainReportActivity.this, Home.class);
                startActivity(intent);
            }
        });
        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        titlebar.setText("Main Reports");
        salesSummaryFragment = new SalesSummaryFragment();
        salesReportFragment = new SalesReportFragment();
        fragmentManager = getFragmentManager();

        txt_info = (TextView) findViewById(R.id.txt_info);
        txt_info.setText("Select report type to view");
        findViewById(R.id.lay_info).setVisibility(View.VISIBLE);
        findViewById(R.id.lay_search).setVisibility(View.GONE);

        String timeStamp = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(Calendar.getInstance().getTime());
        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);
        btn_summary = (Button) findViewById(R.id.btn_summary);
        btn_reports = (Button) findViewById(R.id.btn_reports);

        if (!salesSummaryFragment.isResumed()) {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_reports, salesSummaryFragment);
            fragmentTransaction.commit();
        }

        btn_summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!salesSummaryFragment.isResumed()) {
                    btn_summary.setBackground(getResources().getDrawable(R.drawable.btn_bg_pressed));
                    btn_reports.setBackground(getResources().getDrawable(R.drawable.btn_bg_normal));
                    salesReportFragment.GROUPID.spinner_foodCategory.setSelection(0);
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_reports, salesSummaryFragment);
                    fragmentTransaction.commit();
                }
            }
        });
        btn_reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!salesReportFragment.isResumed()) {
                    btn_summary.setBackground(getResources().getDrawable(R.drawable.btn_bg_normal));
                    btn_reports.setBackground(getResources().getDrawable(R.drawable.btn_bg_pressed));
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_reports, salesReportFragment);
                    fragmentTransaction.commit();
                }
            }
        });
    }

    protected void onResume() {
        super.onResume();
        if (MainActivity.getInstance() == null || user_Login.user_LoginID == null) {
            Intent userLoginIntent = new Intent(); //Created new Intent to
            userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
            userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(userLoginIntent);
            this.finish();
        }
    }

    /**
     * Dialog will come when user press date button in reports screen.
     */
    protected Dialog onCreateDialog(int id) {
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.container_reports);
        if (currentFragment instanceof SalesSummaryFragment) {
            return salesSummaryFragment.onCreateDialog(id);
        } else
            return salesReportFragment.onCreateDialog(id);
    }

    //	/**
//	 * Here we are creating instance for class
//	 * @return Instance
//	 */
    public static MainReportActivity getInstance() {
        return mainreportView;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
