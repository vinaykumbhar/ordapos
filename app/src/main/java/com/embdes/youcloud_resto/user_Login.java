package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Typeface;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.instabug.library.InstabugTrackingDelegate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

@SuppressLint("NewApi")
public class user_Login extends Activity {
    SQLiteDatabase db;
    static String nameid;
    TextView date;
    String terminal;
    Spinner username, location;
    TextView blank;
    String[] locationidname;
    EditText passWord;
    View view;
    TextView titlebar;
    ImageView imageView;
    TextView store, user;
    TextView storename, useranme1;
    BluetoothAdapter mBluetoothAdapter;
    public static user_Login user_LoginID;
    private static final int REQUEST_ENABLE_BT = 2;
    public String LOCATIONNO;
    int userpermissionlocation = 0;
    int userpermissionmenu = 0;
    int userid = 0;
    int locationPermission = 0;
    public int TOrder = 50;
    public int PBill = 70;
    public int Tchange = 60;
    public int MTUpdate = 80;
    public int BPrint = 60;
    //search/info
    TextView txt_info;

    Boolean isWifiPrint = false;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        @SuppressLint("WifiManagerLeak") WifiManager wifiManager = (WifiManager) user_Login.this.getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }
        user_LoginID = this;
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_main);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        ImageButton toSetButton = (ImageButton) findViewById(R.id.toButton);
        ImageButton fromSetButton = (ImageButton) findViewById(R.id.fromButton);
        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);
        txt_info = (TextView) findViewById(R.id.txt_info);
        findViewById(R.id.lay_info).setVisibility(View.VISIBLE);
        findViewById(R.id.lay_search).setVisibility(View.GONE);

        txt_info.setText("Enter Usercode and Password to log in.");
        Button menu = (Button) findViewById(R.id.Imageview);
        date = (TextView) findViewById(R.id.date);
        titlebar = (TextView) findViewById(R.id.titelabar);
        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        titlebar.setText("Login");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        blank = (TextView) findViewById(R.id.blank);
        if (mBluetoothAdapter == null) {

        }
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent =
                    new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        int[] androidColors = getResources().getIntArray(R.array.colors);

        userpermissionlocation = 0;
        userpermissionmenu = 0;
        userid = 0;
        locationPermission = 0;
        passWord = (EditText) findViewById(R.id.password);
        imageView = (ImageView) findViewById(R.id.logo);
        store = (TextView) findViewById(R.id.Cname);
        user = (TextView) findViewById(R.id.user);
        storename = (TextView) findViewById(R.id.Cname1);
        //useranme1 = (TextView) findViewById(R.id.userna);
        passWord.setFocusable(true);
        passWord.setTypeface(Typeface.DEFAULT);
        passWord.setTextSize(14);
        String cardName = Environment.getExternalStorageDirectory().getPath();
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE if not exists userterminal(userid INTEGER NOT NULL,username VARCHAR[35] NOT NULL,password VARCHAR[35] NOT NULL,permissionsmenu " +
                "INTEGER NOT NULL,permissionterminal INTEGER NOT NULL,permissionslocation INTEGER NOT NULL);");
        ArrayList<String> nameArray = new ArrayList<String>();
        username = (Spinner) findViewById(R.id.spinner1);
        Cursor c = db.rawQuery("SELECT username FROM userterminal;", null);
        int count = c.getCount();
        c.moveToFirst();
        for (Integer j = 0; j < count; j++) {
            String name = "";
            name = c.getString(c.getColumnIndex("username"));
            nameArray.add(name);
            c.moveToNext();
        }
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(getApplicationContext(), R.layout.loginspinner, nameArray);
        username.setAdapter(adapter);


        ArrayList<String> locName = new ArrayList<String>();
        location = (Spinner) findViewById(R.id.locspinner);
        Cursor data = db.rawQuery("SELECT * FROM location;", null);
        int loccount = data.getCount();
        data.moveToFirst();
        for (Integer j = 0; j < loccount; j++) {
            String name = "";
            String locationname = "";
            name = data.getString(data.getColumnIndex("locationid"));
            locationname = data.getString(data.getColumnIndex("locationname"));
            locName.add(name + ": " + locationname);
            data.moveToNext();
        }
        ArrayAdapter<String> locadapter =
                new ArrayAdapter<String>(getApplicationContext(), R.layout.loginspinner, locName);
        location.setAdapter(locadapter);
        Button okButton = (Button) findViewById(R.id.ok);
        okButton.setOnClickListener(okhandler);
        db.close();

    }

    View.OnClickListener okhandler = new View.OnClickListener() {
        @SuppressLint("NewApi")
        public void onClick(View v) {
            nameid = String.valueOf(username.getItemAtPosition(username.getSelectedItemPosition()));
            locationidname = (location.getItemAtPosition(location.getSelectedItemPosition()).toString()).split(":");
            terminal = String.valueOf(location.getItemAtPosition(location.getSelectedItemPosition()));
            EditText passWord = (EditText) findViewById(R.id.password);
            if (passWord.toString().length() == 0 && passWord.toString().length() >= 10) {
                passWord.setText("");

            } else {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                Cursor login = db.rawQuery("SELECT password,permissionsmenu,permissionterminal,permissionslocation,userid from userterminal;", null);
                int logcount = login.getCount();
                if (logcount > 0) {
                    int userpermissionterminal = 0;
                    Cursor c = db.rawQuery("SELECT permissionsmenu,permissionterminal,permissionslocation,userid from userterminal WHERE username ='" + username.getSelectedItem().toString() + "' AND password ='" + passWord.getText().toString() + "';", null);
                    int count = c.getCount();
                    if (count > 0) {
                        c.moveToFirst();
                        userid = c.getInt(3);
                        userpermissionlocation = c.getInt(2);
                        userpermissionmenu = c.getInt(0);
                        userpermissionterminal = c.getInt(1);
                        Cursor masterterminalpermission = db.rawQuery("SELECT terminalpermisiions from masterterminal;", null);
                        int permissioncount = masterterminalpermission.getCount();
                        if (permissioncount > 0) {
                            masterterminalpermission.moveToFirst();
                            if (userpermissionterminal >= masterterminalpermission.getInt(0)) {
                                Cursor locationpermissionCursor = db.rawQuery("SELECT permissionslocation FROM location WHERE locationid= " + locationidname[0], null);
                                int locationpermissionCount = locationpermissionCursor.getCount();
                                if (locationpermissionCount > 0) {
                                    locationpermissionCursor.moveToFirst();
                                    locationPermission = locationpermissionCursor.getInt(0);
                                    if (userpermissionlocation >= locationPermission) {
                                        Toast.makeText(user_Login.this, "User Login Success ", Toast.LENGTH_SHORT)
                                                .show();
                                        MainActivity.getInstance();
                                        MainActivity.getInstance().USERNAME = username.getSelectedItem().toString();
                                        passWord.getText().clear();
                                        String timeStamp = new SimpleDateFormat("yy/MM/dd HH:mm").format(Calendar.getInstance().getTime());
                                        db.beginTransaction();
                                        SQLiteStatement statement = db.compileStatement("INSERT INTO loginrecord VALUES((select userid from userterminal where username =" +
                                                " '" + MainActivity.getInstance().USERNAME + "'),'" + timeStamp + "', 0, " +
                                                locationidname[0] + ", 0);");
                                        statement.executeInsert();
                                        db.execSQL("UPDATE masterterminal SET lastloginusername='" + MainActivity.getInstance().USERNAME + "';");
                                        Cursor userId = db.rawQuery("SELECT userid from userterminal WHERE username =" +
                                                "'" + MainActivity.getInstance().USERNAME + "';", null);
                                        userId.moveToFirst();
                                        String userid = userId.getString(0);
                                        db.setTransactionSuccessful();
                                        db.endTransaction();
                                        if (MainActivity.getInstance() == null) {
                                            MainActivity.getInstance();
                                        } else {
                                        }
                                        user_Login.user_LoginID.LOCATIONNO = locationidname[0];
                                        MainActivity.getInstance().messageid = "M27";
                                        MainActivity.getInstance().checkid = 1;
                                        MainActivity.getInstance().messagecontentarray = "1111";
                                        MainActivity.getInstance().noofpackets = 1;
                                        MainActivity.getInstance().packetno = 1;
                                        MainActivity.getInstance().message = userid + "|" + timeStamp + "|" + "0" + "|" +
                                                locationidname[0];
                                        MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
                                        MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
                                        Toast.makeText(user_Login.this, "Send Login Data To Server ... ", Toast.LENGTH_SHORT)
                                                .show();
                                        db.close();
                                        Intent mIntent = new Intent(); //Created new Intent to
                                        mIntent.setClass(getApplicationContext(), Group.class);
                                        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(mIntent);
                                    } else {
                                        passWord.getText().clear();
                                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                                user_Login.this, android.R.style.Theme_Dialog);
                                        msg.setTitle("");
                                        msg.setMessage("User Don't have the permission to login this location");
                                        msg.setPositiveButton("OK", null);
                                        msg.show();
                                    }
                                }


                            } else {
                                passWord.getText().clear();
                                AlertDialog.Builder msg = new AlertDialog.Builder(
                                        user_Login.this, android.R.style.Theme_Dialog);
                                msg.setTitle("");
                                msg.setMessage("User Don't have the permission to login");
                                msg.setPositiveButton("OK", null);
                                msg.show();

                            }
                        }
                    } else {
                        passWord.getText().clear();
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                user_Login.this, android.R.style.Theme_Dialog);
                        msg.setTitle("");
                        msg.setMessage("User Login Fails");
                        msg.setPositiveButton("OK", null);
                        msg.show();
                    }
                    db.close();
                } else {
                    db.close();
                    passWord.getText().clear();
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            user_Login.this, android.R.style.Theme_Dialog);
                    msg.setTitle("");
                    msg.setMessage("Update terminal before start");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                }
                db.close();
            }
        }
    };

    protected void onPause() {
        super.onPause();

    }

    ;

    protected void onResume() {
        super.onResume();
        if (MainActivity.getInstance() == null || user_Login.user_LoginID == null) {
            Intent userLoginIntent = new Intent(); //Created new Intent to
            userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
            userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(userLoginIntent);
            this.finish();
        }
    }

    /**
     * @return
     */
    public static Object getInstance() {
        return user_LoginID;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
