package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.instabug.library.InstabugTrackingDelegate;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author EMBDES.
 * @version 1.0.
 * This Activity display order details based on the location and table number.
 * 
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Payment extends Activity {

	ScrollView sview ;
	TableRow bottomRow;
	TableRow colorRow;
	TableRow titleRow;
	TextView productName, textView2, textView3, textView4, textView5, textView6, textView;
	SQLiteDatabase db;
	TextView date,titlebar;
	TableLayout t,tableLayout;
	public TableLayout screenLayout;
	public static String clicksno="0";
	String oldsno="0";
	String editLocation ;
	String editTable ;
	String tableToPay;
	float grandTotal;
	static Payment PayId;
	private String  grand;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Group.getInstance().discountAmount = 0;
		Group.getInstance().totalAmount = 0;
		Group.getInstance().voucherAmount = 0;
		Group.getInstance().taxAmount = 0;
		Group.getInstance().subTotalAmount =0;
		grandTotal = 0;
		Intent intent = getIntent();
		tableToPay = "0";
		Bundle bundle = intent.getExtras();
		tableToPay = bundle.getString("TABLE");
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.cart_screen);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.titlemenu);
		ImageButton toSetButton = (ImageButton) findViewById(R.id.toButton);
		ImageButton fromSetButton = (ImageButton) findViewById(R.id.fromButton);
		Button menu = (Button) findViewById(R.id.Imageview);
		menu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent  =new Intent(Payment.this,Home.class);
				startActivity(intent);
			}
		});
		date = (TextView) findViewById(R.id.date);
		titlebar = (TextView) findViewById(R.id.titelabar);
		final Calendar now = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM");
		String nowDate = formatter.format(now.getTime());
		date.setText(nowDate);
		titlebar.setText("Payment");
		tableLayout = (TableLayout) findViewById(R.id.table);
		TableRow grandTotalLayout = (TableRow) findViewById(R.id.grandTotalRow);
		grandTotalLayout.bringToFront();
		sview = new ScrollView(getApplicationContext());
		colorRow = new TableRow (getApplicationContext());
		InsertTable();
	}
	protected void onResume() {
		super.onResume();
		if(MainActivity.getInstance() ==null || user_Login.user_LoginID==null){
		   Intent userLoginIntent = new Intent(); //Created new Intent to
		   userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
		   userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		   startActivity(userLoginIntent);
		   this.finish();
		}
	};

	/**
	 * void  InsertTable()
	 * @param : NULL
	 * @return: NULL
	 * 
	 */

	public void InsertTable() {
		titleRow = (TableRow) findViewById(R.id.tableRow100);
		screenLayout = new TableLayout(getApplicationContext());
		editLocation = user_Login.user_LoginID.LOCATIONNO;
		editTable = tableToPay ;
		setTitle("	Table No :"+editTable+"   BILL PAY    Locatio Id :"+editLocation);
		if((int)editLocation.length() != 0 && (int)editTable.length() != 0)
		{
			int count=1;
			if(count == 0)
			{
			}
			else
			{
				productName = new TextView(getApplicationContext());
				productName.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, (float) 0.5)); // Here you can set weight to your TextView
				productName.setText("Product");
				productName.setGravity(Gravity.LEFT);
				productName.setTextColor(Color.BLUE);
				productName.setTextSize(12);
				productName.setTypeface(Typeface.DEFAULT_BOLD);
				productName.setGravity(Gravity.CENTER);


				textView2 = new TextView(getApplicationContext());
				textView2.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, (float) 0.5)); // Here you can set weight to your TextView
				textView2.setText("Type");
				textView2.setGravity(Gravity.LEFT);
				textView2.setTextSize(12);
				textView2.setTypeface(Typeface.DEFAULT_BOLD);
				textView2.setTextColor(Color.BLUE);

				textView3 = new TextView(getApplicationContext());
				textView3.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, (float) 0.5)); // Here you can set weight to your TextView
				textView3.setText("Price");
				textView3.setGravity(Gravity.LEFT);
				textView3.setTextSize(12);
				textView3.setTypeface(Typeface.DEFAULT_BOLD);
				textView3.setTextColor(Color.BLUE);

				textView4 = new TextView(getApplicationContext());
				textView4.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, (float) 0.5)); // Here you can set weight to your TextView
				textView4.setText("TPrice");
				textView3.setGravity(Gravity.LEFT);
				textView4.setTextSize(12);
				textView4.setTypeface(Typeface.DEFAULT_BOLD);
				textView4.setTextColor(Color.BLUE);

				textView5 = new TextView(getApplicationContext());
				textView5.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, (float) 0.5)); // Here you can set weight to your TextView
				textView5.setText("Qty");
				textView5.setGravity(Gravity.CENTER);
				textView5.setTextSize(12);
				textView5.setTypeface(Typeface.DEFAULT_BOLD);
				textView5.setTextColor(Color.BLUE);

				textView6 = new TextView(getApplicationContext());
				textView6.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, (float) 0.5)); // Here you can set weight to your TextView
				textView6.setText("TQty");
				textView6.setGravity(Gravity.CENTER);
				textView6.setTextSize(12);
				textView6.setTypeface(Typeface.DEFAULT_BOLD);
				textView6.setTextColor(Color.BLUE);

				TextView billQty = new TextView(getApplicationContext());
				billQty.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, (float) 0.5)); // Here you can set weight to your TextView
				billQty .setText("BillQty");
				billQty.setGravity(Gravity.CENTER);
				billQty.setTextSize(12);
				billQty.setTypeface(Typeface.DEFAULT_BOLD);
				billQty.setTextColor(Color.BLUE);

				TextView billTypeQty  = new TextView(getApplicationContext());
				billTypeQty.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, (float) 0.5)); // Here you can set weight to your TextView
				billTypeQty.setText("BillTQty");
				billTypeQty.setGravity(Gravity.CENTER);
				billTypeQty.setTextSize(12);
				billTypeQty.setTypeface(Typeface.DEFAULT_BOLD);
				billTypeQty.setTextColor(Color.BLUE);

				textView = new TextView(getApplicationContext());
				textView.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, (float) 0.5)); // Here you can set weight to your TextView
				textView.setText("Total Price");
				textView.setGravity(Gravity.CENTER);
				textView.setTextSize(12);
				textView.setTypeface(Typeface.DEFAULT_BOLD);
				textView.setTextColor(Color.BLUE);

				productName.setPadding(20, 20, 20, 20);
				textView2.setPadding(20, 20, 20, 20);
				textView3.setPadding(20, 20, 20, 20);
				textView4.setPadding(20, 20, 20, 20);
				textView5.setPadding(20, 20, 20, 20);
				billQty.setPadding(20, 20, 20, 20);
				billTypeQty.setPadding(20, 20, 20, 20);
				textView6.setPadding(20, 20, 20, 20);
				textView.setPadding(20, 20, 20, 20);


				titleRow.addView(productName);
				titleRow.addView(textView2);
				titleRow.addView(textView3);
				titleRow.addView(textView4);
				titleRow.addView(textView5);
				titleRow.addView(textView6);
				titleRow.addView(billQty);
				titleRow.addView(billTypeQty);
				titleRow.addView(textView);
				addRowsToDisplay();
			}
		}
	}

	/**
	 * void addRowsToDisplay()
	 * @param : NULL
	 * @return: NULL
	 * 
	 */

	public  void addRowsToDisplay()
	{
		db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
		db.execSQL("UPDATE cart SET  billqty = orderqty-billedqty, billtypeqty = ordertypeqty-billedtypeqty WHERE tableno = "+
				editTable+ " AND locationid = "+
				user_Login.user_LoginID.LOCATIONNO+";");

		Cursor c = db.rawQuery("SELECT * from cart WHERE locationid="+editLocation+" AND tableno="+editTable+" AND checkflag = 0 " +
				" AND billflag = 0;", null);
		int count = c.getCount();
		c.moveToFirst();
		grandTotal = 0;
		for(Integer j=0; j< count; j++)
		{
			final TextView productPrice;
			final String sno;
			final TextView typePrice;
			final TextView textView7;
			final EditText qtyEdit;
			final EditText typeQtyEdit;
			final TableRow tableRow;
			tableRow = new TableRow(this);
			//tableRow.setFocusable(true);
			sno = c.getString(c.getColumnIndex("sno")).toString();
			Group.getInstance().ORDERNO = c.getString(c.getColumnIndex("orderno"));
			productName = new TextView(getApplicationContext());
			productName.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, 1)); // Here you can set weight to your TextView
			productName.setText(c.getString(c.getColumnIndex("productname")));
			productName.setTextColor(Color.BLACK);
			productName.setGravity(Gravity.LEFT);
			productName.setTextSize(12);

			textView2 = new TextView(getApplicationContext());
			textView2.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, 1)); // Here you can set weight to your TextView
			textView2.setText(c.getString(c.getColumnIndex("typename")));
			textView2.setTextColor(Color.BLACK);
			textView2.setGravity(Gravity.LEFT);
			textView2.setTextSize(12);

			productPrice = new TextView(getApplicationContext());
			productPrice.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, 1)); // Here you can set weight to your TextView
			productPrice.setText(c.getString(c.getColumnIndex("productprice")));
			productPrice.setTextColor(Color.BLACK);
			productPrice.setGravity(Gravity.LEFT);
			productPrice.setTextSize(12);

			typePrice = new TextView(getApplicationContext());
			typePrice.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, 1)); // Here you can set weight to your TextView
			typePrice.setText(c.getString(c.getColumnIndex("typeprice")));
			typePrice.setTextColor(Color.BLACK);
			typePrice.setGravity(Gravity.LEFT);
			typePrice.setTextSize(12);

			TextView productQty = new TextView(getApplicationContext());
			productQty.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, 1)); // Here you can set weight to your TextView
			productQty.setText(c.getString(c.getColumnIndex("productqty")));
			productQty.setTextColor(Color.BLACK);
			productQty.setGravity(Gravity.LEFT);
			productQty.setTextSize(12);

			TextView typeQty = new TextView(getApplicationContext());
			typeQty.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, 1)); // Here you can set weight to your TextView
			typeQty.setText(c.getString(c.getColumnIndex("typeqty")));
			typeQty.setTextColor(Color.BLACK);
			typeQty.setGravity(Gravity.LEFT);
			typeQty.setTextSize(12);

			qtyEdit = new EditText(getApplicationContext());
			qtyEdit.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, 1)); // Here you can set weight to your TextView
			qtyEdit.setText(c.getString(c.getColumnIndex("billqty")));
			int billProductQty = c.getInt(c.getColumnIndex("orderqty")) - c.getInt(c.getColumnIndex("billedqty"));
			qtyEdit.setText(""+billProductQty);
			qtyEdit.setGravity(Gravity.LEFT);
			qtyEdit.clearFocus();
			this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			qtyEdit.setTextColor(Color.BLACK);
			qtyEdit.setBackgroundColor(Color.TRANSPARENT);
			qtyEdit.setTextSize(12);


			typeQtyEdit = new EditText(getApplicationContext());
			typeQtyEdit.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, 1)); // Here you can set weight to your TextView
			int billtypeqty = c.getInt(c.getColumnIndex("ordertypeqty")) - c.getInt(c.getColumnIndex("billedtypeqty"));
			typeQtyEdit.setText(""+billtypeqty);
			typeQtyEdit.setTextColor(Color.BLACK);
			typeQtyEdit.setGravity(Gravity.LEFT);
			typeQtyEdit.clearFocus();
			this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			typeQtyEdit.setBackgroundColor(Color.TRANSPARENT);
			typeQtyEdit.setTextSize(12);

			textView7 = new TextView(getApplicationContext());
			float total = ( Float.parseFloat(productPrice.getText().toString()) * Integer.parseInt(qtyEdit.getText().toString()) +
					Float.parseFloat( typePrice.getText().toString()) * Integer.parseInt(typeQtyEdit.getText().toString()));
			grandTotal = grandTotal + total;
			textView7.setTextColor(Color.BLACK);
			textView7.setGravity(Gravity.LEFT);
			textView7.setTextSize(12);
			textView7.setText(""+total);

			InputFilter[] FilterArray1 = new InputFilter[1];
			FilterArray1[0] = new InputFilter.LengthFilter(5);
			qtyEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
			qtyEdit.setFilters(FilterArray1);

			InputFilter[] FilterArray = new InputFilter[1];
			FilterArray[0] = new InputFilter.LengthFilter(5);
			typeQtyEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
			typeQtyEdit.setFilters(FilterArray);
			typeQtyEdit.setTextSize(12);

			if(j == 0)
			{  
				Button dummy = new Button(getApplicationContext());
				dummy.setPadding(20, 20, 20, 20);
			}
			productName.setPadding(20, 20, 20, 20);
			textView2.setPadding(20, 20, 20, 20);	
			productPrice.setPadding(20, 20, 20, 20);
			typePrice.setPadding(20, 20, 20, 20);
			qtyEdit.setPadding(20, 20, 20, 20);
			typeQtyEdit.setPadding(20, 20, 20, 20);
			productQty.setPadding(20, 20, 20, 20);
			typeQty.setPadding(20, 20, 20, 20);
			textView7.setPadding(20, 20, 20, 20);


			tableRow.addView(productName);
			tableRow.addView(textView2);
			tableRow.addView(productPrice);
			tableRow.addView(typePrice);
			tableRow.addView(productQty);
			tableRow.addView(typeQty);
			tableRow.addView(qtyEdit);
			tableRow.addView(typeQtyEdit);
			tableRow.addView(textView7);
			tableLayout.addView(tableRow);
			tableRow.setBackgroundColor(Color.TRANSPARENT);
			c.moveToNext();

			tableRow.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					clicksno = sno;
					if(clicksno.equals(oldsno))
					{						
						tableRow.setBackgroundColor(Color.TRANSPARENT);
						oldsno = "0";
						clicksno = "0";
					}
					else
					{
						colorRow.setBackgroundColor(Color.TRANSPARENT);
						tableRow.setBackgroundColor(Color.BLUE);
						oldsno = clicksno;
						colorRow = tableRow;
					}
				}
			});

			qtyEdit.addTextChangedListener(new TextWatcher() {
				String oldTotal;
				public void onTextChanged(CharSequence s, int start, int before, int count) {
				}
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					oldTotal = textView7.getText().toString();
				}
				@SuppressLint("NewApi")
				public void afterTextChanged(Editable s) {

					float changedTotal=0;
					String newQty;
					if(s.toString().isEmpty()){

						changedTotal =  Float.parseFloat(typePrice.getText().toString()) * Integer.parseInt(typeQtyEdit.getText().toString());
						textView7.setText(""+changedTotal);
						newQty = "0";
						grandTotal = grandTotal - Float.parseFloat(oldTotal) + changedTotal;
						oldTotal = "0";
						TextView totalValue = (TextView)findViewById(R.id.totalValue);
						totalValue.setText(""+grandTotal);
					}
					else{

						db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
						Cursor cartData = db.rawQuery("SELECT orderqty , billedqty from cart WHERE locationid="+
								editLocation+" AND tableno="+editTable+
								" AND orderflag = 1 AND billflag = 0 AND sno="+sno+";", null);
						int count = cartData.getCount();
						changedTotal =( Float.parseFloat(productPrice.getText().toString()) * 
								Integer.parseInt(s.toString()))+( Float.parseFloat(typePrice.getText().toString()) 
										* Integer.parseInt(typeQtyEdit.getText().toString()));
						textView7.setText(""+changedTotal);

						if(count > 0){
							newQty = s.toString();
							cartData.moveToFirst();
							if(Integer.parseInt(newQty) <= (cartData.getInt(cartData.getColumnIndex("orderqty"))-
									cartData.getInt(cartData.getColumnIndex("billedqty")))){
								textView7.setText(""+changedTotal);
								newQty = s.toString();
								db.execSQL("UPDATE cart SET billqty = "+newQty+" WHERE sno="+sno+";");
								grandTotal = grandTotal - Float.parseFloat(oldTotal) + changedTotal;
								oldTotal = "0";
								TextView totalValue = (TextView)findViewById(R.id.totalValue);
								totalValue.setText(""+grandTotal);
								db.close();
							}
							else{
								changedTotal =( Float.parseFloat(productPrice.getText().toString()) * cartData.getInt(cartData.getColumnIndex("orderqty"))-
										cartData.getInt(cartData.getColumnIndex("billedqty") ))+
										( Float.parseFloat(typePrice.getText().toString()) * Integer.parseInt(typeQtyEdit.getText().toString()));
								textView7.setText(""+changedTotal);
								db.execSQL("UPDATE cart SET billqty = orderqty - billedqty WHERE sno="+sno+";");
								tableLayout.removeAllViews();
								db.close();
								addRowsToDisplay();

							}
						}
					}
				}
			});


			typeQtyEdit.addTextChangedListener(new TextWatcher() {

				String oldTotal;
				public void onTextChanged(CharSequence s, int start, int before, int count) {
				}

				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					oldTotal = textView7.getText().toString();
				}

				@SuppressLint("NewApi")
				public void afterTextChanged(Editable s) {
					float changedTotal=0;
					String newTypeQty = "0";
					if(s.toString().isEmpty())
					{
						changedTotal = Float.parseFloat(productPrice.getText().toString()) * Integer.parseInt(qtyEdit.getText().toString());
						textView7.setText(""+changedTotal);
						grandTotal = grandTotal - Float.parseFloat(oldTotal) + changedTotal;
						oldTotal = "0";
						TextView totalValue = (TextView)findViewById(R.id.totalValue);
						totalValue.setText(""+grandTotal);

					}
					else
					{
						db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
						Cursor cartData = db.rawQuery("SELECT ordertypeqty, billedtypeqty from cart WHERE locationid="+editLocation+" AND tableno="+
								editTable + " AND orderflag = 1 AND billflag = 0 AND sno="+sno+";", null);
						int count = cartData.getCount();
						changedTotal = (Float.parseFloat(typePrice.getText().toString()) * Integer.parseInt(s.toString()))+
								( Float.parseFloat(productPrice.getText().toString()) * Integer.parseInt(qtyEdit.getText().toString()));
						if(count > 0)
						{
							newTypeQty = s.toString();
							cartData.moveToFirst();
							if(Integer.parseInt(newTypeQty) <= (cartData.getInt(cartData.getColumnIndex("ordertypeqty"))-
									cartData.getInt(cartData.getColumnIndex("billedtypeqty"))))
							{
								textView7.setText(""+changedTotal);
								newTypeQty = s.toString();
								db.execSQL("UPDATE cart SET billtypeqty = "+newTypeQty+ " WHERE sno="+sno+";");
								grandTotal = grandTotal - Float.parseFloat(oldTotal) + changedTotal;
								oldTotal = "0";
								TextView totalValue = (TextView)findViewById(R.id.totalValue);
								totalValue.setText(""+grandTotal);
								db.close();	
							}
							else
							{
								db.execSQL("UPDATE cart SET billtypeqty = ordertypeqty - billedtypeqty WHERE sno="+
										sno+";");
								textView7.setText(""+changedTotal);
								db.close();
								tableLayout.removeAllViews();
								addRowsToDisplay();
							}

						}

					}
				}
			});

		}
		db.close();
		Button dummy = new Button(getApplicationContext());
		dummy.setPadding(20, 20, 20, 20);
		dummy.setEnabled(false);
		dummy.setBackgroundColor(Color.TRANSPARENT);
		tableLayout.addView(dummy);
		TextView totalValue = (TextView)findViewById(R.id.totalValue);
		totalValue.setText(""+grandTotal);
		ImageView printer = (ImageView)findViewById(R.id.imageView2);
		printer.setLayoutParams(new TableRow.LayoutParams(0 , LayoutParams.WRAP_CONTENT, 1));
		printer.setOnClickListener(new View.OnClickListener() {
			@SuppressLint("NewApi")
			public void onClick(View v) {
				db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
				Cursor productDetails = db.rawQuery("SELECT productname,productprice,productqty,billqty,discount,billedqty,typename,typeqty,typeprice," +
						"orderno,billtypeqty FROM cart WHERE tableno = "+editTable+ " AND locationid= "+editLocation+" AND billflag= 0 AND checkflag=0 AND " +
						"tbillflag=0 AND (billtypeqty != 0 OR billqty != 0) ;", null);
				int count = productDetails.getCount();
				db.close();
				if( count > 0)
				{
					Group.getInstance().totalAmount = grandTotal;
					Group.getInstance().subTotalAmount = grandTotal;
					cashpay();
				}
				else
				{
					AlertDialog.Builder adb = new AlertDialog.Builder(
							Payment.this, android.R.style.Theme_Dialog);
					adb.setTitle("");
					adb.setMessage("Products Not Found To Pay");
					adb.setCancelable(false);
					adb.setPositiveButton("Ok", new 
							OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
							db.execSQL("UPDATE cart SET checkflag = 0 WHERE locationid="+editLocation+" AND tableno="+
									editTable+" AND checkflag = 1 AND billflag = 0;");
							db.close();

							Intent intent=new Intent(Payment.this, Home.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
						}
					});
					adb.show();
				}
			}
		});

		bottomRow = (TableRow)findViewById(R.id.tableRow12);
		bottomRow.setBackgroundColor(Color.TRANSPARENT);
		bottomRow.setVisibility(View.VISIBLE);
		tableLayout.setVisibility(View.VISIBLE);
		titleRow.setVisibility(View.VISIBLE);
		bottomRow.setEnabled(true);
//		bottomRow.setFocusable(true);
		bottomRow.bringToFront();
		titleRow.bringToFront();
	}


	public void cashpay(){
		float cash = 0;
		{if (Group.GROUPID.finaltotal==null){
			pay();
			cash = Float.parseFloat(Group.GROUPID.finaltotal);
		}else {
			pay();
			cash = Float.parseFloat(Group.GROUPID.finaltotal);}
			if(cash > 0)
			{
					db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
					String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
					String datetime = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());
					Cursor recieptno =  db.rawQuery("SELECT * FROM receiptnos WHERE receipthead = '"+datetime+"' ;", null);
					if(recieptno.getCount() == 0)
					{
						if(cash > 0)
						{
							db.execSQL("INSERT INTO receiptnos VALUES('"+datetime+"',1);");
							db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
									"username,orderid,checkflag) VALUES('R"+datetime+ "1','CASH', '0',"+cash+" ,'"+timeStamp+"','"+ MainActivity.getInstance().USERNAME+"','"+ Group.getInstance().ORDERNO +"',0 );");
						}

						if(!Group.getInstance().voucherNumber.equals("0"))
						{
							db.execSQL("UPDATE voucher SET usedflag = 1 WHERE voucherid = "+ Group.getInstance().voucherNumber+";");
							db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
									"username,orderid,checkflag) VALUES('"+ Group.getInstance().voucherNumber+"','VOUCHER', '"+ Group.getInstance().voucherNumber+"',"+
									Group.getInstance().voucherAmount+" ,'"+timeStamp+"','"+ MainActivity.getInstance().USERNAME+"','"+ Group.getInstance().ORDERNO +"',0 );");
						}
					}
					else
					{
						if(cash > 0)
						{
							recieptno.moveToLast();
							String newReceiptNo = "R" +recieptno.getString(0)+ (recieptno.getInt(1)+(int)1);
							db.execSQL("UPDATE receiptnos SET receiptnumber = receiptnumber+1 WHERE receipthead= '"+
									recieptno.getString(0)+"';");
							db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
									"username,orderid,checkflag) VALUES('"+newReceiptNo+"','CASH', '0',"+cash+" ,'"+timeStamp+"','"+ MainActivity.getInstance().USERNAME+"','"+ Group.getInstance().ORDERNO +"',0 );");

						}
						if(!Group.getInstance().voucherNumber.equals("0"))
						{
							db.execSQL("UPDATE voucher SET usedflag = 1 WHERE voucherid = "+ Group.getInstance().voucherNumber+";");
							db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
									"username,orderid,checkflag) VALUES('"+ Group.getInstance().voucherNumber+"','VOUCHER', '"+ Group.getInstance().voucherNumber+"',"+
									Group.getInstance().voucherAmount+" ,'"+timeStamp+"','"+ MainActivity.getInstance().USERNAME+"','"+ Group.getInstance().ORDERNO +"',0 );");
						}
					}

					db.execSQL("UPDATE cart SET discount = "+ Group.getInstance().discountAmount+
							" WHERE locationid="+ user_Login.user_LoginID.LOCATIONNO+" AND tableno="+editTable+" AND billflag = 0 AND orderflag = 1;");
					db.close();
					Intent intent = new Intent(Payment.this, Printer.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("TABLENO", Integer.parseInt(editTable));
					intent.putExtra("LOCATIONNO", Integer.parseInt(user_Login.user_LoginID.LOCATIONNO));
					intent.putExtra("FLAG", 1);
					startActivity(intent);
			}
		}
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);;
		db.execSQL("UPDATE cart SET checkflag = 0 WHERE locationid="+editLocation+" AND tableno="+
				editTable+" AND checkflag = 1 AND billflag = 0;");
		db.close();
	}
	public static Payment getInstance(){
		return PayId;
	}
	public void pay(){
		double  grandTotal = 0;
		db =openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
		Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid="+editLocation+" AND tableno="+editTable+
				" AND billflag = 0;", null);
		int count = cartData.getCount();
		if(count > 0){
			cartData.moveToLast();
			do {
				String productqty = cartData.getString(cartData.getColumnIndex("productqty"));
				String productprice = cartData.getString(cartData.getColumnIndex("productprice"));
				String typeprice = cartData.getString(cartData.getColumnIndex("typeprice"));
				String typeqty = cartData.getString(cartData.getColumnIndex("typeqty"));
				Double totalvalue = Double.valueOf(( Float.parseFloat(productprice) *
						            Integer.parseInt(productqty)))+Double.valueOf(typeprice)*Double.valueOf(typeqty);
				grandTotal = grandTotal+totalvalue;
			}while (cartData.moveToPrevious());
		}
		db.close();
		db =openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
		Cursor taxDetails = db.rawQuery("SELECT servicetaxper,taxper1,taxper2,taxper3 FROM tax;", null);
		int taxDetailsCount = taxDetails.getCount();
		double tax1 = 0;
		Log.d("Check", "pay:taxcount "+taxDetails.getCount());
		if(taxDetailsCount > 0)
		{
			taxDetails.moveToFirst();			//Here we are inserting the tax information into the structure
			tax1 = taxDetails.getFloat(taxDetails.getColumnIndex("servicetaxper"));
			tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper1"));
			tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper2"));
			tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper3"));

		}else
		{
		}
		db.close();
		grand =String.format("%.2f",grandTotal);
		double taxAmount = (tax1 * grandTotal)/100;
		String grand_tax =String.format("%.2f", taxAmount);
		grandTotal  += (tax1* grandTotal)/100;
		String grand_Amount =String.format("%.2f", grandTotal);
		Group.GROUPID.finaltotal = grand_Amount;

	}
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
		return super.dispatchTouchEvent(ev);
	}
}

