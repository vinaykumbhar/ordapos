package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.embdes.model.ProductData;
import com.github.mikephil.charting.charts.BarChart;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Vinay on 12/09/2017.
 */

public class SalesReportFragment extends Fragment {
    static SalesReportFragment GROUPID;
    TableLayout tableA;
    TableLayout tableB;
    TableLayout tableC;
    TableLayout tableD;

    HorizontalScrollView horizontalScrollViewB;
    HorizontalScrollView horizontalScrollViewD;

    ScrollView scrollViewC;
    ScrollView scrollViewD;
    SQLiteDatabase db;
    String year;
    //    TableLayout.LayoutParams tableLayoutParams;
    private Context context;
    private ProgressDialog pd;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Button btn_go;
    LinearLayout btn_send, btn_print;
    String toDateString;
    String fromDateString;
    public static String toReportsDate;
    public static String fromReportsDate;
    DatePickerDialog toDate;//
    TextView fromDate, toDateTime;
    public int yearSelected, monthSelected, daySelected, hourSelected, minuteSelected;

    // declare  the variables to show the date and time whenTime and Date Picker Dialog first appears
    private int mYear, mMonth, mDay, mHour, mMinute/*mMinute1*/;
    boolean isShow = false;
    boolean dateFlag = false;

    static final int DATE_DIALOG_ID = 0;
    static final int FROM_DATE_DIALOG_ID = 2;
    Spinner spinner_foodCategory;
    ArrayList<String> foodCategory = new ArrayList<String>();
    ImageView image_type;
    RelativeLayout container_layout;

    public SalesReportFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_sales_report, container, false);
        GROUPID = this;
        container_layout = (RelativeLayout) rootView.findViewById(R.id.container_layout);
        context = getActivity();
        // initialize the main components (TableLayouts, HorizontalScrollView, ScrollView)
        initComponents();
        setComponentsId();
        setScrollViewAndHorizontalScrollViewTag();

        // no need to assemble component A, since it is just a table
        horizontalScrollViewB.addView(tableB);

        scrollViewC.addView(tableC);

        scrollViewD.addView(horizontalScrollViewD);
        horizontalScrollViewD.addView(tableD);

        // add the components to be part of the main layout
        addComponentToMainLayout();
        fromDate = (TextView) rootView.findViewById(R.id.fromDateView);
        toDateTime = (TextView) rootView.findViewById(R.id.toDateView);
        image_type = (ImageView) rootView.findViewById(R.id.image_type);

        String timeStamp = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(Calendar.getInstance().getTime());
        toDateTime.setText(" " + timeStamp);
        fromDate.setText("" + timeStamp);

        foodCategory.clear();
        foodCategory.add("PRODUCTS");
        foodCategory.add("CATEGORIES");
        foodCategory.add("GROUPS");
        spinner_foodCategory = (Spinner) rootView.findViewById(R.id.spinner_type);
        ArrayAdapter<String> oppor_adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.productpinner, foodCategory);

        oppor_adapter.setDropDownViewResource(R.layout.productpinner);
        spinner_foodCategory.setAdapter(oppor_adapter);

//        spinner_foodCategory.setOnItemSelectedListener(foodCategoryListenter);

        ImageButton toSetButton = (ImageButton) rootView.findViewById(R.id.toButton);
        ImageButton fromButton = (ImageButton) rootView.findViewById(R.id.fromButton);
//        btn_send = (LinearLayout) rootView.findViewById(R.id.send);
        btn_go = (Button) rootView.findViewById(R.id.btn_go);
        btn_go.setOnClickListener(btngoclickListener);
        fromButton.setOnClickListener(fromsethandler);
        toSetButton.setOnClickListener(tosethandler);
        image_type.setOnClickListener(typeselctListener);
//        btn_send.setOnClickListener(btnsendListener);

//        // 1) Find tableLayout and set its params
//        tlGridTable = (TableLayout) rootView.findViewById(R.id.tlGridTable);
//        tableRowParams = new TableRow.LayoutParams();
//        tableRowParams.setMargins(1, 1, 1, 1);
//        tableRowParams.weight = 1;
//        tableLayoutParams = new TableLayout.LayoutParams();


        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        cal.set(Calendar.DAY_OF_MONTH, 1);

        SimpleDateFormat timeformat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        year = new SimpleDateFormat("yyyy").format(java.util.Calendar.getInstance().getTime());
        Calendar mycal = Calendar.getInstance();
        // Get the number of days in that month
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH); // 28
        mycal.set(Calendar.DAY_OF_MONTH, daysInMonth);

        toReportsDate = timeformat.format(Calendar.getInstance().getTime());
        fromReportsDate = timeformat.format(cal.getTime());

        try {
            productReports(fromReportsDate, toReportsDate, dateFlag, 0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return rootView;
    }


    // initalized components
    private void initComponents() {
        tableA = new TableLayout(context);
        tableB = new TableLayout(context);
        tableC = new TableLayout(context);
        tableD = new TableLayout(context);

        horizontalScrollViewB = new MyHorizontalScrollView(context);
        horizontalScrollViewD = new MyHorizontalScrollView(context);

        scrollViewC = new MyScrollView(context);
        scrollViewD = new MyScrollView(context);

        tableA.setBackgroundColor(Color.WHITE);
        horizontalScrollViewB.setBackgroundColor(Color.WHITE);
    }

    // set essential component IDs
    private void setComponentsId() {
        tableA.setId(1);
        horizontalScrollViewB.setId(2);
        scrollViewC.setId(3);
        scrollViewD.setId(4);
    }

    // set tags for some horizontal and vertical scroll view
    private void setScrollViewAndHorizontalScrollViewTag() {

        horizontalScrollViewB.setTag("horizontal scroll view b");
        horizontalScrollViewD.setTag("horizontal scroll view d");
        scrollViewC.setTag("scroll view c");
        scrollViewD.setTag("scroll view d");
    }


    // we add the components here in our TableMainLayout
    private void addComponentToMainLayout() {
        // RelativeLayout params were very useful here
        // the addRule method is the key to arrange the components properly
        RelativeLayout.LayoutParams componentB_Params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        componentB_Params.addRule(RelativeLayout.RIGHT_OF, tableA.getId());

        RelativeLayout.LayoutParams componentC_Params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        componentC_Params.addRule(RelativeLayout.BELOW, tableA.getId());

        RelativeLayout.LayoutParams componentD_Params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        componentD_Params.addRule(RelativeLayout.RIGHT_OF, scrollViewC.getId());
        componentD_Params.addRule(RelativeLayout.BELOW, horizontalScrollViewB.getId());

        // 'this' is a relative layout,
        // we extend this table layout as relative layout as seen during the creation of this class
        container_layout.addView(tableA);
        container_layout.addView(horizontalScrollViewB, componentB_Params);
        container_layout.addView(scrollViewC, componentC_Params);
        container_layout.addView(scrollViewD, componentD_Params);

    }

    // generate table row of table A
    TableRow componentATableRow() {
        TableRow componentATableRow = new TableRow(context);
        LayoutInflater inflater2 = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mTableHeader = inflater2.inflate(R.layout.row_table_header, null);

        TableRow.LayoutParams params = new TableRow.LayoutParams(200, 80);
        params.setMargins(1, 1, 1, 1);

        TextView ingr_name = (TextView) mTableHeader.findViewById(R.id.columntitle);
        ingr_name.setText("ITEMS");//i=row  j=column

        mTableHeader.setLayoutParams(params);


        componentATableRow.addView(mTableHeader);
        return componentATableRow;
    }

    // generate table row of table B
    View componentBTableRow(String header) {
        LayoutInflater inflater2 = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mTableHeader = inflater2.inflate(R.layout.row_table_header, null);
        LinearLayout.LayoutParams params;
        if (header.contains("\n")) {
            params = new TableRow.LayoutParams(150, LinearLayout.LayoutParams.MATCH_PARENT);
        } else {
            params = new TableRow.LayoutParams(150, 80);
        }
        params.setMargins(1, 1, 1, 1);
        TextView ingr_name = (TextView) mTableHeader.findViewById(R.id.columntitle);
        ingr_name.setText(header);//i=row  j=column
        mTableHeader.setLayoutParams(params);
        return mTableHeader;
    }

    // a TableRow for table D
    View tableRowForTableD(String amt, String qty) {
        // TableRow tableRowForTableC = new TableRow(this.context);
        LayoutInflater inflater2 = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mTableHeader = inflater2.inflate(R.layout.row_table_data, null);

        TextView textqty = (TextView) mTableHeader.findViewById(R.id.txt_data);
        TextView txt_amt = (TextView) mTableHeader.findViewById(R.id.txt_amt);

        TableRow.LayoutParams params = new TableRow.LayoutParams(150, 60);
        params.setMargins(1, 1, 1, 1);

        textqty.setText(qty);//i=row  j=column
        txt_amt.setText(amt);
        mTableHeader.setLayoutParams(params);
        // tableRowForTableC.addView(mTableHeader);
        return mTableHeader;
    }

    // a TableRow for table C
    TableRow tableRowForTableC(String itemName) {
        TableRow tableRowForTableC = new TableRow(context);
        LayoutInflater inflater2 = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mTableHeader = inflater2.inflate(R.layout.row_table_header, null);

        TableRow.LayoutParams params = new TableRow.LayoutParams(200, 60);
        params.setMargins(1, 1, 1, 1);

        TextView ingr_name = (TextView) mTableHeader.findViewById(R.id.columntitle);
        ingr_name.setText(itemName);//i=row  j=column
        mTableHeader.setLayoutParams(params);
        tableRowForTableC.addView(mTableHeader);
        return tableRowForTableC;
    }


    AdapterView.OnItemSelectedListener foodCategoryListenter = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            try {
                productReports(fromReportsDate, toReportsDate, dateFlag, position);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    View.OnClickListener btngoclickListener = new View.OnClickListener() {
        @SuppressWarnings("deprecation")
        @SuppressLint("NewApi")
        public void onClick(View v) {
//            try {
//                Date finalfromdate = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(fromReportsDate);
//                Date finaltodate = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(toReportsDate);
//                Long diff = getDateDiff(finalfromdate, finaltodate, TimeUnit.DAYS);
//                if (diff > 31) {
//                    Toast.makeText(getActivity(), "Please choose dates in between 1 month", Toast.LENGTH_SHORT).show();
//                } else {
            try {
                dateFlag = true;
                productReports(fromReportsDate, toReportsDate, dateFlag, spinner_foodCategory.getSelectedItemPosition());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//                }
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
        }
    };

    private long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    View.OnClickListener fromsethandler = new View.OnClickListener() {
        @SuppressWarnings("deprecation")
        @SuppressLint("NewApi")
        public void onClick(View v) {
            final Calendar c = Calendar.getInstance();
            isShow = true;
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);
            MainReportActivity.getInstance().showDialog(FROM_DATE_DIALOG_ID);
        }
    };

    View.OnClickListener typeselctListener = new View.OnClickListener() {
        @SuppressWarnings("deprecation")
        @SuppressLint("NewApi")
        public void onClick(View v) {
            spinner_foodCategory.performClick();
        }
    };
    View.OnClickListener tosethandler = new View.OnClickListener() {
        @SuppressWarnings("deprecation")
        @SuppressLint("NewApi")
        public void onClick(View v) {
            final Calendar c = Calendar.getInstance();
            isShow = true;
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);
            MainReportActivity.mainreportView.showDialog(DATE_DIALOG_ID);
        }
    };

    /**
     * Dialog will come when user press date button in reports screen.
     */

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case FROM_DATE_DIALOG_ID:
                final DatePickerDialog fromDate = new DatePickerDialog(getActivity(),
                        fromDateSetListener,
                        mYear, mMonth, mDay);
                fromDate.getDatePicker().setMaxDate(System.currentTimeMillis());
                fromDate.setCancelable(true);
                fromDate.setCanceledOnTouchOutside(true);
                fromDate.setButton(DialogInterface.BUTTON_NEGATIVE,
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                isShow = false; //Cancel flag, used in mTimeSetListener
                            }
                        });
                return fromDate;

            case DATE_DIALOG_ID:
                toDate = new DatePickerDialog(getActivity(),
                        toDateSetListener,
                        mYear, mMonth, mDay);
                toDate.getDatePicker().setMaxDate(System.currentTimeMillis());
                toDate.setButton(DialogInterface.BUTTON_NEGATIVE,
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                isShow = false; //Cancel flag, used in mTimeSetListener
                            }
                        });
                return toDate;
        }
        return null;
    }

    // Register  fromDatePickerDialog listener
    private DatePickerDialog.OnDateSetListener fromDateSetListener =
            new DatePickerDialog.OnDateSetListener() {                 // the callback received when the user "sets" the Date in the DatePickerDialog
                public void onDateSet(DatePicker view, int yearSelected,
                                      int monthOfYear, int dayOfMonth) {
                    if (isShow == true) {
                        isShow = false;
                        monthSelected = ++monthOfYear;
                        daySelected = dayOfMonth;
                        //  TextView fromDate = (TextView) findViewById(R.id.fromDateView);
                        String newYear = "" + yearSelected;
                        fromDateString = String.format("" + String.format("%02d", daySelected) + "-" +
                                String.format("%02d", monthSelected) + "-" + newYear);
                        fromDate.setText("" + fromDateString + " " + String.format("%02d", hourSelected) + ":" +
                                String.format("%02d", minuteSelected));
                        fromTime();
                    }
                }
            };


    // Register  toDatePickerDialog listener
    private DatePickerDialog.OnDateSetListener toDateSetListener =
            new DatePickerDialog.OnDateSetListener() {                 // the callback received when the user "sets" the Date in the DatePickerDialog
                public void onDateSet(DatePicker view, int yearSelected,
                                      int monthOfYear, int dayOfMonth) {
                    if (isShow == true) {
                        isShow = false;
                        System.out.println("*** To Date Set on click Listner ***");
                        monthSelected = ++monthOfYear;
                        daySelected = dayOfMonth;
                        String newYear = "" + yearSelected;
                        toDateString = String.format("" + String.format("%02d", daySelected) + "-" +
                                String.format("%02d", monthSelected) + "-" + newYear);
                        toDateTime.setText("" + toDateString + " " + String.format("%02d", hourSelected) + ":" +
                                String.format("%02d", minuteSelected));
                        toTime();
                    }
                }
            };

    public void fromTime() {
        TimePickerDialog fromTime = new TimePickerDialog(getActivity(),
                fromTimeSetListener, mHour, mMinute, false);
        fromTime.show();
    }

    public void toTime() {
        TimePickerDialog toTime = new TimePickerDialog(getActivity(),
                toTimeSetListener, mHour, mMinute, false);
        toTime.show();
    }

    // Register  TimePickerDialog listener
    private TimePickerDialog.OnTimeSetListener fromTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                // the callback received when the user "sets" the TimePickerDialog in the dialog
                public void onTimeSet(TimePicker view, int hourOfDay, int min) {
                    hourSelected = hourOfDay;
                    minuteSelected = min;
                    fromReportsDate = "" + fromDateString + " " + String.format("%02d", hourSelected) + ":" +
                            String.format("%02d", minuteSelected);
                    fromDate.setText(fromReportsDate);
                }
            };

    // Register  TimePickerDialog listener
    private TimePickerDialog.OnTimeSetListener toTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                // the callback received when the user "sets" the TimePickerDialog in the dialog
                public void onTimeSet(TimePicker view, int hourOfDay, int min) {
                    hourSelected = hourOfDay;
                    minuteSelected = min;
                    String finaltime = toDateString + " " + String.format("%02d", hourSelected) + ":" +
                            String.format("%02d", minuteSelected);
                    try {
//                        Date finalfromdate = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(fromReportsDate);
//                        Calendar cal = Calendar.getInstance();
//                        cal.clear();
//                        cal.setTimeInMillis(finalfromdate.getTime());
//                        cal.add(Calendar.MONTH, 1);

                        Date finaldate = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(finaltime);
                        if (finaldate.getTime() > System.currentTimeMillis()) {
                            Toast.makeText(getActivity(), "Please choose dates in between 1 month", Toast.LENGTH_SHORT).show();
                        } else {
                            toReportsDate = finaltime;
                            toDateTime.setText(finaltime);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            };


    private class CreateTable extends AsyncTask<String, Void, Boolean> {
        ProgressDialog pd = new ProgressDialog(getActivity());
        ArrayList<ProductData> mProductList = new ArrayList<>();
        DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        DateFormat monthformat = new SimpleDateFormat("MMMM");
        DateFormat yearFormat = new SimpleDateFormat("yyyy");

        String startDate, endDate;
        Boolean dateFlag = false;
        List<Date> dates;
        int foodCategory = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProductList = new ArrayList<>();
            pd.setMessage("Getting data....");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            String mstartDate = params[0];
            String mendDate = params[1];
            dateFlag = Boolean.parseBoolean(params[2]);
            foodCategory = Integer.parseInt(params[3]);
            dates = getDates(mstartDate, mendDate);

            startDate = reverseDateFormat(mstartDate);
            endDate = reverseDateFormat(mendDate);

            try {
                db = getActivity().openOrCreateDatabase("rebelPOS.db", Context.MODE_APPEND, null);
                Cursor shopDetails = db.rawQuery("SELECT terminalname FROM masterterminal;", null);
                int shopCount = shopDetails.getCount();
                if (shopCount > 0) {
                    shopDetails.moveToFirst();
                } else {
                    return false;
                }
                shopDetails.close();
                String products_query = "SELECT productdescriptionterminal FROM product GROUP BY productdescriptionterminal;";
                if (foodCategory == 0) {
                    products_query = "SELECT productdescriptionterminal FROM product GROUP BY productdescriptionterminal;";
                } else if (foodCategory == 1) {
                    products_query = "SELECT categoryname FROM category;";
                } else if (foodCategory == 2) {
                    products_query = "SELECT groupname FROM grouptable;";// GROUP BY groupname
                }
                final String finalProducts_query = products_query;
                final Cursor product = db.rawQuery(finalProducts_query, null);
                int products_Count = product.getCount();
                int count = 0;
                //customize dates
                Date fdate = df1.parse(mstartDate);
                Date tdate = df1.parse(mendDate);
                Calendar startCalendar = new GregorianCalendar();
                startCalendar.setTime(fdate);

                Calendar endCalendar = new GregorianCalendar();
                endCalendar.setTime(tdate);
                int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
                int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);

                int dateMonth = tdate.getMonth();
                int dateYear = Integer.parseInt(yearFormat.format(tdate.getTime()));
                if (products_Count > 0) {
                    mProductList.clear();
                    while (product.moveToNext()) {
                        ProductData main_product = new ProductData();
                        String grp_name = product.getString(0);
                        //TableRow tableRow = new TableRow(getActivity());
                        if (count == 0) {

                            Calendar c = Calendar.getInstance();
                            c.setTime(fdate);

                            ArrayList<ProductData> mProductList = new ArrayList<>();
                            ProductData mProductData1 = new ProductData();
                            mProductData1.setName("");
                            mProductData1.setAmount("");
                            mProductData1.setDatetime("");
                            mProductData1.setQuantity("");
                            mProductList.add(mProductData1);
                            mProductList.add(mProductData1);//added two blank data
                            if (dateFlag && dateMonth != fdate.getMonth()) {
                                int x = 0;
                                int q = 0;
                                do {//                dates.add(c.getTime());
                                    int calMonth = c.get(Calendar.MONTH);
                                    int calYear = c.get(Calendar.YEAR);
                                    if (x == 0) {
                                        ProductData hData = new ProductData();
                                        hData.setDatetime(monthformat.format(c.getTime()) + "\n" + yearFormat.format(c.getTime()));
                                        mProductList.add(hData);
                                        x++;
                                    } else if (calMonth == dateMonth && calYear == dateYear) {
                                        Calendar cal = Calendar.getInstance();
                                        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
                                        cal.clear(Calendar.MINUTE);
                                        cal.clear(Calendar.SECOND);
                                        cal.clear(Calendar.MILLISECOND);
                                        cal.set(Calendar.DAY_OF_MONTH, 1);
                                        List<Date> dates = getDates(df1.format(cal.getTime()), mendDate);
                                        for (int j = 0; j < dates.size(); j++) {
                                            ProductData hData = new ProductData();
                                            hData.setDatetime(df1.format(dates.get(j)).split(" ")[0]);
                                            mProductList.add(hData);
                                        }
                                    } else {
                                        ProductData hData = new ProductData();
                                        hData.setDatetime(monthformat.format(c.getTime()) + "\n" + yearFormat.format(c.getTime()));
                                        mProductList.add(hData);
                                    }
                                    c.add(Calendar.MONTH, 1);
                                    q++;
                                } while (q <= diffMonth);
                            } else {
                                for (int i = 0; i < dates.size(); i++) {
                                    ProductData hData = new ProductData();

                                    String timestamp = df1.format(dates.get(i));
                                    hData.setDatetime(timestamp.split(" ")[0]);
                                    mProductList.add(hData);
                                }
                            }
                            main_product.setList(mProductList);
                            product.moveToPrevious();
                        } else {

                            Calendar c = Calendar.getInstance();
                            c.setTime(fdate);
                            ArrayList<ProductData> mProductList = new ArrayList<>();
                            ProductData mProductData1 = new ProductData();
                            mProductData1.setName(product.getString(0));
                            mProductData1.setAmount("");
                            mProductData1.setDatetime("");
                            mProductData1.setQuantity("");
                            mProductList.add(mProductData1);
                            mProductData1 = new ProductData();
                            mProductData1.setName("");
                            mProductData1.setQuantity("QTY");
                            mProductData1.setAmount("AMT");
                            mProductList.add(mProductData1);
                            if (dateFlag && dateMonth != fdate.getMonth()) {
                                int x = 0;
                                int q = 0;
                                do {
                                    int calMonth = c.get(Calendar.MONTH);
                                    int calYear = c.get(Calendar.YEAR);
                                    if (x == 0) { //get first month data by start date and end date of the month
//                                        startDate,endDate
                                        String sDate = startDate;                                      //start date of the month
                                        String eDate = reverseDateFormat(getEndDate(fdate.getMonth(), Integer.parseInt(yearFormat.format(fdate.getTime()))));//end date time of the first month
                                        String query;
                                        if (foodCategory == 0) {
                                            query = "SELECT  productqty, productprice,datetime,strftime('%d-%m-%Y', datetime) as valMonth FROM cart WHERE datetime >='" + sDate + "' AND datetime <= '" + eDate + "' AND productname='" + product.getString(0) + "';";// WHERE datetime >='" + startDate + "' AND datetime <= '" + endDate + "' AND productname=
                                        } else if (foodCategory == 1) {
                                            query = "SELECT  productqty, productprice,datetime,strftime('%d-%m-%Y', datetime) as valMonth FROM cart WHERE datetime >='" + sDate + "' AND datetime <= '" + eDate + "' AND pcategory='" + product.getString(0) + "';";// WHERE datetime >='" + startDate + "' AND datetime <= '" + endDate + "' AND productname=
                                        } else if (foodCategory == 2) {
                                            query = "SELECT  productqty, productprice,datetime,strftime('%d-%m-%Y', datetime) as valMonth FROM cart WHERE datetime >='" + sDate + "' AND datetime <= '" + eDate + "' AND pgroup='" + product.getString(0) + "';";// WHERE datetime >='" + startDate + "' AND datetime <= '" + endDate + "' AND productname=
                                        } else
                                            query = "SELECT  productqty, productprice,datetime,strftime('%d-%m-%Y', datetime) as valMonth FROM cart WHERE datetime >='" + sDate + "' AND datetime <= '" + eDate + "' AND productname='" + product.getString(0) + "';";// WHERE datetime >='" + startDate + "' AND datetime <= '" + endDate + "' AND productname=

                                        Cursor productbymonth = db.rawQuery(query, null);
                                        if (productbymonth.getCount() > 0) {
                                            int totalQty = 0;
                                            Float totalValue = 0f;
                                            while (productbymonth.moveToNext()) {
                                                if (productbymonth.getString(0) != null) {
                                                    totalQty += productbymonth.getInt(0);
                                                    totalValue += productbymonth.getInt(0) * productbymonth.getFloat(1);
                                                } else {
                                                    totalQty += 0;
                                                    totalValue += 0;
                                                }
                                            }
                                            mProductData1.setQuantity(String.valueOf(totalQty));
                                            mProductData1.setAmount(String.valueOf(totalValue));
                                        } else {
                                            mProductData1.setQuantity("0");
                                            mProductData1.setAmount("0");
                                        }
                                        mProductList.add(mProductData1);
                                        productbymonth.close();
                                        x++;
                                    } else if (calMonth == dateMonth && calYear == dateYear) {// if month is current month
                                        Calendar cal = Calendar.getInstance();
                                        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
                                        cal.clear(Calendar.MINUTE);
                                        cal.clear(Calendar.SECOND);
                                        cal.clear(Calendar.MILLISECOND);
                                        cal.set(Calendar.DAY_OF_MONTH, 1);
                                        List<Date> dates = getDates(df1.format(cal.getTime()), mendDate);
                                        for (int j = 0; j < dates.size(); j++) {
                                            mProductData1 = new ProductData();
                                            DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                                            String timestamp = df1.format(dates.get(j));
                                            String query;
                                            String sDate = startDate;
                                            String eDate = endDate;
                                            if (j == 0) {
                                                sDate = reverseDateFormat(timestamp);
                                                eDate = reverseDateFormat(timestamp).split(" ")[0] + " 23:59";
                                            } else if (j == dates.size() - 1) {
                                                sDate = reverseDateFormat(timestamp).split(" ")[0] + " 00:00";
                                                eDate = endDate;
                                            } else {
                                                sDate = reverseDateFormat(timestamp).split(" ")[0] + " 00:00";
                                                eDate = reverseDateFormat(timestamp).split(" ")[0] + " 23:59";
                                            }
                                            if (foodCategory == 0) {
                                                query = "SELECT  productqty, productprice,datetime,strftime('%d-%m-%Y', datetime) as valMonth FROM cart WHERE datetime >='" + sDate + "' AND datetime <= '" + eDate + "' AND productname='" + product.getString(0) + "';";// WHERE datetime >='" + startDate + "' AND datetime <= '" + endDate + "' AND productname=
                                            } else if (foodCategory == 1) {
                                                query = "SELECT  productqty, productprice,datetime,strftime('%d-%m-%Y', datetime) as valMonth FROM cart WHERE datetime >='" + sDate + "' AND datetime <= '" + eDate + "' AND pcategory='" + product.getString(0) + "';";// WHERE datetime >='" + startDate + "' AND datetime <= '" + endDate + "' AND productname=
                                            } else if (foodCategory == 2) {
                                                query = "SELECT  productqty, productprice,datetime,strftime('%d-%m-%Y', datetime) as valMonth FROM cart WHERE datetime >='" + sDate + "' AND datetime <= '" + eDate + "' AND pgroup='" + product.getString(0) + "';";// WHERE datetime >='" + startDate + "' AND datetime <= '" + endDate + "' AND productname=
                                            } else
                                                query = "SELECT  productqty, productprice,datetime,strftime('%d-%m-%Y', datetime) as valMonth FROM cart WHERE datetime >='" + sDate + "' AND datetime <= '" + eDate + "' AND productname='" + product.getString(0) + "';";// WHERE datetime >='" + startDate + "' AND datetime <= '" + endDate + "' AND productname=

                                            Cursor productbymonth = db.rawQuery(query, null);
                                            if (productbymonth.getCount() > 0) {
                                                int totalQty = 0;
                                                Float totalValue = 0f;
                                                while (productbymonth.moveToNext()) {
                                                    if (productbymonth.getString(0) != null) {
                                                        totalQty += productbymonth.getInt(0);
                                                        totalValue += productbymonth.getInt(0) * productbymonth.getFloat(1);
                                                    } else {
                                                        totalQty += 0;
                                                        totalValue += 0;
                                                    }
                                                }
                                                mProductData1.setQuantity(String.valueOf(totalQty));
                                                mProductData1.setAmount(String.valueOf(totalValue));
                                            } else {
                                                mProductData1.setQuantity("0");
                                                mProductData1.setAmount("0");
                                            }
                                            mProductList.add(mProductData1);
                                            productbymonth.close();
                                        }
                                    } else {     //other months data
                                        String query;
                                        String valMonth = String.format("%02d", calMonth) + "-" + calYear;

                                        if (foodCategory == 0) {
                                            query = "SELECT  productqty, productprice,datetime,strftime('%m-%Y', datetime) as valMonth FROM cart WHERE valMonth ='" + valMonth + "' AND productname='" + product.getString(0) + "';";// WHERE datetime >='" + startDate + "' AND datetime <= '" + endDate + "' AND productname=
                                        } else if (foodCategory == 1) {
                                            query = "SELECT  productqty, productprice,datetime,strftime('%m-%Y', datetime) as valMonth FROM cart WHERE valMonth ='" + valMonth + "' AND pcategory='" + product.getString(0) + "';";// WHERE datetime >='" + startDate + "' AND datetime <= '" + endDate + "' AND productname=
                                        } else if (foodCategory == 2) {
                                            query = "SELECT  productqty, productprice,datetime,strftime('%m-%Y', datetime) as valMonth FROM cart WHERE valMonth ='" + valMonth + "' AND pgroup='" + product.getString(0) + "';";// WHERE datetime >='" + startDate + "' AND datetime <= '" + endDate + "' AND productname=
                                        } else
                                            query = "SELECT  productqty, productprice,datetime,strftime('%m-%Y', datetime) as valMonth FROM cart WHERE valMonth ='" + valMonth + "' AND productname='" + product.getString(0) + "';";// WHERE datetime >='" + startDate + "' AND datetime <= '" + endDate + "' AND productname=

                                        Cursor productbymonth = db.rawQuery(query, null);
                                        if (productbymonth.getCount() > 0) {
                                            int totalQty = 0;
                                            Float totalValue = 0f;
                                            while (productbymonth.moveToNext()) {
                                                if (productbymonth.getString(0) != null) {
                                                    totalQty += productbymonth.getInt(0);
                                                    totalValue += productbymonth.getInt(0) * productbymonth.getFloat(1);
                                                } else {
                                                    totalQty += 0;
                                                    totalValue += 0;
                                                }
                                            }
                                            mProductData1.setQuantity(String.valueOf(totalQty));
                                            mProductData1.setAmount(String.valueOf(totalValue));
                                        } else {
                                            mProductData1.setQuantity("0");
                                            mProductData1.setAmount("0");
                                        }
                                        mProductList.add(mProductData1);
                                        productbymonth.close();
                                    }
                                    c.add(Calendar.MONTH, 1);
                                    q++;
                                } while (q <= diffMonth);
                            } else {
                                for (int i = 0; i < dates.size(); i++) {
                                    mProductData1 = new ProductData();
                                    DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                                    String timestamp = df1.format(dates.get(i));
                                    String query;
                                    if (foodCategory == 0) {
                                        query = "SELECT  productqty, productprice,datetime,strftime('%d-%m-%Y', datetime) as valMonth,pgroup FROM cart WHERE valMonth='" + timestamp.split(" ")[0] + "' AND productname='" + product.getString(0) + "';";
                                    } else if (foodCategory == 1) {
                                        query = "SELECT  productqty, productprice,datetime,strftime('%d-%m-%Y', datetime) as valMonth,pgroup FROM cart WHERE valMonth='" + timestamp.split(" ")[0] + "' AND pcategory='" + product.getString(0) + "';";
                                    } else if (foodCategory == 2) {
                                        query = "SELECT  productqty, productprice,datetime,strftime('%d-%m-%Y', datetime) as valMonth,pgroup FROM cart WHERE valMonth='" + timestamp.split(" ")[0] + "' AND pgroup='" + product.getString(0) + "';";
                                    } else {
                                        query = "SELECT  productqty, productprice,datetime,strftime('%d-%m-%Y', datetime) as valMonth,pgroup FROM cart WHERE valMonth='" + timestamp.split(" ")[0] + "' AND productname='" + product.getString(0) + "';";
                                    }

                                    Cursor productbymonth = db.rawQuery(query, null);
                                    if (productbymonth.getCount() > 0) {
                                        int totalQty = 0;
                                        Float totalValue = 0f;
                                        while (productbymonth.moveToNext()) {
                                            if (productbymonth.getString(0) != null) {
                                                totalQty += productbymonth.getInt(0);
                                                totalValue += productbymonth.getInt(0) * productbymonth.getFloat(1);
                                            } else {
                                                totalQty += 0;
                                                totalValue += 0;
                                            }
                                        }
                                        mProductData1.setQuantity(String.valueOf(totalQty));
                                        mProductData1.setAmount(String.valueOf(totalValue));
                                    } else {
                                        mProductData1.setQuantity("0");
                                        mProductData1.setAmount("0");
                                    }
                                    mProductList.add(mProductData1);
                                    productbymonth.close();
                                }
                            }
                            main_product.setList(mProductList);
                        }
                        mProductList.add(main_product);
                        count++;
                    }
                }
                product.close();
                db.close();
            } catch (SQLException e) {
                e.printStackTrace();
                db.close();
                return false;
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean productDatas) {
            super.onPostExecute(productDatas);
            if (productDatas) {
                if (mProductList.size() > 0) {
                    tableA.removeAllViews();
                    tableB.removeAllViews();
                    tableC.removeAllViews();
                    tableD.removeAllViews();
                    for (int i = 0; i < mProductList.size(); i++) {//rows
                        TableRow componentBTableRow = new TableRow(context);
                        TableRow taleRowForTableD = new TableRow(context);
                        ProductData rows = mProductList.get(i);
                        for (int j = 0; j < rows.getList().size(); j++) {//columns
                            ProductData columns = rows.getList().get(j);
                            if (i == 0 && j == 0) {
                                tableA.addView(componentATableRow());//
                            } else if (i == 0 && j == 1) {// first two blank colmn
                                componentBTableRow.addView(componentBTableRow(""));
                            } else if (i == 0) {
                                componentBTableRow.addView(componentBTableRow(columns.getDatetime()));
                            } else if (j == 0) {
                                tableC.addView(tableRowForTableC(columns.getName()));
                            } else if (j == 1) {
                                taleRowForTableD.addView(tableRowForTableD("AMT", "QTY"));
                            } else {
                                taleRowForTableD.addView(tableRowForTableD(columns.getAmount(), columns.getQuantity()));
                            }
                        }
                        tableD.addView(taleRowForTableD);
                        tableB.addView(componentBTableRow);
                    }
                    spinner_foodCategory.setOnItemSelectedListener(foodCategoryListenter);
                }
            } else {
                Toast.makeText(getActivity(), "Failed to load data", Toast.LENGTH_SHORT).show();
            }
            pd.dismiss();
        }

        private String getEndDate(int month, int year) {

            Calendar calendar = Calendar.getInstance();
            // passing month-1 because 0-->jan, 1-->feb... 11-->dec
            calendar.set(year, month - 1, 1);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
            calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, calendar.getActualMaximum(Calendar.MINUTE));
            Date date = calendar.getTime();
            DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            return DATE_FORMAT.format(date);
        }
    }


    // read a view's height
    private int viewHeight(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    // match all height in a table row
    // to make a standard TableRow height
    private void matchLayoutHeight(TableRow tableRow, int height) {

        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {

            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {

            View view = tableRow.getChildAt(x);

            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }

    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);
            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }

        return heighestViewPosition == layoutPosition;
    }

    private static List<Date> getDates(String dateString1, String dateString2) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    /**
     * @param startDate
     * @param endDate
     * @return void
     * @brief Reports::productReports here we are passing staring data and ending date to
     * get the reports according to the product sold beteween that dates.
     */

    // horizontal scroll view custom class
    class MyHorizontalScrollView extends HorizontalScrollView {

        public MyHorizontalScrollView(Context context) {
            super(context);
        }

        @Override
        protected void onScrollChanged(int l, int t, int oldl, int oldt) {
            String tag = (String) this.getTag();

            if (tag.equalsIgnoreCase("horizontal scroll view b")) {
                horizontalScrollViewD.scrollTo(l, 0);
            } else {
                horizontalScrollViewB.scrollTo(l, 0);
            }
        }

    }

    // scroll view custom class
    class MyScrollView extends ScrollView {

        public MyScrollView(Context context) {
            super(context);
        }

        @Override
        protected void onScrollChanged(int l, int t, int oldl, int oldt) {

            String tag = (String) this.getTag();

            if (tag.equalsIgnoreCase("scroll view c")) {
                scrollViewD.scrollTo(0, t);
            } else {
                scrollViewC.scrollTo(0, t);
            }
        }
    }

    public void productReports(String startDate, String endDate, Boolean date_flag, int catType) throws InterruptedException {
        CreateTable createTable = new CreateTable();
        createTable.execute(startDate, endDate, String.valueOf(date_flag), String.valueOf(catType));
    }

    /**
     * @return void
     * @brief reverseDateFormat here we are passing staring data and ending date to
     * get the reports according to the product sold beteween that dates.
     */

    public String reverseDateFormat(String date) {
        //return date;
        return date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2) + date.substring(10, 16);
    }

    public static SalesReportFragment getInstance() {
        return GROUPID;
    }
}
