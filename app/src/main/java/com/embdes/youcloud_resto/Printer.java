package com.embdes.youcloud_resto;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.instabug.library.InstabugTrackingDelegate;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author EMBDES.
 * @version 1.0.
 *          This Activity doing kitchen, bill and order printing.
 */

@SuppressLint({"NewApi", "DefaultLocale"})
public class Printer extends Activity {

    BluetoothAdapter mBluetoothAdapter;
    MyDb myDb;
    private static final int REQUEST_ENABLE_BT = 2;
    public static final int MESSAGE_READ1 = 2;
    public static final int MESSAGE_WRITE1 = 3;
    boolean printerclassCheck = true;
    InputStream mmInStream;
    static OutputStream mmOutStream;
    public static Socket mysocket = null;
    ProgressDialog pDialog;
    Handler handler;
    int tableNo;
    int locationNo;
    String strName;
    int PRINTBILL = 0;
    int PAYBILLPRINT = 1;
    int ORDERNOPRINT = 2;
    int printFlag;
    String dataToPrint;
    Dialog settingDialog;
    ListView lv;
    BluetoothDevice mdevice, kdevice;
    String printdata;
    String btPrinterid, btkitchenPrinterid;
    private static int SERVERPORT = 9100;
    private static String SERVER_IP = "192.168.1.87";
    ArrayAdapter<BluetoothDevice> adapter;
    ArrayList<BluetoothDevice> connections = new ArrayList<BluetoothDevice>();
    ArrayAdapter<String> adaptername;
    ArrayList<String> connectionsname = new ArrayList<String>();
    String globledevice, globalsbuff;
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_TOAST = 4;
    private static final boolean D = true;
    private static final String TAG = "WIFIPrinterActivity";
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    public static int revBytes = 0;
    BluetoothSocket mmSocket;
    boolean printerDeviceCheck = false;
    int pFormate = 10;
    static int kitchenid;
    static Printer PrinterId;

    class details {
        String name;
        String add1;
        String add2;
        String city;
        String pin;
        String ph;
        String fid;
        String email;
        String tid;
        String webadd;
        String mess1;
        String mess2;
        int table;
        int dummytable = 0;
        float st;
        float vt;
        float dis;
        float tax;
        float tax1;
        float tax2;
        float tax3;
        int location;
        float cash;
        String lastloginusername;
    }

    ;

    class voucher {
        String voucherno;
        float voucheramount;
        int vouchercount;

        public voucher(String newVoucherno, float newVoucheramount) {
            voucherno = newVoucherno;
            voucheramount = newVoucheramount;
        }

        public voucher(int newVoucherCount) {
            vouchercount = newVoucherCount;
        }

    }

    class products {
        String productName;
        int qty;
        float price;
        float total;
        float subtotal;
        String typeName;
        float typeprice;
        int typeqty;
        float discount;
        String orderno;

        public products(String productName2, int qty2, float price2,
                        float total2, float subtotal2, String typeName2,
                        float typeprice2, int typeqty2, float discount2,
                        String orderno2) {
            productName = productName2;
            qty = qty2;
            price = price2;
            total = total2;
            subtotal = subtotal2;
            typeName = typeName2;
            typeprice = typeprice2;
            typeqty = typeqty2;
            discount = discount2;
            orderno = orderno2;
        }
    }

    ;

    //btOperation bo = new btOperation();
    //// ���뷽ʽ
    SQLiteDatabase db;
    /**
     * printing text align left
     */
    public static final int AT_LEFT = 0;
    /**
     * printing text align center
     */
    public static final int AT_CENTER = 1;
    /**
     * printing text align right
     */
    public static final int AT_RIGHT = 2;
    //	private regoPrinter mobileprint = null;
    boolean bConnect = true;
    //private int iObjectCode;

    //IntentFilter filter1,filter2,filter3;
    boolean isRegister = false;

    /**
     * Called when the activity is first created.
     */
    /* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        tableNo = bundle.getInt("TABLENO");
        locationNo = bundle.getInt("LOCATIONNO");
        printFlag = bundle.getInt("FLAG");
        setContentView(R.layout.progress);
        myDb = new MyDb(getApplicationContext());
        myDb.open();
        if (true == MainActivity.getInstance().isWifiPrinterEnable) {
            PrinterId = this;
            strName = "192.168.1.87:9100";
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor printerDetails = db.rawQuery("SELECT ipaddress, portno FROM locationprinter;", null);
            int printerDetailsCount = printerDetails.getCount();
            if (printerDetailsCount > 0) {
                printerDetails.moveToFirst();
                strName = printerDetails.getString(0) + ":" + printerDetails.getString(1);
                SERVER_IP = printerDetails.getString(0);
                SERVERPORT = Integer.parseInt(printerDetails.getString(1));
            }
            db.close();
            if (strName.length() == 0) {
                Toast.makeText(Printer.this, "Error:port name empty", Toast.LENGTH_SHORT).show();
                return;
            }
            if (bConnect) {
                try {
                    if (mysocket == null) {
                        new ClientThread().execute("ok");
                    }
                } finally {
                    if (printFlag == PRINTBILL) {
                        billClick(tableNo, locationNo, 0);
                        Intent mIntent = new Intent();
                        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntent.setClass(getApplicationContext(), Home.class);
                        startActivity(mIntent);
                    } else if (printFlag == PAYBILLPRINT) {
                        billClick(tableNo, locationNo, 1);
                        updatePayInfo();
                        Intent mIntent = new Intent();
                        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntent.setClass(getApplicationContext(), Home.class);
                        startActivity(mIntent);

                    } else if (printFlag == ORDERNOPRINT) {
//							orderNoBillPrint(""+tableNo);
                    } else {

                        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                        Cursor FoodId = db.rawQuery("SELECT DISTINCT foodid from cart WHERE tableno=" + tableNo + " AND locationid= " + locationNo + ";", null);
                        int FoodIddetails = FoodId.getCount();
                        int[] Foodlist = new int[10];
                        int i = 0;
                        while (FoodId.moveToNext()) {
                            Foodlist[i] = FoodId.getInt(0);
                            i++;
                        }
                        db.close();
                        int foodflag = 0;
                        for (int j = 0; j < i; j++) {

                            if ((j + 1) < i) {
                                click(tableNo, locationNo, Foodlist[j]);
                            } else {
                                foodflag = 1;
                                click(tableNo, locationNo, Foodlist[j]);
                            }
                        }
                    }
                    bConnect = false;
                }
            } else {
                bConnect = true;
            }
        } else {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                // Device does not support Bluetooth
            }
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent =
                        new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
            if (printFlag == PRINTBILL || printFlag == PAYBILLPRINT || printFlag == ORDERNOPRINT) {
                final SharedPreferences mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String restoredText = mSharedPreference.getString("btPrinterid", null);
                if (restoredText != null) {
                    btPrinterid = mSharedPreference.getString("btPrinterid", null);
                    mdevice = mBluetoothAdapter
                            .getRemoteDevice(btPrinterid);
                    if (printFlag == PRINTBILL) {
                        billClick(tableNo, locationNo, 0);
                    } else if (printFlag == PAYBILLPRINT) {
                        billClick(tableNo, locationNo, 1);
                    } else if (printFlag == ORDERNOPRINT) {
                        // orderNoBillPrint("" + tableNo);
                    }

                } else {
                    ListPairedDevices();
                }
            } else {
                if (MainActivity.getInstance().kotPrinterEnable == true) {
                    final SharedPreferences mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    String restoredText = mSharedPreference.getString("btkPrinterid", null);
                    if (restoredText != null) {
                        btPrinterid = mSharedPreference.getString("btkPrinterid", null);
                        kdevice = mBluetoothAdapter
                                .getRemoteDevice(btPrinterid);
                    } else {
                        ListPairedDevices();
                    }
                }
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                Cursor FoodId = db.rawQuery("SELECT DISTINCT foodid from cart WHERE tableno=" + tableNo + " AND locationid= " + locationNo + ";", null);
                int FoodIddetails = FoodId.getCount();
                int[] Foodlist = new int[10];
                int i = 0;
                while (FoodId.moveToNext()) {
                    Foodlist[i] = FoodId.getInt(0);
                    i++;
                }
                db.close();
                int foodflag = 0;
                for (int j = 0; j < i; j++) {
                    if ((j + 1) < i) {
                        click(tableNo, locationNo, Foodlist[j]);
                    } else {
                        foodflag = 1;
                        click(tableNo, locationNo, Foodlist[j]);
                    }
                }
            }
        }
    }

    public class ClientThread extends AsyncTask<String, Void, Boolean> {

		/* progress dialog to show user that the backup is processing. */
        /**
         * application context.
         */
        boolean isconnected = false;

        public void onPreExecute() {
            isconnected = false;


        }

        public Boolean doInBackground(final String... args) {
            try {
                mysocket = new Socket();
                mysocket.connect(new InetSocketAddress(SERVER_IP, SERVERPORT), 10000);
                mmOutStream = mysocket.getOutputStream();
                mmInStream = mysocket.getInputStream();
                isconnected = true;
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
                isconnected = false;
            } catch (IOException e1) {
                e1.printStackTrace();
                isconnected = false;
            }
            return false;
        }

        @SuppressLint("NewApi")
        @Override
        public void onPostExecute(final Boolean success) {
            if (isconnected == true) {

            } else {
                Toast.makeText(getApplicationContext(), "Selected WIFi KOT Printer is not Online trying to connect to bluetooth", Toast.LENGTH_LONG).show();
                Intent mIntent = new Intent();
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.setClass(getApplicationContext(), Home.class);
                startActivity(mIntent);
            }
        }
    }
    /**
     * This click function get called when user click on the kitchen print
     *
     * @param tbl
     * @param loc
     */
    @SuppressWarnings("null")
    public void click(int tbl, int loc, int foodid) {
        String productName1 = "product";
        int qty1 = 0;
        float price1 = 0;
        float total1 = 0;
        float subtotal1 = 0;
        String typeName1 = "some";
        float typeprice1 = 0;
        int typeqty1 = 0;
        float discount1 = 0;
        String orderno1 = "10001";
        String voucherno1 = "1001";
        details k_detail = new details();
        k_detail.table = tbl;
        k_detail.location = loc;
        k_detail.lastloginusername = "EmbDEs";
        kitchenid = foodid;
        products[] cart_items = new products[100];
        int item = 0;
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor shopDetails = db.rawQuery("SELECT terminalname,lastloginusername FROM masterterminal;", null);
        int shopCount = shopDetails.getCount();
        if (shopCount > 0) {
            shopDetails.moveToFirst();
            k_detail.name = shopDetails.getString(shopDetails.getColumnIndex("terminalname"));
            k_detail.lastloginusername = shopDetails.getString(shopDetails.getColumnIndex("lastloginusername"));

        }
        Cursor productDetails = db.rawQuery("SELECT * FROM cart WHERE tableno= " + tbl +
                " AND locationid= " + loc + " AND (billflag = 0 AND productqty-orderqty > 0 OR typeqty-ordertypeqty > 0) AND foodid= " + foodid + " ;", null);
        int count = productDetails.getCount();
        int updateQtyFlag = 0;
        if (count > 0) {
            productDetails.moveToFirst();
            for (Integer rowCount = 0; rowCount < count; rowCount++) {
                qty1 = 0;
                price1 = 0;
                if (productDetails.getInt(productDetails.getColumnIndex("productqty")) -
                        productDetails.getInt(productDetails.getColumnIndex("orderqty")) > 0) {
                    orderno1 = productDetails.getString(productDetails.getColumnIndex("orderno"));
                    productName1 = productDetails.getString(productDetails.getColumnIndex("productname"));
                    price1 = productDetails.getFloat(productDetails.getColumnIndex("productprice"));
                    qty1 = productDetails.getInt(productDetails.getColumnIndex("productqty"))
                            - productDetails.getInt(productDetails.getColumnIndex("orderqty"));
                    discount1 = productDetails.getFloat(productDetails.getColumnIndex("discount"));
                    updateQtyFlag = 1;
                }

                if (productDetails.getInt(productDetails.getColumnIndex("typeqty")) -
                        productDetails.getInt(productDetails.getColumnIndex("ordertypeqty")) > 0) {
                    orderno1 = productDetails.getString(productDetails.getColumnIndex("orderno"));
                    typeName1 = productDetails.getString(productDetails.getColumnIndex("typename"));
                    typeprice1 = productDetails.getFloat(productDetails.getColumnIndex("typeprice"));
                    typeqty1 = productDetails.getInt(productDetails.getColumnIndex("typeqty")) -
                            productDetails.getInt(productDetails.getColumnIndex("ordertypeqty"));
                    updateQtyFlag = 1;
                }
                cart_items[item] = new products(productName1, qty1, price1, total1, subtotal1, typeName1,
                        typeprice1, typeqty1, discount1, orderno1);
                item++;
                productDetails.moveToNext();

            }
            db.close();
            if (updateQtyFlag == 1) {

                if (printFlag == PRINTBILL) {
                    billClick(tableNo, locationNo, 0);
                } else if (printFlag == PAYBILLPRINT) {
                    updatePayInfo();
                } else if (printFlag == ORDERNOPRINT) {
                    //orderNoBillPrint(""+tableNo);
                } else {
                    Group.GROUPID.cartUpdate();
                }
                insert(k_detail, cart_items, item);

            } else {
                AlertDialog.Builder adb = new AlertDialog.Builder(
                        Printer.this, android.R.style.Theme_Dialog);
                adb.setMessage("Cart Table Empty For Current TableNo");
                adb.setPositiveButton("Ok", new
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent mIntent = new Intent();
                                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                mIntent.setClass(getApplicationContext(), Home.class);
                                startActivity(mIntent);
                            }
                        });
                adb.show();
                return;
            }

        } else {
            db.close();
            AlertDialog.Builder adb = new AlertDialog.Builder(
                    Printer.this, android.R.style.Theme_Dialog);
            adb.setMessage("Cart Table Empty For Current TableNo");
            adb.setPositiveButton("Ok", new
                    DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent mIntent = new Intent();
                            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mIntent.setClass(getApplicationContext(), Home.class);
                            startActivity(mIntent);
                        }
                    });
            return;
        }
    }

    /**
     * This method inputs details and products from the click method
     *
     * @param k_details
     * @param k_item
     * @param pcount
     */

    public void insert(details k_details, products k_item[], int pcount) {
        String[] buff = null;
        String cut, tcut;
        String dummy = "dummy", bill1, product1, fdot1, user, orderno, table;
        int i = 0, j = 0, x = 0;
        String sbuff;
        final String kitchenMessage;
        orderno = String.format("Order:%-9s", k_item[0].orderno);
        String timeStamp = new SimpleDateFormat("dd/MM/yy HH:mm").format(Calendar.getInstance().getTime());
        user = String.format("User:%-10s  Table:%-9d\n", k_details.lastloginusername, k_details.table);
        bill1 = String.format("    Loc id:%-7d   Time:%s\n", k_details.location, timeStamp);
        product1 = "PRODUCT                      QTY\n\n";
        for (j = 1; j < pcount; j++) {
            for (x = 0; x < j; x++) {
                if (k_item[j].productName.equals(k_item[x].productName))    //Here we are adding the quantity of the same products
                {
                    k_item[x].qty = k_item[x].qty + k_item[j].qty;
                    k_item[j].qty = 0;
                    if (k_item[j].typeName != null && k_item[x].typeName != null) {
                        if (k_item[j].typeName.equals(k_item[x].typeName)) //Here we are adding the quantity of the same types
                        {
                            k_item[x].typeqty = k_item[x].typeqty + k_item[j].typeqty;
                            k_item[j].typeqty = 0;
                        }
                    }
                }
            }
        }
        for (j = 0; j < pcount; j++) {

            if (k_item[j].qty > 0)                  //Here we are inserting the product details into the buffer
            {
                cut = k_item[j].productName;
                sbuff = String.format(" %-27s%4d \n", cut, k_item[j].qty);
                if (dummy.equals("dummy")) {
                    dummy = sbuff;
                } else {
                    dummy = dummy + sbuff;
                }
            }

            if (k_item[j].typeqty > 0)//Here we are inserting the type details into the buffer
            {
                tcut = k_item[j].typeName;
                if (dummy != null) {
                    dummy = dummy + String.format("*%-27s%4d \n\n", tcut, k_item[j].typeqty);
                } else {
                    dummy = "*" + String.format("*%-27s%4d \n\n", tcut, k_item[j].typeqty);
                }
            }
        }

        sbuff = String.format("-------------------------------\n");
        fdot1 = sbuff;

        kitchenMessage = String.format(/*%s\n*/"%s%s%s%s%22s   %s\n",/*shopName,*/user, bill1, product1, dummy, fdot1, orderno); //Here we are inserting buffer data into the print buffer
        deviceconnection(kitchenMessage, 4);
    }

    /**
     * This method inputs table, location number from the user.
     *
     * @param tableNo
     * @param locationNo
     * @param printFormat
     */
    public void billClick(int tableNo, int locationNo, int printFormat) {
        details detail = new details();
        detail.table = tableNo;
        int item = 0;
        products[] bill_items = new products[100];
        String productName1 = "product";
        int qty1 = 0;
        float price1 = 0;
        float total1 = 0;
        float subtotal1 = 0;
        String typeName1 = "some";
        float typeprice1 = 0;
        int typeqty1 = 0;
        float discount1 = 0;
        String orderno1 = "10001";
        String voucherno1 = "1001";
        voucher[] voucherDetails = new voucher[100];
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor shopDetails = db.rawQuery("SELECT terminalname,add1,add2,city,pinno,mobileno,webadd,facebookid,twitterid," +
                "email,message1,message2,lastloginusername FROM masterterminal;", null);
        int shopCount = shopDetails.getCount();
        if (shopCount > 0) {                        //Here we are inserting the masterterminal into the structure
            shopDetails.moveToFirst();

            detail.name = shopDetails.getString(shopDetails.getColumnIndex("terminalname"));

            detail.add1 = shopDetails.getString(shopDetails.getColumnIndex("add1"));

            detail.add2 = shopDetails.getString(shopDetails.getColumnIndex("add2"));

            detail.city = shopDetails.getString(shopDetails.getColumnIndex("city"));

            detail.pin = shopDetails.getString(shopDetails.getColumnIndex("pinno"));

            detail.ph = shopDetails.getString(shopDetails.getColumnIndex("mobileno"));

            detail.webadd = shopDetails.getString(shopDetails.getColumnIndex("webadd"));

            detail.fid = shopDetails.getString(shopDetails.getColumnIndex("facebookid"));

            detail.tid = shopDetails.getString(shopDetails.getColumnIndex("twitterid"));

            detail.email = shopDetails.getString(shopDetails.getColumnIndex("email"));

            detail.mess1 = shopDetails.getString(shopDetails.getColumnIndex("message1"));

            detail.mess2 = shopDetails.getString(shopDetails.getColumnIndex("message2"));

            detail.lastloginusername = shopDetails.getString(shopDetails.getColumnIndex("lastloginusername"));

        } else {
            db.close();
            AlertDialog.Builder adb = new AlertDialog.Builder(
                    Printer.this, android.R.style.Theme_Dialog);
            adb.setMessage("MasterTerminal Table is Empty");
            adb.setPositiveButton("Ok", new
                    DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent mIntent = new Intent();
                            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mIntent.setClass(getApplicationContext(), Home.class);
                            startActivity(mIntent);
                        }
                    });
            adb.show();
        }
        Cursor productDetails = db.rawQuery("SELECT * FROM cart WHERE tableno = " + tableNo + " AND locationid= " + locationNo +
                " AND billflag= 0 AND checkflag=0 AND " +
                "tbillflag=0;", null);
        int count = productDetails.getCount();
        if (count > 0) {
            productDetails.moveToFirst();
            for (Integer rowCount = 0; rowCount < count; rowCount++) {
                productName1 = productDetails.getString(productDetails.getColumnIndex("productname"));
                price1 = productDetails.getFloat(productDetails.getColumnIndex("productprice"));
                if (printFlag == PRINTBILL) {
                    qty1 = productDetails.getInt(productDetails.getColumnIndex("orderqty"));
                    typeqty1 = productDetails.getInt(productDetails.getColumnIndex("ordertypeqty"));
                } else {
                    qty1 = productDetails.getInt(productDetails.getColumnIndex("billqty"));
                    typeqty1 = productDetails.getInt(productDetails.getColumnIndex("billtypeqty"));
                }

                discount1 = productDetails.getFloat(productDetails.getColumnIndex("discount"));
                typeName1 = productDetails.getString(productDetails.getColumnIndex("typename"));
                typeprice1 = productDetails.getFloat(productDetails.getColumnIndex("typeprice"));
                orderno1 = productDetails.getString(productDetails.getColumnIndex("orderno"));
                bill_items[item] = new products(productName1, qty1, price1, total1, subtotal1, typeName1, typeprice1,
                        typeqty1, discount1, orderno1);
                item++;
                productDetails.moveToNext();
            }
        } else {
            db.close();
            AlertDialog.Builder adb = new AlertDialog.Builder(
                    Printer.this, android.R.style.Theme_Dialog);
            adb.setMessage("Cart Table Empty For Current TableNo");
            adb.setPositiveButton("Ok", new
                    DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent mIntent = new Intent();
                            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mIntent.setClass(getApplicationContext(), Home.class);
                            startActivity(mIntent);
                        }
                    });
            adb.show();
            return;
        }

        Cursor taxDetails = db.rawQuery("SELECT servicetaxper,taxper1,taxper2,taxper3 FROM tax;", null);

        int taxDetailsCount = taxDetails.getCount();
        if (taxDetailsCount > 0) {
            taxDetails.moveToFirst();            //Here we are inserting the tax information into the structure
            detail.tax = taxDetails.getFloat(taxDetails.getColumnIndex("servicetaxper"));
            detail.tax1 = taxDetails.getFloat(taxDetails.getColumnIndex("taxper1"));
            detail.tax2 = taxDetails.getFloat(taxDetails.getColumnIndex("taxper2"));
            detail.tax3 = taxDetails.getFloat(taxDetails.getColumnIndex("taxper3"));
        } else {
        }
        Cursor receiptDetails = db.rawQuery("SELECT docid ,amount FROM receipt WHERE orderid='" +
                orderno1 + "' AND docname='VOUCHER' AND checkflag=0", null);
        int receiptsCount = receiptDetails.getCount();
        count = 0;
        if (receiptsCount > 0) {
            receiptDetails.moveToFirst();
            for (Integer rowCount = 0; rowCount < receiptsCount; rowCount++) {
                String voucherno = receiptDetails.getString(0);
                float voucheramount = receiptDetails.getFloat(1);
                voucherDetails[count] = new voucher(voucherno, voucheramount);
                count++;
                receiptDetails.moveToNext();
            }
            voucherDetails[0].vouchercount = count;
        } else {
            voucherDetails[count] = new voucher(0);
        }
        Cursor amountDetails = db.rawQuery("SELECT amount FROM receipt WHERE orderid='" +
                orderno1 + "' AND docname='CASH' AND checkflag=0", null);
        int amountCount = amountDetails.getCount();
        if (amountCount > 0) {
            amountDetails.moveToFirst();
            for (Integer rowCount = 0; rowCount < amountCount; rowCount++) {
                detail.cash = detail.cash + amountDetails.getFloat(amountDetails.getColumnIndex("amount"));
                amountDetails.moveToNext();
            }

        } else {
        }
        db.close();
        billInsert(detail, bill_items, voucherDetails, item, printFormat);
    }


    /**
     * This method order number from the user.
     *
     * @param
     */
//	public void orderNoBillPrint(String orderno)
//	{
//		details detail = new details();
//		int item = 0;
//		products[] bill_items = new products[100];
//		detail.tax=0;
//		detail.tax1=0;
//		detail.tax2=0;
//		detail.tax3=0;
//		detail.cash=0;
//		String productName1 = "product";
//		int count=0;
//		int  qty1 = 0;
//		float price1 = 0;
//		float total1=0;
//		float subtotal1=0;
//		String typeName1="some";
//		float typeprice1 =0;
//		int typeqty1 = 0;
//		float discount1=0;
//		String orderno1 = "10001";
//		String voucherno1 = "1001";
//		voucher[] voucherDetails = new voucher[100];
//		db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
//		Cursor shopDetails= db.rawQuery("SELECT terminalname,add1,add2,city,pinno,mobileno,webadd,facebookid,twitterid," +
//				"email,message1,message2,lastloginusername FROM masterterminal;",null);
//		int shopCount = shopDetails.getCount();
//		if(shopCount > 0)
//		{
//			shopDetails.moveToFirst();
//
//			detail.name = shopDetails.getString(shopDetails.getColumnIndex("terminalname"));
//
//			detail.add1 = shopDetails.getString(shopDetails.getColumnIndex("add1"));
//
//			detail.add2 = shopDetails.getString(shopDetails.getColumnIndex("add2"));
//
//			detail.city = shopDetails.getString(shopDetails.getColumnIndex("city"));
//
//			detail.pin = shopDetails.getString(shopDetails.getColumnIndex("pinno"));
//
//			detail.ph = shopDetails.getString(shopDetails.getColumnIndex("mobileno"));
//
//			detail.webadd = shopDetails.getString(shopDetails.getColumnIndex("webadd"));
//
//			detail.fid = shopDetails.getString(shopDetails.getColumnIndex("facebookid"));
//
//			detail.tid = shopDetails.getString(shopDetails.getColumnIndex("twitterid"));
//
//			detail.email = shopDetails.getString(shopDetails.getColumnIndex("email"));
//
//			detail.mess1 = shopDetails.getString(shopDetails.getColumnIndex("message1"));
//
//			detail.mess2 = shopDetails.getString(shopDetails.getColumnIndex("message2"));
//
//			detail.lastloginusername = shopDetails.getString(shopDetails.getColumnIndex("lastloginusername"));
//
//		}
//		else
//		{
//			db.close();
//			AlertDialog.Builder adb = new AlertDialog.Builder(
//					Printer.this, android.R.style.Theme_Dialog);
//			adb.setMessage("MasterTerminal Table is Empty");
//			adb.setPositiveButton("Ok", new
//					DialogInterface.OnClickListener() {
//				public void onClick(DialogInterface dialog, int which) {
//					Intent mIntent = new Intent();
//					mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//					mIntent.setClass(getApplicationContext(), Home.class);
//					startActivity(mIntent);
//				}
//			});
//			adb.show();
//		}
//
//		Cursor taxDetails = db.rawQuery("SELECT servicetaxper,taxper1,taxper2,taxper3 FROM tax;", null);
//		int taxDetailsCount = taxDetails.getCount();
//		if(taxDetailsCount > 0)
//		{
//			taxDetails.moveToFirst();			//Here we are inserting the tax information into the structure
//			detail.tax = taxDetails.getFloat(taxDetails.getColumnIndex("servicetaxper"));
//			detail.tax1 = taxDetails.getFloat(taxDetails.getColumnIndex("taxper1"));
//			detail.tax2 = taxDetails.getFloat(taxDetails.getColumnIndex("taxper2"));
//			detail.tax3 = taxDetails.getFloat(taxDetails.getColumnIndex("taxper3"));
//		}
//		else
//		{
//		}
//		Cursor productDetails = db.rawQuery("SELECT productname,productprice,billqty,discount,billedqty,typename," +
//				"billedtypeqty,typeprice,tableno FROM cart WHERE orderno='"+orderno+"';", null);
//		int productCount = productDetails.getCount();
//		if(productCount > 0)
//		{
//			for(Integer rowCount = 0; rowCount < productCount; rowCount++)
//			{
//				productDetails.moveToFirst();
//				productName1 =  productDetails.getString(productDetails.getColumnIndex("productname"));
//				price1 = productDetails.getFloat(productDetails.getColumnIndex("productprice"));
//				qty1 = productDetails.getInt(productDetails.getColumnIndex("billedqty"));
//				discount1 = productDetails.getFloat(productDetails.getColumnIndex("discount"));
//				typeName1 = productDetails.getString(productDetails.getColumnIndex("typename"));
//				typeqty1 = productDetails.getInt(productDetails.getColumnIndex("billedtypeqty"));
//				typeprice1 = productDetails.getFloat(productDetails.getColumnIndex("typeprice"));
//				detail.table = productDetails.getInt(productDetails.getColumnIndex("tableno"));
//				orderno1 = orderno;
//				bill_items[item] = new products(productName1, qty1, price1, total1, subtotal1,typeName1, typeprice1,
//						typeqty1, discount1, orderno1);
//				item++;
//				productDetails.moveToNext();
//			}
//		}
//		else
//		{
//			AlertDialog.Builder adb = new AlertDialog.Builder(
//					Printer.this, android.R.style.Theme_Dialog);
//			adb.setMessage("Cart Table Empty For Current OrderNo");
//
//			adb.setPositiveButton("Ok", new
//					DialogInterface.OnClickListener() {
//				public void onClick(DialogInterface dialog, int which) {
//					Intent mIntent = new Intent();
//					mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//					mIntent.setClass(getApplicationContext(), Home.class);
//					startActivity(mIntent);
//				}
//			});
//			adb.show();
//			db.close();
//			return;
//		}
//		Cursor receiptDetails = db.rawQuery("SELECT docid ,amount FROM receipt WHERE orderid='"+orderno1+"' AND " +
//				"docname='VOUCHER' AND checkflag=1", null);
//		int receiptsCount = receiptDetails.getCount();
//		count =0;
//		if(receiptsCount > 0)
//		{
//			receiptDetails.moveToFirst();
//			for(Integer rowCount = 0; rowCount < receiptsCount; rowCount++)
//			{
//				String voucherno = receiptDetails.getString(0);
//				float voucheramount = receiptDetails.getFloat(1);
//				voucherDetails[count] = new voucher(voucherno, voucheramount);
//				count++;
//				receiptDetails.moveToNext();
//			}
//			voucherDetails[0].vouchercount = count;
//		}
//		else
//		{
//			voucherDetails[count] = new voucher(0);
//		}
//
//		Cursor amountDetails = db.rawQuery("SELECT amount FROM receipt WHERE orderid='"+orderno1+"' AND docname='CASH'" +
//				" AND checkflag=1", null);
//		int amountCount = amountDetails.getCount();
//		if(amountCount > 0)
//		{
//			amountDetails.moveToFirst();
//			for(Integer rowCount = 0; rowCount < amountCount; rowCount++)
//			{
//				detail.cash=detail.cash + amountDetails.getFloat(amountDetails.getColumnIndex("amount"));
//				amountDetails.moveToNext();
//			}
//		}
//		else
//		{
//		}
//		for(int j=0; j < item; j++)
//		{
//			Cursor groupid = db.rawQuery("SELECT groupid FROM product WHERE productdescriptionbill='"+bill_items[j].productName+"';", null);
//			groupid.moveToFirst();
//			Cursor groupNameDetails = db.rawQuery("SELECT groupname FROM grouptable WHERE groupid = "+
//					groupid.getInt(groupid.getColumnIndex("groupid"))+";", null);
//			groupNameDetails.moveToFirst();
//			productName1 = groupNameDetails.getString(groupNameDetails.getColumnIndex("groupname"));
//			price1 = bill_items[j].qty * bill_items[j].price + bill_items[j].typeqty * bill_items[j].typeprice;
//			qty1 = bill_items[j].qty + bill_items[j].typeqty;
//			bill_items[j] = new products(productName1, qty1, price1, total1, subtotal1,typeName1, typeprice1,
//					typeqty1, discount1, orderno1);
//
//		}
//		db.close();
//		billInsert(detail, bill_items ,voucherDetails, item, 2);
//	}
    public static Printer getInstance() {
        return PrinterId;
    }

    /**
     * This method inputs @param from the billClick method.
     *
     * @param details_items
     * @param pro_items
     * @param voucherDetails
     * @param pro_count
     * @param printFormat
     */
    public void bill(details details_items, products pro_items[], voucher voucherDetails[], int pro_count, int printFormat) {
        String buff[];
        String cut = null, dummy = "dummy", ph1, bill1, product1, sub1, sdot1, fdot1, ddot1;
        int i = 0, j = 0, count = 0, x = 0, vcount = 0;
        float total = 0, subtotal = 0, service_charge = 0, dis_amount = 0, gtotal = 0, typetotal = 0, service_charge1 = 0,
                service_charge2 = 0, service_charge3 = 0, realsubtotal = 0;
        String sbuff = null;
        String total1, date, time, tax = ".", user;
        ph1 = String.format("Tel:%s\n", details_items.ph);
        bill1 = String.format("Bill: %-7s    Table: %-7d\n", pro_items[0].orderno, details_items.table);
        product1 = String.format("Product   Qty  Price      Total\n");
        for (j = 1; j < pro_count; j++) {
            for (x = 0; x < j; x++) {
                if (pro_items[j].productName.equals(pro_items[x].productName))    //Here we are adding the quantity of the same products
                {
                    pro_items[x].qty = pro_items[x].qty + pro_items[j].qty;
                    if (printFormat != 0)
                        pro_items[x].price = pro_items[x].price + pro_items[j].price;
                    pro_items[j].qty = 0;
                }
                if (pro_items[j].typeName.equals(pro_items[x].typeName))    //Here we are adding the quantity of the same types
                {
                    pro_items[x].typeqty = pro_items[x].typeqty + pro_items[j].typeqty;
                    pro_items[j].typeqty = 0;
                }

            }
        }

        for (j = 0; j < pro_count; j++) {
            if (pro_items[j].qty > 0 && pro_items[j].price > 0)    //Here we are inserting the product details into the buffer
            {
                total = pro_items[j].qty * pro_items[j].price;
                if (printFormat != 0) {
                    subtotal = subtotal + pro_items[j].price;
                } else {
                    subtotal = subtotal + total;
                }
                if (printFormat != 0) {
                    if (pro_items[j].productName.length() > 9) {
                        cut = pro_items[j].productName.substring(0, 9);
                        //reducing product name to 9 character
                    } else {
                        cut = pro_items[j].productName.substring(0, pro_items[j].productName.length());
                    }
                    sbuff = String.format("%-13s %4d %12.2f\n", cut, pro_items[j].qty, pro_items[j].price);
                } else {
                    if (pro_items[j].productName.length() > 9) {
                        cut = pro_items[j].productName.substring(0, 9);
                        //Here reducing product name to 9 character
                    } else {
                        cut = pro_items[j].productName.substring(0, pro_items[j].productName.length());
                    }
                    sbuff = String.format("%-9s%3d%8.2f%11.2f\n", cut, pro_items[j].qty, pro_items[j].price, total);
                }
                if (dummy.equals("dummy")) {
                    dummy = sbuff;
                } else {
                    dummy = dummy + sbuff;
                }
            }

            if (printFormat == 0) {
                if (pro_items[j].typeqty > 0 && pro_items[j].typeprice > 0)    //Here we are inserting the type details into the buffer
                {
                    if (pro_items[j].typeName.length() > 8) {
                        cut = pro_items[j].typeName.substring(0, 8);
                        //Here reducing type name to 8 character
                    } else {
                        cut = pro_items[j].typeName.substring(0, pro_items[j].typeName.length());
                    }
                    typetotal = pro_items[j].typeqty * pro_items[j].typeprice;
                    subtotal = subtotal + typetotal;
                    dummy = dummy + String.format(">%-8s%3d%8.2f%11.2f\n", cut, pro_items[j].typeqty, pro_items[j].typeprice, typetotal);
                }
            }
        }
        sdot1 = String.format("                       --------\n");
        realsubtotal = subtotal;
        sub1 = String.format("Sub Total:      %15.2f\n", subtotal);
        if (printFormat != 0) {
            if (pro_items[0].discount > 0) {
                // If discount is more than 0 it will print the Bill
                dis_amount = (pro_items[0].discount * 100) / (subtotal);
                subtotal = subtotal - pro_items[0].discount;
                if (printFormat == 2) {
                    sub1 = sub1 + String.format("Discount:        %14.2f\n", pro_items[0].discount);
                } else {
                    sub1 = sub1 + String.format("Discount@%5.2f%%  %14.2f\n", dis_amount, pro_items[0].discount);
                }
            }

            for (vcount = 0; vcount < voucherDetails[0].vouchercount; vcount++) {
                if (voucherDetails[vcount].voucheramount > 0) {
                    subtotal = subtotal - voucherDetails[vcount].voucheramount;
                    sbuff = String.format("Voucher: %-13s%9.2f\n", voucherDetails[vcount].voucherno, voucherDetails[vcount].voucheramount);
                    sub1 = sub1 + sbuff;
                }

            }

            if (pro_items[0].discount > 0 || vcount > 0) {
                sbuff = String.format("                       --------\nSub Total:      %15.2f\n", subtotal);
                sub1 = sub1 + sbuff;
            }

        }

        fdot1 = String.format("-------------------------------\n");
        if (details_items.tax > 0)                    // If Service tax is more than 0 it will print the Bill
        {
            service_charge = (realsubtotal * details_items.tax) / 100;
            subtotal = subtotal + service_charge;
            tax = String.format("GST        @%5.2f%% %12.2f\n", details_items.tax, service_charge);
        }

        if (details_items.tax1 > 0)                    // If Service tax1 is more than 0 it will print the Bill
        {
            service_charge1 = (realsubtotal * details_items.tax1) / 100;
            subtotal = subtotal + service_charge1;
            tax = tax + String.format("Service tax @%5.2f%%%12.2f\n", details_items.tax1, service_charge1);
        }
        if (details_items.tax2 > 0)                    // If Service tax2 is more than 0 it will print the Bill
        {
            service_charge2 = (realsubtotal * details_items.tax2) / 100;
            subtotal = subtotal + service_charge2;
            tax = tax + String.format("Service tax2@%5.2f%% %11.2f\n", details_items.tax2, service_charge2);
        }

        if (details_items.tax3 > 0)                    // If Service tax3 is more than 0 it will print the Bill
        {
            service_charge3 = (realsubtotal * details_items.tax3) / 100;
            subtotal = subtotal + service_charge3;
            tax = tax + String.format("Service tax3@%5.2f%%%12.2f\n", details_items.tax3, service_charge3);

        }
        if (tax.equals(".")) {
            tax = String.format("                       --------\n");
        } else {
            tax = tax + String.format("                       --------\n");
        }
        gtotal = subtotal;

        if (printFormat != 0) {
            details_items.cash = Float.parseFloat(Group.GROUPID.finaltotal);//cashdiscount.cash;
            //balance
            total1 = String.format("Total Amount:%18.2f\nCash Paid:%21.2f", gtotal,
                    details_items.cash);

        } else {
            total1 = String.format("Total Amount To Pay%12.2f\n", gtotal);
        }
        ddot1 = String.format("                       ========\n");
        user = String.format("User: %s", details_items.lastloginusername);
        String timeStamp = new SimpleDateFormat("dd/MM/yy HH:mm").format(Calendar.getInstance().getTime());
        sbuff = String.format("%s\n%s\n%s\n%s\n%s%s\n%s%s%s%s%s%s%s%s%s%s\n%s\n%s%s %15s  ", details_items.name, details_items.add1,
                details_items.add2, details_items.city, ph1, details_items.webadd, bill1, product1, dummy, sdot1,
                sub1, tax, total1, ddot1, fdot1, details_items.mess1, details_items.mess2, fdot1, user, timeStamp);
        // Here we are storing all the data into one String sbuff.
        if (true == MainActivity.getInstance().isWifiPrinterEnable) {
            Begin();
            write((sbuff).getBytes());
            write(("\r\n").getBytes());
        } else {
            globalsbuff = sbuff;
            deviceconnection(sbuff, printFormat);
        }
    }

    public void billInsert(details details_items, products pro_items[], voucher voucherDetails[], int pro_count, int printFormat) {
        String buff[];
        String cut = null, dummy = "dummy", ph1, bill1, product1, sub1, sdot1, fdot1, ddot1;
        int i = 0, j = 0, count = 0, x = 0, vcount = 0;
        float total = 0, subtotal = 0, service_charge = 0, dis_amount = 0, gtotal = 0, typetotal = 0, service_charge1 = 0,
                service_charge2 = 0, service_charge3 = 0, realsubtotal = 0;
        String sbuff = null;
        String total1, date, time, tax = ".", user;
        ph1 = String.format("Tel:%s\n", details_items.ph);
        String dine = "DINE IN";
        String take = "T/A";
        if (Integer.parseInt(Group.GROUPID.table) < 1500) {
            bill1 = String.format("O/No: %-7s           %-7s \n", pro_items[0].orderno, dine);
        } else {
            bill1 = String.format("O/No: %-7s           %-7s \n", pro_items[0].orderno, take);
        }
        product1 = String.format("Product   Qty  Price      Total\n");
        for (j = 1; j < pro_count; j++) {
            for (x = 0; x < j; x++) {
                if (pro_items[j].productName.equals(pro_items[x].productName))    //Here we are adding the quantity of the same products
                {
                    pro_items[x].qty = pro_items[x].qty + pro_items[j].qty;
                    pro_items[j].qty = 0;
                    if (pro_items[j].typeName.equals(pro_items[x].typeName))    //Here we are adding the quantity of the same types
                    {
                        pro_items[x].typeqty = pro_items[x].typeqty + pro_items[j].typeqty;
                        pro_items[j].typeqty = 0;
                    }
                }


            }
        }

        for (j = 0; j < pro_count; j++) {
            if (pro_items[j].qty > 0 && pro_items[j].price > 0)    //Here we are inserting the product details into the buffer
            {
                total = pro_items[j].qty * pro_items[j].price;
                {
                    subtotal = subtotal + total;
                }
                {
                    if (pro_items[j].productName.length() > 9) {
                        cut = pro_items[j].productName.substring(0, 9);
                        //Here reducing product name to 9 character
                    } else {
                        cut = pro_items[j].productName.substring(0, pro_items[j].productName.length());
                    }
                    sbuff = String.format("%-9s%3d%8.2f%11.2f\n", cut, pro_items[j].qty, pro_items[j].price, total);
                }
                if (dummy.equals("dummy")) {
                    dummy = sbuff;
                } else {
                    dummy = dummy + sbuff;
                }
            }
            {
                if (pro_items[j].typeqty > 0 && pro_items[j].typeprice > 0)    //Here we are inserting the type details into the buffer
                {
                    if (pro_items[j].typeName.length() > 8) {
                        cut = pro_items[j].typeName.substring(0, 8);
                        //Here reducing type name to 8 character
                    } else {
                        cut = pro_items[j].typeName.substring(0, pro_items[j].typeName.length());
                    }
                    typetotal = pro_items[j].typeqty * pro_items[j].typeprice;
                    subtotal = subtotal + typetotal;
                    dummy = dummy + String.format(">%-8s%3d%8.2f%11.2f\n", cut, pro_items[j].typeqty, pro_items[j].typeprice, typetotal);
                }
            }
        }
        sdot1 = String.format("                       --------\n");
        realsubtotal = subtotal;
        sub1 = String.format("Sub Total:      %15.2f\n", subtotal);
        fdot1 = String.format("-------------------------------\n");
        if (details_items.tax > 0)                    // If Service tax is more than 0 it will print the Bill
        {
            service_charge = (realsubtotal * details_items.tax) / 100;
            subtotal = subtotal + service_charge;
            tax = String.format("GST        @%5.2f%% %12.2f\n", details_items.tax, service_charge);
        }

        if (details_items.tax1 > 0)                    // If Service tax1 is more than 0 it will print the Bill
        {
            service_charge1 = (realsubtotal * details_items.tax1) / 100;
            subtotal = subtotal + service_charge1;
            tax = tax + String.format("Service tax @%5.2f%%%12.2f\n", details_items.tax1, service_charge1);
        }
        if (details_items.tax2 > 0)                    // If Service tax2 is more than 0 it will print the Bill
        {
            service_charge2 = (realsubtotal * details_items.tax2) / 100;
            subtotal = subtotal + service_charge2;
            tax = tax + String.format("Service tax2@%5.2f%% %11.2f\n", details_items.tax2, service_charge2);
        }

        if (details_items.tax3 > 0)                    // If Service tax3 is more than 0 it will print the Bill
        {
            service_charge3 = (realsubtotal * details_items.tax3) / 100;
            subtotal = subtotal + service_charge3;
            tax = tax + String.format("Service tax3@%5.2f%%%12.2f\n", details_items.tax3, service_charge3);

        }
        if (tax.equals(".")) {
            tax = String.format("                       --------\n");
        } else {
            tax = tax + String.format("                       --------\n");
        }
        gtotal = subtotal;
        if (printFormat != 0) {
            details_items.cash = Float.parseFloat(Group.GROUPID.finaltotal);//cashdiscount.cash;
            total1 = String.format("Total Amount:%18.2f\nCash Paid:%21.2f", gtotal,
                    details_items.cash);
        } else {
            total1 = String.format("Total Amount To Pay%12.2f\n", gtotal);

        }
        ddot1 = String.format("                       ========\n");
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor chairs = db.rawQuery("SELECT covers FROM cart WHERE tableno =" +
                Group.GROUPID.table + " AND locationid =" + user_Login.user_LoginID.LOCATIONNO + ";", null);
        int cou = chairs.getCount();
        chairs.moveToFirst();
        String guests = "";
        if (cou > 0) {
            guests = chairs.getString(chairs.getColumnIndex("covers"));
        }
        db.close();
        String timeStamp = new SimpleDateFormat("dd/MM/yy HH:mm").format(Calendar.getInstance().getTime());
        String terminal = user_Login.user_LoginID.terminal;
        user = String.format("User: %-7s %-5s ", details_items.lastloginusername, terminal);
        String ticket = String.format("                 %-3s \n", timeStamp);
        String table = String.format("Table: %-7s          Guests:%-2s", details_items.table, guests);
        sbuff = String.format("%s\n%s\n%s\n%s\n%s%s\n\n%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n%s\n%s  ", details_items.name, details_items.add1,
                details_items.add2, details_items.city, ph1, details_items.webadd, user, fdot1, ticket, bill1, table, fdot1, product1, dummy, sdot1,
                sub1, tax, total1, ddot1, fdot1, details_items.mess1, details_items.mess2, fdot1);
        // Here we are storing all the data into one String sbuff.
        if (true == MainActivity.getInstance().isWifiPrinterEnable) {
            Begin();
            write((sbuff).getBytes());
            write(("\r\n").getBytes());
        } else {
            globalsbuff = sbuff;
            deviceconnection(sbuff, printFormat);
        }
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     *
     * @param
     */
    @SuppressLint("NewApi")
    public synchronized void connect(BluetoothDevice device) throws IOException {

        BluetoothSocket tmp = null;
        try {
            Method m;
            m = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
            tmp = (BluetoothSocket) m.invoke(device, 1);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            Toast.makeText(Printer.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            e.printStackTrace();
            Toast.makeText(Printer.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            e.printStackTrace();
            Toast.makeText(Printer.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            e.printStackTrace();
            Toast.makeText(Printer.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        }
        mmSocket = tmp;
        mBluetoothAdapter.cancelDiscovery();
        new Connection().execute("ok");
    }

    public class Connection extends AsyncTask<String, Void, Boolean> {

		/* progress dialog to show user that the backup is processing. */
        /**
         * application context.
         */
        boolean isconnected = false;

        public void onPreExecute() {
            isconnected = false;
        }

        public Boolean doInBackground(final String... args) {
            try {
                mmSocket.connect();
                mmInStream = mmSocket.getInputStream();
                mmOutStream = mmSocket.getOutputStream();
                isconnected = true;
            } catch (IOException e) {
                e.printStackTrace();
                isconnected = false;
            }

            return false;
        }

        @SuppressLint("NewApi")
        @Override
        public void onPostExecute(final Boolean success) {
            try {
                Begin();
                write((printdata + "\n").getBytes());
                Pcut();
                mmSocket.close();
                if (pFormate == PAYBILLPRINT) {
                    updatePayInfo();
                } else {
                }
                Intent mIntent = new Intent();
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.setClass(getApplicationContext(), Home.class);
                startActivity(mIntent);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void ListPairedDevices() {

        Set<BluetoothDevice> bondedDevices = BluetoothAdapter
                .getDefaultAdapter().getBondedDevices();
        for (int k = 0; k < bondedDevices.size(); k++) {
        }
        if (bondedDevices.size() > 0) {
            for (BluetoothDevice bondDeice : bondedDevices) {
                connections.add(bondDeice);
                connectionsname.add(bondDeice.getName());
                printerDeviceCheck = true;
            }
        }
        if (printerDeviceCheck == true) {
            printerDeviceCheck = false;
            Device_Select();
        } else {
            Toast.makeText(getApplicationContext(), "Printer not found", Toast.LENGTH_SHORT).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        }

    }

    private void Device_Select() {
        settingDialog = new Dialog(Printer.this);
        settingDialog.setContentView(R.layout.fragment_scanner_device_selection);
        settingDialog.setTitle("Paired Device");
        settingDialog.setCancelable(true);
        lv = (ListView) settingDialog.findViewById(R.id.list);
        settingDialog.show();
        adapter = new ArrayAdapter<BluetoothDevice>(this, android.R.layout.simple_list_item_1, connections);
        adaptername = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, connectionsname);
        lv.setAdapter(adaptername);
        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                try {
                    globledevice = adapter.getItem(arg2).getAddress();
                    if (printFlag == PRINTBILL || printFlag == PAYBILLPRINT || printFlag == ORDERNOPRINT) {
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("btPrinterid", globledevice);
                        editor.commit();
                        mdevice = mBluetoothAdapter
                                .getRemoteDevice(globledevice);
                        if (printFlag == PRINTBILL) {
                            billClick(tableNo, locationNo, 0);
                        } else if (printFlag == PAYBILLPRINT) {
                            billClick(tableNo, locationNo, 1);
                        } else if (printFlag == ORDERNOPRINT) {
                            //orderNoBillPrint(""+tableNo);
                        } else {
                            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                            Cursor FoodId = db.rawQuery("SELECT DISTINCT foodid from cart WHERE tableno=" + tableNo + " AND locationid= " + locationNo + ";", null);
                            int FoodIddetails = FoodId.getCount();
                            int[] Foodlist = new int[10];
                            int i = 0;
                            while (FoodId.moveToNext()) {
                                Foodlist[i] = FoodId.getInt(0);
                                i++;
                            }
                            db.close();
                            int foodflag = 0;
                            for (int j = 0; j < i; j++) {
                                if ((j + 1) < i) {
                                    foodflag = 0;
                                    click(tableNo, locationNo, Foodlist[j]);
                                } else {
                                    foodflag = 1;
                                    click(tableNo, locationNo, Foodlist[j]);
                                }
                            }

                        }
                    } else {
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("btkPrinterid", globledevice);
                        editor.commit();
                        kdevice = mBluetoothAdapter
                                .getRemoteDevice(globledevice);
                        if (printFlag == PRINTBILL) {
                            billClick(tableNo, locationNo, 0);
                        } else if (printFlag == PAYBILLPRINT) {
                            billClick(tableNo, locationNo, 1);
                        } else if (printFlag == ORDERNOPRINT) {
                            //orderNoBillPrint(""+tableNo);
                        } else {
                            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                            Cursor FoodId = db.rawQuery("SELECT DISTINCT foodid from cart WHERE tableno=" + tableNo + " AND locationid= " + locationNo + ";", null);
                            int FoodIddetails = FoodId.getCount();
                            int[] Foodlist = new int[10];
                            int i = 0;
                            while (FoodId.moveToNext()) {
                                Foodlist[i] = FoodId.getInt(0);
                                i++;
                            }
                            db.close();
                            int foodflag = 0;
                            for (int j = 0; j < i; j++) {
                                if ((j + 1) < i) {
                                    foodflag = 0;
                                    click(tableNo, locationNo, Foodlist[j]);
                                } else {
                                    foodflag = 1;
                                    click(tableNo, locationNo, Foodlist[j]);
                                }
                            }

                        }

                    }
                } catch (RuntimeException e) {
                    Intent mIntent = new Intent();
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.setClass(getApplicationContext(), Home.class);
                    startActivity(mIntent);
                    Toast.makeText(Printer.this, " insert run time exception" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                settingDialog.dismiss();

            }
        });
    }

    /**
     * @param data
     * @param printformate
     */
    private void deviceconnection(String data, int printformate) {
        // TODO Auto-generated method stub
        printdata = data;
        pFormate = printformate;
        if (pFormate == PRINTBILL || pFormate == PAYBILLPRINT) {
            try {
                connect(mdevice);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            WiFi();
        }

    }

    public void WiFi() {
        boolean isconnected = false;
        strName = "192.168.1.87:9100";
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor printerDetails = db.rawQuery("SELECT ipaddress, portno FROM locationprinter where foodid =" + kitchenid + ";", null);
        int printerDetailsCount = printerDetails.getCount();
        if (printerDetailsCount > 0) {
            printerDetails.moveToFirst();
            strName = printerDetails.getString(0) + ":" + printerDetails.getString(1);
            SERVER_IP = printerDetails.getString(0);
            SERVERPORT = Integer.parseInt(printerDetails.getString(1));
        }
        db.close();
        if (strName.length() == 0) {
            Toast.makeText(Printer.this, "Error:port name empty", Toast.LENGTH_SHORT).show();
            return;
        }
        isconnected = false;
        try {
            if (mysocket != null) {
                if (mmOutStream != null) {
                    mmOutStream.close();
                }
                if (mmInStream != null) {
                    mmInStream.close();
                }
                mmOutStream = null;
                mmInStream = null;
                mysocket.close();
                mysocket = null;
            }
            mysocket = new Socket();
            mysocket.connect(new InetSocketAddress(SERVER_IP, SERVERPORT), 5000);
            mmOutStream = mysocket.getOutputStream();
            mmInStream = mysocket.getInputStream();
            isconnected = true;
        } catch (UnknownHostException e1) {
            e1.printStackTrace();
            isconnected = false;
        } catch (IOException e1) {
            e1.printStackTrace();
            isconnected = false;
        }
        for (int i = 0; i < 100; i++) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (isconnected == true) {
            Begin();
            write((printdata + "\n").getBytes());
            Pcut();
            if (MainActivity.getInstance().isWifiPrinterEnable == true) {
                optionalwifi();
            }
            if (MainActivity.getInstance().kotPrinterEnable == true) {
                if (kdevice != null) {
                    try {
                        connect(kdevice);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        } else {
            Toast.makeText(getApplicationContext(), "Selected WIFi KOT Printer is not Online trying to connect to bluetooth", Toast.LENGTH_LONG).show();
            if (mysocket != null) {
                try {
                    if (mmOutStream != null) {
                        mmOutStream.close();
                    }
                    if (mmInStream != null) {
                        mmInStream.close();
                    }
                    mmInStream = null;
                    mmOutStream = null;
                    mysocket.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                mysocket = null;
            }
            try {
                if (kdevice != null) {
                    connect(kdevice);
                } else {
                    Intent mIntent = new Intent();
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.setClass(getApplicationContext(), Home.class);
                    startActivity(mIntent);
                    Toast.makeText(getApplicationContext(), "Bluetooth KOT Printer is not available ", Toast.LENGTH_LONG).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


    }

    public void optionalwifi() {
        boolean isconnected = false;
        strName = "192.168.1.87:9100";
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor printerDetails = db.rawQuery("SELECT ipaddress, portno FROM locationprinter where foodid =" + 0 + ";", null);
        int printerDetailsCount = printerDetails.getCount();
        if (printerDetailsCount > 0) {
            printerDetails.moveToFirst();
            strName = printerDetails.getString(0) + ":" + printerDetails.getString(1);
            SERVER_IP = printerDetails.getString(0);
            SERVERPORT = Integer.parseInt(printerDetails.getString(1));
        }
        db.close();
        if (strName.length() == 0) {
            Toast.makeText(Printer.this, "Error:port name empty", Toast.LENGTH_SHORT).show();
            return;
        }
        isconnected = false;
        try {
            if (mysocket != null) {
                if (mmOutStream != null) {
                    if (mmInStream != null) {
                        mmOutStream.close();
                    }
                    if (mmInStream != null) {
                        mmInStream.close();
                    }
                    mmOutStream = null;
                    mmInStream = null;

                }
                mysocket.close();
                mysocket = null;
            }
            mysocket = new Socket();
            mysocket.connect(new InetSocketAddress(SERVER_IP, SERVERPORT), 5000);
            mmOutStream = mysocket.getOutputStream();
            mmInStream = mysocket.getInputStream();
            isconnected = true;
        } catch (UnknownHostException e1) {
            e1.printStackTrace();
            isconnected = false;
        } catch (IOException e1) {
            e1.printStackTrace();
            isconnected = false;
        }
        for (int i = 0; i < 150; i++) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (isconnected == true) {
            Begin();
            write((printdata + "\n").getBytes());
        }

    }

    public static void Begin() {
        WakeUpPritner();
        InitPrinter();
    }

    public static void InitPrinter() {
        byte[] combyte = new byte[]{(byte) 27, (byte) 64};

        try {
            if (mmOutStream != null) {
                mmOutStream.write(combyte);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void WakeUpPritner() {
        byte[] b = new byte[3];

        try {
            if (mmOutStream != null) {
                mmOutStream.write(b);
            }

            Thread.sleep(100L);
        } catch (Exception var2) {
            var2.printStackTrace();
        }
    }

    public static void Pcut() {
        byte[] pcut = new byte[]{(byte) 0x1D, (byte) 0x56, (byte) 0x42, (byte) 0x00};
        try {
            if (mmOutStream != null) {
                mmOutStream.write(pcut);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* Call this from the main activity to send data to the remote device */
    public void write(byte[] bytes) {
        try {
            byte[] cAlign = {(byte) 0x1b, (byte) 0x61, (byte) 0x01};
            byte[] letterSize = {(byte) 0x1D, (byte) 0x21, (byte) 0x01};
            if (mmOutStream != null) {
                mmOutStream.write(cAlign);
                if (pFormate != PRINTBILL && pFormate != PAYBILLPRINT) {
                    mmOutStream.write(letterSize);
                }
                mmOutStream.write(bytes);
            }
        } catch (IOException e) {
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                pDialog = new ProgressDialog(
                        Printer.this, android.R.style.Theme_Dialog);
                pDialog.setMessage("Connecting To Printer...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pDialog.show();
                return pDialog;

        }
        return null;
    }


    /* (non-Javadoc)
	 * @see android.app.Activity#onDestroy()
	 */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (settingDialog != null) {
            if (settingDialog.isShowing()) {
                settingDialog.dismiss();
            }
            settingDialog = null;
        }
        if (mysocket != null) {
            try {
                if (mmOutStream != null) {
                    mmOutStream.close();
                    if (mmInStream != null) {
                        mmInStream.close();
                    }

                    mmOutStream = null;
                    mmInStream = null;
                }
                mysocket.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            mysocket = null;
        }
    }

    public void updatePayInfo() {
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        db.execSQL("UPDATE cart SET billedqty = (billedqty + billqty), billedtypeqty = (billedtypeqty + billtypeqty) WHERE tableno = " + tableNo +
                " AND locationid = " + user_Login.user_LoginID.LOCATIONNO + " AND billflag = 0 AND checkflag = 0;");
        db.execSQL("UPDATE cart SET  billqty = orderqty-billedqty, billtypeqty = ordertypeqty-billedtypeqty WHERE tableno = " +
                tableNo + " AND locationid = " + user_Login.user_LoginID.LOCATIONNO + ";");
        Cursor updateCount = db.rawQuery("SELECT * from cart  WHERE tableno = " + tableNo + " AND locationid = " +
                user_Login.user_LoginID.LOCATIONNO + " AND orderqty >= billedqty AND ordertypeqty >= billedtypeqty And billflag = 0;", null);
        int count = updateCount.getCount();
        updateCount.moveToPrevious();
        String newMessage = null;
        String message = null;
        String orderNo = null;
        while (updateCount.moveToNext()) {
            newMessage = updateCount.getString(updateCount.getColumnIndex("sno")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("productname")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("typename")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("productprice")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("productqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("billqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("typeprice")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("typeqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("billtypeqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("totalprice")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("tableno")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("locationid")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("orderflag")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("billflag")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("orderqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("billedqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("checkflag")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("orderno")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("covers")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("tdiscount")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("discount")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("totaltax")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("ordertypeqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("billedtypeqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("datetime")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("tbillflag")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("username"));
            orderNo = updateCount.getString(updateCount.getColumnIndex("orderno"));
            if (message == null) {
                message = newMessage;
            } else {
                message += "|?" + newMessage;
            }
        }
        myDb.Insert(String.valueOf(MainActivity.MAINID.outletid), String.valueOf(tableNo), "0", "NULL", "NULL");
        Intent intent = new Intent(Printer.this, ServiceOccupiedChairs.class);
        startService(intent);
        if (count > 0) {
            String totalReceipts = null;
            Cursor receiptDetails = db.rawQuery("SELECT receiptno,docname,docid,amount,datetime,username," +
                    "orderid FROM receipt where orderid ='" + orderNo + "' AND checkflag = 0;", null);
            int productCount = receiptDetails.getCount();
            if (productCount > 0) {
                receiptDetails.moveToPrevious();
                while (receiptDetails.moveToNext()) {

                    String receipts = receiptDetails.getString(0) + "|" +
                            receiptDetails.getString(1) + "|" +
                            receiptDetails.getString(2) + "|" +
                            receiptDetails.getString(3) + "|" +
                            receiptDetails.getString(4) + "|" +
                            receiptDetails.getString(5) + "|" +
                            receiptDetails.getString(6) + "?";
                    if (totalReceipts == null) {
                        totalReceipts = receipts;
                    } else {
                        totalReceipts += "|?" + receipts;
                    }
                }
                message += "?|!" + totalReceipts;
            } else {
            }
            MainActivity.getInstance().messageid = "M05";
            MainActivity.getInstance().checkid = 1;
            MainActivity.getInstance().messagecontentarray = "111111111111111111111111111";
            MainActivity.getInstance().noofpackets = 1;
            MainActivity.getInstance().packetno = 1;
            MainActivity.getInstance().message = message;
            MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
            MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
        }

        db.execSQL("UPDATE cart SET  billflag= 1 WHERE tableno = " + tableNo + " AND locationid = " +
                user_Login.user_LoginID.LOCATIONNO + " AND orderqty = billedqty AND ordertypeqty = billedtypeqty;");

        db.execSQL("UPDATE receipt SET checkflag = 1 where orderid ='" + orderNo + "' AND checkflag = 0;");

        Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO +
                " AND tableno = " + tableNo + " AND orderflag = 1 AND billflag = 0 AND checkflag = 1;", null);
        int cartCount = cartData.getCount();
        if (cartCount < 1) {
            Cursor qtyData = db.rawQuery("SELECT * from cart WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO +
                    " AND tableno = " + tableNo + " AND orderflag = 1 AND billflag = 0 AND (orderqty > billedqty OR typeqty > " +
                    " billedtypeqty);", null);

            if (qtyData.getCount() == 0) {
                Toast.makeText(Printer.this, "Order Successfully Closed ", Toast.LENGTH_SHORT)
                        .show();
                db.execSQL("UPDATE tables SET occupiedchairs = 0 WHERE tableno = " + tableNo +
                        " AND locationid = " + user_Login.user_LoginID.LOCATIONNO + ";");
                db.execSQL("UPDATE ordernos SET  billflag= 1 WHERE tableno = " + tableNo + " AND locationid = " +
                        user_Login.user_LoginID.LOCATIONNO + ";");
                db.execSQL("UPDATE cart SET  tbillflag= 1 WHERE tableno = " + tableNo + " AND locationid = " +
                        user_Login.user_LoginID.LOCATIONNO + " AND billflag =1;");
                CartStaticSragment.CARTStaticSragment.noofguests.setText("0");

            } else {
                qtyData.moveToFirst();

            }
        } else {
            db.execSQL("UPDATE cart SET checkflag = 0 WHERE tableno = " + tableNo + " AND locationid = " +
                    user_Login.user_LoginID.LOCATIONNO + " AND billflag = 0 AND checkflag = 1;");
        }
        db.close();
        if (count > 0) {
            Socket_Connection socket = new Socket_Connection();
            int status = socket.connectionToServer();
            if (status != 0) {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                db.execSQL("INSERT INTO queuetable(message) VALUES('" + MainActivity.getInstance().finnalMesgToSend + "');");
                MainActivity.getInstance().queueMessage = "0";
                db.close();
            }
        }
        return;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}