/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.instabug.survey;

public final class R {
    public static final class anim {
        public static final int anim_invocation_dialog_enter = 0x7f04000a;
        public static final int anim_invocation_dialog_exit = 0x7f04000b;
        public static final int anim_recording_audio_dialog_enter = 0x7f04000c;
        public static final int anim_recording_audio_dialog_exit = 0x7f04000d;
        public static final int instabug_anim_bottom_sheet_enter = 0x7f040014;
        public static final int instabug_anim_bottom_sheet_exit = 0x7f040015;
        public static final int notification_close_anim = 0x7f040016;
        public static final int notification_open_anim = 0x7f040017;
        public static final int notification_swipe_left_anim = 0x7f040018;
        public static final int notification_swipe_right_anim = 0x7f040019;
    }
    public static final class attr {
        public static final int ibg_civ_border_color = 0x7f0100f5;
        public static final int ibg_civ_border_overlay = 0x7f0100f6;
        public static final int ibg_civ_border_width = 0x7f0100f4;
        public static final int ibg_civ_fill_color = 0x7f0100f7;
        public static final int instabug_attachment_bg_color = 0x7f010004;
        public static final int instabug_attachment_bg_icon_color = 0x7f010005;
        public static final int instabug_background_color = 0x7f010006;
        public static final int instabug_bg_with_bottom_border_for_edit_text = 0x7f010007;
        public static final int instabug_dialog_button_text_color = 0x7f010008;
        public static final int instabug_dialog_item_text_color = 0x7f010009;
        public static final int instabug_divider = 0x7f01000a;
        public static final int instabug_divider_color = 0x7f01000b;
        public static final int instabug_fab_colorDisabled = 0x7f010126;
        public static final int instabug_fab_colorNormal = 0x7f010127;
        public static final int instabug_fab_colorPressed = 0x7f010125;
        public static final int instabug_fab_icon = 0x7f010128;
        public static final int instabug_fab_size = 0x7f010129;
        public static final int instabug_fab_stroke_visible = 0x7f01012b;
        public static final int instabug_fab_title = 0x7f01012a;
        public static final int instabug_foreground_color = 0x7f01000c;
        public static final int instabug_icon = 0x7f01012e;
        public static final int instabug_item_border = 0x7f01000d;
        public static final int instabug_message_date_text_color = 0x7f01000e;
        public static final int instabug_message_sender_text_color = 0x7f01000f;
        public static final int instabug_message_snippet_text_color = 0x7f010010;
        public static final int instabug_primary_color = 0x7f010011;
        public static final int instabug_received_message_text_color = 0x7f010012;
        public static final int instabug_sent_message_text_color = 0x7f010013;
        public static final int instabug_survey_dialog_footer_transition = 0x7f010014;
        public static final int instabug_survey_dialog_header_transition = 0x7f010015;
        public static final int instabug_theme_hint_text_color = 0x7f010016;
        public static final int instabug_theme_tinting_color = 0x7f010017;
        public static final int instabug_unread_message_background_color = 0x7f010018;
        public static final int view_orientation = 0x7f010109;
    }
    public static final class bool {
        public static final int isTablet = 0x7f0b0001;
    }
    public static final class color {
        public static final int dark_theme_status_bar_color = 0x7f0c0025;
        public static final int instabug_annotation_color_blue = 0x7f0c0042;
        public static final int instabug_annotation_color_default = 0x7f0c0043;
        public static final int instabug_annotation_color_gray = 0x7f0c0044;
        public static final int instabug_annotation_color_green = 0x7f0c0045;
        public static final int instabug_annotation_color_red = 0x7f0c0046;
        public static final int instabug_annotation_color_yellow = 0x7f0c0047;
        public static final int instabug_dark_theme_hint_text_color = 0x7f0c0048;
        public static final int instabug_dialog_bg_color = 0x7f0c0049;
        public static final int instabug_light_theme_hint_text_color = 0x7f0c004a;
        public static final int instabug_text_color_grey = 0x7f0c004b;
        public static final int instabug_theme_tinting_color_dark = 0x7f0c004c;
        public static final int instabug_theme_tinting_color_light = 0x7f0c004d;
        public static final int instabug_transparent_color = 0x7f0c004e;
        public static final int instabug_url_color_blue = 0x7f0c004f;
        public static final int light_theme_status_bar_color = 0x7f0c0050;
    }
    public static final class dimen {
        public static final int Instabug_text_medium = 0x7f080001;
        public static final int instabug_actionbar_height = 0x7f080022;
        public static final int instabug_attachment_placeholder_margin = 0x7f08008f;
        public static final int instabug_bottom_sheet_padding = 0x7f080090;
        public static final int instabug_button_text_size = 0x7f080006;
        public static final int instabug_chat_item_avatar_size = 0x7f080091;
        public static final int instabug_container_padding = 0x7f080092;
        public static final int instabug_date_text_size = 0x7f080007;
        public static final int instabug_dialog_container_padding = 0x7f080093;
        public static final int instabug_dialog_padding_left_right = 0x7f080008;
        public static final int instabug_dialog_padding_top_bottom = 0x7f080009;
        public static final int instabug_fab_actions_spacing = 0x7f080094;
        public static final int instabug_fab_circle_icon_size = 0x7f080095;
        public static final int instabug_fab_circle_icon_stroke = 0x7f080096;
        public static final int instabug_fab_distance = 0x7f080097;
        public static final int instabug_fab_icon_size_mini = 0x7f080098;
        public static final int instabug_fab_icon_size_normal = 0x7f080099;
        public static final int instabug_fab_labels_margin = 0x7f08009a;
        public static final int instabug_fab_shadow_offset = 0x7f08009b;
        public static final int instabug_fab_shadow_radius = 0x7f08009c;
        public static final int instabug_fab_size_mini = 0x7f08009d;
        public static final int instabug_fab_size_normal = 0x7f08009e;
        public static final int instabug_fab_stroke_width = 0x7f08009f;
        public static final int instabug_fab_text_size = 0x7f0800a0;
        public static final int instabug_normal_text_size = 0x7f08000a;
        public static final int instabug_question_text_size = 0x7f08000b;
        public static final int instabug_remove_attachment_button_padding = 0x7f08000c;
        public static final int instabug_remove_attachment_button_size = 0x7f08000d;
        public static final int instabug_toolbar_button_padding = 0x7f08000e;
        public static final int instabug_vertical_separator_height = 0x7f0800a1;
    }
    public static final class drawable {
        public static final int chat_bubble_received = 0x7f02006e;
        public static final int chat_bubble_sent = 0x7f02006f;
        public static final int instabug_bg_active_record = 0x7f0200a9;
        public static final int instabug_bg_blue_oval_with_bottom_left_shadow = 0x7f0200aa;
        public static final int instabug_bg_border_dark = 0x7f0200ab;
        public static final int instabug_bg_border_light = 0x7f0200ac;
        public static final int instabug_bg_card = 0x7f0200ad;
        public static final int instabug_bg_dark = 0x7f0200ae;
        public static final int instabug_bg_default_record = 0x7f0200af;
        public static final int instabug_bg_divider_list_dark = 0x7f0200b0;
        public static final int instabug_bg_divider_list_light = 0x7f0200b1;
        public static final int instabug_bg_edit_text = 0x7f0200b2;
        public static final int instabug_bg_edit_text_active = 0x7f0200b3;
        public static final int instabug_bg_edit_text_default = 0x7f0200b4;
        public static final int instabug_bg_gray_oval_with_bottom_left_shadow = 0x7f0200b5;
        public static final int instabug_bg_green_oval_with_bottom_left_shadow = 0x7f0200b6;
        public static final int instabug_bg_light = 0x7f0200b7;
        public static final int instabug_bg_notification_container = 0x7f0200b8;
        public static final int instabug_bg_red_oval_with_bottom_left_shadow = 0x7f0200b9;
        public static final int instabug_bg_white_oval = 0x7f0200ba;
        public static final int instabug_bg_with_bottom_gray_stroke_dark = 0x7f0200bb;
        public static final int instabug_bg_with_bottom_gray_stroke_light = 0x7f0200bc;
        public static final int instabug_bg_with_thin_bottom_gray_stroke_dark = 0x7f0200bd;
        public static final int instabug_bg_with_thin_bottom_gray_stroke_light = 0x7f0200be;
        public static final int instabug_bg_with_thin_top_gray_stroke_dark = 0x7f0200bf;
        public static final int instabug_bg_with_thin_top_gray_stroke_light = 0x7f0200c0;
        public static final int instabug_bg_yellow_oval_with_bottom_left_shadow = 0x7f0200c1;
        public static final int instabug_fab_bg_mini = 0x7f0200c2;
        public static final int instabug_fab_bg_normal = 0x7f0200c3;
        public static final int instabug_ic_attach = 0x7f0200c4;
        public static final int instabug_ic_attach_gallery_image = 0x7f0200c5;
        public static final int instabug_ic_avatar = 0x7f0200c6;
        public static final int instabug_ic_back = 0x7f0200c7;
        public static final int instabug_ic_capture_screenshot = 0x7f0200c8;
        public static final int instabug_ic_check = 0x7f0200c9;
        public static final int instabug_ic_close = 0x7f0200ca;
        public static final int instabug_ic_erase = 0x7f0200cb;
        public static final int instabug_ic_floating_btn = 0x7f0200cc;
        public static final int instabug_ic_mic = 0x7f0200cd;
        public static final int instabug_ic_next = 0x7f0200ce;
        public static final int instabug_ic_pause = 0x7f0200cf;
        public static final int instabug_ic_play = 0x7f0200d0;
        public static final int instabug_ic_plus = 0x7f0200d1;
        public static final int instabug_ic_record_audio = 0x7f0200d2;
        public static final int instabug_ic_record_video = 0x7f0200d3;
        public static final int instabug_ic_remove = 0x7f0200d4;
        public static final int instabug_ic_report_bug = 0x7f0200d5;
        public static final int instabug_ic_send = 0x7f0200d6;
        public static final int instabug_ic_send_feedback = 0x7f0200d7;
        public static final int instabug_ic_stop = 0x7f0200d8;
        public static final int instabug_ic_talk_to_us = 0x7f0200d9;
        public static final int instabug_ic_video = 0x7f0200da;
        public static final int instabug_ic_video_received = 0x7f0200db;
        public static final int instabug_img_audio_placeholder = 0x7f0200dc;
        public static final int instabug_img_placeholder = 0x7f0200dd;
        public static final int instabug_img_shake = 0x7f0200de;
        public static final int instabug_img_two_fingers = 0x7f0200df;
        public static final int instabug_img_two_fingers_touch = 0x7f0200e0;
        public static final int instabug_transition_from_default_bg_to_default_bg_with_thin_bottom_gray_stroke_dark = 0x7f0200e1;
        public static final int instabug_transition_from_default_bg_to_default_bg_with_thin_bottom_gray_stroke_light = 0x7f0200e2;
        public static final int instabug_transition_from_default_bg_to_default_bg_with_thin_top_gray_stroke_dark = 0x7f0200e3;
        public static final int instabug_transition_from_default_bg_to_default_bg_with_thin_top_gray_stroke_light = 0x7f0200e4;
    }
    public static final class id {
        public static final int HorizontalScrollActionBar = 0x7f0e014c;
        public static final int animation_container = 0x7f0e0194;
        public static final int animation_description = 0x7f0e0193;
        public static final int animation_frame = 0x7f0e0192;
        public static final int audio = 0x7f0e0049;
        public static final int avatar = 0x7f0e004a;
        public static final int back = 0x7f0e004b;
        public static final int blur = 0x7f0e004c;
        public static final int brush = 0x7f0e004d;
        public static final int brush_indicator = 0x7f0e0148;
        public static final int bug = 0x7f0e004e;
        public static final int cancel = 0x7f0e004f;
        public static final int capture_screenshot = 0x7f0e0050;
        public static final int capture_video = 0x7f0e0051;
        public static final int close = 0x7f0e0052;
        public static final int conversation_list_item_container = 0x7f0e0159;
        public static final int divider = 0x7f0e0141;
        public static final int erase = 0x7f0e0053;
        public static final int feedback = 0x7f0e0054;
        public static final int fingersImageView = 0x7f0e0196;
        public static final int floating_bubble = 0x7f0e0055;
        public static final int horizontal = 0x7f0e0042;
        public static final int ib_dialog_survey_scrollview_container = 0x7f0e0162;
        public static final int icon_blur = 0x7f0e014a;
        public static final int icon_brush = 0x7f0e0147;
        public static final int icon_brush_layout = 0x7f0e0146;
        public static final int icon_magnify = 0x7f0e0149;
        public static final int icon_undo = 0x7f0e014b;
        public static final int image_instabug_logo = 0x7f0e01ae;
        public static final int instabug = 0x7f0e0056;
        public static final int instabug_actions_list = 0x7f0e0176;
        public static final int instabug_actions_list_container = 0x7f0e0174;
        public static final int instabug_annotation_actions_container = 0x7f0e0145;
        public static final int instabug_annotation_image = 0x7f0e0143;
        public static final int instabug_annotation_image_border = 0x7f0e0142;
        public static final int instabug_annotation_image_container = 0x7f0e0140;
        public static final int instabug_attach_audio_icon = 0x7f0e0157;
        public static final int instabug_attach_audio_label = 0x7f0e0158;
        public static final int instabug_attach_gallery_image = 0x7f0e0153;
        public static final int instabug_attach_gallery_image_icon = 0x7f0e0154;
        public static final int instabug_attach_gallery_image_label = 0x7f0e0155;
        public static final int instabug_attach_gallery_image_text = 0x7f0e017b;
        public static final int instabug_attach_screenshot = 0x7f0e0150;
        public static final int instabug_attach_screenshot_icon = 0x7f0e0151;
        public static final int instabug_attach_screenshot_label = 0x7f0e0152;
        public static final int instabug_attach_screenshot_text = 0x7f0e0179;
        public static final int instabug_attach_video = 0x7f0e014d;
        public static final int instabug_attach_video_icon = 0x7f0e014e;
        public static final int instabug_attach_video_label = 0x7f0e014f;
        public static final int instabug_attach_video_text = 0x7f0e017a;
        public static final int instabug_attach_voice_note = 0x7f0e0156;
        public static final int instabug_attach_voice_note_text = 0x7f0e017c;
        public static final int instabug_attachment_progress_bar = 0x7f0e017d;
        public static final int instabug_attachments_actions_bottom_sheet = 0x7f0e0178;
        public static final int instabug_attachments_bottom_sheet_container = 0x7f0e018f;
        public static final int instabug_attachments_bottom_sheet_dim_view = 0x7f0e0177;
        public static final int instabug_audio_attachment = 0x7f0e01c3;
        public static final int instabug_audio_attachment_progress_bar = 0x7f0e01c5;
        public static final int instabug_bk_record_audio = 0x7f0e01b3;
        public static final int instabug_btn_add_attachment = 0x7f0e017f;
        public static final int instabug_btn_attach = 0x7f0e018c;
        public static final int instabug_btn_audio_play_attachment = 0x7f0e0182;
        public static final int instabug_btn_floating_bar = 0x7f0e016d;
        public static final int instabug_btn_new_chat = 0x7f0e0191;
        public static final int instabug_btn_play_audio = 0x7f0e01c4;
        public static final int instabug_btn_play_video = 0x7f0e01c2;
        public static final int instabug_btn_record_audio = 0x7f0e01b2;
        public static final int instabug_btn_remove_attachment = 0x7f0e0184;
        public static final int instabug_btn_send = 0x7f0e018e;
        public static final int instabug_btn_submit = 0x7f0e0167;
        public static final int instabug_btn_submit_container = 0x7f0e0165;
        public static final int instabug_btn_toolbar_left = 0x7f0e01ba;
        public static final int instabug_btn_toolbar_right = 0x7f0e01bb;
        public static final int instabug_btn_video_play_attachment = 0x7f0e0188;
        public static final int instabug_cancel_audio_record = 0x7f0e01b5;
        public static final int instabug_color_picker = 0x7f0e0144;
        public static final int instabug_container = 0x7f0e0173;
        public static final int instabug_container_edit_text_answer = 0x7f0e016a;
        public static final int instabug_content = 0x7f0e016f;
        public static final int instabug_conversation_item = 0x7f0e015b;
        public static final int instabug_decor_view = 0x7f0e0007;
        public static final int instabug_dialog_survey_container = 0x7f0e0161;
        public static final int instabug_disclaimer_details = 0x7f0e0198;
        public static final int instabug_disclaimer_list = 0x7f0e0197;
        public static final int instabug_edit_text_answer = 0x7f0e016b;
        public static final int instabug_edit_text_email = 0x7f0e0199;
        public static final int instabug_edit_text_message = 0x7f0e019a;
        public static final int instabug_edit_text_new_message = 0x7f0e018d;
        public static final int instabug_fab_expand_menu_button = 0x7f0e0008;
        public static final int instabug_fab_label = 0x7f0e0009;
        public static final int instabug_floating_bar_container = 0x7f0e016c;
        public static final int instabug_floating_button = 0x7f0e000a;
        public static final int instabug_fragment_container = 0x7f0e013d;
        public static final int instabug_fragment_title = 0x7f0e0175;
        public static final int instabug_grid_audio_item = 0x7f0e0180;
        public static final int instabug_grid_img_item = 0x7f0e0185;
        public static final int instabug_grid_video_item = 0x7f0e0186;
        public static final int instabug_ic_survey_logo = 0x7f0e0169;
        public static final int instabug_icon = 0x7f0e0171;
        public static final int instabug_img_attachment = 0x7f0e017e;
        public static final int instabug_img_audio_attachment = 0x7f0e0181;
        public static final int instabug_img_message_sender = 0x7f0e01bd;
        public static final int instabug_img_record_audio = 0x7f0e01b4;
        public static final int instabug_img_video_attachment = 0x7f0e0187;
        public static final int instabug_inbox_messages_count = 0x7f0e01a2;
        public static final int instabug_invocation_dialog_container = 0x7f0e019d;
        public static final int instabug_invocation_dialog_items_container = 0x7f0e019f;
        public static final int instabug_invocation_dialog_items_container_scrollView = 0x7f0e019e;
        public static final int instabug_label = 0x7f0e0172;
        public static final int instabug_lst_conversations = 0x7f0e0190;
        public static final int instabug_lst_messages = 0x7f0e018a;
        public static final int instabug_lyt_attachments_grid = 0x7f0e019c;
        public static final int instabug_message_actions_container = 0x7f0e01bf;
        public static final int instabug_message_sender_avatar = 0x7f0e015a;
        public static final int instabug_new_message_container = 0x7f0e018b;
        public static final int instabug_option_cancel = 0x7f0e01a6;
        public static final int instabug_option_ok = 0x7f0e01b9;
        public static final int instabug_option_report_bug = 0x7f0e0170;
        public static final int instabug_option_report_bug_text = 0x7f0e01a3;
        public static final int instabug_option_send_feedback = 0x7f0e01a4;
        public static final int instabug_option_send_feedback_text = 0x7f0e01a5;
        public static final int instabug_option_talk_to_us = 0x7f0e01a0;
        public static final int instabug_option_talk_to_us_text = 0x7f0e01a1;
        public static final int instabug_pbi_container = 0x7f0e01ad;
        public static final int instabug_pbi_footer = 0x7f0e013e;
        public static final int instabug_radio_group_answers = 0x7f0e0164;
        public static final int instabug_recording_audio_dialog_container = 0x7f0e01af;
        public static final int instabug_success_dialog_container = 0x7f0e01b6;
        public static final int instabug_success_dialog_items_container = 0x7f0e01b7;
        public static final int instabug_survey_bottom_separator = 0x7f0e0166;
        public static final int instabug_survey_dialog_container = 0x7f0e0160;
        public static final int instabug_survey_top_separator = 0x7f0e0168;
        public static final int instabug_text_view_disclaimer = 0x7f0e019b;
        public static final int instabug_text_view_question = 0x7f0e0163;
        public static final int instabug_toolbar = 0x7f0e016e;
        public static final int instabug_txt_attachment_length = 0x7f0e0183;
        public static final int instabug_txt_message_body = 0x7f0e01be;
        public static final int instabug_txt_message_sender = 0x7f0e015c;
        public static final int instabug_txt_message_snippet = 0x7f0e015d;
        public static final int instabug_txt_message_time = 0x7f0e015e;
        public static final int instabug_txt_recording_title = 0x7f0e01b0;
        public static final int instabug_txt_success_note = 0x7f0e01b8;
        public static final int instabug_txt_timer = 0x7f0e01b1;
        public static final int instabug_unread_messages_count = 0x7f0e015f;
        public static final int instabug_video_attachment = 0x7f0e01c0;
        public static final int instabug_video_attachment_progress_bar = 0x7f0e01c1;
        public static final int instabug_video_mute_button = 0x7f0e000b;
        public static final int instabug_video_stop_button = 0x7f0e000c;
        public static final int magnify = 0x7f0e0057;
        public static final int messageTextView = 0x7f0e01ac;
        public static final int mini = 0x7f0e0048;
        public static final int next = 0x7f0e0058;
        public static final int normal = 0x7f0e0021;
        public static final int notificationLinearLayout = 0x7f0e01a9;
        public static final int pause = 0x7f0e0059;
        public static final int photo_library = 0x7f0e005a;
        public static final int play = 0x7f0e005b;
        public static final int plus = 0x7f0e005c;
        public static final int plus_attach = 0x7f0e005d;
        public static final int send = 0x7f0e005e;
        public static final int senderAvatarImageView = 0x7f0e01aa;
        public static final int senderNameTextView = 0x7f0e01ab;
        public static final int stop = 0x7f0e005f;
        public static final int survey = 0x7f0e0060;
        public static final int talk_to_us = 0x7f0e0061;
        public static final int text_view_pb = 0x7f0e013f;
        public static final int tick = 0x7f0e0062;
        public static final int toolbar_chat = 0x7f0e0189;
        public static final int touchesImageView = 0x7f0e0195;
        public static final int tvKey = 0x7f0e01a7;
        public static final int tvValue = 0x7f0e01a8;
        public static final int undo = 0x7f0e0063;
        public static final int vertical = 0x7f0e0043;
        public static final int video_view = 0x7f0e01bc;
    }
    public static final class integer {
        public static final int instabug_icon_lang_rotation = 0x7f0a0000;
    }
    public static final class layout {
        public static final int instabug_activity = 0x7f030045;
        public static final int instabug_annotation_view = 0x7f030046;
        public static final int instabug_attachments_action_bar = 0x7f030047;
        public static final int instabug_conversation_list_item = 0x7f030048;
        public static final int instabug_dialog_mcq_survey = 0x7f030049;
        public static final int instabug_dialog_text_survey = 0x7f03004a;
        public static final int instabug_floating_bar = 0x7f03004b;
        public static final int instabug_fragment_toolbar = 0x7f03004c;
        public static final int instabug_item_actions_list = 0x7f03004d;
        public static final int instabug_lyt_actions_list = 0x7f03004e;
        public static final int instabug_lyt_add_attachments = 0x7f03004f;
        public static final int instabug_lyt_annotation = 0x7f030050;
        public static final int instabug_lyt_attachment = 0x7f030051;
        public static final int instabug_lyt_attachment_add = 0x7f030052;
        public static final int instabug_lyt_attachment_audio = 0x7f030053;
        public static final int instabug_lyt_attachment_image = 0x7f030054;
        public static final int instabug_lyt_attachment_video = 0x7f030055;
        public static final int instabug_lyt_conversation = 0x7f030056;
        public static final int instabug_lyt_conversations = 0x7f030057;
        public static final int instabug_lyt_dialog_shake_animation = 0x7f030058;
        public static final int instabug_lyt_dialog_toolbar = 0x7f030059;
        public static final int instabug_lyt_dialog_two_fingers_swipe_animation = 0x7f03005a;
        public static final int instabug_lyt_disclaimer = 0x7f03005b;
        public static final int instabug_lyt_disclaimer_details = 0x7f03005c;
        public static final int instabug_lyt_feedback = 0x7f03005d;
        public static final int instabug_lyt_invocation = 0x7f03005e;
        public static final int instabug_lyt_item_disclaimer = 0x7f03005f;
        public static final int instabug_lyt_notification = 0x7f030060;
        public static final int instabug_lyt_pbi = 0x7f030061;
        public static final int instabug_lyt_record_audio = 0x7f030062;
        public static final int instabug_lyt_success = 0x7f030063;
        public static final int instabug_lyt_toolbar = 0x7f030064;
        public static final int instabug_lyt_video_view = 0x7f030065;
        public static final int instabug_message_list_item = 0x7f030066;
        public static final int instabug_message_list_item_img = 0x7f030067;
        public static final int instabug_message_list_item_img_me = 0x7f030068;
        public static final int instabug_message_list_item_me = 0x7f030069;
        public static final int instabug_message_list_item_video = 0x7f03006a;
        public static final int instabug_message_list_item_video_me = 0x7f03006b;
        public static final int instabug_message_list_item_voice = 0x7f03006c;
        public static final int instabug_message_list_item_voice_me = 0x7f03006d;
        public static final int instabug_vertical_separator = 0x7f03006e;
    }
    public static final class raw {
        public static final int new_message = 0x7f060000;
    }
    public static final class string {
        public static final int app_name = 0x7f07005e;
        public static final int font_fontFamily_medium = 0x7f07006a;
        public static final int instabug_audio_recorder_permission_denied = 0x7f070026;
        public static final int instabug_err_invalid_comment = 0x7f070027;
        public static final int instabug_err_invalid_email = 0x7f070028;
        public static final int instabug_external_storage_permission_denied = 0x7f070029;
        public static final int instabug_str_action_submit = 0x7f07002a;
        public static final int instabug_str_add_photo = 0x7f07002b;
        public static final int instabug_str_alert_message_max_attachments = 0x7f07002c;
        public static final int instabug_str_alert_title_max_attachments = 0x7f07002d;
        public static final int instabug_str_annotate = 0x7f07002e;
        public static final int instabug_str_audio = 0x7f07002f;
        public static final int instabug_str_bug_comment_hint = 0x7f070030;
        public static final int instabug_str_bug_header = 0x7f070031;
        public static final int instabug_str_cancel = 0x7f070032;
        public static final int instabug_str_conversations = 0x7f070033;
        public static final int instabug_str_dialog_message_preparing = 0x7f070034;
        public static final int instabug_str_email_hint = 0x7f070035;
        public static final int instabug_str_empty = 0x7f070070;
        public static final int instabug_str_error_survey_without_answer = 0x7f070036;
        public static final int instabug_str_feedback_comment_hint = 0x7f070037;
        public static final int instabug_str_feedback_header = 0x7f070038;
        public static final int instabug_str_hint_enter_your_answer = 0x7f070039;
        public static final int instabug_str_hold_to_record = 0x7f07003a;
        public static final int instabug_str_image = 0x7f07003b;
        public static final int instabug_str_image_loading_error = 0x7f07003c;
        public static final int instabug_str_invocation_dialog_title = 0x7f07003d;
        public static final int instabug_str_messages = 0x7f07003e;
        public static final int instabug_str_notification_title = 0x7f07003f;
        public static final int instabug_str_notifications_body = 0x7f070040;
        public static final int instabug_str_ok = 0x7f070041;
        public static final int instabug_str_please_wait = 0x7f070042;
        public static final int instabug_str_plus = 0x7f070071;
        public static final int instabug_str_powered_by_instabug = 0x7f070072;
        public static final int instabug_str_record_audio = 0x7f070043;
        public static final int instabug_str_record_video = 0x7f070044;
        public static final int instabug_str_release_stop_record = 0x7f070045;
        public static final int instabug_str_sending_message_hint = 0x7f070046;
        public static final int instabug_str_shake_hint = 0x7f070047;
        public static final int instabug_str_success_note = 0x7f070048;
        public static final int instabug_str_swipe_hint = 0x7f070049;
        public static final int instabug_str_take_screenshot = 0x7f07004a;
        public static final int instabug_str_talk_to_us = 0x7f07004b;
        public static final int instabug_str_thank_you = 0x7f07004c;
        public static final int instabug_str_video = 0x7f07004d;
        public static final int instabug_str_video_encoder_busy = 0x7f07004e;
        public static final int instabug_str_video_encoding_error = 0x7f07004f;
        public static final int instabug_str_video_player = 0x7f070050;
        public static final int instabug_str_video_recording_hint = 0x7f070051;
    }
    public static final class style {
        public static final int InstabugAnnotationColorIconContainer = 0x7f0900dc;
        public static final int InstabugAnnotationColorIconImage = 0x7f0900dd;
        public static final int InstabugAnnotationContainer = 0x7f0900de;
        public static final int InstabugBaseContainer = 0x7f0900df;
        public static final int InstabugBaseText = 0x7f0900e0;
        public static final int InstabugBaseToolbarContainer = 0x7f0900e1;
        public static final int InstabugBorderlessDialog = 0x7f0900e2;
        public static final int InstabugBottomBarContainer = 0x7f0900e3;
        public static final int InstabugBottomSheetContainer = 0x7f0900e4;
        public static final int InstabugBottomSheetItemBaseImage = 0x7f0900e5;
        public static final int InstabugBottomSheetItemContainer = 0x7f0900e6;
        public static final int InstabugBottomSheetItemImage = 0x7f0900e7;
        public static final int InstabugBottomSheetItemText = 0x7f0900e8;
        public static final int InstabugChatsItemContainer = 0x7f0900e9;
        public static final int InstabugDialogButton = 0x7f0900ea;
        public static final int InstabugDialogButtonText = 0x7f0900eb;
        public static final int InstabugDialogComposeMessageContainer = 0x7f0900ec;
        public static final int InstabugDialogContainer = 0x7f0900ed;
        public static final int InstabugDialogIQuestion = 0x7f0900ee;
        public static final int InstabugDialogItemBaseImage = 0x7f0900ef;
        public static final int InstabugDialogItemContainer = 0x7f0900f0;
        public static final int InstabugDialogItemImage = 0x7f0900f1;
        public static final int InstabugDialogItemText = 0x7f0900f2;
        public static final int InstabugDialogToolbarContainer = 0x7f0900f3;
        public static final int InstabugEditTextStyle = 0x7f0900f4;
        public static final int InstabugImageButton = 0x7f0900f5;
        public static final int InstabugSdkTheme = 0x7f0900f6;
        public static final int InstabugSdkTheme_Base_Dark = 0x7f090020;
        public static final int InstabugSdkTheme_Base_Light = 0x7f090021;
        public static final int InstabugSdkTheme_Dark = 0x7f0900f7;
        public static final int InstabugSdkTheme_Light = 0x7f0900f8;
        public static final int InstabugText = 0x7f0900f9;
        public static final int InstabugToolbarTitle = 0x7f0900fa;
        public static final int InstabugTopDialogButton = 0x7f0900fb;
        public static final int InstabugUnreadMessagesCountIcon = 0x7f0900fc;
    }
    public static final class styleable {
        public static final int[] CircleImageView = { 0x7f0100f4, 0x7f0100f5, 0x7f0100f6, 0x7f0100f7 };
        public static final int CircleImageView_ibg_civ_border_color = 1;
        public static final int CircleImageView_ibg_civ_border_overlay = 2;
        public static final int CircleImageView_ibg_civ_border_width = 0;
        public static final int CircleImageView_ibg_civ_fill_color = 3;
        public static final int[] ColorPickerPopUpView = { 0x7f010109 };
        public static final int ColorPickerPopUpView_view_orientation = 0;
        public static final int[] FloatingActionButton = { 0x7f01005f, 0x7f010120, 0x7f010121, 0x7f010122, 0x7f010123, 0x7f010124, 0x7f010125, 0x7f010126, 0x7f010127, 0x7f010128, 0x7f010129, 0x7f01012a, 0x7f01012b, 0x7f010199, 0x7f01019a };
        public static final int FloatingActionButton_instabug_fab_colorDisabled = 7;
        public static final int FloatingActionButton_instabug_fab_colorNormal = 8;
        public static final int FloatingActionButton_instabug_fab_colorPressed = 6;
        public static final int FloatingActionButton_instabug_fab_icon = 9;
        public static final int FloatingActionButton_instabug_fab_size = 10;
        public static final int FloatingActionButton_instabug_fab_stroke_visible = 12;
        public static final int FloatingActionButton_instabug_fab_title = 11;
        public static final int[] IconView = { 0x01010095, 0x01010098, 0x010100d5, 0x7f01012e };
        public static final int IconView_android_padding = 2;
        public static final int IconView_android_textColor = 1;
        public static final int IconView_android_textSize = 0;
        public static final int IconView_instabug_icon = 3;
    }
}
